#!/bin/sh
objcopy --only-keep-debug Targets/amd64/iso/boot/kernel.bin kernel.sym.pre
nm kernel.sym.pre | grep " T " | awk '{ print $1" "$3 }' | c++filt > kernel.sym
