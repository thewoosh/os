// TODO license

#define SHOW_PRINT_OVERLOADS

#include "InterruptHandlers.hpp"

#include <Core/Lambda.hpp>

#include "Kernel/CPU/InterruptSource.hpp"
#include "Kernel/Runtime/Crash.hpp"
#include "Kernel/Runtime/Debug.hpp"
#include "Kernel/Forward.hpp"
#include "Kernel/IO.hpp"
#include "Kernel/Memory/Page.hpp"
#include "Kernel/Memory/MemoryManager.hpp"
#include "Kernel/Memory/MemoryMapRegion.hpp"
#include "Kernel/Memory/PageTableEntry.hpp"
#include "Kernel/Memory/PageDirectoryTable.hpp"
#include "Kernel/Memory/PageDirectoryPointerTable.hpp"
#include "Kernel/Memory/VirtualAddress.hpp"
#include "Kernel/PCI/PCIMain.hpp"
#include "Kernel/Print.hpp"
#include "Include/Kernel/Runtime/Symbolizer.hpp"

void
dump_page_structure();

extern kernel::PCIMain *g_pci;

namespace kernel {

    struct TrapFrame {
        std::uint64_t ip;
        std::uint64_t cs;
        std::uint64_t flags;
        std::uint64_t sp;
        std::uint64_t ss;
    };

#define KERNEL_IRS_HANDLER_FUNCTION_NAME(name) kernel_irs_handler_##name

#define DEFINE_IRS(name) \
        [[gnu::no_caller_saved_registers]] void \
        KERNEL_IRS_HANDLER_FUNCTION_NAME(name)(const TrapFrame *); \
        \
        extern "C" [[gnu::interrupt]] void \
        KERNEL_IRS_ENTRYPOINT_FUNCTION_NAME(name) (const TrapFrame *trap_frame) { \
            KERNEL_IRS_HANDLER_FUNCTION_NAME(name) (trap_frame); \
        } \
        \
        void \
        KERNEL_IRS_HANDLER_FUNCTION_NAME(name)

#define DEFINE_IRS_WITH_ERROR_CODE(name) \
        [[gnu::no_caller_saved_registers]] void \
        KERNEL_IRS_HANDLER_FUNCTION_NAME(name)(const TrapFrame *, kernel::InterruptErrorCode); \
        \
        extern "C" [[gnu::interrupt]] void \
        KERNEL_IRS_ENTRYPOINT_FUNCTION_NAME(name) (const TrapFrame *trap_frame, kernel::InterruptErrorCode error_code) { \
            KERNEL_IRS_HANDLER_FUNCTION_NAME(name) (trap_frame, error_code); \
        } \
        \
        void \
        KERNEL_IRS_HANDLER_FUNCTION_NAME(name)

    DEFINE_IRS(generic)(const TrapFrame *) {
        klog<LogLevel::DEBUG>("InterruptHandler") << "generic interrupt handler invoked.";
        BREAKPOINT();
        CRASH("Unknown Interrupt");
    }

    DEFINE_IRS(divide_by_zero)(const TrapFrame *trap_frame) {
        klog<LogLevel::DEBUG>("InterruptHandler") << "Name: Divide By Zero";

        klog<LogLevel::DEBUG>("InterruptHandler") << "IP: " << trap_frame->ip;
        klog<LogLevel::DEBUG>("InterruptHandler") << "CS: " << trap_frame->cs;
        klog<LogLevel::DEBUG>("InterruptHandler") << "FLAGS: " << trap_frame->flags;
        klog<LogLevel::DEBUG>("InterruptHandler") << "SP: " << trap_frame->sp;
        klog<LogLevel::DEBUG>("InterruptHandler") << "SS: " << trap_frame->ss;
    }

    DEFINE_IRS(breakpoint)(const TrapFrame *) {
        CRASH("breakpoint interrupt");
    }

    DEFINE_IRS(non_maskable_interrupt)([[maybe_unused]] const TrapFrame *trap_frame) {
        klog<LogLevel::DEBUG>("InterruptHandler") << "NMI";
        union {
            std::uint8_t raw;
            struct [[gnu::packed]] {
                bool alternateHotReset : 1;
                bool alternateGateA20 : 1;
                bool reserved : 1;
                bool securityLock : 1;
                bool watchdogTimerStatus : 1;
                bool reserved2 : 1;
                bool hdd2DriveActivity : 1;
                bool hdd1DriveActivity : 1;
            } data;
        } portA{.raw=inb(0x92)};

#define LOG_BIT(struct, name) if (struct.data.name) klog<LogLevel::DEBUG>(" > ") << #name;

        LOG_BIT(portA, alternateHotReset)
        LOG_BIT(portA, alternateGateA20)
        LOG_BIT(portA, reserved)
        LOG_BIT(portA, securityLock)
        LOG_BIT(portA, watchdogTimerStatus)
        LOG_BIT(portA, reserved2)
        LOG_BIT(portA, hdd2DriveActivity)
        LOG_BIT(portA, hdd1DriveActivity)

        union {
            std::uint8_t raw;
            struct [[gnu::packed]] {
                bool timer2TiedToSpeaker : 1;
                bool speakerDataEnable : 1;
                bool parityCheckEnable : 1;
                bool channelCheckEnable : 1;
                bool refreshRequest : 1;
                bool timer2Output : 1;
                bool channelCheck : 1;
                bool parityCheck : 1;
            } data;
        } portB{.raw=inb(0x61)};

        LOG_BIT(portB, timer2TiedToSpeaker)
        LOG_BIT(portB, speakerDataEnable)
        LOG_BIT(portB, parityCheckEnable)
        LOG_BIT(portB, channelCheckEnable)
        LOG_BIT(portB, refreshRequest)
        LOG_BIT(portB, timer2Output)
        LOG_BIT(portB, channelCheck)
        LOG_BIT(portB, parityCheck)

        while (true) asm("hlt");
    }

    [[nodiscard]] std::uint64_t
    read_cr2() {
        std::uint64_t res;
        asm("mov %%cr2, %%rax" : "=a" (res) :);
        return res;
    }

    [[nodiscard]] unsigned int
    read_cr3() {
        unsigned int res;
        asm("mov %%cr3, %%rax" : "=a" (res) :);
        return res;
    }

    namespace test {

        extern "C" std::uint64_t ***paging_pml4[1024];

        void
        check_address(void *address) {
            klog<LogLevel::INFO>("PageTreeWalker") << "=======================================";
            klog<LogLevel::INFO>("PageTreeWalker") << "  Address To Check: "
                                                   << reinterpret_cast<std::uint64_t>(address);

            const auto pml4_address = static_cast<std::uint64_t>(read_cr3());
            klog<LogLevel::INFO>("PageTreeWalker") << "  CR3:           " << pml4_address;
            klog<LogLevel::INFO>("PageTreeWalker") << "  page_table_l4: "
                                                   << reinterpret_cast<std::uint64_t>(paging_pml4);

            if (pml4_address < 0x1000) {
                klog<LogLevel::CRITICAL>("PageTreeWalker") << "CR3 is invalid!";
                return;
            }

            if (reinterpret_cast<std::uint64_t>(paging_pml4) != pml4_address) {
                klog<LogLevel::CRITICAL>("PageTreeWalker") << "CR3 is not the page_table_l4 address!";
                return;
            }

            VirtualAddress virtual_addr{address};
            virtual_addr.debug = true;
            klog<LogLevel::DEBUG>("VirtualAddress")
                    << "PML4E=" << virtual_addr.pml4e()
                    << " PDPE=" << virtual_addr.pdpe()
                    << " PDE=" << virtual_addr.pde()
                    << " PTE=" << virtual_addr.pte()
                    << " PPO=" << virtual_addr.physical_page_offset();
            auto pt_table = virtual_addr.pt_table();
            if (!pt_table)
                klog<LogLevel::CRITICAL>("PageTableWalker") << "VA::pt_table() ret nullptr";
            else {
                klog<LogLevel::DEBUG>("PageTableWalker") << "pt_table.address = "
                        << (pt_table->address << 12);
            }

            auto *entry = PageDirectoryPointerTable::the().find(virtual_addr);
            klog<LogLevel::DEBUG>("PDPT:the") << "Pointer " << reinterpret_cast<std::uint64_t>(entry);
            if (entry)
                klog<LogLevel::DEBUG>("PDPT:the") << "Valuing " << *entry;
            klog<LogLevel::DEBUG>("PrinterTest") << "0 = " << BitPrinter{0};
            klog<LogLevel::DEBUG>("PrinterTest") << "0xFFFFFFFFFFFFFFFF = " << BitPrinter{0xFFFFFFFFFFFFFFFF};
            klog<LogLevel::DEBUG>("PrinterTest") << "1 = " << BitPrinter{1};
            klog<LogLevel::DEBUG>("PrinterTest") << *reinterpret_cast<std::uint64_t *>(entry) << " = " << BitPrinter{*reinterpret_cast<std::uint64_t *>(entry)};

            klog<LogLevel::DEBUG>("InvalidBits") << "OffsetMask: " << std::uint64_t(0x0000ffffffffffff >> 9)
                    << " PPF: " << std::uint64_t(0x7FE1081 & 0x000fffffffffe000);
        }

    } // namespace test

    DEFINE_IRS_WITH_ERROR_CODE(page_fault)(const TrapFrame *trap_frame, kernel::InterruptErrorCode error_code) {
        static std::size_t isInPageFaultHandler = 0;
        if (isInPageFaultHandler > 1) {
            while (true)
                asm("hlt");
        }
        ++isInPageFaultHandler;
        if (isInPageFaultHandler == 2)
            CRASH("#PF in #PF handler");

        //
        // Disable this when done debugging (I think)
        //
//        asm("cli");
        //
        //  =========
        //
//        test::check_address(reinterpret_cast<void *>(read_cr2()));
        g_textModeBufferView.clear();
        if (isInPageFaultHandler == 1)
            g_textModeBufferView.enable_print_to_screen();
        else
            g_textModeBufferView.disable_print_to_screen();
        klog<LogLevel::DEBUG>("InterruptHandler") << "PageFault";
        const auto mapRegion = MemoryMapRegionManager::getRegion(reinterpret_cast<void *>(read_cr2()));
        klog<LogLevel::DEBUG>("InterruptHandler") << "  CR2: " << read_cr2()
                << " in " << toString(mapRegion.type()) << " [" << std::size_t(mapRegion.begin()) << ", "
                << std::size_t(mapRegion.begin()) + mapRegion.size() << "]";
        klog<LogLevel::DEBUG>("InterruptHandler") << "  IP: " << trap_frame->ip;
        klog<LogLevel::DEBUG>("InterruptHandler") << "  CS: " << trap_frame->cs;
        klog<LogLevel::DEBUG>("InterruptHandler") << "  FLAGS: " << trap_frame->flags;
        klog<LogLevel::DEBUG>("InterruptHandler") << "  SP: " << trap_frame->sp;
        klog<LogLevel::DEBUG>("InterruptHandler") << "  SS: " << trap_frame->ss;
        klog<LogLevel::DEBUG>("InterruptHandler") << "  Error Code: " << error_code;
//        klog<LogLevel::DEBUG>("InterruptHandler") << "  Function: "
//                << symbolizer::get_name_of_function(reinterpret_cast<void *>(trap_frame->ip));
        {
            auto flag_stream = klog<LogLevel::DEBUG>("InterruptHandler");
            flag_stream << "  Flags: " << error_code;
            if (error_code & 1) flag_stream << " protection-violation";
            else flag_stream << " not-present";
            if (error_code & 2) flag_stream << " write";
            else flag_stream << " read";
            if (error_code & 4) flag_stream << " user";
            else flag_stream << " kernel";
            if (error_code & 8) flag_stream << " reserved-write";
            if (error_code & 16) flag_stream << " execute";
            else flag_stream << " non-execute";
            if (error_code & 32) flag_stream << " protection-key";
            if (error_code & 64) flag_stream << " shadow-stack";
            if (error_code & 128) flag_stream << " SGX-violation";
        }

        auto *address = reinterpret_cast<void *>(read_cr2());
        VirtualAddress virtual_addr{address};
        virtual_addr.debug = true;
        klog<LogLevel::DEBUG>("PageFault/VirtualAddress")
                << "PML4E=" << virtual_addr.pml4e()
                << " PDPE=" << virtual_addr.pdpe()
                << " PDE=" << virtual_addr.pde()
                << " PTE=" << virtual_addr.pte()
                << " PPO=" << virtual_addr.physical_page_offset();
        SomePageTableEntry<void> *ptr = MemoryManager::find_table(virtual_addr);
        klog<LogLevel::DEBUG>("PageFault") << "VA=" << read_cr2() << " eqs " << reinterpret_cast<std::uint64_t>(ptr);
        if (std::size_t(page_floor(ptr)) == read_cr2()) {
            klog<LogLevel::DEBUG>("PageFault") << "Pointing to itself";
            ptr = nullptr;
        }
        switch (std::uint64_t(ptr)) {
            case 0:
                klog<LogLevel::DEBUG>("PageFault") << "Table: (null)";
                break;
            case 0x10:
                klog<LogLevel::DEBUG>("PageFault") << "Table: (PML4-PDPE [" << virtual_addr.pml4e() << "] absent)";
                break;
            case 0x11:
                klog<LogLevel::DEBUG>("PageFault") << "Table: (PML4-PDPE [" << virtual_addr.pml4e() << "] not-present)";
                break;
            case 0x20:
                klog<LogLevel::DEBUG>("PageFault") << "Table: (PDP-PD [" << virtual_addr.pml4e() << "][" << virtual_addr.pdpe() << "] absent)";
                break;
            case 0x21:
                klog<LogLevel::DEBUG>("PageFault") << "Table: (PDP-PD [" << virtual_addr.pml4e() << "][" << virtual_addr.pdpe() << "] not-present)";
                break;
            case 0x30:
                klog<LogLevel::DEBUG>("PageFault") << "Table: (PD-PT [" << virtual_addr.pml4e() << "][" << virtual_addr.pdpe() << "][" << virtual_addr.pde() << "] absent)";
                break;
            case 0x31:
                klog<LogLevel::DEBUG>("PageFault") << "Table: (PD-PT [" << virtual_addr.pml4e() << "][" << virtual_addr.pdpe() << "][" << virtual_addr.pde() << "] not-present)";
                break;
            default:
                klog<LogLevel::DEBUG>("PageFault") << "Table (non-missing): " << *ptr;
        }
#ifdef ARE_YOU_SURE_TO_CHECK_TABLE
        if (reinterpret_cast<std::uint64_t>(ptr) > 0x150000)
            klog<LogLevel::DEBUG>("PageFault") << "Table: " << *ptr;
#endif

//        dump_page_structure();

        if (!(error_code & 4)) {
            klog<LogLevel::CRITICAL>("PageFault") << "PF in kernel-space!";
            CRASH("PF in kernel-space");
        }
        static_cast<void>(trap_frame);

        isInPageFaultHandler = false;
    }

    DEFINE_IRS_WITH_ERROR_CODE(double_fault)(const TrapFrame *trap_frame, kernel::InterruptErrorCode error_code) {
        klog<LogLevel::DEBUG>("InterruptHandler") << "Double Fault";
        klog<LogLevel::DEBUG>("InterruptHandler") << "IP: " << trap_frame->ip;
        klog<LogLevel::DEBUG>("InterruptHandler") << "CS: " << trap_frame->cs;
        klog<LogLevel::DEBUG>("InterruptHandler") << "FLAGS: " << trap_frame->flags;
        klog<LogLevel::DEBUG>("InterruptHandler") << "SP: " << trap_frame->sp;
        klog<LogLevel::DEBUG>("InterruptHandler") << "SS: " << trap_frame->ss;
        klog<LogLevel::DEBUG>("InterruptHandler") << "ErrorCode: " << error_code;
        klog<LogLevel::DEBUG>("InterruptHandler") << "  Function: "
                << symbolizer::get_name_of_function(reinterpret_cast<void *>(trap_frame->ip));
    }

    DEFINE_IRS_WITH_ERROR_CODE(general_protection_fault)(const TrapFrame *trap_frame, kernel::InterruptErrorCode error_code) {
        g_textModeBufferView.resetCursor();
        klog<LogLevel::DEBUG>("InterruptHandler") << "General Protection Fault";
        klog<LogLevel::DEBUG>("InterruptHandler") << "CS: " << trap_frame->cs;
        klog<LogLevel::DEBUG>("InterruptHandler") << "FLAGS: " << trap_frame->flags;
        klog<LogLevel::DEBUG>("InterruptHandler") << "SP: " << trap_frame->sp;
        klog<LogLevel::DEBUG>("InterruptHandler") << "SS: " << trap_frame->ss;
        klog<LogLevel::DEBUG>("InterruptHandler") << "ErrorCode: " << error_code;
        if (error_code != 0) {
            static constexpr const std::array tables{"GDT", "IDT", "LDT", "IDT2"};
            union {
                kernel::InterruptErrorCode error_code;
                struct [[gnu::packed]] {
                    bool is_external : 1;
                    std::uint8_t table : 2;
                    std::uint16_t selector : 13;
                    std::uint64_t reserved : 48;
                } data;
            } selector{.error_code = error_code};
            static_assert(sizeof(decltype(selector)::data) == 8);

            klog<LogLevel::DEBUG>("InterruptHandler") << "  External=" << (selector.data.is_external ? "true" : "false")
                    << " Table=" << tables[selector.data.table] << " Selector=" << selector.data.selector;
        }

        klog<LogLevel::DEBUG>("InterruptHandler") << "IP: " << trap_frame->ip;
        klog<LogLevel::DEBUG>("InterruptHandler") << "  Function: "
                << symbolizer::get_name_of_function(reinterpret_cast<void *>(trap_frame->ip));

        if (!(error_code & 4)) {
            klog<LogLevel::CRITICAL>("GeneralProtectionFault") << "GPF in kernel-space!";
            CRASH("GPF in kernel-space");
        }
    }

    void
    handleIRQ(std::uint8_t irq, const TrapFrame &frame) {
        if (irq == 0) // PIT, don't care >;3
            return;

        klog<LogLevel::DEBUG>("IRQ (Old APIC)") << TextModeBufferViewWriter::Decimal << irq;
        static_cast<void>(frame);
        CRASH("Life is complete :)");
    }

    void
    handleIRQ_APIC(std::uint8_t irq, const TrapFrame &frame) {
        if (irq == 0) // PIT, don't care >;3
            return;

        klog<LogLevel::DEBUG>("IRQ (APIC)") << TextModeBufferViewWriter::Decimal << irq;
        static_cast<void>(frame);
        CRASH("Life is complete :)");
    }

    void
    handleIRQ_IOAPIC(std::uint8_t irq, const TrapFrame &frame) {
        klog<LogLevel::DEBUG>("IRQ (I/O APIC)") << TextModeBufferViewWriter::Decimal << irq;
        static_cast<void>(frame);
        CHECK(g_pci != nullptr);

        g_pci->forEachDevice(core::lambda([&](pci::AbstractDevice &device) {
            std::size_t handled{};
            if (device.onInterrupt)
                if (device.onInterrupt(InterruptSource::IO_APIC, irq))
                    ++handled;
            klog<LogLevel::DEBUG>("Interrupt") << "I/O APIC IRQ #" << handled << " Handled by " << TextModeBufferViewWriter::Decimal << handled << " devices";
        }));
    }

    DEFINE_IRS(irqAPICSpurious)(const TrapFrame *frame) {
        klog<LogLevel::DEBUG>("Got APIC Spurious") << frame->ip;
        static_cast<void>(frame);
    }

    DEFINE_IRS(irqAPICTimer)(const TrapFrame *frame) {
        klog<LogLevel::DEBUG>("Got APIC Timer") << frame->ip;
        static_cast<void>(frame);
    }

    DEFINE_IRS(irqPIC0)(const TrapFrame *frame) { handleIRQ(0, *frame); }
    DEFINE_IRS(irqPIC1)(const TrapFrame *frame) { handleIRQ(1, *frame); }
    DEFINE_IRS(irqPIC2)(const TrapFrame *frame) { handleIRQ(2, *frame); }
    DEFINE_IRS(irqPIC3)(const TrapFrame *frame) { handleIRQ(3, *frame); }
    DEFINE_IRS(irqPIC4)(const TrapFrame *frame) { handleIRQ(4, *frame); }
    DEFINE_IRS(irqPIC5)(const TrapFrame *frame) { handleIRQ(5, *frame); }
    DEFINE_IRS(irqPIC6)(const TrapFrame *frame) { handleIRQ(6, *frame); }
    DEFINE_IRS(irqPIC7)(const TrapFrame *frame) { handleIRQ(7, *frame); }
    DEFINE_IRS(irqPIC8)(const TrapFrame *frame) { handleIRQ(8, *frame); }
    DEFINE_IRS(irqPIC9)(const TrapFrame *frame) { handleIRQ(9, *frame); }
    DEFINE_IRS(irqPIC10)(const TrapFrame *frame) { handleIRQ(10, *frame); }
    DEFINE_IRS(irqPIC11)(const TrapFrame *frame) { handleIRQ(11, *frame); }
    DEFINE_IRS(irqPIC12)(const TrapFrame *frame) { handleIRQ(12, *frame); }
    DEFINE_IRS(irqPIC13)(const TrapFrame *frame) { handleIRQ(13, *frame); }
    DEFINE_IRS(irqPIC14)(const TrapFrame *frame) { handleIRQ(14, *frame); }
    DEFINE_IRS(irqPIC15)(const TrapFrame *frame) { handleIRQ(15, *frame); }

    DEFINE_IRS(irqAPIC0)(const TrapFrame *frame) { handleIRQ_APIC(0, *frame); }
    DEFINE_IRS(irqAPIC1)(const TrapFrame *frame) { handleIRQ_APIC(1, *frame); }

    DEFINE_IRS(irqIOAPIC0)(const TrapFrame *frame) { handleIRQ_IOAPIC(0, *frame); }
    DEFINE_IRS(irqIOAPIC1)(const TrapFrame *frame) { handleIRQ_IOAPIC(1, *frame); }
    DEFINE_IRS(irqIOAPIC2)(const TrapFrame *frame) { handleIRQ_IOAPIC(2, *frame); }

#define DEFINE_FALLBACK(number) DEFINE_IRS(fallback_##number)(const TrapFrame *frame) { \
    klog<LogLevel::DEBUG>("FallbackInterruptHandler") << "Num " << std::size_t(number); \
    static_cast<void>(frame); \
    CRASH("backtrace"); \
}

    DEFINE_FALLBACK(0)
    DEFINE_FALLBACK(1)
    DEFINE_FALLBACK(2)
    DEFINE_FALLBACK(3)
    DEFINE_FALLBACK(4)
    DEFINE_FALLBACK(5)
    DEFINE_FALLBACK(6)
    DEFINE_FALLBACK(7)
    DEFINE_FALLBACK(8)
    DEFINE_FALLBACK(9)

    DEFINE_FALLBACK(10)
    DEFINE_FALLBACK(11)
    DEFINE_FALLBACK(12)
    DEFINE_FALLBACK(13)
    DEFINE_FALLBACK(14)
    DEFINE_FALLBACK(15)
    DEFINE_FALLBACK(16)
    DEFINE_FALLBACK(17)
    DEFINE_FALLBACK(18)
    DEFINE_FALLBACK(19)

    DEFINE_FALLBACK(20)
    DEFINE_FALLBACK(21)
    DEFINE_FALLBACK(22)
    DEFINE_FALLBACK(23)
    DEFINE_FALLBACK(24)
    DEFINE_FALLBACK(25)
    DEFINE_FALLBACK(26)
    DEFINE_FALLBACK(27)
    DEFINE_FALLBACK(28)
    DEFINE_FALLBACK(29)

    DEFINE_FALLBACK(30)
    DEFINE_FALLBACK(31)
    DEFINE_FALLBACK(32)
    DEFINE_FALLBACK(33)
    DEFINE_FALLBACK(34)
    DEFINE_FALLBACK(35)
    DEFINE_FALLBACK(36)
    DEFINE_FALLBACK(37)
    DEFINE_FALLBACK(38)
    DEFINE_FALLBACK(39)

    DEFINE_FALLBACK(40)
    DEFINE_FALLBACK(41)
    DEFINE_FALLBACK(42)
    DEFINE_FALLBACK(43)
    DEFINE_FALLBACK(44)
    DEFINE_FALLBACK(45)
    DEFINE_FALLBACK(46)
    DEFINE_FALLBACK(47)
    DEFINE_FALLBACK(48)
    DEFINE_FALLBACK(49)

    DEFINE_FALLBACK(50)
    DEFINE_FALLBACK(51)
    DEFINE_FALLBACK(52)
    DEFINE_FALLBACK(53)
    DEFINE_FALLBACK(54)
    DEFINE_FALLBACK(55)
    DEFINE_FALLBACK(56)
    DEFINE_FALLBACK(57)
    DEFINE_FALLBACK(58)
    DEFINE_FALLBACK(59)

    DEFINE_FALLBACK(60)
    DEFINE_FALLBACK(61)
    DEFINE_FALLBACK(62)
    DEFINE_FALLBACK(63)
    DEFINE_FALLBACK(64)
    DEFINE_FALLBACK(65)
    DEFINE_FALLBACK(66)
    DEFINE_FALLBACK(67)
    DEFINE_FALLBACK(68)
    DEFINE_FALLBACK(69)

    DEFINE_FALLBACK(70)
    DEFINE_FALLBACK(71)
    DEFINE_FALLBACK(72)
    DEFINE_FALLBACK(73)
    DEFINE_FALLBACK(74)
    DEFINE_FALLBACK(75)
    DEFINE_FALLBACK(76)
    DEFINE_FALLBACK(77)
    DEFINE_FALLBACK(78)
    DEFINE_FALLBACK(79)

    DEFINE_FALLBACK(80)
    DEFINE_FALLBACK(81)
    DEFINE_FALLBACK(82)
    DEFINE_FALLBACK(83)
    DEFINE_FALLBACK(84)
    DEFINE_FALLBACK(85)
    DEFINE_FALLBACK(86)
    DEFINE_FALLBACK(87)
    DEFINE_FALLBACK(88)
    DEFINE_FALLBACK(89)

    DEFINE_FALLBACK(90)
    DEFINE_FALLBACK(91)
    DEFINE_FALLBACK(92)
    DEFINE_FALLBACK(93)
    DEFINE_FALLBACK(94)
    DEFINE_FALLBACK(95)
    DEFINE_FALLBACK(96)
    DEFINE_FALLBACK(97)
    DEFINE_FALLBACK(98)
    DEFINE_FALLBACK(99)

    DEFINE_FALLBACK(100)
    DEFINE_FALLBACK(101)
    DEFINE_FALLBACK(102)
    DEFINE_FALLBACK(103)
    DEFINE_FALLBACK(104)
    DEFINE_FALLBACK(105)
    DEFINE_FALLBACK(106)
    DEFINE_FALLBACK(107)
    DEFINE_FALLBACK(108)
    DEFINE_FALLBACK(109)

    DEFINE_FALLBACK(110)
    DEFINE_FALLBACK(111)
    DEFINE_FALLBACK(112)
    DEFINE_FALLBACK(113)
    DEFINE_FALLBACK(114)
    DEFINE_FALLBACK(115)
    DEFINE_FALLBACK(116)
    DEFINE_FALLBACK(117)
    DEFINE_FALLBACK(118)
    DEFINE_FALLBACK(119)

    DEFINE_FALLBACK(120)
    DEFINE_FALLBACK(121)
    DEFINE_FALLBACK(122)
    DEFINE_FALLBACK(123)
    DEFINE_FALLBACK(124)
    DEFINE_FALLBACK(125)
    DEFINE_FALLBACK(126)
    DEFINE_FALLBACK(127)
    DEFINE_FALLBACK(128)
    DEFINE_FALLBACK(129)

    DEFINE_FALLBACK(130)
    DEFINE_FALLBACK(131)
    DEFINE_FALLBACK(132)
    DEFINE_FALLBACK(133)
    DEFINE_FALLBACK(134)
    DEFINE_FALLBACK(135)
    DEFINE_FALLBACK(136)
    DEFINE_FALLBACK(137)
    DEFINE_FALLBACK(138)
    DEFINE_FALLBACK(139)

    DEFINE_FALLBACK(140)
    DEFINE_FALLBACK(141)
    DEFINE_FALLBACK(142)
    DEFINE_FALLBACK(143)
    DEFINE_FALLBACK(144)
    DEFINE_FALLBACK(145)
    DEFINE_FALLBACK(146)
    DEFINE_FALLBACK(147)
    DEFINE_FALLBACK(148)
    DEFINE_FALLBACK(149)

    DEFINE_FALLBACK(150)
    DEFINE_FALLBACK(151)
    DEFINE_FALLBACK(152)
    DEFINE_FALLBACK(153)
    DEFINE_FALLBACK(154)
    DEFINE_FALLBACK(155)
    DEFINE_FALLBACK(156)
    DEFINE_FALLBACK(157)
    DEFINE_FALLBACK(158)
    DEFINE_FALLBACK(159)

    DEFINE_FALLBACK(160)
    DEFINE_FALLBACK(161)
    DEFINE_FALLBACK(162)
    DEFINE_FALLBACK(163)
    DEFINE_FALLBACK(164)
    DEFINE_FALLBACK(165)
    DEFINE_FALLBACK(166)
    DEFINE_FALLBACK(167)
    DEFINE_FALLBACK(168)
    DEFINE_FALLBACK(169)

    DEFINE_FALLBACK(170)
    DEFINE_FALLBACK(171)
    DEFINE_FALLBACK(172)
    DEFINE_FALLBACK(173)
    DEFINE_FALLBACK(174)
    DEFINE_FALLBACK(175)
    DEFINE_FALLBACK(176)
    DEFINE_FALLBACK(177)
    DEFINE_FALLBACK(178)
    DEFINE_FALLBACK(179)

    DEFINE_FALLBACK(180)
    DEFINE_FALLBACK(181)
    DEFINE_FALLBACK(182)
    DEFINE_FALLBACK(183)
    DEFINE_FALLBACK(184)
    DEFINE_FALLBACK(185)
    DEFINE_FALLBACK(186)
    DEFINE_FALLBACK(187)
    DEFINE_FALLBACK(188)
    DEFINE_FALLBACK(189)

    DEFINE_FALLBACK(190)
    DEFINE_FALLBACK(191)
    DEFINE_FALLBACK(192)
    DEFINE_FALLBACK(193)
    DEFINE_FALLBACK(194)
    DEFINE_FALLBACK(195)
    DEFINE_FALLBACK(196)
    DEFINE_FALLBACK(197)
    DEFINE_FALLBACK(198)
    DEFINE_FALLBACK(199)

    DEFINE_FALLBACK(200)
    DEFINE_FALLBACK(201)
    DEFINE_FALLBACK(202)
    DEFINE_FALLBACK(203)
    DEFINE_FALLBACK(204)
    DEFINE_FALLBACK(205)
    DEFINE_FALLBACK(206)
    DEFINE_FALLBACK(207)
    DEFINE_FALLBACK(208)
    DEFINE_FALLBACK(209)

    DEFINE_FALLBACK(210)
    DEFINE_FALLBACK(211)
    DEFINE_FALLBACK(212)
    DEFINE_FALLBACK(213)
    DEFINE_FALLBACK(214)
    DEFINE_FALLBACK(215)
    DEFINE_FALLBACK(216)
    DEFINE_FALLBACK(217)
    DEFINE_FALLBACK(218)
    DEFINE_FALLBACK(219)

    DEFINE_FALLBACK(220)
    DEFINE_FALLBACK(221)
    DEFINE_FALLBACK(222)
    DEFINE_FALLBACK(223)
    DEFINE_FALLBACK(224)
    DEFINE_FALLBACK(225)
    DEFINE_FALLBACK(226)
    DEFINE_FALLBACK(227)
    DEFINE_FALLBACK(228)
    DEFINE_FALLBACK(229)

    DEFINE_FALLBACK(230)
    DEFINE_FALLBACK(231)
    DEFINE_FALLBACK(232)
    DEFINE_FALLBACK(233)
    DEFINE_FALLBACK(234)
    DEFINE_FALLBACK(235)
    DEFINE_FALLBACK(236)
    DEFINE_FALLBACK(237)
    DEFINE_FALLBACK(238)
    DEFINE_FALLBACK(239)

    DEFINE_FALLBACK(240)
    DEFINE_FALLBACK(241)
    DEFINE_FALLBACK(242)
    DEFINE_FALLBACK(243)
    DEFINE_FALLBACK(244)
    DEFINE_FALLBACK(245)
    DEFINE_FALLBACK(246)
    DEFINE_FALLBACK(247)
    DEFINE_FALLBACK(248)
    DEFINE_FALLBACK(249)

    DEFINE_FALLBACK(250)
    DEFINE_FALLBACK(251)
    DEFINE_FALLBACK(252)
    DEFINE_FALLBACK(253)
    DEFINE_FALLBACK(254)
    DEFINE_FALLBACK(255)

} // namespace kernel
