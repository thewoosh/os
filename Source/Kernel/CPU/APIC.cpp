#include "Kernel/CPU/APIC.hpp"

#include <Core/Pointer.hpp>

#include "Kernel/Assert.hpp"
#include "Kernel/ACPI/MADT.hpp"
#include "Kernel/CPU/CPU.hpp"
#include "Kernel/CPU/IOAPIC.hpp"
#include "Kernel/CPU/LocalAPIC.hpp"
#include "Kernel/CPUInformation.hpp"
#include "Kernel/Memory/MemoryManager.hpp"
#include "Kernel/Memory/Page.hpp"
#include "Kernel/Print.hpp"

namespace kernel {

    using namespace Kernel;

    inline constexpr std::uint64_t kAPICBSP = 0x100;
    inline constexpr std::uint64_t kAPICEnable = 0x800;

    [[nodiscard]] static void *
    getAPICBase() {
        auto msr = cpu::readMSR(cpu::MSR::APIC_BASE);
        return reinterpret_cast<void *>((msr.rax & 0xfffffffffffff000) | ((msr.rdx & 0x0f) << 32));
    }

    static void
    setAPICBase(void *apic) {
        cpu::writeMSR(cpu::MSR::APIC_BASE, {
            .rax = (std::uint64_t(apic) & 0xffffffffffff0000) | kAPICEnable,
            .rdx = (std::uint64_t(apic) >> 32) & 0x0f
        });
    }

    bool
    APIC::initialize(const acpi::MADT *madt) {
        const auto cpuInfo = ProduceCPUInformation();
        if (!cpuInfo.SupportsFeature(CPUFeature::APIC)) {
            klog<LogLevel::ERROR>("APIC") << "CPU does not support APIC \"Advanced Programmable Interrupt Controller\"";
            return false;
        }

        if (!parseMADT(madt))
            return false;

        asm("cli");

        auto *apicBase = getAPICBase();
        // Continue: make map of how the memory should be structured
        // to find a location where the APIC base should be mapped to.
        MemoryManager::the().map(VirtualAddress{apicBase}, apicBase, MemoryManager::MAP_FLAG_CACHE_DISABLED);
        setAPICBase(apicBase);

        auto *lapicData = m_coreAPICData.front().localAPICStructure;
        klog<LogLevel::DEBUG>("LAPIC #0 Data") << "ID: " << lapicData->apicID;
        klog<LogLevel::DEBUG>("LAPIC #0 Data") << "ProcessorUID: " << lapicData->acpiProcessorUID;
        klog<LogLevel::DEBUG>("LAPIC #0 Data") << "Flags: " << lapicData->flags;

        // NOTE: This address (0xFEE00000) is per core! It will only affect
        //       core 0 and we can't access other cores' LAPIC registers from
        //       a single core!
        auto *lapicStartPage = reinterpret_cast<std::uint8_t *>(0xFEE00000);
        MemoryManager::the().map(VirtualAddress{lapicStartPage}, lapicStartPage,
                                 MemoryManager::MAP_FLAG_CACHE_DISABLED);
        MemoryManager::the().map(VirtualAddress{lapicStartPage + kPageSize}, lapicStartPage + kPageSize,
                                 MemoryManager::MAP_FLAG_CACHE_DISABLED);
        MemoryManager::the().map(VirtualAddress{lapicStartPage + kPageSize * 2}, lapicStartPage + kPageSize * 2,
                                 MemoryManager::MAP_FLAG_CACHE_DISABLED);
        MemoryManager::the().map(VirtualAddress{lapicStartPage + kPageSize * 3}, lapicStartPage + kPageSize * 3,
                                 MemoryManager::MAP_FLAG_CACHE_DISABLED);
        MemoryManager::the().map(VirtualAddress{lapicStartPage + kPageSize * 4}, lapicStartPage + kPageSize * 4,
                                 MemoryManager::MAP_FLAG_CACHE_DISABLED);
        LocalAPIC lapic{const_cast<volatile std::uint32_t *>(reinterpret_cast<std::uint32_t *>(lapicStartPage))};
        klog<LogLevel::DEBUG>("LAPIC #0 Registers") << "ID: " << lapic.id();
        klog<LogLevel::DEBUG>("LAPIC #0 Registers") << "Version: " << lapic.version();

        auto lint0 = lapic.lint0();
        lint0.vector = 0x61;
        lapic.setLINT0(lint0);

        auto lint1 = lapic.lint1();
        lint1.vector = 0x62;
        lapic.setLINT1(lint1);

        auto timer = lapic.timer();
        timer.vector = 0x63;
        lapic.setTimer(timer);

        auto spuriousInterruptVector = lapic.spuriousInterruptVector();
        klog<LogLevel::DEBUG>("LAPIC #0 Registers") << "Spurious Interrupt Vector: " << spuriousInterruptVector;
        *reinterpret_cast<std::uint8_t *>(&spuriousInterruptVector) = 0xFF;
        spuriousInterruptVector |= 0x100;
        klog<LogLevel::DEBUG>("LAPIC #0 Registers") << "    Change: " << spuriousInterruptVector;
        lapic.setSpuriousInterruptVector(spuriousInterruptVector);
        klog<LogLevel::DEBUG>("LAPIC #0 Registers") << "    After Mod: " << lapic.spuriousInterruptVector();

        setupIOAPIC();

//        m_ioAPICStructure->ioAPICAddress

        // TODO do something with this... Figure out how to initialize the IOAPIC.
        asm("sti");
        return true;
    }

    bool
    APIC::parseMADT(const acpi::MADT *madt) {
        const auto *data = reinterpret_cast<const void *>(madt);
        for (std::size_t offset = 0x2C; offset < madt->header.length; ) {
            const auto *entry = static_cast<const acpi::MADTCommonHeader *>(core::advance_pointer_with_byte_offset(data, offset));
//            if (recordLength + offset > g_acpiData.madt->header.length)
//                break;
            switch (entry->type) {
                case acpi::InterruptControllerStructureType::PROCESSOR_LOCAL_APIC:
                    if (!parseMADTProcessorLocalAPIC(reinterpret_cast<const acpi::ProcessorLocalAPICStructure *>(entry))) {
                        klog<LogLevel::ERROR>("APIC") << "Failed to parse MADT/ProcessorLocalAPIC interrupt controller structure!";
                        return false;
                    }
                    break;
                case acpi::InterruptControllerStructureType::IOAPIC:
                    if (!parseMADTIOAPIC(reinterpret_cast<const acpi::IOAPICStructure *>(entry))) {
                        klog<LogLevel::ERROR>("APIC") << "Failed to parse MADT/IOAPIC interrupt controller structure!";
                        return false;
                    }
                    break;
                default:
                    klog<LogLevel::DEBUG>("APIC") << "Unknown MADT Entry of type " << std::size_t(entry->type) << " length " << entry->length;
                    break;
            }
            offset += entry->length;
        }

        if (m_coreAPICData.empty()) {
            klog<LogLevel::ERROR>("APIC") << "No ProcessorLocalAPIC in MADT";
            return false;
        }

        if (m_ioAPICStructure == nullptr) {
            klog<LogLevel::ERROR>("APIC") << "No I/O APIC present in MADT!";
            return false;
        }

        if (m_coreAPICData.size() == 1)
            klog<LogLevel::DEBUG>("APIC") << "We discovered that there is one core in this system";
        else
            klog<LogLevel::DEBUG>("APIC") << "We discovered that there are " << TextModeBufferViewWriter::Decimal
                    << m_coreAPICData.size() << " cores in this system";
        return true;
    }

    bool
    APIC::parseMADTIOAPIC(const acpi::IOAPICStructure *acpiIOAPICStructure) {
        if (m_ioAPICStructure != nullptr) {
            klog<LogLevel::ERROR>("APIC") << "There can't be more than one I/O APIC structure in MADT!";
            return false;
        }

        m_ioAPICStructure = acpiIOAPICStructure;
        return true;
    }

    bool
    APIC::parseMADTProcessorLocalAPIC(const acpi::ProcessorLocalAPICStructure *acpiLocalAPICStructure) {
        if (m_coreAPICData.full()) {
            klog<LogLevel::ERROR>("APIC") << "Hello Future! I didn't expect there to be more than "
                    << TextModeBufferViewWriter::Decimal << kMaxCores << " cores in a system";
            return false;
        }

        auto &coreData = m_coreAPICData.emplace_back();
        coreData.localAPICStructure = acpiLocalAPICStructure;
        return true;
    }

    void
    APIC::setupIOAPIC() {
        klog<LogLevel::DEBUG>("I/O APIC") << "Setting Up...";

        auto *ioAPICAddress = reinterpret_cast<void *>(m_ioAPICStructure->ioAPICAddress);
        // TODO is one page enough?
        MemoryManager::the().map(VirtualAddress{ioAPICAddress}, ioAPICAddress, MemoryManager::MAP_FLAG_CACHE_DISABLED);

        IOAPIC ioAPIC{reinterpret_cast<volatile std::uint32_t *>(ioAPICAddress), m_ioAPICStructure->ioAPICID,
                      m_ioAPICStructure->globalSystemInterruptBase};

        auto version = ioAPIC.version();
        klog<LogLevel::DEBUG>("I/O APIC") << "Version: " << version.version;
        klog<LogLevel::DEBUG>("I/O APIC") << "Max Redirection Entry: " << version.maxRedirectionEntry;
        klog<LogLevel::DEBUG>("I/O APIC") << "Global System Interrupt Base: " << m_ioAPICStructure->globalSystemInterruptBase;

        for (std::size_t index = m_ioAPICStructure->globalSystemInterruptBase; index < version.maxRedirectionEntry + 1; ++index) {
            auto entry = ioAPIC.redirectionEntry(std::uint8_t(index));
            entry.vector = std::uint8_t(200 + index);
            entry.mask = false;
            ioAPIC.setRedirectionEntry(std::uint8_t(index), entry);
            entry = ioAPIC.redirectionEntry(std::uint8_t(index));
        }
    }

} // namespace kernel
