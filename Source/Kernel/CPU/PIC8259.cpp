#include "Kernel/CPU/PIC8259.hpp"

#include "Kernel/Assert.hpp"
#include "Kernel/IO.hpp"


// TODO: is waiting necessary?
#define IO_WAIT() ioWait()

namespace kernel {

    inline constexpr const std::uint8_t kPIC1Command = 0x20;
    inline constexpr const std::uint8_t kPIC1Data = 0x21;
    inline constexpr const std::uint8_t kPIC2Command = 0xA0;
    inline constexpr const std::uint8_t kPIC2Data = 0xA1;

    namespace icw1 {
        inline constexpr const std::uint8_t icw4 = 0x01;
        inline constexpr const std::uint8_t single = 0x02;
        inline constexpr const std::uint8_t interval4 = 0x04;
        inline constexpr const std::uint8_t levelTriggeredEdgeMode = 0x08;
        inline constexpr const std::uint8_t initialization = 0x10;
    } // namespace icw1

    namespace icw4 {
        inline constexpr const std::uint8_t k8086 = 0x01;
    } // namespace icw4

    enum class PIC8259Command
            : std::uint8_t {
        END_OF_INTERRUPT = 0x20,

    };

    void
    PIC8259::disable() {
        initialize(0x50, 0x58);

        m_enabled = false;
        outb(kPIC1Data, 0xFF);
        outb(kPIC2Data, 0xFF);
    }

    void
    PIC8259::initialize(std::uint8_t offset1, std::uint8_t offset2) {
        CHECK(!m_enabled);

        const auto pic1Data = inb(kPIC1Data);
        const auto pic2Data = inb(kPIC2Data);

        outb(kPIC1Command, icw1::initialization | icw1::icw4);
        outb(kPIC2Command, icw1::initialization | icw1::icw4);
        IO_WAIT();

        outb(kPIC1Data, offset1);
        outb(kPIC2Data, offset2);
        IO_WAIT();

        outb(kPIC1Data, 4);
        outb(kPIC2Data, 2);
        IO_WAIT();

        outb(kPIC1Data, icw4::k8086);
        outb(kPIC2Data, icw4::k8086);
        IO_WAIT();

        outb(kPIC1Data, pic1Data);
        outb(kPIC2Data, pic2Data);
        IO_WAIT();

        outb(kPIC1Data, 0x00);
        outb(kPIC2Data, 0x00);

        asm("sti");
        m_enabled = true;
    }

    void
    PIC8259::maskClear(std::uint8_t irqLine) {
        CHECK(m_enabled);

        if (irqLine < 8)
            outb(kPIC1Data, static_cast<std::uint8_t>(inb(kPIC1Data) & ~(1 << irqLine)));
        else
            outb(kPIC2Data, static_cast<std::uint8_t>(inb(kPIC2Data) & ~(1 << (irqLine - 8))));
    }

    void
    PIC8259::maskSet(std::uint8_t irqLine) {
        CHECK(m_enabled);

        if (irqLine < 8)
            outb(kPIC1Data, static_cast<std::uint8_t>(inb(kPIC1Data) | (1 << irqLine)));
        else
            outb(kPIC2Data, static_cast<std::uint8_t>(inb(kPIC2Data) | (1 << (irqLine - 8))));
    }

    void
    PIC8259::sendCommand(Controller controller, PIC8259Command command) {
        CHECK(m_enabled);

        if (controller == Controller::PIC2)
            outb(kPIC2Command, static_cast<std::uint8_t>(command));

        outb(kPIC1Command, static_cast<std::uint8_t>(command));
    }

    void
    PIC8259::sendEndOfInterrupt(std::uint8_t irq) {
        sendCommand(irq >= 8 ? Controller::PIC2 : Controller::PIC1, PIC8259Command::END_OF_INTERRUPT);
    }

} // namespace kernel
