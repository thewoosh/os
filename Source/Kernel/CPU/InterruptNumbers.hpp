// TODO license

#pragma once

#include <string_view>

#include <cstdint>

namespace kernel::interrupt_number {

    using Type = std::uint8_t;

#define ITERATE_INTERRUPT_NUMBERS(REGISTER_INTERRUPT) \
        REGISTER_INTERRUPT(divide_by_zero,              0x00, "Divide By Zero") \
        REGISTER_INTERRUPT(debug,                       0x01, "Debug") \
        REGISTER_INTERRUPT(non_maskable_interrupt,      0x02, "Non-maskable Interrupt") \
        REGISTER_INTERRUPT(breakpoint,                  0x03, "Breakpoint") \
        REGISTER_INTERRUPT(overflow,                    0x04, "Overflow") \
        REGISTER_INTERRUPT(bound_range_exceeded,        0x05, "Bound Range Exceeded") \
        REGISTER_INTERRUPT(invalid_opcode,              0x06, "Invalid Opcode") \
        REGISTER_INTERRUPT(device_not_available,        0x07, "Device Not Available") \
        REGISTER_INTERRUPT(double_fault,                0x08, "Double Fault") \
        REGISTER_INTERRUPT(invalid_tss,                 0x0A, "Invalid TSS") \
        REGISTER_INTERRUPT(segment_not_present,         0x0B, "Segment Not Present") \
        REGISTER_INTERRUPT(stack_segment_fault,         0x0C, "Stack Segment Fault") \
        REGISTER_INTERRUPT(general_protection_fault,    0x0D, "General Protection Fault") \
        REGISTER_INTERRUPT(page_fault,                  0x0E, "Page Fault") \
        REGISTER_INTERRUPT(irqPIC0,                     0x50, "IRQ PIC 0") \
        REGISTER_INTERRUPT(irqPIC1,                     0x51, "IRQ PIC 1") \
        REGISTER_INTERRUPT(irqPIC2,                     0x52, "IRQ PIC 2") \
        REGISTER_INTERRUPT(irqPIC3,                     0x53, "IRQ PIC 3") \
        REGISTER_INTERRUPT(irqPIC4,                     0x54, "IRQ PIC 4") \
        REGISTER_INTERRUPT(irqPIC5,                     0x55, "IRQ PIC 5") \
        REGISTER_INTERRUPT(irqPIC6,                     0x56, "IRQ PIC 6") \
        REGISTER_INTERRUPT(irqPIC7,                     0x57, "IRQ PIC 7") \
        REGISTER_INTERRUPT(irqPIC8,                     0x58, "IRQ PIC 8") \
        REGISTER_INTERRUPT(irqPIC9,                     0x59, "IRQ PIC 9") \
        REGISTER_INTERRUPT(irqPIC10,                    0x5A, "IRQ PIC 10") \
        REGISTER_INTERRUPT(irqPIC11,                    0x5B, "IRQ PIC 11") \
        REGISTER_INTERRUPT(irqPIC12,                    0x5C, "IRQ PIC 12") \
        REGISTER_INTERRUPT(irqPIC13,                    0x5D, "IRQ PIC 13") \
        REGISTER_INTERRUPT(irqPIC14,                    0x5E, "IRQ PIC 14") \
        REGISTER_INTERRUPT(irqPIC15,                    0x5F, "IRQ PIC 15") \
        REGISTER_INTERRUPT(irqAPICSpurious,             0x60, "IRQ APIC Spurious") \
        REGISTER_INTERRUPT(irqAPIC0,                    0x61, "IRQ APIC 0") \
        REGISTER_INTERRUPT(irqAPIC1,                    0x62, "IRQ APIC 1") \
        REGISTER_INTERRUPT(irqAPICTimer,                0x63, "IRQ APIC Timer") \
        REGISTER_INTERRUPT(irqPIC0ExBIOS,               0x70, "IRQ PIC 0 (IRQ8-15)") \
        REGISTER_INTERRUPT(irqPIC1ExBIOS,               0x71, "IRQ PIC 1 (IRQ8-15)") \
        REGISTER_INTERRUPT(irqPIC2ExBIOS,               0x72, "IRQ PIC 2 (IRQ8-15)") \
        REGISTER_INTERRUPT(irqPIC3ExBIOS,               0x73, "IRQ PIC 3 (IRQ8-15)") \
        REGISTER_INTERRUPT(irqPIC4ExBIOS,               0x74, "IRQ PIC 4 (IRQ8-15)") \
        REGISTER_INTERRUPT(irqPIC5ExBIOS,               0x75, "IRQ PIC 5 (IRQ8-15)") \
        REGISTER_INTERRUPT(irqPIC6ExBIOS,               0x76, "IRQ PIC 6 (IRQ8-15)") \
        REGISTER_INTERRUPT(irqPIC7ExBIOS,               0x77, "IRQ PIC 7 (IRQ8-15)") \
        REGISTER_INTERRUPT(irqPIC8ExBIOS,               0x78, "IRQ PIC 8 (IRQ8-15)") \
        REGISTER_INTERRUPT(irqIOAPIC0,                  0xC8, "IRQ I/O APIC 0") \
        REGISTER_INTERRUPT(irqIOAPIC1,                  0xC9, "IRQ I/O APIC 1") \
        REGISTER_INTERRUPT(irqIOAPIC2,                  0xCA, "IRQ I/O APIC 2") \
        REGISTER_INTERRUPT(irqIOAPIC3,                  0xCB, "IRQ I/O APIC 3") \
        REGISTER_INTERRUPT(irqIOAPIC4,                  0xCC, "IRQ I/O APIC 4") \
        REGISTER_INTERRUPT(irqIOAPIC5,                  0xCD, "IRQ I/O APIC 5") \
        REGISTER_INTERRUPT(irqIOAPIC6,                  0xCE, "IRQ I/O APIC 6") \
        REGISTER_INTERRUPT(irqIOAPIC7,                  0xCF, "IRQ I/O APIC 7") \
        REGISTER_INTERRUPT(irqIOAPIC8,                  0xD0, "IRQ I/O APIC 8") \
        REGISTER_INTERRUPT(irqIOAPIC9,                  0xD1, "IRQ I/O APIC 9") \
        REGISTER_INTERRUPT(irqIOAPICA,                  0xD2, "IRQ I/O APIC A") \
        REGISTER_INTERRUPT(irqIOAPICB,                  0xD3, "IRQ I/O APIC B") \
        REGISTER_INTERRUPT(irqIOAPICC,                  0xD4, "IRQ I/O APIC C") \
        REGISTER_INTERRUPT(irqIOAPICD,                  0xD5, "IRQ I/O APIC D") \
        REGISTER_INTERRUPT(irqIOAPICE,                  0xD6, "IRQ I/O APIC E") \
        REGISTER_INTERRUPT(irqIOAPICF,                  0xD7, "IRQ I/O APIC F") \

#define REGISTER_INTERRUPT(constant_name, id, _) \
        inline constexpr Type constant_name = id;
    ITERATE_INTERRUPT_NUMBERS(REGISTER_INTERRUPT)
#undef REGISTER_INTERRUPT

    struct InterruptNumberInformation {
        Type id;
        std::string_view internal_name;
        std::string_view pretty_name;
    };

} // namespace kernel::interrupt_number

namespace kernel {

    [[nodiscard]] inline constexpr interrupt_number::InterruptNumberInformation
    get_interrupt_by_number(interrupt_number::Type number) {
        switch (number) {
#define REGISTER_INTERRUPT(internal_name, id, pretty_name) \
            case id: \
                return {id, #internal_name, pretty_name};

            ITERATE_INTERRUPT_NUMBERS(REGISTER_INTERRUPT)
#undef REGISTER_INTERRUPT
            default:
                return {0, "(invalid)", "(invalid)"};
        }
    }

} // namespace kernel
