#include "Kernel/ACPI/AML/Interpreter.hpp"

#include <optional>

#include "Kernel/ACPI/AML.hpp"
#include "Kernel/ACPI/AML/Expression.hpp"
#include "Kernel/ACPI/AML/Method.hpp"
#include "Kernel/ACPI/AML/Package.hpp"
#include "Kernel/ACPI/AML/Statement.hpp"

namespace kernel::acpi::aml {

    struct MethodContext {
        enum class Status {
#define ACPI_AML_ITERATE_METHOD_CONTEXT_STATUSES(REGISTER_STATUS) \
            REGISTER_STATUS(OK) \
            \
            REGISTER_STATUS(JUMP_STATEMENT_BREAK) \
            REGISTER_STATUS(JUMP_STATEMENT_CONTINUE) \
            REGISTER_STATUS(JUMP_STATEMENT_RETURN) \
            \
            REGISTER_STATUS(WHILE_STATEMENT_FAILED_TO_RESOLVE_CONDITION) \
            \
            REGISTER_STATUS(LOCAL_OBJECT_NULL_REFERENCE) \
            \
            REGISTER_STATUS(INVOKE_EXPRESSION_GOT_NULL_EXPRESSION) \
            REGISTER_STATUS(UNCOMPARIBLE_OBJECTS) \

#define REGISTER_STATUS(status) status,
            ACPI_AML_ITERATE_METHOD_CONTEXT_STATUSES(REGISTER_STATUS)
#undef REGISTER_STATUS
        };

        Method *const method;
        Status status{Status::OK};

        [[nodiscard]] inline constexpr
        MethodContext(Method *method)
                : method(method) {
        }

        MethodContext(MethodContext &&) = delete;
        MethodContext(const MethodContext &) = delete;

        core::StackVector<std::unique_ptr<AMLObject>, 8> localObjects{};
        std::unique_ptr<AMLObject> returnValue{};
    };

    [[nodiscard]] constexpr std::string_view
    toString(MethodContext::Status status) {
        switch (status) {
#define CHECK_STATUS(status) case MethodContext::Status::status: return #status;
            ACPI_AML_ITERATE_METHOD_CONTEXT_STATUSES(CHECK_STATUS)
#undef CHECK_STATUS
        }
        return "<invalid_interpreter_status>";
    }

    bool
    Interpreter::areValuesEqual(MethodContext &methodContext, const AMLObject &inLeft, const AMLObject &inRight) {
//        resolve
        static_cast<void>(methodContext);
        static_cast<void>(inLeft);
        static_cast<void>(inRight);

        auto resolveObject = [&] (const AMLObject &in) {
            const auto *object = &in;
            while (object) {
                switch (object->objectType()) {
                    case AMLObject::Type::CONST_NUMBER:
                        return object;
                    case AMLObject::Type::LOCAL_OBJECT_REFERENCE:
                        object = methodContext.localObjects[static_cast<const LocalObjectReference *>(object)->value()].get();
                        break;
                    default:
                        klog<LogLevel::ERROR>("aml::Interpreter::areValuesEqual::resolveObject") << "(Inner) AMLObject::Type not supported: "
                                << toString(object->objectType());
                        CHECK_NOT_REACHED();
                }
            }
            return object;
        };

        const auto *left = resolveObject(inLeft);
        const auto *right = resolveObject(inRight);

        CHECK(left);
        CHECK(right);

        if (left->objectType() != right->objectType()) {
            klog<LogLevel::DEBUG>("aml::Interpreter::areValuesEqual") << "Types differ: " << toString(left->objectType())
                    << " and " << toString(right->objectType());
            return false;
        }

        if (left->objectType() == AMLObject::Type::CONST_NUMBER)
            return static_cast<const ConstNumber *>(left)->value() == static_cast<const ConstNumber *>(right)->value();

        klog<LogLevel::DEBUG>("aml::Interpreter::areValuesEqual") << "Uncomparible type: " << toString(left->objectType());
        methodContext.status = MethodContext::Status::UNCOMPARIBLE_OBJECTS;
        return false;
    }

    std::unique_ptr<AMLObject>
    Interpreter::copyObject(const AMLObject &object) {
        auto stream = klog<LogLevel::DEBUG>("aml::Interpreter::copyObject");
        stream << "ObjectType: " << toString(object.objectType());

        switch (object.objectType()) {
            case AMLObject::Type::CONST_NUMBER:
                return std::make_unique<ConstNumber>(static_cast<const ConstNumber &>(object).value());
            case AMLObject::Type::NAME_STRING: {
                const std::string_view string{static_cast<const NameString &>(object).value().c_str()};
                stream << " value \"" << string << "\"";
                return std::make_unique<NameString>(std::string(string));
            }
            case AMLObject::Type::PACKAGE: {
                stream.finish();
                std::vector<std::unique_ptr<AMLObject>> elements{};
                // TODO reserve when it works correctly again

                for (const auto &inElement : static_cast<const Package &>(object).elements()) {
                    auto element = copyObject(*inElement);
                    CHECK(element.get() != nullptr);
                    elements.push_back(std::move(element));
                }

                return std::make_unique<Package>(std::move(elements));
            }
            default:
                break;
        }

        CHECK_NOT_REACHED();
        return nullptr;
    }

    std::unique_ptr<AMLObject>
    Interpreter::invokeExpression(MethodContext &methodContext, const Expression *inExpression) {
        if (!inExpression) {
            methodContext.status = MethodContext::Status::INVOKE_EXPRESSION_GOT_NULL_EXPRESSION;
            return nullptr;
        }

#ifdef ACPI_AML_INVOKE_EXPRESSION_DEBUG
        klog<LogLevel::DEBUG>("aml::Interpreter::invokeExpression") << "Expression Type: " << toString(inExpression->expressionType());
#endif // ACPI_AML_INVOKE_EXPRESSION_DEBUG
        switch (inExpression->expressionType()) {
            case Expression::Type::ACQUIRE: {
                const auto &expr = static_cast<const AcquireExpression &>(*inExpression);
                klog<LogLevel::DEBUG>("ACQUIRE") << "us @ " << std::size_t(&expr);
                klog<LogLevel::DEBUG>("ACQUIRE") << "MutexObj @ " << std::size_t(expr.mutexObject());
                CHECK(expr.mutexObject()->objectType() == AMLObject::Type::NAME_STRING);

                const auto &strVal = static_cast<const NameString &>(*expr.mutexObject()).value();
                const std::string_view name{strVal.c_str(), strVal.length()};
                klog<LogLevel::DEBUG>("ACQUIRE") << name;
                CHECK_NOT_REACHED();
            }
            case Expression::Type::ADD:
                return invokeLOROTExpression(methodContext, inExpression, [] (auto l, auto r) { return l + r; });
            case Expression::Type::AND:
                return invokeLOROTExpression(methodContext, inExpression, [] (auto l, auto r) { return l & r; });
            case Expression::Type::SUBTRACT:
                return invokeLOROTExpression(methodContext, inExpression, [] (auto l, auto r) { return l - r; });
            case Expression::Type::OR:
                return invokeLOROTExpression(methodContext, inExpression, [] (auto l, auto r) { return l | r; });

            case Expression::Type::EQUAL_TO: {
                const auto &expr = static_cast<const EqualsExpression &>(*inExpression);
                const auto areSame = areValuesEqual(methodContext, expr.left(), expr.right());
                if (methodContext.status != MethodContext::Status::OK)
                    return nullptr;
                return std::make_unique<ConstNumber>(areSame ? 0xFFFFFFFFFFFFFFFF : 0x0000000000000000);
            }
            case Expression::Type::INCREMENT: {
                const auto &expr = static_cast<const IncrementExpression &>(*inExpression);

                auto *const objectReference = resolveSuperNameToReference(methodContext, expr.superName());
                CHECK(objectReference != nullptr);
                CHECK(objectReference->get() != nullptr);

                const auto integer = resolveTermArgToInteger(methodContext, **objectReference);
                CHECK(integer.has_value());
                *objectReference = std::make_unique<ConstNumber>(integer.value() + 1);
                return std::make_unique<DataRefObject>(objectReference);
            }
            case Expression::Type::INDEX: {
                const auto &expr = static_cast<const IndexExpression &>(*inExpression);
#ifdef ACPI_AML_INVOKE_EXPRESSION_DEBUG
                klog<LogLevel::DEBUG>("INDEX") << toString(expr.buffPkgStrObj().objectType());
#endif // ACPI_AML_INVOKE_EXPRESSION_DEBUG

                auto *const objectReference = resolveSuperNameToReference(methodContext, expr.buffPkgStrObj());
                CHECK(objectReference != nullptr);
                CHECK(objectReference->get() != nullptr);

                const auto index = resolveTermArgToInteger(methodContext, expr.indexValue());
#ifdef ACPI_AML_INVOKE_EXPRESSION_DEBUG
                klog<LogLevel::DEBUG>("INDEX") << "Index: " << index.value();
#endif // ACPI_AML_INVOKE_EXPRESSION_DEBUG
                CHECK(index.has_value());

#ifdef ACPI_AML_INVOKE_EXPRESSION_DEBUG
                klog<LogLevel::DEBUG>("INDEX") << "BuffPkgStrObj.type: " << toString(objectReference->get()->objectType());
#endif // ACPI_AML_INVOKE_EXPRESSION_DEBUG
                CHECK_EQ(std::size_t(objectReference->get()->objectType()), std::size_t(AMLObject::Type::PACKAGE));

                auto &package = *static_cast<Package *>(objectReference->get());
#ifdef ACPI_AML_INVOKE_EXPRESSION_DEBUG
                klog<LogLevel::DEBUG>("INDEX") << "Package.size: " << package.elements().size();
#endif // ACPI_AML_INVOKE_EXPRESSION_DEBUG
                CHECK(package.elements().size() > index.value());

                return std::make_unique<DataRefObject>(&package.elements()[index.value()]);
            }
            case Expression::Type::LESS_THAN: {
                const auto *expr = static_cast<const LessThanExpression *>(inExpression);
                auto left = resolveTermArgToInteger(methodContext, expr->left());
                CHECK(left.has_value());
                auto right = resolveTermArgToInteger(methodContext, expr->right());
                CHECK(right.has_value());
                return std::make_unique<ConstNumber>(left.value() < right.value());
            }
            case Expression::Type::SHIFT_LEFT:
            case Expression::Type::SHIFT_RIGHT:
            {
                const auto *expr = static_cast<const ShiftExpression<Expression::Type::SHIFT_LEFT> *>(inExpression);
                auto operand = resolveTermArgToInteger(methodContext, expr->operand());
                CHECK(operand.has_value());
                auto shiftCount = resolveTermArgToInteger(methodContext, expr->shiftCount());
                CHECK(shiftCount.has_value());
                auto value = inExpression->expressionType() == Expression::Type::SHIFT_LEFT
                           ? operand.value() << shiftCount.value()
                           : operand.value() >> shiftCount.value();

                if (expr->target().objectType() != AMLObject::Type::NULL_NAME) {
                    // TODO store the value into the target
                    // TODO figure out if we should copy the value also as the
                    //      return value.
                    CHECK_NOT_REACHED();
                }

                return std::make_unique<ConstNumber>(value);
            }
            // https://uefi.org/specs/ACPI/6.4/19_ASL_Reference/ACPI_Source_Language_Reference.html#store-store-an-object
            case Expression::Type::STORE: {
                const auto *expr = static_cast<const StoreExpression *>(inExpression);
#ifdef ACPI_AML_INVOKE_EXPRESSION_DEBUG
                klog<LogLevel::DEBUG>("aml::Interpreter/StoreExpression") << "Begin";
#endif // ACPI_AML_INVOKE_EXPRESSION_DEBUG

                auto value = resolveObjectToValue(methodContext, expr->termArg());
                if (methodContext.status != MethodContext::Status::OK)
                    return nullptr;

#ifdef ACPI_AML_INVOKE_EXPRESSION_DEBUG
                klog<LogLevel::DEBUG>("aml::Interpreter/StoreExpression") << "Value: " << (value ? toString(value->objectType()) : "null");
                klog<LogLevel::DEBUG>("aml::Interpreter/StoreExpression") << "SuperName: " << toString(expr->superName().objectType());
#endif // ACPI_AML_INVOKE_EXPRESSION_DEBUG

                return store(methodContext, expr->superName(), std::move(value));
            }
            default:
                CHECK_NOT_REACHED();
        }
        return nullptr;
    }

    void
    Interpreter::invokeIfElseStatement(MethodContext &methodContext, const IfElseStatement &statement) {
        ASSERT(statement.condition() != nullptr);
        auto result = resolveTermArgToInteger(methodContext, *statement.condition());
        CHECK(result.has_value());

        auto invokeScope = [&] (const AMLScope &scope) {
            for (const auto &child : scope.children()) {
                ASSERT(child.get() != nullptr);
                invokeStatementOrExpression(methodContext, *child);
                if (methodContext.status != MethodContext::Status::OK)
                    return;
            }
        };

        if (result.value())
            invokeScope(statement.trueScope());
        else
            invokeScope(statement.falseScope());

    }

    std::unique_ptr<AMLObject>
    Interpreter::invokeLOROTExpression(MethodContext &methodContext, const Expression *inExpression, auto operator_) {
        const auto *expr = static_cast<const LeftOperandRightOperandTargetExpression<Expression::Type::RELEASE> *>(inExpression);
        auto left = resolveTermArgToInteger(methodContext, expr->left());
        CHECK(left.has_value());
        auto right = resolveTermArgToInteger(methodContext, expr->right());
        CHECK(right.has_value());
        auto value = operator_(left.value(), right.value());

        if (expr->target().objectType() != AMLObject::Type::NULL_NAME) {
            // TODO copy the value into the target
            // TODO figure out if we should copy the value also as the
            //      return value.
            CHECK_NOT_REACHED();
        }

        return std::make_unique<ConstNumber>(value);
    }

    std::unique_ptr<AMLObject>
    Interpreter::invokeMethod(Method *method) {
        MethodContext methodContext{method};

        for (const auto &child : method->children()) {
#ifdef ACPI_AML_INVOKE_METHOD_DEBUG
            klog<LogLevel::DEBUG>("Interpreter::invokeMethod") << "ChildType: " << toString(child->objectType());
#endif // ACPI_AML_INVOKE_METHOD_DEBUG

            CHECK(child.get() != nullptr);

            invokeStatementOrExpression(methodContext, *child);

            if (methodContext.status == MethodContext::Status::JUMP_STATEMENT_RETURN)
                return std::move(methodContext.returnValue);

#ifdef ACPI_AML_INVOKE_METHOD_DEBUG
            klog<LogLevel::DEBUG>("Interpreter::invokeMethod") << "Status after Expr/Stmt: " << toString(methodContext.status);
#endif // ACPI_AML_INVOKE_METHOD_DEBUG
            CHECK(methodContext.status == MethodContext::Status::OK);
        }

        ASSERT(methodContext.returnValue.get() == nullptr);
        return {};
    }

    void
    Interpreter::invokeStatementOrExpression(MethodContext &methodContext, const AMLObject &object) {
        switch (object.objectType()) {
            case AMLObject::Type::EXPRESSION: {
                const auto *expression = static_cast<const Expression *>(&object);
#ifdef ACPI_AML_INVOKE_STATEMENT_OR_EXPRESSION_DEBUG
                klog<LogLevel::DEBUG>("Interpreter::invokeStatementOrExpression") << "Expression of type " << toString(expression->expressionType());
#endif // ACPI_AML_INVOKE_STATEMENT_OR_EXPRESSION_DEBUG
                static_cast<void>(invokeExpression(methodContext, expression));
                break;
            }
            case AMLObject::Type::STATEMENT: {
                const auto &statement = static_cast<const Statement &>(object);
#ifdef ACPI_AML_INVOKE_STATEMENT_OR_EXPRESSION_DEBUG
                klog<LogLevel::DEBUG>("Interpreter::invokeStatementOrExpression") << "Statement of type " << toString(statement.type());
#endif // ACPI_AML_INVOKE_STATEMENT_OR_EXPRESSION_DEBUG
                switch (statement.type()) {
                    case Statement::Type::IF_ELSE:
                        invokeIfElseStatement(methodContext, static_cast<const IfElseStatement &>(statement));
                        break;
                    case Statement::Type::RETURN: {
                        const auto &returnStatement = static_cast<const ReturnStatement &>(statement);
                        if (returnStatement.result().objectType() == AMLObject::Type::EXPRESSION)
                            methodContext.returnValue = invokeExpression(methodContext, &static_cast<const Expression &>(returnStatement.result()));
                        else if (returnStatement.result().objectType() == AMLObject::Type::LOCAL_OBJECT_REFERENCE) {
                            methodContext.returnValue = resolveObjectToValue(methodContext, returnStatement.result());
                        } else {
                            klog<LogLevel::CRITICAL>("RETURN_STATEMENT") << "Unknown return type: " << toString(returnStatement.result().objectType())
                                    << " for param @ " << std::size_t(&returnStatement.result());
                            CHECK_NOT_REACHED();
                        }
                        if (methodContext.status == MethodContext::Status::OK)
                            methodContext.status = MethodContext::Status::JUMP_STATEMENT_RETURN;
                        break;
                    }
                    case Statement::Type::WHILE:
                        invokeWhileStatement(methodContext, static_cast<const WhileStatement &>(statement));
                        if (methodContext.status != MethodContext::Status::OK)
                            return;
                        break;
                    default:
                        CHECK_NOT_REACHED();
                }
                break;
            }
            default:
                CHECK_NOT_REACHED();
        }
    }

    void
    Interpreter::invokeWhileStatement(MethodContext &methodContext, const WhileStatement &whileStatement) {
        while (true) {
            auto predicate = resolveTermArgToInteger(methodContext, whileStatement.condition());
            if (!predicate.has_value()) {
                methodContext.status = MethodContext::Status::WHILE_STATEMENT_FAILED_TO_RESOLVE_CONDITION;
                return;
            }

            if (!predicate.value())
                return;

            for (const auto &child : whileStatement.children()) {
                ASSERT(child.get() != nullptr);
                invokeStatementOrExpression(methodContext, *child);

                switch (methodContext.status) {
                    case MethodContext::Status::JUMP_STATEMENT_BREAK:
                        methodContext.status = MethodContext::Status::OK;
                        [[fallthrough]];
                    case MethodContext::Status::JUMP_STATEMENT_RETURN:
                        return;
                    case MethodContext::Status::JUMP_STATEMENT_CONTINUE:
                        methodContext.status = MethodContext::Status::OK;
                        continue;
                    case MethodContext::Status::OK:
                        break;
                    default:
                        // Error! Return to caller.
                        return;
                }
            }
        }
    }

    std::unique_ptr<AMLObject>
    Interpreter::resolveObjectToValue(MethodContext &methodContext, const AMLObject &object) {
#ifdef ACPI_AML_RESOLVE_OBJECT_TO_VALUE_DEBUG
        klog<LogLevel::DEBUG>("aml::Interpreter::resolveObjectToValue") << "ObjectType: " << toString(object.objectType());
#endif // ACPI_AML_RESOLVE_OBJECT_TO_VALUE_DEBUG
        switch (object.objectType()) {
            case AMLObject::Type::CONST_NUMBER:
            case AMLObject::Type::NAME_STRING:
                return copyObject(object);
            case AMLObject::Type::EXPRESSION:
                return invokeExpression(methodContext, &static_cast<const Expression &>(object));
            case AMLObject::Type::LOCAL_OBJECT_REFERENCE: {
                const auto index = static_cast<const LocalObjectReference &>(object).value();
#ifdef ACPI_AML_RESOLVE_OBJECT_TO_VALUE_DEBUG
                klog<LogLevel::DEBUG>("aml::Interpreter::resolveObjectToValue") << "Local Object #" << TextModeBufferViewWriter::Decimal
                    << index;
#endif // ACPI_AML_RESOLVE_OBJECT_TO_VALUE_DEBUG
                ASSERT(index < 8);
                auto *localObject = methodContext.localObjects[index].get();
                if (localObject == nullptr)
                    return nullptr;
                return copyObject(*localObject);
            }
            case AMLObject::Type::PACKAGE: {
                const auto &inPackage = static_cast<const Package &>(object);
#ifdef ACPI_AML_RESOLVE_OBJECT_TO_VALUE_DEBUG
                klog<LogLevel::DEBUG>("Count") << inPackage.elements().size();
#endif // ACPI_AML_RESOLVE_OBJECT_TO_VALUE_DEBUG

                std::vector<std::unique_ptr<AMLObject>> resultElements{};
                resultElements.reserve(inPackage.elements().size());

                for (const auto &element : inPackage.elements()) {
                    if (!element)
                        resultElements.push_back(nullptr);
                    else
                        resultElements.push_back(resolveObjectToValue(methodContext, *element));
                }

                return std::make_unique<Package>(std::move(resultElements));
            }
            default:
                break;
        }
        CHECK_NOT_REACHED();
        return nullptr;
    }

    [[nodiscard]] std::unique_ptr<AMLObject> *
    Interpreter::resolveSuperNameToReference(MethodContext &methodContext, const AMLObject &superName) {
        switch (superName.objectType()) {
            case AMLObject::Type::LOCAL_OBJECT_REFERENCE: {
                const auto index = static_cast<const LocalObjectReference &>(superName).value();
                ASSERT(index < 8);
//                klog<LogLevel::DEBUG>("aml::Interpreter::resolveSuperNameToReference") << "LocalObjectReference.index: " << index
//                        << " valuing " << std::size_t(methodContext.localObjects[index].get());
                return &methodContext.localObjects[index];
            }
            default:
//                klog<LogLevel::ERROR>("aml::Interpreter::resolveSuperNameToReference") << "Type: " << toString(superName.objectType());
                CHECK_NOT_REACHED();
                return nullptr;
        }
    }

    std::optional<std::uint64_t>
    Interpreter::resolveTermArgToInteger(MethodContext &methodContext, const AMLObject &termArg) {
//        klog<LogLevel::DEBUG>("AMLInterpreter::resolveTermArgToInteger") << "Initial ObjectType: " << toString(termArg.objectType()) << " for TermArg @ " << std::size_t(&termArg);

        switch (termArg.objectType()) {
            case AMLObject::Type::EXPRESSION: {
                auto result = invokeExpression(methodContext, static_cast<const Expression *>(&termArg));
//                klog<LogLevel::DEBUG>("AMLInterpreter::resolveTermArgToInteger") << "AfterExpr ObjectType: " << toString(termArg.objectType());
                CHECK(result->objectType() == AMLObject::Type::CONST_NUMBER);
                return static_cast<const ConstNumber *>(result.get())->value();
            }
            case AMLObject::Type::CONST_NUMBER:
                return static_cast<const ConstNumber &>(termArg).value();
            case AMLObject::Type::LOCAL_OBJECT_REFERENCE: {
                const auto index = static_cast<const LocalObjectReference &>(termArg).value();
//                klog<LogLevel::DEBUG>("aml::Interpreter::resolveTermArgToInteger") << "Local Object #" << TextModeBufferViewWriter::Decimal
//                        << index;
                ASSERT(index < 8);
                auto *object = methodContext.localObjects[index].get();
                if (object == nullptr) {
                    methodContext.status = MethodContext::Status::LOCAL_OBJECT_NULL_REFERENCE;
                    return std::nullopt;
                }
                return resolveTermArgToInteger(methodContext, *object);
            }
            default:
                break;
        }

        CHECK_NOT_REACHED();
        return {0};
    }

    // NOTE: param `value` may be null!
    std::unique_ptr<DataRefObject>
    Interpreter::store(MethodContext &methodContext, const AMLObject &superName, std::unique_ptr<AMLObject> &&value) {
        static std::size_t idManager = 0;
        const auto id = idManager++;

//        klog<LogLevel::DEBUG>("aml::Interpreter::store") << id << " Store value of type " << (value ? toString(value->objectType()) : "null");
        switch (superName.objectType()) {
            case AMLObject::Type::CONST_NUMBER: {
                const auto &constNumber = static_cast<const ConstNumber &>(superName);
//                klog<LogLevel::DEBUG>("ConstNumber") << id << " " << constNumber.value();
                CHECK(constNumber.value() < 2);
                return nullptr;
            }
            case AMLObject::Type::EXPRESSION: {
                const auto &expression = static_cast<const Expression &>(superName);
//                klog<LogLevel::DEBUG>("aml::Interpreter::store") << id << " ExpressionType: " << toString(expression.expressionType());
                auto result = invokeExpression(methodContext, &expression);
                CHECK(methodContext.status == MethodContext::Status::OK);
                if (result.get() == nullptr)
                    return nullptr;
//                klog<LogLevel::DEBUG>("aml::Interpreter::store") << id << " Result: " << toString(result->objectType());
                CHECK(result->objectType() == AMLObject::Type::DATA_REF_OBJECT);

                auto *reference = static_cast<DataRefObject *>(result.get())->objectReference();
                CHECK(reference != nullptr);

                *reference = std::move(value);

                // Return the DataRefObject, that is the `result`.
//                klog<LogLevel::DEBUG>("aml::Interpreter::store") << id << " Value @ " << std::size_t(reference->get());
                return std::move(result);
            }
            case AMLObject::Type::LOCAL_OBJECT_REFERENCE: {
                const auto index = static_cast<const LocalObjectReference &>(superName).value();
//                klog<LogLevel::DEBUG>("aml::Interpreter::store") << id << " Local Object #" << TextModeBufferViewWriter::Decimal
//                        << index << " for value " << TextModeBufferViewWriter::Hexadecimal << std::size_t(value.get());
                auto *ptr = value.get();
                ASSERT(index < 8);
                auto &localObject = methodContext.localObjects[index];
                localObject = std::move(value);
                ASSERT(localObject.get() == ptr);
                return std::make_unique<DataRefObject>(&localObject);
            }
            default:
                klog<LogLevel::DEBUG>("aml::Interpreter::store") << id << " Unknown SuperName(Target): " << toString(superName.objectType());
                CHECK_NOT_REACHED();
                return nullptr;
        }
    }

} // namespace kernel::acpi::aml
