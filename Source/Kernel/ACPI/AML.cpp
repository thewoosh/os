#include "Kernel/ACPI/AML.hpp"

#include <array>
#include <utility>

#include "Kernel/ACPI/AML/AMLObject.hpp"
#include "Kernel/ACPI/AML/DataPrefix.hpp"
#include "Kernel/ACPI/AML/Expression.hpp"
#include "Kernel/ACPI/AML/Field.hpp"
#include "Kernel/ACPI/AML/Method.hpp"
#include "Kernel/ACPI/AML/NamedObject.hpp"
#include "Kernel/ACPI/AML/OpCode.hpp"
#include "Kernel/ACPI/AML/Package.hpp"
#include "Kernel/ACPI/AML/RegionSpace.hpp"
#include "Kernel/ACPI/AML/Statement.hpp"
#include "Kernel/Assert.hpp"
#include "Kernel/Print.hpp"
#include "Kernel/Runtime/Crash.hpp"

//#define AML_PARSER_FUNCTION_NEXT
//#define AML_PARSER_FUNCTION_STACK_SCOPE
//
//#define AML_PARSER_DEBUG_ASSIGN_OBJECT_TO_PARENT_SCOPE
//#define AML_PARSER_DEBUG_FIND_ALL_OBJECTS_WITH_NAME
//#define AML_PARSER_DEBUG_FIND_INVOCATION_TARGET
//#define AML_PARSER_DEBUG_FIND_OBJECT
//#define AML_PARSER_DEBUG_FIND_OBJECT_ABSOLUTE
//#define AML_PARSER_DEBUG_FIND_OBJECT_RELATIVE_DOWNWARDS
//#define AML_PARSER_DEBUG_FIND_PARENT
//#define AML_PARSER_DEBUG_METHOD_INVOCATION_EXPRESSION
//#define AML_PARSER_DEBUG_READ_CONST_NUMBER
//#define AML_PARSER_DEBUG_READ_DEF_BLOCK_HEADER
//#define AML_PARSER_DEBUG_READ_DEF_BUFFER
//#define AML_PARSER_DEBUG_READ_DEF_DEVICE
//#define AML_PARSER_DEBUG_READ_DEF_FIELD
//#define AML_PARSER_DEBUG_READ_DEF_METHOD

#define AML_PARSER_ERRORS
#define AML_PARSER_WARNINGS
#define AML_PARSER_INFO
#define AML_PARSER_TODO

namespace kernel::acpi::aml {

    struct ScopedScope {
        [[nodiscard]] inline constexpr
        ScopedScope(Parser *parser, AMLScope *scope, AMLScope *scopeOverride = nullptr)
                : m_parser(parser)
                , m_oldScope(scopeOverride ?: parser->m_currentScope) {
            parser->m_currentScope = scope;
            scope->setParent(m_oldScope);
        }

        inline constexpr
        ~ScopedScope() {
            m_parser->m_currentScope = m_oldScope;
        }

    private:
        Parser *const m_parser;
        AMLScope *const m_oldScope;
    };

    struct ScopedPkgLength {
        [[nodiscard]] explicit
        ScopedPkgLength(Parser *parser, std::size_t begin, PkgLength pkgLength, std::string_view name)
                : m_parser(parser)
                , m_resetValue(parser->m_pkgLengths) {
            m_parser->m_pkgLengths.push_back({
                .begin = begin,
                .size = pkgLength,
                .origin = name
            });
        }

        ~ScopedPkgLength() {
            // Check if the diff in element count is one?
            m_parser->m_pkgLengths = m_resetValue;
        }

    private:
        Parser *m_parser;
        core::StackVector<Parser::PkgLengthEntry, kMaxRecursionDepth> m_resetValue;
    };

#define DUMP() \
        klog<LogLevel::DEBUG>("AMLParser::NamedElement") << "Dumping data: "; \
        { \
            auto stream = klog<LogLevel::DEBUG>("Row"); \
            std::size_t count{0}; \
            for (auto __it = m_iter; __it < m_data.end(); ++__it) { \
                stream << std::uint64_t(*__it) << " "; \
                if (++count % 50 == 0) {\
                    stream.finish(); \
                    stream = klog<LogLevel::DEBUG>("Row"); \
                } \
            } \
        }

#ifdef AML_PARSER_FUNCTION_NEXT
    void
    nextImplementation(core::RegionPointer<const std::uint8_t>::ConstIterator it,
                       core::RegionPointer<const std::uint8_t>::ConstIterator end) {
        auto stream = klog<LogLevel::DEBUG>("> Next");
        for (std::size_t i = 0; i < 10; ++i) {
            if (it + i == end)
                break;
            stream << std::size_t(it[i]) << " ";
        }
    }

#define NEXT() nextImplementation(m_iter, m_data.end())
//#define NEXT() klog<LogLevel::DEBUG>("> Next") << std::size_t(*m_iter) << " " << std::size_t(m_iter[1]) \
//            << " " << std::size_t(m_iter[2]) << " " << std::size_t(m_iter[3]) << " " << std::size_t(m_iter[4]) \
//            << " " << std::size_t(m_iter[5]) << " " << std::size_t(m_iter[6]) << " " << std::size_t(m_iter[7]) \
//            << " " << std::size_t(m_iter[8]) << " " << std::size_t(m_iter[9])
#else // AML_PARSER_FUNCTION_NEXT
#define NEXT() static_cast<void>(0)
#endif // AML_PARSER_FUNCTION_NEXT

#define READ(variable_name, status_when_failed) \
    if (!read(variable_name)) { \
        klog<LogLevel::ERROR>("AMLParser") << "Couldn't read into \"" #variable_name "\", retval=" #status_when_failed; \
        return Status::status_when_failed; \
    }

#define READ_VAR_STRING(variable_name, status_when_failed) \
    for (auto &element : variable_name) { \
        READ(element, status_when_failed) \
        if (element == '\0') \
            break; \
    }

#ifdef AML_PARSER_FUNCTION_STACK_SCOPE
#   define DUMP_SCOPE_CHAIN(printTag, beginScope) \
        for (auto *scope = beginScope; scope; scope = scope->parent()) \
            klog<LogLevel::DEBUG>(printTag) << std::string_view{scope->name().c_str()} << " typed " \
                    << (scope->asObject() ? toString(scope->asObject()->objectType()) : "NotAnObject")

#   define STACK_SCOPE() DUMP_SCOPE_CHAIN("StackScope", m_currentScope)
#else // AML_PARSER_FUNCTION_STACK_SCOPE
#   define DUMP_SCOPE_CHAIN(printTag, beginScope) static_cast<void>(printTag, beginScope);
#   define STACK_SCOPE() static_cast<void>(0)
#endif // AML_PARSER_FUNCTION_STACK_SCOPE

    void
    Parser::assignObjectToParentScope(std::unique_ptr<AMLObject> &&object, AMLScope &relativeParent) {
        auto *asScope = const_cast<AMLScope *>(object->asScope());

        if (object->objectType() == AMLObject::Type::SCOPE) {
            ASSERT(asScope != nullptr);
            const std::string_view name{asScope->name().c_str(), asScope->name().length()};
#ifdef AML_PARSER_DEBUG_ASSIGN_OBJECT_TO_PARENT_SCOPE
            klog<LogLevel::DEBUG>("Name") << name;
#endif
            auto *objectWithTheSameName = findObject(name);

            if (objectWithTheSameName != nullptr && objectWithTheSameName->asScope() != nullptr) {
#ifdef AML_PARSER_DEBUG_ASSIGN_OBJECT_TO_PARENT_SCOPE
                klog<LogLevel::DEBUG>("AMLParser/assignObjectToParentScope") << "object->type() = "
                        << toString(object->objectType()) << " @ " << std::size_t(object.get());
                klog<LogLevel::DEBUG>("AMLParser/assignObjectToParentScope") << "objectWithTheSameName->type() = "
                        << toString(objectWithTheSameName->objectType()) << " @ " << std::size_t(objectWithTheSameName);
#endif

                CHECK(objectWithTheSameName->objectType() == AMLObject::Type::DEVICE
                      || objectWithTheSameName->objectType() == AMLObject::Type::SCOPE
                      || objectWithTheSameName->objectType() == AMLObject::Type::ROOT_OBJECT);

#ifdef AML_PARSER_DEBUG_ASSIGN_OBJECT_TO_PARENT_SCOPE
                klog<LogLevel::DEBUG>("AMLParser/assignObjectToParentScope") << " named " << objectWithTheSameName->asScope()->name().c_str();
#endif
                const auto childCount = asScope->children().size();
                if (childCount != 0) {
                    auto *sameNameObjectAsScope = const_cast<AMLScope *>(objectWithTheSameName->asScope());
                    // TODO std::vector::reserve doesn't set the m_data_end correctly.
    //                sameNameObjectAsScope->children().reserve(sameNameObjectAsScope->children().size() + childCount);
                    for (auto &child : asScope->children()) {
                        if (auto *childAsScope = const_cast<AMLScope *>(child->asScope()))
                            childAsScope->setParent(sameNameObjectAsScope);

#ifdef AML_PARSER_DEBUG_ASSIGN_OBJECT_TO_PARENT_SCOPE
                        klog<LogLevel::DEBUG>("Child") << std::size_t(child.get()) << " of type " << toString(child->objectType());
#endif
                        sameNameObjectAsScope->children().push_back(std::move(child));
                    }
                }

#ifdef AML_PARSER_DEBUG_ASSIGN_OBJECT_TO_PARENT_SCOPE
                klog<LogLevel::DEBUG>("AMLParser/assignObjectToParentScope") << "Found scope that already existed! Moved "
                        << TextModeBufferViewWriter::Decimal << childCount << " child(ren)";
#endif
                return;
            }
        }

        auto parentInfo = findParent(*object);
#ifdef AML_PARSER_DEBUG_ASSIGN_OBJECT_TO_PARENT_SCOPE
        {
            auto stream = klog<LogLevel::DEBUG>("AMLParser/assignObjectToParentScope");
            stream << "Assign absolute child @ " << std::size_t(object.get());
            if (asScope)
                stream << " full-named \"" << asScope->name().c_str() << "\"";
            stream << " of type " << toString(object->objectType())
                   << " with prefix \"" << parentInfo.parentPrefix << "\" and actualName \"" << parentInfo.name
                   << "\" to parent with name \"" << (parentInfo.parent ? parentInfo.parent->name().c_str() : relativeParent.name().c_str()) << "\"";
        }
        if (object->objectType() == AMLObject::Type::STATEMENT)
            klog<LogLevel::DEBUG>("AMLParser/assignObjectToParentScope") << "Child Statement::Type: " << toString(static_cast<const Statement *>(object.get())->type());
        else if (object->objectType() == AMLObject::Type::EXPRESSION)
            klog<LogLevel::DEBUG>("AMLParser/assignObjectToParentScope") << "Child Expression::Type: " << toString(static_cast<const Expression *>(object.get())->expressionType());
#endif

        // Assign the new name. The current name is an absolute (root) name,
        // e.g.: "\._SB_.PCI0.blah.blah". If the parentPrefix is empty, the name
        // isn't absolute, for example: "DBG_".
        if (asScope != nullptr && !parentInfo.name.empty() && !parentInfo.parentPrefix.empty())
            asScope->overrideName(std::string(parentInfo.name));

        if (!parentInfo.parent) {
            if (asScope != nullptr) {
#ifdef AML_PARSER_DEBUG_ASSIGN_OBJECT_TO_PARENT_SCOPE
                klog<LogLevel::INFO>("AMLParser/assignObjectToParentScope") << "Scope \"" << asScope->name().c_str()
                        << "\"'s parent couldn't be found! Using relative parent.";
#endif
                asScope->setParent(&relativeParent);
            }
            relativeParent.children().push_back(std::move(object));
            return;
        }

        if (asScope != nullptr)
            asScope->setParent(const_cast<AMLScope *>(parentInfo.parent));
        const_cast<AMLScope *>(parentInfo.parent)->children().push_back(std::move(object));

//
//        if (asScope != nullptr) {
//            if (asScope->parent() != nullptr) {
//                klog<LogLevel::INFO>("AMLParser/AdoptionAgency") << "Object already has a parent :)";
//                asScope->children().push_back(std::move(object));
//                return;
//            }
//
//            const auto name = std::string_view{asScope->name().c_str(), asScope->name().length()};
//            klog<LogLevel::DEBUG>("AMLParser/TermObject") << "Scope with name \"" << name << "\"";
//
//            if (name.starts_with("\\.")) {
//                const auto dotPos = name.rfind('.');
//                const auto prefix = name.substr(0, dotPos);
//                const auto actualName = name.substr(dotPos + 1);
//                klog<LogLevel::DEBUG>("AMLParser/TermObject") << "> Prefix \"" << prefix << "\"";
//                klog<LogLevel::DEBUG>("AMLParser/TermObject") << "> ActualName \"" << actualName << "\"";
//
//                auto absoluteParent = findObject(prefix);
//                CHECK(absoluteParent != nullptr);
//
//                auto *const absoluteParentAsScope = const_cast<AMLScope *>(absoluteParent->asScope());
//                CHECK(absoluteParentAsScope != nullptr);
//
//                const_cast<AMLScope *>(absoluteParent->asScope())->children().push_back(std::move(object));
//                asScope->setParent(const_cast<AMLScope *>(absoluteParent->asScope()));
//                asScope->overrideName(std::string(actualName));
//                return;
//            }
//        }
//
//        relativeParent.children().push_back(std::move(object));
//        if (asScope)
//            asScope->setParent(&relativeParent);
    }

    template<typename CallbackType>
    void
    Parser::findAllObjectsWithName(std::string_view targetName, const AMLScope &parent, CallbackType callback) {
//        if (std::string_view{parent.name().c_str(), parent.name().length()} == targetName)
//            callback(parent);
        for (const auto &child : parent.children()) {
            std::string_view name;
#ifdef AML_PARSER_DEBUG_FIND_ALL_OBJECTS_WITH_NAME
            klog<LogLevel::DEBUG>("Checking child of type") << toString(child->objectType());
#endif

            if (const auto *childAsScope = child->asScope()) {
                name = {childAsScope->name().c_str(), child->asScope()->name().length()};
                findAllObjectsWithName(targetName, *childAsScope, callback);
            } else {
                switch (child->objectType()) {
                    case AMLObject::Type::CREATE_BYTE_FIELD:
                    case AMLObject::Type::CREATE_WORD_FIELD:
                    case AMLObject::Type::CREATE_DWORD_FIELD:
                    case AMLObject::Type::CREATE_QWORD_FIELD: {
                        const auto *namedObject = static_cast<const NamedObject *>(child.get());
                        name = {namedObject->name().c_str(), namedObject->name().length()};
                        break;
                    }
                    case AMLObject::Type::NAME_DEFINITION: {
                        const auto *def = static_cast<const NameDefinition *>(child.get());
                        name = {def->name().c_str(), def->name().length()};
                        break;
                    }
                    case AMLObject::Type::OPERATION_REGION: {
                        const auto *region = static_cast<const OperationRegion *>(child.get());
                        name = {region->name().c_str(), region->name().length()};
                        break;
                    }
                    case AMLObject::Type::NAME_STRING: {
                        const auto *nameString = static_cast<const NameString *>(child.get());
                        name = {nameString->value().c_str(), nameString->value().length()};
                        break;
                    }
                    default:
                        break;
                }
            }

            if (!name.empty() && name == targetName) {
#ifdef AML_PARSER_DEBUG_FIND_ALL_OBJECTS_WITH_NAME
                klog<LogLevel::DEBUG>("Checking name") << name << " is the same as " << targetName;
#endif
                callback(parent, *child);
            }
        }
    }

    Parser::InvocationTarget
    Parser::findInvocationTarget(std::string_view nameToFind) const {
#ifdef AML_PARSER_DEBUG_FIND_INVOCATION_TARGET
        klog<LogLevel::INFO>("AMLParser/findInvocationTarget") << nameToFind;
#endif
        const auto *object = findObject(nameToFind);
        if (object != nullptr && object->objectType() == AMLObject::Type::METHOD)
            return {object, static_cast<const Method *>(object)->flags().argumentCount()};
        return {object, 0};
    }

    const AMLObject *
    Parser::findObject(std::string_view nameToFind) const {
#ifdef AML_PARSER_DEBUG_FIND_OBJECT
        klog<LogLevel::INFO>("AMLParser/findObject") << nameToFind;
#endif

        if (nameToFind.empty())
            return nullptr;

        if (nameToFind[0] == '\\') {
            if (nameToFind.size() <= 2)
                return &m_rootObject;
            return findObjectAbsolute(nameToFind.substr(2), &m_rootObject);
        }

        return findObjectRelative(nameToFind, m_currentScope);
    }

    const AMLObject *
    Parser::findObjectAbsolute(std::string_view nameToFind, const AMLScope *scope) const {
#if defined(AML_PARSER_DEBUG_FIND_OBJECT_ABSOLUTE) || defined(AML_PARSER_ERRORS)
        const auto fullName = nameToFind;
#endif


        ASSERT(scope != nullptr);
        {
#ifdef AML_PARSER_DEBUG_FIND_OBJECT_ABSOLUTE
            klog<LogLevel::DEBUG>("Scope") << std::size_t(scope) << " of name " << scope->name().c_str()
                    << " with parent @ " << std::size_t(scope->parent());
            klog<LogLevel::DEBUG>("Name") << nameToFind << " sized " << nameToFind.size();
#endif

            for (const auto &child : scope->children()) {
#ifdef AML_PARSER_DEBUG_FIND_OBJECT_ABSOLUTE
                klog<LogLevel::DEBUG>("ChildMeta") << std::size_t(child.get()) << " of type " << toString(child->objectType());
#endif

                const auto *scopeChild = child->asScope();
                if (!scopeChild) {
                    if (child->objectType() == AMLObject::Type::NAME_DEFINITION) {
                        const auto *nameDef = static_cast<const NameDefinition *>(child.get());
#ifdef AML_PARSER_DEBUG_FIND_OBJECT_ABSOLUTE
                        klog<LogLevel::DEBUG>("NameDef") << std::string_view{nameDef->name().c_str(), nameDef->name().length()};
#endif
                        // TODO shouldn't there be a return here ?
                        static_cast<void>(nameDef);
                    }

                    continue;
                }

                if (child->objectType() == AMLObject::Type::FIELD) {
                    for (const auto &childOfChild : scopeChild->children()) {
#ifdef AML_PARSER_DEBUG_FIND_OBJECT_ABSOLUTE
                        klog<LogLevel::DEBUG>("FieldElement") << "Type: " << toString(childOfChild->objectType());
#endif
                        if (childOfChild->objectType() == AMLObject::Type::FIELD_ELEMENT_NAMED) {
                            const auto fieldElementName =
                                    std::string_view{static_cast<const NamedFieldElement *>(childOfChild.get())->name().c_str()};
#ifdef AML_PARSER_DEBUG_FIND_OBJECT_ABSOLUTE
                            klog<LogLevel::DEBUG>(">") << fieldElementName;
#endif
                            if (fieldElementName == nameToFind)
                                return childOfChild.get();
                        }
                    }
                }

                const std::string_view childName{scopeChild->name().c_str(), scopeChild->name().length()};
#ifdef AML_PARSER_DEBUG_FIND_OBJECT_ABSOLUTE
                klog<LogLevel::DEBUG>("ChildName") << childName;
#endif

                if (nameToFind.starts_with(childName)) {
                    if (childName.size() + 1 >= nameToFind.size()) {
#ifdef AML_PARSER_DEBUG_FIND_OBJECT_ABSOLUTE
                        klog<LogLevel::ERROR>("AMLParser/findObjectAbsolute") << "Supposed done?";
#endif
                        return child.get();
                    }
//                    scope = scopeChild;
//                    nameToFind = nameToFind.substr(childName.size() + 1);
//                    foundChild = true;
//                    break;
                    if (auto *object = findObjectAbsolute(nameToFind.substr(childName.size() + 1), scopeChild))
                        return object;
                }
            }
        }

#ifdef AML_PARSER_ERRORS
        klog<LogLevel::ERROR>("AMLParser/findObjectAbsolute") << "Part \"" << nameToFind << "\" of name \""
                                                              << fullName << "\" wasn't found!";
#endif
        return nullptr;
    }

    const AMLObject *
    Parser::findObjectRelative(std::string_view nameToFind, const AMLScope *scope) const {
        while (scope != nullptr) {
            if (auto *object = findObjectRelativeDownwards(nameToFind, scope))
                return object;

            scope = scope->parent();
        }

        return nullptr;
    }

    const AMLObject *
    Parser::findObjectRelativeDownwards(std::string_view nameToFind, const AMLScope *scope) const {
#ifdef AML_PARSER_DEBUG_FIND_OBJECT_RELATIVE_DOWNWARDS
        klog<LogLevel::DEBUG>("Downwards") << std::size_t(scope) << " Scope: " << (scope->asObject() ? toString(scope->asObject()->objectType()) : "NotAnObject") << " " << scope->name().c_str() << " childcount=" << TextModeBufferViewWriter::Decimal << scope->children().size();
#endif
        if (scope->asObject() && std::string_view{scope->name().c_str(), scope->name().length()} == nameToFind)
            return scope->asObject();

        for (const auto &child : scope->children()) {
            CHECK(child.get() != nullptr);
#ifdef AML_PARSER_DEBUG_FIND_OBJECT_RELATIVE_DOWNWARDS
            klog<LogLevel::DEBUG>("Downwards") << std::size_t(scope) << "     Child: " << std::size_t(child.get()) << " " << toString(child->objectType()) << " " << (child->asScope() ? child->asScope()->name().c_str() : "not");
#endif

            auto *childAsScope = child->asScope();
            if (childAsScope) {
                if (auto *object = findObjectRelativeDownwards(nameToFind, childAsScope))
                    return object;
                continue;
            }

            switch (child->objectType()) {
                case AMLObject::Type::CREATE_BYTE_FIELD:
                case AMLObject::Type::CREATE_DWORD_FIELD:
                case AMLObject::Type::CREATE_QWORD_FIELD:
                case AMLObject::Type::CREATE_WORD_FIELD: {
                    const auto *namedObject = static_cast<const NamedObject *>(child.get());
                    const std::string_view name{namedObject->name().c_str(), namedObject->name().length()};
#ifdef AML_PARSER_DEBUG_FIND_OBJECT_RELATIVE_DOWNWARDS
                    klog<LogLevel::DEBUG>(">") << name;
#endif
                    if (name == nameToFind)
                        return child.get();
                    break;
                }
                case AMLObject::Type::FIELD_ELEMENT_NAMED: {
                    const auto &nameOfElement = static_cast<const NamedFieldElement *>(child.get())->name();
                    if (std::string_view{nameOfElement.c_str(), nameOfElement.length()} == nameToFind)
                        return child.get();
                    break;
                }
                case AMLObject::Type::NAME_DEFINITION: {
                    const auto *nameDefinition = static_cast<const NameDefinition *>(child.get());
                    const std::string_view name{nameDefinition->name().c_str(), nameDefinition->name().length()};
#ifdef AML_PARSER_DEBUG_FIND_OBJECT_RELATIVE_DOWNWARDS
                    klog<LogLevel::DEBUG>(">") << name;
#endif
                    if (name == nameToFind)
                        return child.get();
                    break;
                }
                default:
                    break;
            }
        }

        return nullptr;
    }

    Parser::ParentInformation
    Parser::findParent(const AMLObject &object) const {
        const auto *const asScope = object.asScope();

        if (asScope == nullptr)
            return {};

        const auto name = std::string_view{asScope->name().c_str(), asScope->name().length()};
#ifdef AML_PARSER_DEBUG_FIND_PARENT
        klog<LogLevel::DEBUG>("AMLParser/findParent") << "Scope with name \"" << name << "\" and "
                << TextModeBufferViewWriter::Decimal << asScope->children().size() << " children";
#endif
        const auto dotPos = name.rfind('.');
        const auto prefix = dotPos == std::string_view::npos ? std::string_view{} : name.substr(0, dotPos);
        const auto actualName = name.substr(dotPos + 1);
#ifdef AML_PARSER_DEBUG_FIND_PARENT
        klog<LogLevel::DEBUG>("AMLParser/findParent") << "    Dissected \"" << prefix << "\" > \"" << actualName << "\"";
#endif

        if (asScope->parent() != nullptr) {
#ifdef AML_PARSER_DEBUG_FIND_PARENT
            klog<LogLevel::DEBUG>("AMLParser/findParent") << "    Already has a parent ^_^";
#endif
            return {asScope->parent(), prefix, actualName};
        }

        if (!name.starts_with("\\.")) {
#ifdef AML_PARSER_DEBUG_FIND_PARENT
            klog<LogLevel::DEBUG>("AMLParser/findParent") << "    Isn't relative";
#endif
            return {};
        }
#ifdef AML_PARSER_DEBUG_FIND_PARENT
        klog<LogLevel::DEBUG>("AMLParser/findParent") << "> Prefix \"" << prefix << "\"";
        klog<LogLevel::DEBUG>("AMLParser/findParent") << "> ActualName \"" << actualName << "\"";
#endif

        auto absoluteParent = findObject(prefix);
        CHECK(absoluteParent != nullptr);

        auto *const absoluteParentAsScope = const_cast<AMLScope *>(absoluteParent->asScope());
        CHECK(absoluteParentAsScope != nullptr);
#ifdef AML_PARSER_DEBUG_FIND_PARENT
        klog<LogLevel::DEBUG>("AMLParser/findParent") << "    Parent's name: " << absoluteParentAsScope->name().c_str();
#endif
        return {absoluteParentAsScope, prefix, actualName};

//        const_cast<AMLScope *>(absoluteParent->asScope())->children().push_back(std::move(object));
//        asScope->setParent(const_cast<AMLScope *>(absoluteParent->asScope()));
//        asScope->overrideName(std::string(actualName));
    }

    template <typename T>
    bool
    Parser::peek(T &data) const {
        std::size_t pkgLengthIndex = 0;
        bool isValidRead = true;
        for (auto pkgLength : m_pkgLengths) {
            const auto max = pkgLength.begin + pkgLength.size;
            if (m_iter.base() >= max) {
#ifdef AML_PARSER_ERRORS
                klog<LogLevel::ERROR>("AMLParser/peek") << TextModeBufferViewWriter::Decimal << "m_iter="
                        << m_iter.base() << " PkgLength{.begin="
                        << pkgLength.begin << ", .size=" << pkgLength.size << ", .origin=\"" << pkgLength.origin
                        << "\"} max=" << max << " #" << TextModeBufferViewWriter::Decimal << pkgLengthIndex
                        << " value would've been " << TextModeBufferViewWriter::Hexadecimal << *m_iter;
#endif
                isValidRead = false;
            }
            ++pkgLengthIndex;
        }

        if (!isValidRead) {
#if 0
            CRASH("illegal read according to PkgLength");
#endif
            return false;
        }

        return m_iter.copy_with_status(data);
    }

    template <typename T>
    bool
    Parser::read(T &data) {
        std::size_t pkgLengthIndex = 0;
        bool isValidRead = true;
        for (auto pkgLength : m_pkgLengths) {
            const auto max = pkgLength.begin + pkgLength.size;
            if (m_iter.base() >= max) {
#ifdef AML_PARSER_ERRORS
                klog<LogLevel::ERROR>("AMLParser/read") << TextModeBufferViewWriter::Decimal << "m_iter="
                        << m_iter.base() << " PkgLength{.begin="
                        << pkgLength.begin << ", .size=" << pkgLength.size << ", .origin=\"" << pkgLength.origin
                        << "\"} max=" << max << " #" << TextModeBufferViewWriter::Decimal << pkgLengthIndex
                        << " value would've been " << TextModeBufferViewWriter::Hexadecimal << *m_iter;
#endif
                isValidRead = false;
            }
            ++pkgLengthIndex;
        }

        if (!isValidRead) {
#if 0
            CRASH("illegal read according to PkgLength");
#endif
            return false;
        }

        if (!m_iter.copy_with_status(data)) {
            klog<LogLevel::DEBUG>("AMLParser/read") << "Couldn't copy_with_status";
            return false;
        }

        m_iter += sizeof(T);
        return true;
    }

    Parser::Status
    Parser::read_const_number(std::uint64_t &out) {
        DataPrefix prefix;
        if (!read(prefix)) {
            return Status::EOS_WHILE_READING_CONST_NUMBER;
        }

#ifdef AML_PARSER_DEBUG_READ_CONST_NUMBER
        klog<LogLevel::DEBUG>("AMLParser/ConstNumber/Prefix") << std::size_t(prefix);
#endif

        switch (prefix) {
            case DataPrefix::BytePrefix: {
                std::uint8_t data;
                if (!read(data)) {
                    --m_iter;
                    return Status::EOS_WHILE_READING_CONST_NUMBER;
                }
                out = data;
                return Status::OK;
            }
            case DataPrefix::WordPrefix: {
                std::uint16_t data;
                if (!read(data)) {
                    --m_iter;
                    return Status::EOS_WHILE_READING_CONST_NUMBER;
                }
#ifdef AML_PARSER_DEBUG_READ_CONST_NUMBER
                klog<LogLevel::DEBUG>("Word") << data;
#endif
                out = data;
                return Status::OK;
            }
            case DataPrefix::DWordPrefix: {
                std::uint32_t data;
                if (!read(data)) {
                    --m_iter;
                    return Status::EOS_WHILE_READING_CONST_NUMBER;
                }
                out = data;
                return Status::OK;
            }
            case DataPrefix::QWordPrefix:
                if (!read(out)) {
                    --m_iter;
                    return Status::EOS_WHILE_READING_CONST_NUMBER;
                }
                return Status::OK;
            default:
                switch (static_cast<std::uint8_t>(prefix)) {
                    case 0x00: // ZeroOp
                        out = 0x00;
                        return Status::OK;
                    case 0x01: // OneOp
                        out = 0x01;
                        return Status::OK;
                    case 0xFF: // OnesOp
                        out = 0xFF;
                        return Status::OK;
                    default:
                        --m_iter;
                        return Status::NOT_A_CONST_NUMBER;
                }
        }
    }

    Parser::StatusAnd<std::unique_ptr<AMLObject>>
    Parser::readDataObject(bool silentOnFailure) {
        const auto savedIt = m_iter;
        std::uint64_t constNumber;
        auto constNumberStatus = read_const_number(constNumber);
        if (constNumberStatus == Status::OK)
            return {Status::OK, std::make_unique<ConstNumber>(constNumber)};
        CHECK(m_iter == savedIt);
        if (constNumberStatus != Status::NOT_A_CONST_NUMBER)
            return {constNumberStatus, nullptr};

        // ConstObj (zero, one, ones) ecc.
        OpCode opCode;
        if (!read(opCode))
            return {Status::DATA_OBJECT_OP_CODE_END_OF_STREAM, nullptr};

        switch (opCode) {
            case OpCode::ZeroOp: return {Status::OK, std::make_unique<AMLObject>(AMLObject::Type::CONST_ZERO)};
            case OpCode::OneOp: return {Status::OK, std::make_unique<AMLObject>(AMLObject::Type::CONST_ONE)};
            case OpCode::OnesOp: return {Status::OK, std::make_unique<AMLObject>(AMLObject::Type::CONST_ONES)};
            case OpCode::StringPrefix:
                return readString();
            case OpCode::BufferOp:
                if (auto buffer = readDefBuffer(); buffer.status() == Status::OK)
                    return {Status::OK, std::move(buffer.contained())};
                else
                    return {buffer.status(), nullptr};
            case OpCode::PackageOp:
                if (auto package = readDefPackage(); package.status() == Status::OK)
                    return {Status::OK, std::move(package.contained())};
                else
                    return {package.status(), nullptr};
            default:
                m_iter = savedIt;

                if (!silentOnFailure) {
#ifdef AML_PARSER_ERRORS
                    klog<LogLevel::ERROR>("AMLParser/DataObject") << "Unexpected opCode " << std::size_t(opCode);
#endif
                }
                return {Status::DATA_OBJECT_OP_CODE_UNKNOWN, nullptr};
        }
    }

    Parser::Status
    Parser::read_def_block_header() {
        [[maybe_unused]] std::uint32_t table_signature;
        READ(table_signature, BLOCK_HEADER_TABLE_SIGNATURE_FIELD_MISSING)
#ifdef AML_PARSER_DEBUG_READ_DEF_BLOCK_HEADER
        klog<LogLevel::DEBUG>("AMLParser/BlockHeader") << "table_signature=" << table_signature;
#endif

        [[maybe_unused]] std::uint32_t table_length;
        READ(table_length, BLOCK_HEADER_TABLE_LENGTH_FIELD_MISSING)
#ifdef AML_PARSER_DEBUG_READ_DEF_BLOCK_HEADER
        klog<LogLevel::DEBUG>("AMLParser/BlockHeader") << "table_length=" << table_length;
#endif

        [[maybe_unused]] std::uint8_t spec_compliance;
        READ(spec_compliance, BLOCK_HEADER_TABLE_SPEC_COMPLIANCE_FIELD_MISSING)
#ifdef AML_PARSER_DEBUG_READ_DEF_BLOCK_HEADER
        klog<LogLevel::DEBUG>("AMLParser/BlockHeader") << "spec_compliance=" << spec_compliance;
#endif

        [[maybe_unused]] std::uint8_t checksum;
        READ(checksum, BLOCK_HEADER_TABLE_CHECKSUM_FIELD_MISSING)
#ifdef AML_PARSER_DEBUG_READ_DEF_BLOCK_HEADER
        klog<LogLevel::DEBUG>("AMLParser/BlockHeader") << "checksum=" << checksum;
#endif

        [[maybe_unused]] std::array<char, 6> oem_id{};
        READ_VAR_STRING(oem_id, BLOCK_HEADER_TABLE_OEMID_MISSING)
#ifdef AML_PARSER_DEBUG_READ_DEF_BLOCK_HEADER
        klog<LogLevel::DEBUG>("AMLParser/BlockHeader") << "oem_id=" << std::string_view{oem_id.data(), 6};
#endif

        [[maybe_unused]] std::array<char, 8> oem_table_id{};
        READ_VAR_STRING(oem_table_id, BLOCK_HEADER_TABLE_OEM_TABLE_ID_MISSING)
#ifdef AML_PARSER_DEBUG_READ_DEF_BLOCK_HEADER
        klog<LogLevel::DEBUG>("AMLParser/BlockHeader") << "oem_table_id=" << std::string_view{oem_table_id.data(), 8};
#endif

        [[maybe_unused]] std::uint32_t oem_revision;
        READ(oem_revision, BLOCK_HEADER_TABLE_OEM_REVISION_FIELD_MISSING)
#ifdef AML_PARSER_DEBUG_READ_DEF_BLOCK_HEADER
        klog<LogLevel::DEBUG>("AMLParser/BlockHeader") << "oem_revision=" << oem_revision;
#endif

        [[maybe_unused]] std::uint32_t creator_id;
        READ(creator_id, BLOCK_HEADER_TABLE_CREATOR_ID_FIELD_MISSING)
#ifdef AML_PARSER_DEBUG_READ_DEF_BLOCK_HEADER
        klog<LogLevel::DEBUG>("AMLParser/BlockHeader") << "creator_id=" << creator_id << " or " << std::string_view{reinterpret_cast<const char *>(&creator_id), 4};
#endif

        [[maybe_unused]] std::uint32_t creator_revision;
        READ(creator_revision, BLOCK_HEADER_TABLE_CREATOR_REVISION_FIELD_MISSING)
#ifdef AML_PARSER_DEBUG_READ_DEF_BLOCK_HEADER
        klog<LogLevel::DEBUG>("AMLParser/BlockHeader") << "creator_revision=" << creator_revision;
#endif

        return Status::OK;
    }

    Parser::StatusAnd<std::unique_ptr<BufferData>>
    Parser::readDefBuffer() {
        const auto start = m_iter;
        auto pkgLength = read_package_length();
        if (pkgLength == k_invalid_package_length)
            return {Status::DEF_BUFFER_INVALID_PACKAGE_LENGTH, nullptr};
#ifdef AML_PARSER_DEBUG_READ_DEF_BUFFER
        klog<LogLevel::DEBUG>("AMLParser/DefBuffer") << "PkgLength: " << pkgLength;
#endif

        auto bufferSize = read_term_arg();
        if (bufferSize.status() != Status::OK)
            return {bufferSize.status(), nullptr};
        ASSERT(bufferSize.contained());

#ifdef AML_PARSER_DEBUG_READ_DEF_BUFFER
        klog<LogLevel::DEBUG>("AMLParser/DefBuffer") << "BufferSize: .type=" << toString(bufferSize.contained()->objectType());
#endif
//
//        if (bufferSize.contained().type() == TermArg::Type::DATA_OBJECT) {
//            klog<LogLevel::DEBUG>("AMLParser/DefBuffer/BufferSize") << "DataObject.type=" << std::size_t(
//                    bufferSize.contained().dataObject().type())
//                    << " number = " << bufferSize.contained().dataObject().asNumber();
//        }

        const auto calculatedBufferSize = pkgLength - (m_iter.base() - start.base());
#ifdef AML_PARSER_DEBUG_READ_DEF_BUFFER
        klog<LogLevel::DEBUG>("AMLParser/DefBuffer") << "calculatedBufferSize: " << calculatedBufferSize;
#endif

        if (bufferSize.contained()->objectType() == AMLObject::Type::CONST_NUMBER) {
            const auto bufferSizeAsNumber = static_cast<const ConstNumber *>(bufferSize.contained().get())->value();
            if (bufferSizeAsNumber > pkgLength)
                return {Status::DEF_BUFFER_BUFFER_SIZE_GREATER_THAN_PACKAGE_LENGTH, nullptr};
            if (bufferSizeAsNumber != calculatedBufferSize)
                return {Status::DEF_BUFFER_BUFFER_SIZE_DOES_NOT_ADD_UP, nullptr};
        }

        std::vector<char> data;
        data.reserve(calculatedBufferSize);
        // TODO use resize() here when it is available.
        for (std::size_t i = 0; i < calculatedBufferSize; ++i) {
            if (!read(data[i])) {
#ifdef AML_PARSER_ERRORS
                klog<LogLevel::ERROR>("AMLParser/DefBuffer") << "Failed to read data byte #" << i;
#endif
                return {Status::DEF_BUFFER_FAILED_TO_READ_DATA, nullptr};
            }
        }

        return {Status::OK, std::make_unique<BufferData>(std::move(bufferSize.contained()), std::move(data))};
    }

    // DefDevice := DeviceOp PkgLength NameString TermList
    // https://uefi.org/specs/ACPI/6.4/20_AML_Specification/AML_Specification.html#defdevice
    Parser::StatusAnd<std::unique_ptr<AMLObject>>
    Parser::readDefDevice() {
#ifdef AML_PARSER_DEBUG_READ_DEF_DEVICE
        klog<LogLevel::DEBUG>("AMLParser/DefDevice") << "Begin";
        NEXT();
#endif

        const auto start = m_iter.base();
        const auto pkgLength = read_package_length();
        if (pkgLength == k_invalid_package_length)
            return {Parser::Status::DEF_DEVICE_ERROR_WITH_PACKAGE_LENGTH, nullptr};

        auto nameString = read_name_string();
        if (nameString.status() != Status::OK) {
            return {nameString.status(), nullptr};
        }

#ifdef AML_PARSER_DEBUG_READ_DEF_DEVICE
        klog<LogLevel::DEBUG>("AMLParser/DefDevice") << "Opened \"" << nameString.contained().c_str() << "\"";
        NEXT();
#endif

//        auto device = std::make_unique<Device>(std::move(nameString.contained()));
//        auto &deviceRef = *device;
//        m_currentScope->children().push_back(std::move(device));
//        ScopedScope scopedScope{this, &deviceRef};
//        if (auto status = read_term_list(pkgLength - PkgLength(m_iter.base() - start), deviceRef); status != Status::OK)
//            return {status, nullptr};
//
//        CHECK(!m_currentScope->children().empty());
//        CHECK(m_currentScope->children().back().get() == &deviceRef);
//        return {Status::OK, m_currentScope->children().take_back()};
        return readTermListAndReturn(std::make_unique<Device>(std::move(nameString.contained())), pkgLength - PkgLength(m_iter.base() - start));
    }

    // Assumes that the FieldOp is already read
    //
    // DefField := FieldOp PkgLength NameString FieldFlags FieldList
    // https://uefi.org/specs/ACPI/6.4/20_AML_Specification/AML_Specification.html#deffield
    Parser::StatusAnd<std::unique_ptr<AMLObject>>
    Parser::read_def_field() {
        const auto begin = m_iter.base();
        const auto pkg_length = read_package_length();
        if (pkg_length == k_invalid_package_length)
            return {Status::DEF_FIELD_ERROR_WITH_PACKAGE_LENGTH, nullptr};

        auto name_string = read_name_string();
        if (name_string.status() != Status::OK)
            return {name_string.status(), nullptr};

#ifdef AML_PARSER_DEBUG_READ_DEF_FIELD
        klog<LogLevel::DEBUG>("AMLParser/DefField") << "Name: " << name_string.contained().c_str();
#endif

        FieldFlags field_flags{};
        if (!read(field_flags))
            return {Status::DEF_FIELD_EOS_WHEN_READING_FIELD_FLAGS, nullptr};

        auto field = std::make_unique<Field>(std::move(name_string.contained()), field_flags);
        auto &fieldRef = *field;
        m_currentScope->children().push_back(std::move(field));

        // The PkgLength is inclusive from the start, so for `read_field_list`
        // to know how many bytes it can ready, we need to subtract the amount
        // of bytes we already read.
        const auto already_read = std::uint32_t(m_iter.base() - begin);
        {
            ScopedScope scopedScope{this, &fieldRef};
            if (auto status = read_field_list(pkg_length - already_read, fieldRef); status != Status::OK)
                return {status, nullptr};
        }

        CHECK(!m_currentScope->children().empty());
        CHECK_EQ(std::size_t(m_currentScope->children().back().get()), std::size_t(&fieldRef));
#ifdef AML_PARSER_DEBUG_READ_DEF_FIELD
        klog<LogLevel::DEBUG>("AMLParser/DefField") << "End: " << name_string.contained().c_str();
#endif
        return {Status::OK, m_currentScope->children().take_back()};
    }

    // Assumes that the MethodOp is already read
    //
    // DefMethod := MethodOp PkgLength NameString MethodFlags TermList
    // DefMethod := MethodOp PkgLength NameString MethodFlags TermList
    Parser::StatusAnd<std::unique_ptr<Method>>
    Parser::read_def_method() {
#ifdef AML_PARSER_DEBUG_READ_DEF_METHOD
        klog<LogLevel::DEBUG>("AMLParser/DefMethod") << "Begin";
        NEXT();
#endif

        const auto start = m_iter.base();
        auto pkg_length = read_package_length();
        if (pkg_length == k_invalid_package_length)
            return {Status::DEF_METHOD_ERROR_WITH_PACKAGE_LENGTH, nullptr};

        auto name = read_name_string();
        if (name.status() != Status::OK)
            return {name.status(), nullptr};

#ifdef AML_PARSER_DEBUG_READ_DEF_METHOD
        klog<LogLevel::DEBUG>("AMLParser/DefMethod") << "Name: " << std::string_view{name.contained().c_str()};
#endif

        MethodFlags flags{};
        if (!read(flags))
            return {Status::DEF_METHOD_EOS_READING_METHOD_FLAGS, nullptr};

        return readTermListAndReturn<Method>(std::make_unique<Method>(std::move(name.contained()), flags),
                                             pkg_length - PkgLength(m_iter.base() - start));

//        m_callbacks.open_method(pkg_length, std::string(name.contained().data()), flags);
//
//        klog<LogLevel::DEBUG>("AMLParser/DefMethod") << "TermList";
//        NEXT();
//
//        auto method = std::make_unique<Method>(std::move(name.contained()), flags);
//        ScopedScope scopedScope{this, method.get()};
//        if (auto status = read_term_list(pkg_length - PkgLength(m_iter.base() - start), *method); status != Status::OK)
//            return {status, nullptr};
//
//        return {Status::OK, std::move(method)};
    }

    // DefMutex := MutexOp NameString SyncFlags
    // https://uefi.org/specs/ACPI/6.4/20_AML_Specification/AML_Specification.html#defmutex
    Parser::StatusAnd<std::unique_ptr<MutexDefinition>>
    Parser::readDefMutex() {
        auto name = read_name_string();
        if (name.status() != Status::OK)
            return {name.status(), nullptr};

        MutexDefinition::SyncFlags flags;
        if (!read(flags))
            return {Status::DEF_MUTEX_FAILED_TO_READ_SYNC_FLAGS, nullptr};

        return {Status::OK, std::make_unique<MutexDefinition>(std::move(name.contained()), flags)};
    }

    // Assumes that the OpRegionOp is already read
    //
    // DefOpRegion := OpRegionOp NameString RegionSpace RegionOffset RegionLen
    // https://uefi.org/specs/ACPI/6.4/20_AML_Specification/AML_Specification.html#defopregion
    Parser::StatusAnd<std::unique_ptr<AMLObject>>
    Parser::read_def_op_region() {
        auto name_string = read_name_string();
        if (name_string.status() != Status::OK)
            return {name_string.status(), nullptr};

#ifdef AML_PARSER_DEBUG_READ_DEF_OP_REGION
        klog<LogLevel::DEBUG>("AMLParser") << "DefOpRegion name=\"" << std::string_view{name_string.contained().data(),
                                                                                        name_string.contained().size()} << "\"";
#endif

        RegionSpace region_space;
        if (!read(region_space))
            return {Status::DEF_OP_REGION_FAILED_TO_READ_REGION_SPACE, nullptr};

        Status status;
        std::uint64_t region_offset;
        if (status = read_const_number(region_offset); status != Status::OK)
            return {status, nullptr};
        std::uint64_t region_length;
        if (status = read_const_number(region_length); status != Status::OK)
            return {status, nullptr};

        return {Status::OK, std::make_unique<OperationRegion>(std::move(name_string.contained()), region_space, region_offset, region_length)};
    }

    // DefPackage := PackageOp PkgLength NumElements PackageElementList
    // https://uefi.org/specs/ACPI/6.4/20_AML_Specification/AML_Specification.html#defpackage
    Parser::StatusAnd<std::unique_ptr<Package>>
    Parser::readDefPackage() {
        const auto start = m_iter.base();

        auto pkgLength = read_package_length();
        if (pkgLength == k_invalid_package_length)
            return {Status::DEF_PACKAGE_INVALID_PACKAGE_LENGTH, nullptr};

        ScopedPkgLength scopedPkgLength{this, start, pkgLength, "Package"};
#ifdef AML_PARSER_DEBUG_READ_DEF_PACKAGE
        klog<LogLevel::DEBUG>("AMLParser/Package") << "PkgLength #" << TextModeBufferViewWriter::Decimal << m_pkgLengths.size()
                                                   << " > " << start << ", " << pkgLength;
#endif

        // NumElements := ByteData
        // https://uefi.org/specs/ACPI/6.4/20_AML_Specification/AML_Specification.html#numelements
        std::uint8_t numElements;
        if (!read(numElements))
            return {Status::DEF_PACKAGE_FAILED_TO_READ_NUM_ELEMENTS, nullptr};

#ifdef AML_PARSER_DEBUG_READ_DEF_PACKAGE
        klog<LogLevel::DEBUG>("AMLParser/Package") << "There should be " << TextModeBufferViewWriter::Decimal << numElements << " element(s)";
#endif

        // PackageElementList := Nothing | <packageelement packageelementlist>
        // https://uefi.org/specs/ACPI/6.4/20_AML_Specification/AML_Specification.html#packageelementlist
        std::vector<std::unique_ptr<AMLObject>> elements;
        elements.reserve(numElements);

        // PackageElement := DataRefObject | NameString
        // https://uefi.org/specs/ACPI/6.4/20_AML_Specification/AML_Specification.html#packageelement
        for (std::size_t i = 0; i < numElements; ++i) {
            std::uint8_t leadByte;
            if (!peek(leadByte)) {
                // TODO
                // https://uefi.org/specs/ACPI/6.4/19_ASL_Reference/ACPI_Source_Language_Reference.html#package-declare-package-object
                // If NumElements is present and greater than the number of
                // elements in the PackageList, the default entry of type
                // Uninitialized (see ObjectType) is used to initialize the
                // package elements beyond those initialized from the
                // PackageList.
                for (; i < numElements; ++i) {
                    elements.push_back(nullptr);
                }
                break;
            }
#ifdef AML_PARSER_DEBUG_READ_DEF_PACKAGE
            klog<LogLevel::DEBUG>("PackageElement") << "leadbyte: " << std::size_t(leadByte);
            NEXT();
#endif

            const auto savedIterAsRestorePoint = m_iter;
            auto dataObject = readDataObject(false);
            if (dataObject.status() == Status::OK) {
#ifdef AML_PARSER_DEBUG_READ_DEF_PACKAGE
                auto stream = klog<LogLevel::DEBUG>("AMLParser/PackageElement");
                stream << "#" << TextModeBufferViewWriter::Decimal << i << " data object: "
                        << toString(dataObject.contained()->objectType());
                if (dataObject.contained()->objectType() == AMLObject::Type::CONST_NUMBER)
                    stream << " value=" << static_cast<ConstNumber *>(dataObject.contained().get())->value();
#endif
                elements.push_back(std::move(dataObject.contained()));
                continue;
            }

            auto nameString = read_name_string();
            if (nameString.status() == Status::OK) {
#ifdef AML_PARSER_DEBUG_READ_DEF_PACKAGE
                klog<LogLevel::DEBUG>("AMLParser/PackageElement") << "#" << TextModeBufferViewWriter::Decimal << i << " string: " << std::string_view{nameString.contained().c_str()};
#endif
                elements.push_back(std::make_unique<NameString>(std::move(nameString.contained())));
                continue;
            }
            m_iter = savedIterAsRestorePoint;

            m_iter = core::RegionPointer<const unsigned char>::ConstIterator(m_data, start);
#ifdef AML_PARSER_ERRORS
            klog<LogLevel::ERROR>("AMLParser/DefPackage") << "Failed to read element! NameString Status="
                    << to_string(nameString.status()) << " DataObject Status=" << to_string(dataObject.status());
#endif
            return {Status::DEF_PACKAGE_FAILED_TO_READ_ELEMENT, nullptr};
        }

        return {Status::OK, std::make_unique<Package>(std::move(elements))};
    }

    // DefProcessor := ProcessorOp PkgLength NameString ProcID PblkAddr PblkLen TermList
    // ProcessorOp := ExtOpPrefix 0x83
    // ProcID := ByteData
    // PblkAddr := DWordData
    // PblkLen := ByteData
    Parser::StatusAnd<std::unique_ptr<AMLObject>>
    Parser::readDefProcessor() {
        const auto start = m_iter.base();
        const auto pkgLength = read_package_length();
        if (pkgLength == k_invalid_package_length)
            return {Parser::Status::DEF_PROCESSOR_INVALID_PACKAGE_LENGTH, nullptr};

        auto name = read_name_string();
        if (name.status() != Status::OK)
            return {name.status(), nullptr};

        std::uint8_t procID;
        if (!read(procID))
            return {Status::DEF_PROCESSOR_FAILED_TO_READ_PROC_ID, nullptr};

        std::uint32_t pblkAddr;
        if (!read(pblkAddr))
            return {Status::DEF_PROCESSOR_FAILED_TO_READ_PROC_ID, nullptr};

        std::uint8_t pblkLen;
        if (!read(pblkLen))
            return {Status::DEF_PROCESSOR_FAILED_TO_READ_PROC_ID, nullptr};

//        auto processorDefinition = std::make_unique<ProcessorDefinition>(std::move(name.contained()), procID, pblkAddr, pblkLen);
//        ScopedScope scopedScope{this, processorDefinition.get()};
//        if (auto status = read_term_list(pkgLength - PkgLength(m_iter.base() - start), *processorDefinition);
//                status != Status::OK)
//            return {status, nullptr};
//
//        return {Status::OK, std::move(processorDefinition)};
        return readTermListAndReturn(std::make_unique<ProcessorDefinition>(std::move(name.contained()), procID, pblkAddr, pblkLen),
                                     pkgLength - PkgLength(m_iter.base() - start));
    }

    // https://uefi.org/specs/ACPI/6.4/20_AML_Specification/AML_Specification.html#defscope
    Parser::StatusAnd<std::unique_ptr<AMLObject>>
    Parser::read_def_scope() {
        const auto start = m_iter.base();
        const auto pkg_length = read_package_length();
        if (pkg_length & 0xC0000000)
            return {Status::DEF_SCOPE_FAILED_TO_READ_PKG_LENGTH, nullptr};

#ifdef AML_PARSER_DEBUG_READ_DEF_SCOPE
        klog<LogLevel::DEBUG>("AMLParser") << "PkgLength=" << TextModeBufferViewWriter::Decimal << pkg_length;
#endif

        auto name_with_status = read_name_string();
        if (name_with_status.status() != Status::OK)
            return {name_with_status.status(), nullptr};

#ifdef AML_PARSER_DEBUG_READ_DEF_SCOPE
        klog<LogLevel::DEBUG>("shah") << "Begin=" << std::uint64_t(name_with_status.contained().begin())
                << " End=" << std::uint64_t(name_with_status.contained().end())
                << " Size=" << TextModeBufferViewWriter::Decimal << name_with_status.contained().size();
#endif
//        auto scope = std::make_unique<Scope>(std::move(name_with_status.contained()));
//
//        ScopedScope scopedScope{this, scope.get()};
//        if (auto status = read_term_list(pkg_length - PkgLength(m_iter.base() - start), *scope); status != Status::OK)
//            return {status, nullptr};
//
//        return {Status::OK, std::move(scope)};

        return readTermListAndReturn(std::make_unique<Scope>(std::move(name_with_status.contained())), pkg_length - PkgLength(m_iter.base() - start));
    }

    // ExpressionOpCode := ...
    // https://uefi.org/specs/ACPI/6.4/20_AML_Specification/AML_Specification.html#expressionopcode
    Parser::StatusAnd<std::unique_ptr<Expression>>
    Parser::readExpression([[maybe_unused]] bool silentOnFailure) {
#ifdef AML_PARSER_DEBUG_READ_EXPRESSION
        klog<LogLevel::DEBUG>("AMLParser/Expression") << "readExpression() has the following bytes to use:";
        NEXT();
#endif
        OpCode opCode;
        if (!read(opCode))
            return {Status::EXPRESSION_OP_CODE_END_OF_STREAM, nullptr};

        OpCode next{};
        static_cast<void>(peek(next));

        switch (opCode) {

            case OpCode::BufferOp:
                if (auto buffer = readDefBuffer(); buffer.status() == Status::OK)
                    return {Status::OK, std::make_unique<BufferExpression>(std::move(buffer.contained()))};
                else
                    return {buffer.status(), nullptr};
            case OpCode::ExtOpPrefix:
                return readExpressionWithDefOpPrefix();
            case OpCode::AddOp:
                return read_operand_operand_target_operation<AddExpression>();
            case OpCode::SubtractOp:
                return read_operand_operand_target_operation<SubtractExpression>();
            case OpCode::IncrementOp: {
                auto superName = read_super_name();
                if (superName.status() != Status::OK)
                    return {superName.status(), nullptr};
                return {Status::OK, std::make_unique<IncrementExpression>(std::move(superName.contained()))};
            }
            case OpCode::ShiftLeftOp:
            case OpCode::ShiftRightOp: {
                auto operand = read_term_arg();
                if (operand.status() != Status::OK)
                    return {operand.status(), nullptr};

                auto shiftCount = read_term_arg();
                if (shiftCount.status() != Status::OK)
                    return {shiftCount.status(), nullptr};

                auto target = read_target();
                if (target.status() != Status::OK)
                    return {target.status(), nullptr};

                if (opCode == OpCode::ShiftLeftOp) {
                    return {Status::OK, std::make_unique<ShiftExpression<Expression::Type::SHIFT_LEFT>>(
                            std::move(operand.contained()), std::move(shiftCount.contained()),
                            std::move(target.contained()))};
                }
                return {Status::OK, std::make_unique<ShiftExpression<Expression::Type::SHIFT_RIGHT>>(
                        std::move(operand.contained()), std::move(shiftCount.contained()),
                        std::move(target.contained()))};
            }
            case OpCode::AndOp:
                return read_operand_operand_target_operation<AndExpression>();
            case OpCode::OrOp:
                return read_operand_operand_target_operation<OrExpression>();
            case OpCode::DerefOfOp:
                return readExpressionDerefOf();
            case OpCode::StoreOp: {
#ifdef AML_PARSER_DEBUG_READ_EXPRESSION
                static std::size_t count{0};
                const auto id = count++;
                klog<LogLevel::DEBUG>("AMLParser/StoreOp") << "TermArg #" << id;
                NEXT();
#endif
                auto termArg = read_term_arg();
                if (termArg.status() != Status::OK)
                    return {termArg.status(), nullptr};
#ifdef AML_PARSER_DEBUG_READ_EXPRESSION
                klog<LogLevel::DEBUG>("AMLParser/StoreOp") << "SuperName #" << id;
                NEXT();
#endif

                auto superName = read_super_name();
                if (superName.status() != Status::OK)
                    return {superName.status(), nullptr};
#ifdef AML_PARSER_DEBUG_READ_EXPRESSION
                klog<LogLevel::DEBUG>("AMLParser/StoreOp") << "Done #" << id;
#endif
                return {Status::OK, std::make_unique<StoreExpression>(std::move(termArg.contained()),
                                                                      std::move(superName.contained()))};
            }
            case OpCode::NotifyOp: {
                auto superName = read_super_name();
                if (superName.status() != Status::OK)
                    return {superName.status(), nullptr};

                auto termArg = read_term_arg();
                if (termArg.status() != Status::OK)
                    return {termArg.status(), nullptr};
                return {Status::OK, std::make_unique<NotifyExpression>(std::move(termArg.contained()), std::move(superName.contained()))};
            }
            case OpCode::SizeofOp: {
#ifdef AML_PARSER_DEBUG_READ_EXPRESSION
                klog<LogLevel::DEBUG>("AMLParser/Expression") << "sizeof() expression";
#endif
                auto result = read_super_name();
                if (result.status() != Status::OK) {
#ifdef AML_PARSER_DEBUG_READ_EXPRESSION
                    if (!silentOnFailure)
                        klog<LogLevel::ERROR>("AMLParser/Expression") << "sizeof: Failed to read super name";
#endif
                    return {result.status(), nullptr};
                }
#ifdef AML_PARSER_DEBUG_READ_EXPRESSION
                klog<LogLevel::DEBUG>("AMLParser/Expression") << "  > SuperName's Type: " << toString(result.contained()->objectType());
#endif
                return {Status::OK, std::make_unique<SizeofExpression>(std::move(result.contained()))};
            }
            case OpCode::IndexOp:
                return readExpressionIndex();
            case OpCode::LandOp: {
                auto result = read_operand_operand_operation<OperandOperandOperationType::LOGICAL_AND>();
                if (result.status() != Status::OK)
                    return {result.status(), nullptr};
                return {result.status(), std::make_unique<LogicalAndExpression>(std::move(result.contained().first),
                                                                                std::move(result.contained().second))};
            }
            case OpCode::LorOp: {
                auto result = read_operand_operand_operation<OperandOperandOperationType::LOGICAL_OR>();
                if (result.status() != Status::OK)
                    return {result.status(), nullptr};
                return {result.status(), std::make_unique<LogicalOrExpression>(std::move(result.contained().first),
                                                                               std::move(result.contained().second))};
            }
            case OpCode::LnotOp: {
                if (next == OpCode::LgreaterOp) {
                    ++m_iter;
                    auto result = read_operand_operand_operation<OperandOperandOperationType::LOGICAL_LESS_EQUAL>();
                    if (result.status() != Status::OK)
                        return {result.status(), nullptr};
                    return {result.status(), std::make_unique<LessEqualExpression>(std::move(result.contained().first),
                                                                                   std::move(result.contained().second))};
                }
                if (next == OpCode::LlessOp) {
                    ++m_iter;
                    auto result = read_operand_operand_operation<OperandOperandOperationType::LOGICAL_GREATER_EQUAL>();
                    if (result.status() != Status::OK)
                        return {result.status(), nullptr};
                    return {result.status(), std::make_unique<GreaterEqualExpression>(std::move(result.contained().first),
                                                                                      std::move(result.contained().second))};
                }
                if (next == OpCode::LequalOp) {
                    ++m_iter;
                    auto result = read_operand_operand_operation<OperandOperandOperationType::LOGICAL_NOT_EQUAL>();
                    if (result.status() != Status::OK)
                        return {result.status(), nullptr};
                    return {result.status(), std::make_unique<NotEqualsExpression>(std::move(result.contained().first),
                                                                                   std::move(result.contained().second))};
                }
                auto operand = read_term_arg();
                if (operand.status() != Status::OK)
                    return {operand.status(), nullptr};
                return {Status::OK, std::make_unique<NotUniExpression>(std::move(operand.contained()))};
            }
            case OpCode::LequalOp: {
                auto result = read_operand_operand_operation<OperandOperandOperationType::LOGICAL_EQUAL>();
                if (result.status() != Status::OK)
                    return {result.status(), nullptr};
                return {result.status(), std::make_unique<EqualsExpression>(std::move(result.contained().first),
                                                                            std::move(result.contained().second))};
            }
            case OpCode::LgreaterOp: {
                auto result = read_operand_operand_operation<OperandOperandOperationType::LOGICAL_GREATER>();
                if (result.status() != Status::OK)
                    return {result.status(), nullptr};
                return {result.status(), std::make_unique<GreaterThanExpression>(std::move(result.contained().first),
                                                                                 std::move(result.contained().second))};
            }
            case OpCode::LlessOp: {
                auto result = read_operand_operand_operation<OperandOperandOperationType::LOGICAL_LESS>();
                if (result.status() != Status::OK)
                    return {result.status(), nullptr};
                return {result.status(), std::make_unique<LessThanExpression>(std::move(result.contained().first),
                                                                              std::move(result.contained().second))};
            }
            case OpCode::ToBufferOp:
                return read_operand_target_operation<ToBufferExpression>();
            case OpCode::ToHexStringOp:
                return read_operand_target_operation<ToHexStringExpression>();
            default:
                break;
        }

        --m_iter;

        auto savedIter = m_iter;
        if (auto result = readMethodInvocationExpression(); result.status() == Status::OK)
            return result;
        m_iter = savedIter;

        return {Status::EXPRESSION_OP_CODE_UNKNOWN, nullptr};
    }

    Parser::StatusAnd<std::unique_ptr<Expression>>
    Parser::readExpressionDerefOf() {
#ifdef AML_PARSER_DEBUG_READ_EXPRESSION_DEREF_OF
        klog<LogLevel::DEBUG>("AMLParser/DerefOp") << "TermArg ->\\";
#endif

        auto termArg = read_term_arg();
        if (termArg.status() != Status::OK)
            return {termArg.status(), nullptr};

        return {Status::OK, std::make_unique<DereferenceExpression>(std::move(termArg.contained()))};
    }

    Parser::StatusAnd<std::unique_ptr<Expression>>
    Parser::readExpressionIndex() {
        auto buffPkgStrObj = read_term_arg();
        if (buffPkgStrObj.status() != Status::OK) {
#ifdef AML_PARSER_DEBUG_READ_EXPRESSION_INDEX
            klog<LogLevel::ERROR>("AMLParser/Expression") << "index: Failed to read BuffPkgStrObj";
#endif
            return {buffPkgStrObj.status(), nullptr};
        }
        auto indexValue = read_term_arg();
        if (indexValue.status() != Status::OK) {
#ifdef AML_PARSER_DEBUG_READ_EXPRESSION_INDEX
            klog<LogLevel::ERROR>("AMLParser/Expression") << "index: Failed to read IndexValue";
#endif
            return {buffPkgStrObj.status(), nullptr};
        }
        auto target = read_target();
        if (target.status() != Status::OK) {
#ifdef AML_PARSER_DEBUG_READ_EXPRESSION_INDEX
            klog<LogLevel::ERROR>("AMLParser/Expression") << "index: Failed to read Target";
#endif
            return {buffPkgStrObj.status(), nullptr};
        }

        return {Status::OK, std::make_unique<IndexExpression>(std::move(buffPkgStrObj.contained()),
                                                              std::move(indexValue.contained()), std::move(target.contained()))};
    }

    Parser::StatusAnd<std::unique_ptr<Expression>>
    Parser::readExpressionWithDefOpPrefix() {
        ExtendedOpCode extendedOpCode;
        if (!read(extendedOpCode))
            return {Status::EXPRESSION_EXTENDED_OP_CODE_END_OF_STREAM, nullptr};

        switch (extendedOpCode) {
            case ExtendedOpCode::AcquireOp: {
                auto mutexObject = read_super_name();
                if (mutexObject.status() != Status::OK)
                    return {mutexObject.status(), nullptr};

                std::uint16_t timeout;
                if (!read(timeout))
                    return {Status::DEF_ACQUIRE_FAILED_TO_READ_TIMEOUT, nullptr};
                klog<LogLevel::DEBUG>("AML") << "ACQUIRE@tggasga @ " << std::size_t(mutexObject.contained().get());
                return {Status::OK, std::make_unique<AcquireExpression>(std::move(mutexObject.contained()), timeout)};
            }
            case ExtendedOpCode::ReleaseOp:
                if (auto mutexObject = read_super_name(); mutexObject.status() == Status::OK)
                    return {Status::OK, std::make_unique<ReleaseExpression>(std::move(mutexObject.contained()))};
                else
                    return {mutexObject.status(), nullptr};
            default:
                --m_iter;
#ifdef AML_PARSER_ERRORS
                klog<LogLevel::ERROR>("AMLParser/Expression/ExtOpPrefix") << "Unknown extended op code: " << static_cast<std::uint8_t>(extendedOpCode);
#endif
                return {Status::EXPRESSION_OP_CODE_UNKNOWN, nullptr};
        }
    }

    // FieldElement := NamedField | ReservedField | AccessField | ExtendedAccessField | ConnectField
    // https://uefi.org/specs/ACPI/6.4/20_AML_Specification/AML_Specification.html#fieldelement
    Parser::StatusAnd<std::unique_ptr<AMLObject>>
    Parser::read_field_element() {
        std::uint8_t peeked_first_byte;
        if (!peek(peeked_first_byte))
            return {Status::FIELD_ELEMENT_END_OF_STREAM, nullptr};

#ifdef AML_PARSER_DEBUG_READ_FIELD_ELEMENT
        klog<LogLevel::DEBUG>("FieldElement") << "peeked_first_byte=" << peeked_first_byte;
#endif
        switch (peeked_first_byte) {
            // ReservedField := 0x00 PkgLength
            // https://uefi.org/specs/ACPI/6.4/20_AML_Specification/AML_Specification.html#reservedfield
            case 0x00: {
                ++m_iter;
                const auto pkg_length = read_package_length();
                if (pkg_length == k_invalid_package_length)
                    return {Status::FIELD_ELEMENT_RESERVED_FIELD_PKG_LENGTH_ERROR, nullptr};
                return {Status::OK, std::make_unique<ReservedFieldElement>(pkg_length)};
            }
            // AccessField := 0x01 AccessType AccessAttrib
            // https://uefi.org/specs/ACPI/6.4/20_AML_Specification/AML_Specification.html#accessfield
            case 0x01: {
                ++m_iter;
                StandaloneAccessType access_type{};
                if (!read(access_type))
                    return {Status::FIELD_ELEMENT_ACCESS_FIELD_ACCESS_TYPE_READ_ERROR, nullptr};

                AccessAttrib access_attrib{};
                if (!read(access_attrib))
                    return {Status::FIELD_ELEMENT_ACCESS_FIELD_ACCESS_ATTRIB_READ_ERROR, nullptr};

                return {Status::OK, std::make_unique<AccessFieldElement>(access_type, access_attrib)};
            }
            // ConnectField: <0x02 NameString> | <0x02 BufferData>
            // https://uefi.org/specs/ACPI/6.4/20_AML_Specification/AML_Specification.html#connectfield
            case 0x02: {
                ++m_iter;
                if (auto nameString = read_name_string(); nameString.status() == Status::OK)
                    return {Status::OK, std::make_unique<ConnectFieldElementWithString>(std::move(nameString.contained()))};

                if (auto defBuffer = readDefBuffer(); defBuffer.status() == Status::OK)
                    return {Status::OK, std::make_unique<ConnectFieldElementWithBuffer>(std::move(defBuffer.contained()))};
                else // TODO is this correctly handled?
                    return {defBuffer.status(), nullptr};
            }
            // ExtendedAccessField := 0x03 AccessType ExtendedAccessAttrib AccessLength
            // https://uefi.org/specs/ACPI/6.4/20_AML_Specification/AML_Specification.html#extendedaccessfield
            case 0x03: {
                ++m_iter;
                CHECK_NOT_REACHED();
            }
            // NamedField := NameSeg PkgLength
            // https://uefi.org/specs/ACPI/6.4/20_AML_Specification/AML_Specification.html#namedfield
            default: {
                std::string s{std::size_t(4), '?'};
                if (auto status = read_name_seg(core::View<char, 4>(s.data())); status != Status::OK)
                    return {status, nullptr};

                const auto pkg_length = read_package_length();
                if (pkg_length == k_invalid_package_length)
                    return {Status::FIELD_ELEMENT_NAMED_FIELD_PKG_LENGTH_ERROR, nullptr};

#ifdef AML_PARSER_DEBUG_READ_FIELD_ELEMENT
                klog<LogLevel::DEBUG>("AMLParser/NamedField") << "\"" << std::string_view{s.c_str(), s.length()} << "\"";
#endif
                STACK_SCOPE();
                return {Status::OK, std::make_unique<NamedFieldElement>(std::move(s), pkg_length)};
            }
        }
    }

    // FieldList := Nothing | <fieldelement fieldlist>
    // https://uefi.org/specs/ACPI/6.4/20_AML_Specification/AML_Specification.html#fieldlist
    Parser::Status
    Parser::read_field_list(const PkgLength pkg_length, Field &parent) {
        if (m_iter.base() + pkg_length > m_data.size())
            return Status::FIELD_LIST_PKG_LENGTH_TOO_LARGE;

        const auto end_field_list = m_iter + pkg_length;
        while (m_iter < end_field_list) {
            const auto begin = m_iter;
            auto element = read_field_element();
            if (element.status() != Status::OK)
                return element.status();
            parent.children().push_back(std::move(element.contained()));
            if (m_iter == begin)
                return Status::FIELD_LIST_LOOP_DETECTED;
        }

        return Status::OK;
    }

    Parser::StatusAnd<std::unique_ptr<Statement>>
    Parser::readIfElseStatement() {
        auto start = m_iter.base();
        auto pkgLength = read_package_length();
        if (pkgLength == k_invalid_package_length)
            return {Status::IF_ELSE_STATEMENT_WITH_INVALID_PACKAGE_LENGTH, nullptr};

        klog<LogLevel::DEBUG>("AMLParser/IfElseStatement") << "Predicate";
        NEXT();
        auto predicate = read_term_arg();
        if (predicate.status() != Status::OK) {
            klog<LogLevel::ERROR>("AMLParser/IfElseStatement") << "Predicate failed me";
            return {predicate.status(), nullptr};
        }
        ASSERT(predicate.contained().get() != nullptr);

        auto statement = std::make_unique<IfElseStatement>(std::move(predicate.contained()));
        ScopedScope parentScope{this, statement.get()};

#ifdef AML_PARSER_DEBUG_READ_IF_ELSE_STATEMENT
        klog<LogLevel::DEBUG>("AMLParser/IfElseStatement") << "TermList";
#endif
        {
            ScopedScope scopedScope{this, &statement->trueScope()};
            if (auto status = read_term_list(pkgLength - PkgLength(m_iter.base() - start), statement->trueScope());
                    status != Status::OK) {
                klog<LogLevel::ERROR>("AMLParser/IfElseStatement") << "DefIf(not Else) failed me :(";
                return {status, nullptr};
            }
        }

        OpCode nextOp;
        if (!peek(nextOp) || nextOp != OpCode::ElseOp) {
            klog<LogLevel::INFO>("AMLParser/IfElseStatement") << "No DefElse";
            return {Status::OK, std::move(statement)};
        }
        ++m_iter;

        start = m_iter.base();
        pkgLength = read_package_length();
        if (pkgLength == k_invalid_package_length) {
            klog<LogLevel::ERROR>("AMLParser/IfElseStatement") << "DefElse got invalid package length";
            return {Status::ELSE_STATEMENT_WITH_INVALID_PACKAGE_LENGTH, nullptr};
        }

#ifdef AML_PARSER_DEBUG_READ_IF_ELSE_STATEMENT
        klog<LogLevel::DEBUG>("AMLParser/IfElseStatement") << "DefElse";
#endif
        {
            ScopedScope scopedScope{this, &statement->falseScope()};
            if (auto status = read_term_list(pkgLength - PkgLength(m_iter.base() - start),
                                             statement->falseScope()); status != Status::OK) {
                klog<LogLevel::ERROR>("AMLParser/IfElseStatement") << "DefElse failed me :(";
                return {status, nullptr};
            }
        }

        // TODO store these term lists with the statement
        return {Status::OK, std::move(statement)};
    }

    Parser::StatusAnd<std::unique_ptr<Expression>>
    Parser::readMethodInvocationExpression() {
        auto savedIter = m_iter;

#ifdef AML_PARSER_DEBUG_METHOD_INVOCATION_EXPRESSION
        klog<LogLevel::DEBUG>("AMLParser/MethodInvocation") << "Trying";
        NEXT();
#endif

        auto nameStringResult = read_name_string();
        if (nameStringResult.status() != Status::OK)
            return {nameStringResult.status(), nullptr};

#ifdef AML_PARSER_DEBUG_METHOD_INVOCATION_EXPRESSION
        klog<LogLevel::DEBUG>("AMLParser/MethodInvocation") << "Name: " << std::string_view{nameStringResult.contained().data(), nameStringResult.contained().size()};
#endif

        const std::string_view name{nameStringResult.contained().begin(), nameStringResult.contained().end()};
        const auto invocationTarget = findInvocationTarget(name);

        if (invocationTarget.object == nullptr) {
#ifdef AML_PARSER_WARNINGS
            klog<LogLevel::WARNING>("AMLParser/MethodInvocation") << "Assuming: " << name
                    << " is a non-method, so no method arguments";
#endif
#ifdef AML_PARSER_DEBUG_METHOD_INVOCATION_EXPRESSION
            STACK_SCOPE();
            findAllObjectsWithName(name, m_rootObject, [] (const AMLScope &parent, const AMLObject &object) {
                klog<LogLevel::INFO>("AMLParser/MethodInvocation") << "  But we did find object @ " << std::size_t(&object)
                        << " of type " << toString(object.objectType()) << " with this name! The parent is named \""
                        << parent.name().c_str() << "\" and the child \"" << object.asScope()->name().c_str() << "\"";
            });
#endif
        }

        auto argumentCount = invocationTarget.argumentCount;
#ifdef AML_PARSER_DEBUG_METHOD_INVOCATION_EXPRESSION
        klog<LogLevel::DEBUG>("AMLParser/MethodInvocation") << TextModeBufferViewWriter::Decimal << argumentCount << " arguments";
#endif

        std::vector<std::unique_ptr<AMLObject>> arguments{};
        arguments.reserve(argumentCount);

        while (argumentCount > 0) {
            savedIter = m_iter;
#ifdef AML_PARSER_DEBUG_METHOD_INVOCATION_EXPRESSION
            klog<LogLevel::DEBUG>("AMLParser/MethodInvocation") << "BEGIN BEGIN BEGIN BEGIN " << argumentCount << " " << m_iter.base() << " next " << *m_iter;
#endif
            auto termArgResult = read_term_arg();
            if (termArgResult.status() != Status::OK) {
#ifdef AML_PARSER_ERRORS
                klog<LogLevel::ERROR>("AMLParser/MethodInvocation/Arguments") << "Failed to read TermArg";
#endif
                CRASH("MethodInvocation error (see above)");
            }

            CHECK(savedIter != m_iter);

            arguments.push_back(std::move(termArgResult.contained()));
#ifdef AML_PARSER_DEBUG_METHOD_INVOCATION_EXPRESSION
            klog<LogLevel::DEBUG>("AMLParser/MethodInvocation") << "END END END END END END " << argumentCount << " " << m_iter.base();
#endif
            --argumentCount;
        }

        return {Status::OK, std::make_unique<MethodInvocationExpression>(std::move(nameStringResult.contained()),
                                                                         std::move(arguments))};
    }

    // https://uefi.org/specs/ACPI/6.4/20_AML_Specification/AML_Specification.html#nameseg
    Parser::Status
    Parser::read_name_seg(core::View<char, 4> view) {
        if (!read(view.at<0>()) || !read(view.at<1>()) || !read(view.at<2>()) || !read(view.at<3>()))
            return Status::NAME_SEG_END_OF_STREAM;
//        klog<LogLevel::DEBUG>("AMLParser") << "read_name_seg: " << std::uint8_t(view.at<0>())
//                << " " << std::uint8_t(view.at<1>()) << " " << std::uint8_t(view.at<2>()) << " " << std::uint8_t(view.at<3>())
//                << " starting @ " << std::size_t(&view.at<0>()) << " to " << std::size_t(&view.at<3>());

        // https://uefi.org/specs/ACPI/6.4/20_AML_Specification/AML_Specification.html#leadnamechar
        auto is_lead_name_char = [] (char character) -> bool {
            return (character >= 'A' && character <= 'Z') || character == '_';
        };

        // https://uefi.org/specs/ACPI/6.4/20_AML_Specification/AML_Specification.html#digitchar
        auto is_digit_char = [] (char character) -> bool {
            return character >= '0' && character <= '9';
        };

        // https://uefi.org/specs/ACPI/6.4/20_AML_Specification/AML_Specification.html#namechar
        auto is_name_char = [is_lead_name_char, is_digit_char] (char character) -> bool {
            return is_lead_name_char(character) || is_digit_char(character);
        };

        if (!is_lead_name_char(view.at<0>())) {
#ifdef AML_PARSER_DEBUG_READ_NAME_SEG
            klog<LogLevel::DEBUG>("AMLParser/NameSeg") << "NAME_SEG_FIRST_CHAR_NOT_A_LEAD_NAME_CHAR: " << static_cast<std::size_t>(view.at<0>())
                    << " as " << std::string_view{reinterpret_cast<const char *>(&view.at<0>()), 1};
//            CRASH("backtrace");
#endif
            m_iter = m_iter - 4;
#ifdef AML_PARSER_DEBUG_READ_NAME_SEG
            NEXT();
#endif
            return Status::NAME_SEG_FIRST_CHAR_NOT_A_LEAD_NAME_CHAR;
        }
        if (!is_name_char(view.at<1>()))
            return Status::NAME_SEG_SECOND_CHAR_NOT_A_NAME_CHAR;
        if (!is_name_char(view.at<2>()))
            return Status::NAME_SEG_THIRD_CHAR_NOT_A_NAME_CHAR;
        if (!is_name_char(view.at<3>()))
            return Status::NAME_SEG_FOURTH_CHAR_NOT_A_NAME_CHAR;

#ifdef AML_PARSER_DEBUG_READ_NAME_SEG
        klog<LogLevel::DEBUG>("AMLParser/NameSeg") << "\"" << std::string_view{&view.at<0>(), 4} << "\"";
#endif
        return Status::OK;
    }

    Parser::StatusAnd<std::unique_ptr<AMLObject>>
    Parser::readNamedObject() {
        const auto savedIterator = m_iter;

        OpCode opCode;
        if (!read(opCode))
            return {Status::NAMED_OBJECT_FAILED_TO_READ_OP_CODE, nullptr};

        switch (opCode) {
            case OpCode::CreateByteFieldOp:
                return readNamedObjectCreateIntegerField<CreateByteField>();
            case OpCode::CreateWordFieldOp:
                return readNamedObjectCreateIntegerField<CreateWordField>();
            case OpCode::CreateDWordFieldOp:
                return readNamedObjectCreateIntegerField<CreateDWordField>();
            case OpCode::CreateQWordFieldOp:
                return readNamedObjectCreateIntegerField<CreateQWordField>();
            case OpCode::ExtOpPrefix: {
                ExtendedOpCode extendedOp;
                if (!read(extendedOp))
                    return {Status::NAMED_OBJECT_FAILED_TO_READ_EXTENDED_OP_CODE, nullptr};
                if (extendedOp == ExtendedOpCode::ProcessorOp)
                    return readDefProcessor();
                break;
            }
            default:
                break;
        }

        m_iter = savedIterator;
        return {Status::NAMED_OBJECT_UNRECOGNIZED_OP_CODE, nullptr};
    }

    template<typename T>
    Parser::StatusAnd<std::unique_ptr<AMLObject>>
    Parser::readNamedObjectCreateIntegerField() {
        auto sourceBuff = read_term_arg();
        if (sourceBuff.status() != Status::OK)
            return {sourceBuff.status(), nullptr};

        auto byteIndex = read_term_arg();
        if (byteIndex.status() != Status::OK)
            return {byteIndex.status(), nullptr};

        auto name = read_name_string();
        if (name.status() != Status::OK)
            return {name.status(), nullptr};
#ifdef AML_PARSER_DEBUG_READ_NAMED_OBJCET_CREATE_INTEGER_FIELD
        klog<LogLevel::DEBUG>("AMLParser/readNamedObjectCreateIntegerField") << "Name: " << name.contained().c_str();
#endif

        return {Status::OK, std::make_unique<T>(std::move(name.contained()), std::move(sourceBuff.contained()),
                                                std::move(byteIndex.contained()))};
    }

    Parser::StatusAnd<std::unique_ptr<AMLObject>>
    Parser::readNameSpaceModifierObj(OpCode opCode) {
        switch (opCode) {
            // DefName := NameOp NameString DataRefObject
            case OpCode::NameOp: {
                auto name = read_name_string();
                if (name.status() != Status::OK)
                    return {name.status(), nullptr};
#ifdef AML_PARSER_DEBUG_READ_NAMESPACE_MODIFIER_OBJ
                klog<LogLevel::DEBUG>("AMLParser") << "DefName: \"" << std::string_view{name.contained().c_str(), name.contained().size()} << "\"";
#endif

                // TODO is this correct?
                auto dataObject = readDataObject(false);
                if (dataObject.status() != Status::OK)
                    return {dataObject.status(), nullptr};
                return {Status::OK, std::make_unique<NameDefinition>(std::move(name.contained()), std::move(dataObject.contained()))};
            }
        default:
            return {Status::NAME_SPACE_MODIFIER_OBJ_INVALID_OP_CODE, nullptr};
        }
    }

    Parser::StatusAnd<std::string>
    Parser::read_name_string() {
        const auto begin = m_iter;
        std::uint8_t first_char;
        if (!read(first_char))
            return {Status::NAME_STRING_NO_FIRST_BYTE, {}};
//        klog<LogLevel::DEBUG>("AMLParser") << "read_name_string(): first_char: " << std::uint8_t(first_char);

        std::string result{};

#ifdef AML_PARSER_DEBUG_READ_NAME_STRING
        klog<LogLevel::DEBUG>("NameString") << "First char " << std::size_t(first_char) << " or " << std::string_view{reinterpret_cast<const char *>(&first_char), 1};
        STACK_SCOPE();
#endif
        ASSERT(first_char != 0x68);

        // RootChar := 0x5C | '\\'
        const bool isRootPath = first_char == '\\';
        if (isRootPath) {
            result.push_back("\\.");
            if (!read(first_char)) {
                m_iter = begin;
#ifdef AML_PARSER_ERRORS
                klog<LogLevel::ERROR>("AMLParser") << "No byte after RootChar";
#endif
                return {Status::NAME_STRING_NO_FIRST_BYTE, {}};
            }
#ifdef AML_PARSER_DEBUG_READ_NAME_STRING
            klog<LogLevel::DEBUG>("NameString") << "isRootPath!! next character: " << std::string_view{&static_cast<const char &>(first_char), 1};
#endif
        }

        // NullName := 0x00
        // https://uefi.org/specs/ACPI/6.4/20_AML_Specification/AML_Specification.html#nullname
        if (first_char == 0x00 && !result.empty()) {
            if (isRootPath)
                return {Status::OK, std::string("\\")};
            return {Status::OK, std::forward<std::string>(result)};
        }

        // DualNamePath := DualNamePrefix NameSeg NameSeg
        // DualNamePrefix := 0x2E
        // https://uefi.org/specs/ACPI/6.4/20_AML_Specification/AML_Specification.html#dualnamepath
        if (first_char == 0x2E) {
            result.push_back("abcd.efgh");
            const auto first_status = read_name_seg(core::View<char, 4>{result.end() - 9});
            if (first_status != Status::OK) {
                m_iter = begin;
                return {first_status, {}};
            }

            const auto second_status = read_name_seg(core::View<char, 4>{result.end() - 4});
            if (second_status != Status::OK) {
                m_iter = begin;
                return {second_status, {}};
            }
            return {Status::OK, std::forward<std::string>(result)};
        }

        // MultiNamePath := MultiNamePrefix SegCount NameSeg(SegCount)
        // MultiNamePrefix := 0x2F
        // https://uefi.org/specs/ACPI/6.4/20_AML_Specification/AML_Specification.html#multinamepath
        if (first_char == 0x2F) {
            std::uint8_t seg_count;
            if (!read(seg_count)) {
                m_iter = begin;
                return {Status::NAME_STRING_INVALID_SEG_COUNT_IN_MULTI_NAME_PATH, {}};
            }
#ifdef AML_PARSER_DEBUG_READ_NAME_STRING
            klog<LogLevel::DEBUG>("AMLParser/NameString") << "MultiNamePath of " << TextModeBufferViewWriter::Decimal
                    << seg_count << " segments (" << std::size_t(result.length() + seg_count * 4) << " characters total)";
#endif
            if (seg_count == 0) {
                m_iter = begin;
                return {Status::NAME_STRING_SEG_COUNT_IS_ZERO, {}};
            }

            auto offset = result.length();
            result.resize(result.length() + 4 + ((seg_count - 1) * 5));

            if (auto status = read_name_seg(core::View<char, 4>{result.begin() + offset}); status != Status::OK) {
                m_iter = begin;
                return {status, {}};
            }

            offset += 4;

            for (std::size_t i = 0; i < seg_count - 1; ++i) {
                auto *position = result.begin() + offset + (i * 5);
                *position = '.';
                auto status = read_name_seg(core::View<char, 4>{position + 1});
                if (status != Status::OK) {
                    m_iter = begin;
                    return {status, {}};
                }
#ifdef AML_PARSER_DEBUG_READ_NAME_STRING
                klog<LogLevel::DEBUG>("AMLParser/MultiNamePath") << "Segment \"" << std::string_view{position, 5} << "\"";
#endif
            }
#ifdef AML_PARSER_DEBUG_READ_NAME_STRING
            klog<LogLevel::DEBUG>("AMLParser/MultiNamePath") << "Result is \"" << std::string_view{result.c_str(), result.length()} << "\"";
#endif
            return {Status::OK, std::forward<std::string>(result)};
        }

        // Reconsume the next byte
        --m_iter;
        auto offset = result.size();
        result.push_back("wwww");
//        klog<LogLevel::DEBUG>("gahahagsdfghaz") << result.capacity() << " vs " << result.size() << " vs " << offset
//                << " giving " << std::size_t(result.begin() + offset) << " to " << std::size_t(result.begin() + offset + 4);
        CHECK(result.size() >= offset + 4);
        auto status = read_name_seg(core::View<char, 4>(result.begin() + offset));
#ifdef AML_PARSER_DEBUG_READ_NAME_STRING
        if (status == Status::OK)
            klog<LogLevel::DEBUG>("AMLParser/NameString") << "\"" << std::string_view{result.c_str(), result.length()} << "\"";
#endif
        return {status, std::forward<std::string>(result)};
    }

    template <Parser::OperandOperandOperationType type>
    Parser::StatusAnd<std::pair<std::unique_ptr<AMLObject>, std::unique_ptr<AMLObject>>>
    Parser::read_operand_operand_operation() {
        auto operandLeft = read_term_arg();
        if (operandLeft.status() != Status::OK)
            return {operandLeft.status(), {nullptr, nullptr}};

        auto operandRight = read_term_arg();
        if (operandRight.status() != Status::OK)
            return {operandRight.status(), {nullptr, nullptr}};

        return {Status::OK, {std::move(operandLeft.contained()), std::move(operandRight.contained())}};
    }

    template <typename ExpressionType>
    Parser::StatusAnd<std::unique_ptr<Expression>>
    Parser::read_operand_operand_target_operation() {
        auto operand_left = read_term_arg();
        if (operand_left.status() != Status::OK)
            return {operand_left.status(), {}};

        auto operand_right = read_term_arg();
        if (operand_right.status() != Status::OK)
            return {operand_right.status(), {}};

        auto target = read_target();
        if (target.status() != Status::OK)
            return {target.status(), {}};

        return {Status::OK, std::make_unique<ExpressionType>(std::move(operand_left.contained()),
                                                             std::move(operand_right.contained()),
                                                             std::move(target.contained()))};
    }

    template <typename Type>
    Parser::StatusAnd<std::unique_ptr<Expression>>
    Parser::read_operand_target_operation() {
        auto operand = read_term_arg();
        if (operand.status() != Status::OK)
            return {operand.status(), nullptr};

        auto target = read_target();
        if (target.status() != Status::OK)
            return {target.status(), nullptr};

        return {Status::OK, std::make_unique<Type>(std::move(operand.contained()), std::move(target.contained()))};
    }

    PkgLength
    Parser::read_package_length() {
        std::uint8_t lead_byte;
        if (!read(lead_byte))
            return k_invalid_package_length;

        const auto byte_count = (lead_byte >> 6) & 0b11;
        if (byte_count == 0)
            return PkgLength(lead_byte & 0b00111111);

        PkgLength result{};// = lead_byte & 0b00001111;

        for (std::uint16_t i = 0; i < byte_count; ++i) {
            std::uint8_t byte_data;
            if (!read(byte_data))
                return k_invalid_package_length;
//            result <<= 8;
//            result += byte_data;
            result |= PkgLength(byte_data << (((byte_count - i) << 3) - 4));
        }

        result |= PkgLength(lead_byte & 0x0F);

        return result;
    }

    Parser::StatusAnd<std::unique_ptr<AMLObject>>
    Parser::readReference() {
        OpCode opCode;
        if (!read(opCode))
            return {Status::REFERENCE_FAILED_TO_READ_OP_CODE, nullptr};

        switch (opCode) {
            case OpCode::DerefOfOp:
                if (auto expression = readExpressionDerefOf(); expression.status() == Status::OK)
                    return {Status::OK, std::move(expression.contained())};
                else
                    return {expression.status(), nullptr};
            case OpCode::IndexOp:
                if (auto expression = readExpressionIndex(); expression.status() == Status::OK)
                    return {Status::OK, std::move(expression.contained())};
                else
                    return {expression.status(), nullptr};
            default:
                return {Status::REFERENCE_INVALID_OP_CODE, nullptr};
        }
    }

    Parser::StatusAnd<std::unique_ptr<Statement>>
    Parser::readStatement() {
        const auto savedIter = m_iter;
        std::uint8_t first_byte;
        if (!read(first_byte))
            return {Status::STATEMENT_NO_FIRST_BYTE, nullptr};

        Parser::StatusAnd<std::unique_ptr<Statement>> statement{{}, {}};

        switch (static_cast<OpCode>(first_byte)) {
            case OpCode::ContinueOp:
                return {Status::OK, std::make_unique<Statement>(Statement::Type::CONTINUE)};
            case OpCode::IfOp:
                statement = readIfElseStatement();
                if (statement.status() != Status::OK)
                    m_iter = savedIter;
                return statement;
            case OpCode::WhileOp:
                statement = readWhileStatement();
                if (statement.status() != Status::OK)
                    m_iter = savedIter;
                return statement;
            case OpCode::ReturnOp:
                if (auto argument = read_term_arg(); argument.status() != Status::OK) {
                    m_iter = savedIter;
                    return {argument.status(), nullptr};
                } else {
                    return {Status::OK, std::make_unique<ReturnStatement>(std::move(argument.contained()))};
                }
            case OpCode::BreakOp:
                return {Status::OK, std::make_unique<Statement>(Statement::Type::BREAK)};
            default:
                break;
        }

        m_iter = savedIter;
        return {Status::STATEMENT_NOT_ON_STREAM, nullptr};
    }

    Parser::StatusAnd<std::unique_ptr<AMLObject>>
    Parser::readString() {
        const auto iteratorResetPosition = m_iter;

        std::string value;
        while (true) {
            std::uint8_t character;
            if (!read(character)) {
                m_iter = iteratorResetPosition;
                return {Status::STRING_FAILED_TO_READ, nullptr};
            }

            if (character == static_cast<std::uint8_t>(OpCode::NullChar))
                break;

            if (character > 0x7F) {
                m_iter = iteratorResetPosition;
                return {Status::STRING_NOT_ASCII_CHAR_OR_NULL_TERMINATOR, nullptr};
            }

            value.push_back(static_cast<char>(character));
        }

#ifdef AML_PARSER_DEBUG_READ_STRING
        klog<LogLevel::DEBUG>("AMLParser/String") << std::string_view{value.c_str(), value.length()};
#endif // AML_PARSER_DEBUG_READ_STRING

        return {Status::OK, std::make_unique<StringObject>(std::move(value))};
    }

    // TODO rename these statuses
    Parser::StatusAnd<std::unique_ptr<AMLObject>>
    Parser::read_super_name() {
        const auto begin = m_iter;
        std::uint8_t peeked_first_name_byte;
        if (!peek(peeked_first_name_byte))
            return {Status::DEF_TO_HEX_STRING_EOS_FIRST_NAME_BYTE, nullptr};
#ifdef AML_PARSER_DEBUG_READ_SUPER_NAME
        klog<LogLevel::DEBUG>("AMLParser/SuperName") << "Peeked FirstNameByte: " << std::size_t(peeked_first_name_byte);
#endif

        if (static_cast<OpCode>(peeked_first_name_byte) == OpCode::ExtOpPrefix) {
            ++m_iter;
            std::uint8_t extended_op_code;
            if (!read(extended_op_code))
                return {Status::DEF_TO_HEX_STRING_EOS_EXT_OP_CODE, nullptr};
            if (static_cast<ExtendedOpCode>(extended_op_code) == ExtendedOpCode::DebugOp) {
                return {Status::OK, std::make_unique<AMLObject>(AMLObject::Type::DEBUG_OBJECT_REFERENCE)};
            }
            return {Status::DEF_TO_HEX_STRING_UNKNOWN_EXT_OP_CODE, nullptr};
        }

        if (peeked_first_name_byte >= 0x60 && peeked_first_name_byte <= 0x67) {
            ++m_iter;
            return {Status::OK, std::make_unique<LocalObjectReference>(std::uint8_t(peeked_first_name_byte - 0x60))};
        }

        if (peeked_first_name_byte >= 0x68 && peeked_first_name_byte <= 0x6E) {
            return {Status::OK, std::make_unique<ArgumentObjectReference>(std::uint8_t(peeked_first_name_byte - 0x68))};
        }

        m_iter = begin;

        if (auto reference = readReference(); reference.status() != Status::REFERENCE_INVALID_OP_CODE)
            return {reference.status(), std::move(reference.contained())};
        m_iter = begin;

        // https://uefi.org/specs/ACPI/6.4/20_AML_Specification/AML_Specification.html#supername
        if (auto name_string = read_name_string(); name_string.status() == Status::OK) {
            return {Status::OK, std::make_unique<NameString>(std::move(name_string.contained()))};
        }
        m_iter = begin;

#ifdef AML_PARSER_DEBUG_READ_SUPER_NAME
        klog<LogLevel::INFO>("AMLParser/TODO") << "SuperName unknown: " << peeked_first_name_byte;
        CRASH("hhu");
#endif
        return {Status::DEF_TO_HEX_STRING_UNKNOWN_TARGET, nullptr};
    }

    // TODO rename these statuses
    Parser::StatusAnd<std::unique_ptr<AMLObject>>
    Parser::read_target() {
        std::uint8_t peeked_first_name_byte;
        if (!peek(peeked_first_name_byte))
            return {Status::DEF_TO_HEX_STRING_EOS_FIRST_NAME_BYTE, nullptr};

        if (peeked_first_name_byte == 0x00) {
            ++m_iter;
            return {Status::OK, std::make_unique<AMLObject>(AMLObject::Type::NULL_NAME)};
        }

        // TODO make sure the type is correct, maybe?
        return read_super_name();
    }

    // https://uefi.org/specs/ACPI/6.4/20_AML_Specification/AML_Specification.html#termarg
    Parser::StatusAnd<std::unique_ptr<AMLObject>>
    Parser::read_term_arg() {
        std::uint8_t first_read_byte;
        if (!read(first_read_byte))
            return {Status::TERM_ARG_FIRST_BYTE_EOS, nullptr};

        switch (static_cast<OpCode>(first_read_byte)) {
            // https://uefi.org/specs/ACPI/6.4/20_AML_Specification/AML_Specification.html#localobj
            case OpCode::Local0Op: return {Status::OK, std::make_unique<LocalObjectReference>(std::uint8_t(0))};
            case OpCode::Local1Op: return {Status::OK, std::make_unique<LocalObjectReference>(std::uint8_t(1))};
            case OpCode::Local2Op: return {Status::OK, std::make_unique<LocalObjectReference>(std::uint8_t(2))};
            case OpCode::Local3Op: return {Status::OK, std::make_unique<LocalObjectReference>(std::uint8_t(3))};
            case OpCode::Local4Op: return {Status::OK, std::make_unique<LocalObjectReference>(std::uint8_t(4))};
            case OpCode::Local5Op: return {Status::OK, std::make_unique<LocalObjectReference>(std::uint8_t(5))};
            case OpCode::Local6Op: return {Status::OK, std::make_unique<LocalObjectReference>(std::uint8_t(6))};
            case OpCode::Local7Op: return {Status::OK, std::make_unique<LocalObjectReference>(std::uint8_t(7))};

            // https://uefi.org/specs/ACPI/6.4/20_AML_Specification/AML_Specification.html#argobj
            case OpCode::Arg0Op: return {Status::OK, std::make_unique<ArgumentObjectReference>(std::uint8_t(0))};
            case OpCode::Arg1Op: return {Status::OK, std::make_unique<ArgumentObjectReference>(std::uint8_t(1))};
            case OpCode::Arg2Op: return {Status::OK, std::make_unique<ArgumentObjectReference>(std::uint8_t(2))};
            case OpCode::Arg3Op: return {Status::OK, std::make_unique<ArgumentObjectReference>(std::uint8_t(3))};
            case OpCode::Arg4Op: return {Status::OK, std::make_unique<ArgumentObjectReference>(std::uint8_t(4))};
            case OpCode::Arg5Op: return {Status::OK, std::make_unique<ArgumentObjectReference>(std::uint8_t(5))};
            case OpCode::Arg6Op: return {Status::OK, std::make_unique<ArgumentObjectReference>(std::uint8_t(6))};

            default:
                --m_iter;

                if (auto result = readDataObject(true); result.status() != Status::DATA_OBJECT_OP_CODE_UNKNOWN)
                    return {Status::OK, std::move(result.contained())};

                if (auto result = readExpression(true); result.status() != Status::EXPRESSION_OP_CODE_UNKNOWN)
                    return {Status::OK, std::move(result.contained())};

#ifdef AML_PARSER_ERRORS
                klog<LogLevel::ERROR>("Parser::read_term_arg") << "Unknown first byte: " << first_read_byte;
#endif
                return {Status::TERM_ARG_FIRST_BYTE_EOS, nullptr};
        }
    }

    Parser::Status
    Parser::read_term_list(PkgLength pkgLength, AMLScope &parentScope) {
        const auto pkgLengthEnd = m_iter.base() + pkgLength;
        if (pkgLengthEnd > m_data.end().base()) {
#ifdef AML_PARSER_ERRORS
            klog<LogLevel::ERROR>("PkgLength") << TextModeBufferViewWriter::Decimal
                    << pkgLength << " m_iter=" << m_iter.base() << " end=" << m_data.end().base();
#endif
            CRASH("AML Illegal PkgLength");
        }
        for (const auto &otherPackageLength : m_pkgLengths) {
            if (otherPackageLength.size + otherPackageLength.begin < pkgLengthEnd) {
                klog<LogLevel::DEBUG>("PkgLength") << "m_iter=" << TextModeBufferViewWriter::Decimal << m_iter.base();
                klog<LogLevel::ERROR>("PkgLength") << "Requested PkgLength: " << TextModeBufferViewWriter::Decimal
                        << " size=" << pkgLength
                        << " end=" << (pkgLengthEnd);
                klog<LogLevel::ERROR>("PkgLength") << "In-Stack PkgLength: " << TextModeBufferViewWriter::Decimal
                        << " begin=" << otherPackageLength.begin
                        << " size=" << otherPackageLength.size
                        << " end=" << (otherPackageLength.begin + otherPackageLength.size)
                        << " origin=" << otherPackageLength.origin;
                CRASH("AML Illegal PkgLength");

                // TODO convert this to a status at release builds
            }
        }

        CHECK(!m_pkgLengths.full());
#ifdef AML_PARSER_DEBUG_READ_TERM_LIST
        klog<LogLevel::DEBUG>("PkgLength") << "BRAND NEW of #" << TextModeBufferViewWriter::Decimal << m_pkgLengths.size()
                << TextModeBufferViewWriter::Hexadecimal << " begin=" << m_iter.base() << " pkgLength=" << std::size_t(pkgLength);
#endif
        ScopedPkgLength scopedPkgLength{this, m_iter.base(), pkgLength, "TermList"};

        const auto end = m_iter + pkgLength;
        while (m_iter < end) {
            const auto iter_restore = m_iter;

            auto object = read_term_object();
            if (object.status() != Status::OK) {
                m_iter = iter_restore;
#ifdef AML_PARSER_DEBUG_READ_TERM_LIST
                klog<LogLevel::DEBUG>("AMLParser") << "No TermObject on the stream, status=" << to_string(object.status())
                        << " first_byte=" << *m_iter;
                NEXT();
                m_iter = m_iter - 8;
                NEXT();
                m_iter = m_iter + 8;
                CRASH("ok");
#endif
                return object.status();
            }

#ifdef AML_PARSER_DEBUG_READ_TERM_LIST
            klog<LogLevel::DEBUG>("AMLParser/TermObject") << "New object! @ " << std::size_t(object.contained().get());
#endif
            CHECK(object.contained().get() != nullptr);

            assignObjectToParentScope(std::move(object.contained()), parentScope);
        }

        return Status::OK;
    }

    template<typename ReturnObjectType, typename T>
    Parser::StatusAnd<std::unique_ptr<ReturnObjectType>>
    Parser::readTermListAndReturn(std::unique_ptr<T> &&object, PkgLength pkgLength) {
#ifdef AML_PARSER_DEBUG_READ_TERM_LIST_AND_RETURN
        klog<LogLevel::DEBUG>("readTermListAndReturn/Start") << std::size_t(object.get()) << " of type " << toString(object->objectType())
                << " named " << object->name().c_str();
#endif

        // Fallback to the current scope, in the case where the object has a
        // relative name.
        AMLScope *targetScope = m_currentScope;

        //
        // Find Parent Information
        //
        // We need to gather information about the parent, since the name of
        // this scoped object could be an absolute one.
        //
        const auto parentInformation = findParent(*object);
        if (parentInformation.parent) {
            targetScope = const_cast<AMLScope *>(parentInformation.parent);
            object->overrideName(std::string(parentInformation.name));
#ifdef AML_PARSER_DEBUG_READ_TERM_LIST_AND_RETURN
            DUMP_SCOPE_CHAIN("readTermListAndReturn/STACK", targetScope);
#endif
        }

        //
        // Store this object in the parent (scope).
        //
        // This way, we can relatively find objects inside the scope, its children's
        // scope, and/or its parents' scope.
        //
        auto &objectRef = *object;
        targetScope->children().push_back(std::move(object));

        // Read the term list.
        {
            ScopedScope scopedScope{this, &objectRef, targetScope};
            if (auto status = read_term_list(pkgLength, objectRef); status != Status::OK) {
#ifdef AML_PARSER_DEBUG_READ_TERM_LIST_AND_RETURN
                klog<LogLevel::DEBUG>("Status") << to_string(status);
                CRASH("status not ok `o`");
#endif
                // TODO this isn't OK since the iterator isn't restored
                return {status, nullptr};
            }
        }

        CHECK(!targetScope->children().empty());
        CHECK(targetScope->children().back().get() == &objectRef);
#ifdef AML_PARSER_DEBUG_READ_TERM_LIST_AND_RETURN
        klog<LogLevel::DEBUG>("readTermListAndReturn/Stop") << std::size_t(&objectRef) << " of type " << toString(objectRef.objectType())
                                                       << " named " << objectRef.name().c_str();
#endif

        // Remove the object from the parent. This isn't because it doesn't
        // belong there, but for other functions in the call stack to be able to
        // know about this object, and maybe modify it.
        //
        // The function read_term_list() puts this object back in the parent
        // when it's done with it. We don't remove the parent pointer from the
        // object, so that is easily done.
        return {Status::OK, targetScope->children().take_back()};
    }

    Parser::StatusAnd<std::unique_ptr<AMLObject>>
    Parser::read_term_object() {
        ExtendedOpCode extended_op_code{};
        std::uint8_t first_byte;
        if (!peek(first_byte)) {
#ifdef AML_PARSER_ERRORS
            klog<LogLevel::ERROR>("AMLParser") << "Failed to peek first byte in read_term_object()";
#endif
            return {Status::NO_TERM_OBJECT_FIRST_BYTE, nullptr};
        }

#ifdef AML_PARSER_DEBUG_READ_TERM_OBJECT
        klog<LogLevel::DEBUG>("AMLParser") << "FirstByte=" << first_byte;
#endif

        switch (static_cast<OpCode>(first_byte)) {
            case OpCode::MethodOp:
                ++m_iter;
                if (auto method = read_def_method(); method.status() == Status::OK) {
                    return {Status::OK, std::move(method.contained())};
                } else
                    return {method.status(), nullptr};
            case OpCode::ScopeOp:
                ++m_iter; // Consume the OpCode.
                return read_def_scope();
            case OpCode::ExtOpPrefix: {
                ++m_iter;
                if (!read(extended_op_code)) {
#ifdef AML_PARSER_ERRORS
                    klog<LogLevel::ERROR>("AMLParser") << "Failed to peek first byte in read_term_object()";
#endif
                    return {Status::TERM_OBJECT_EXT_OP_PREFIX_END_OF_STREAM, nullptr};
                }

#ifdef AML_PARSER_DEBUG_READ_TERM_OBJECT
                klog<LogLevel::DEBUG>("AMLParser") << "ExtOpCode=" << static_cast<std::uint8_t>(extended_op_code);
#endif

                switch (extended_op_code) {
                    case ExtendedOpCode::MutexOp:
                        if (auto defMutex = readDefMutex(); defMutex.status() == Status::OK) {
                            return {Status::OK, std::move(defMutex.contained())};
                        } else {
#ifdef AML_PARSER_ERRORS
                            klog<LogLevel::ERROR>("AMLParser/TermObject") << "Failed to read DefMutex: " << to_string(defMutex.status());
#endif
                            return {defMutex.status(), nullptr};
                        }
                    case ExtendedOpCode::OpRegionOp:
                        return read_def_op_region();
                    case ExtendedOpCode::FieldOp:
                        return read_def_field();
                    case ExtendedOpCode::DeviceOp: {
                        auto device = readDefDevice();
                        return {device.status(), std::move(device.contained())};
                    }
                    case ExtendedOpCode::ProcessorOp:
                        return readDefProcessor();
                    default:
                        // ExtOp probably means an expression here.
                        --m_iter;
                        --m_iter;
                        goto defaultCase;
                }
            }
            defaultCase:
            default: {
                ++m_iter; // wow this is stupid design
                if (auto nameSpaceModifierObject = readNameSpaceModifierObj(static_cast<OpCode>(first_byte));
                        nameSpaceModifierObject.status() != Status::NAME_SPACE_MODIFIER_OBJ_INVALID_OP_CODE) {
//                    CHECK(!m_scopes.empty() && !m_scopes.full());
//                    auto ptr = nameSpaceModifierObject.contained().get();
//                    klog<LogLevel::DEBUG>("TermObject") << "Append to NSMod: " << toString(nameSpaceModifierObject.contained()->objectType())
//                            << " @ " << std::size_t(nameSpaceModifierObject.contained().get()) << " making it index " << m_scopes.back().nameSpaceModifiers.size();
////                    m_scopes.back().nameSpaceModifiers.push_back(std::move(nameSpaceModifierObject.contained()));
//                    klog<LogLevel::DEBUG>("NameSpaceModifiers") << "Ptr of back is: " << std::size_t(m_scopes.back().nameSpaceModifiers.back().get());
//                    klog<LogLevel::DEBUG>("TermObject>Scope") << "@ " << std::size_t(&m_scopes.back());
//                    ASSERT(ptr == m_scopes.back().nameSpaceModifiers.back().get());
//                    for (const auto &entry : m_scopes.back().nameSpaceModifiers)
//                        klog<LogLevel::DEBUG>(">") << std::size_t(entry.get());
//                    CHECK(nameSpaceModifierObject.contained().get() == nullptr);
                    return nameSpaceModifierObject;
                }
                --m_iter; // see above

                if (auto statement = readStatement(); statement.status() != Status::STATEMENT_NOT_ON_STREAM) {
                    return {statement.status(), std::move(statement.contained())};
                }

                auto expression = readExpression(false);
                if (expression.status() == Status::OK) {
                    ASSERT(expression.contained().get() != nullptr);
                    ASSERT(expression.contained()->objectType() == AMLObject::Type::EXPRESSION);
                    return {Status::OK, std::move(expression.contained())};
                }
                if (expression.status() != Status::EXPRESSION_OP_CODE_UNKNOWN) {
#ifdef AML_PARSER_DEBUG_READ_TERM_OBJECT
                    klog<LogLevel::ERROR>("AMLParser/TermObject") << "Forwarding error from readExpression!";
#endif
                    return {expression.status(), nullptr};
                }

                if (auto namedObject = readNamedObject(); namedObject.status() != Status::NAMED_OBJECT_UNRECOGNIZED_OP_CODE)
                    return {namedObject.status(), std::move(namedObject.contained())};

                return {Status::TERM_OBJECT_FIRST_BYTE_UNEXPECTED, nullptr};
            }
        }
    }

    Parser::StatusAnd<std::unique_ptr<Statement>>
    Parser::readWhileStatement() {
        const auto start = m_iter.base();
#ifdef AML_PARSER_DEBUG_READ_WHILE_STATEMENT
        klog<LogLevel::INFO>("AMLParser/WhileStatement") << "Stage: PkgLength";
        NEXT();
#endif
        const auto pkgLength = read_package_length();
        if (pkgLength == k_invalid_package_length)
            return {Status::WHILE_STATEMENT_WITH_INVALID_PACKAGE_LENGTH, nullptr};

#ifdef AML_PARSER_DEBUG_READ_WHILE_STATEMENT
        klog<LogLevel::INFO>("AMLParser/WhileStatement") << "Stage: Predicate, PkgLength=" << pkgLength;
        NEXT();
#endif
        auto predicate = read_term_arg();
        if (predicate.status() != Status::OK) {
            return {predicate.status(), nullptr};
        }

        return readTermListAndReturn<Statement>(std::make_unique<WhileStatement>(std::move(predicate.contained())),
                                                pkgLength - PkgLength(m_iter.base() - start));

//        auto statement = std::make_unique<WhileStatement>(std::move(predicate.contained()));
//
//        klog<LogLevel::INFO>("AMLParser/WhileStatement") << "============================";
//        NEXT();
//
//        ScopedScope scopedScope{this, statement.get()};
//        if (auto status = read_term_list(pkgLength - PkgLength(m_iter.base() - start), *statement); status != Status::OK)
//            return {status, nullptr};
//
//        klog<LogLevel::INFO>("AMLParser/WhileStatement") << "=========== END ============";
//        return {Status::OK, std::move(statement)};
    }

    Parser::Status
    Parser::run() {
        g_textModeBufferView.disable_print_to_screen();
        auto status = read_def_block_header();
        if (status != Status::OK)
            return status;

//        DUMP();

        status = read_term_list(PkgLength(m_data.end().base() - m_iter.base()), m_rootObject);

        auto dumpObject = [] (const AMLObject &object, auto &&dumpObject2, const std::size_t indentation) -> void {
            {
                const std::string indentString(indentation * 4, ' ');
                auto stream = klog<LogLevel::DEBUG>("AML");
                stream << indentString.c_str() << toString(object.objectType());
                if (object.asScope())
                    stream << " ScopeName(\"" << object.asScope()->name().c_str() << "\")";
                switch (object.objectType()) {
                    case AMLObject::Type::EXPRESSION: {
                        const auto &expr = static_cast<const Expression &>(object);
                        stream << " ExpressionType(" << toString(expr.expressionType()) << ")";
                        switch (expr.expressionType()) {
                            case Expression::Type::ADD:
                            case Expression::Type::AND:
                            case Expression::Type::SUBTRACT:
                            case Expression::Type::OR: {
                                stream.finish();
                                const auto &shiftExpr = static_cast<const LeftOperandRightOperandTargetExpression<Expression::Type::AND> &>(expr);
                                dumpObject2(shiftExpr.left(), dumpObject2, indentation + 1);
                                dumpObject2(shiftExpr.right(), dumpObject2, indentation + 1);
                                dumpObject2(shiftExpr.target(), dumpObject2, indentation + 1);
                                break;
                            }
                            case Expression::Type::EQUAL_TO:
                            case Expression::Type::GREATER_THAN:
                            case Expression::Type::GREATER_THAN_OR_EQUAL_TO:
                            case Expression::Type::LESS_THAN:
                            case Expression::Type::LESS_THAN_OR_EQUAL_TO:
                            case Expression::Type::LOGICAL_AND:
                            case Expression::Type::LOGICAL_OR:
                            case Expression::Type::NOT_EQUAL_TO: {
                                stream.finish();
                                const auto &shiftExpr = static_cast<const BiExpression<Expression::Type::EQUAL_TO> &>(expr);
                                dumpObject2(shiftExpr.left(), dumpObject2, indentation + 1);
                                dumpObject2(shiftExpr.right(), dumpObject2, indentation + 1);
                                break;
                            }
                            case Expression::Type::SHIFT_LEFT:
                            case Expression::Type::SHIFT_RIGHT: {
                                stream.finish();
                                const auto &shiftExpr = static_cast<const ShiftExpression<Expression::Type::SHIFT_LEFT> &>(expr);
                                dumpObject2(shiftExpr.operand(), dumpObject2, indentation + 1);
                                dumpObject2(shiftExpr.shiftCount(), dumpObject2, indentation + 1);
                                dumpObject2(shiftExpr.target(), dumpObject2, indentation + 1);
                                break;
                            }
                            case Expression::Type::STORE: {
                                stream.finish();
                                const auto &storeExpr = static_cast<const StoreExpression &>(expr);
                                dumpObject2(storeExpr.termArg(), dumpObject2, indentation + 1);
                                dumpObject2(storeExpr.superName(), dumpObject2, indentation + 1);
                                break;
                            }
                            case Expression::Type::INCREMENT: {
                                stream.finish();
                                const auto &incrementExpr = static_cast<const IncrementExpression &>(expr);
                                dumpObject2(incrementExpr.superName(), dumpObject2, indentation + 1);
                                break;
                            }
                            default:
                                break;
                        }
                        break;
                    }
                    case AMLObject::Type::FIELD_ELEMENT_NAMED:
                        stream << " NamedField(\"" << static_cast<const NamedFieldElement *>(&object)->name().c_str() << "\")";
                        break;
                    case AMLObject::Type::LOCAL_OBJECT_REFERENCE:
                        stream << " " << TextModeBufferViewWriter::Decimal << static_cast<const LocalObjectReference &>(object).value();
                        break;
                    case AMLObject::Type::NAME_DEFINITION: {
                        const auto *def = static_cast<const NameDefinition *>(&object);
                        stream << " NameDefinition(\"" << def->name().c_str() << "\", " << toString(def->value().objectType());
                        switch (def->value().objectType()) {
                            case AMLObject::Type::CONST_NUMBER:
                                stream << ", value: " << static_cast<const ConstNumber &>(def->value()).value();
                                break;
                            case AMLObject::Type::STRING: {
                                const auto &str = static_cast<const StringObject &>(def->value()).value();
                                stream << ", value: \"" << std::string_view{str.c_str(), str.length()} << "\"";
                                break;
                            }
                            default:
                                break;
                        }
                        stream << ")";
                        break;
                    }
                    case AMLObject::Type::CONST_NUMBER: {
                        const auto &obj = static_cast<const ConstNumber &>(object);
                        stream << " ConstNumber(" << obj.value() << ")";
                        break;
                    }
                    case AMLObject::Type::PACKAGE: {
                        const auto &package = static_cast<const Package &>(object);
                        stream << " Entries(" << package.elements().size() << ")";
                        stream.finish();
                        for (const auto &element : package.elements()) {
                            if (element.get() == nullptr)
                                klog<LogLevel::DEBUG>("AML") << indentString.c_str() << "    null";
                            else
                                dumpObject2(*element, dumpObject2, indentation + 1);
                        }
                        break;
                    }
                    case AMLObject::Type::NAME_STRING: {
                        const auto &nameString = static_cast<const NameString &>(object);
                        stream << " Value(\"" << std::string_view{nameString.value().c_str(), nameString.value().length()} << "\")";
                        break;
                    }
                    case AMLObject::Type::STATEMENT:
                        stream << " StatementType(" << toString(static_cast<const Statement *>(&object)->type()) << ")";
                        stream.finish();
                        switch (static_cast<const Statement &>(object).type()) {
                            case Statement::Type::IF_ELSE: {
                                const auto &statement = static_cast<const IfElseStatement &>(object);
                                klog<LogLevel::DEBUG>("AML") << indentString.c_str() << "    CONDITION: @ " << std::size_t(statement.condition());
                                if (statement.condition())
                                    dumpObject2(*statement.condition(), dumpObject2, indentation + 2);
                                klog<LogLevel::DEBUG>("AML") << indentString.c_str() << "    TRUE:";
                                for (const auto &child : statement.trueScope().children())
                                    dumpObject2(*child, dumpObject2, indentation + 2);
                                klog<LogLevel::DEBUG>("AML") << indentString.c_str() << "    FALSE:";
                                for (const auto &child : statement.falseScope().children())
                                    dumpObject2(*child, dumpObject2, indentation + 2);
                            }
                            default:
                                break;
                        }
                        break;
                    default:
                        break;
                }
            }
            if (object.asScope())
                for (const auto &child : object.asScope()->children())
                    dumpObject2(*child, dumpObject2, indentation + 1);
        };

        dumpObject(m_rootObject, dumpObject, 0);
        return status;
    }

} // namespace kernel::acpi::aml
