/**
 * Copyright (C) 2021 Tristan Gerritsen <tristan-legal@outlook.com>
 * All Rights Reserved
 *
 * SPDX-License-Identifier: BSD-3-Clause
 */

#pragma once

namespace Serial {

    enum class COMLine {
        DATA = 0,
        INTERRUPT_ENABLE = 1,
        DLAB_DIVISOR_LSB = 0,
        DLAB_DIVISOR_MSB = 1,
        IRQ_AND_FIFO = 2,
        LINE_CONTROL = 3,
        MODEM_CONTROL = 4,
        LINE_STATUS = 5,
        MODEM_STATUS = 6,
        SCRATCH = 7,
    };

} // namespace Serial
