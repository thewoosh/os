/**
 * Copyright (C) 2021 Tristan Gerritsen <tristan-legal@outlook.com>
 * All Rights Reserved
 *
 * SPDX-License-Identifier: BSD-3-Clause
 */

#include "Kernel/Serial/COMPort.hpp"

#include "Kernel/IO.hpp"
#include "Kernel/Print.hpp"

namespace Serial {

    COMPort::COMPort(PortName portName) noexcept
            : Port(portName) {
        const auto port = intPort();

        write(COMLine::INTERRUPT_ENABLE, 0);
        write(COMLine::LINE_CONTROL, 0x80); // enable DLAB
        write(COMLine::DLAB_DIVISOR_LSB, 0x03);
        write(COMLine::DLAB_DIVISOR_MSB, 0x00);
        write(COMLine::LINE_CONTROL, 0x03);
        write(COMLine::IRQ_AND_FIFO, 0xC7); // Enable FIFO, clear them, with 14-byte threshold
        write(COMLine::MODEM_CONTROL, 0x0B); // IRQs enabled, RTS/DSR set
        write(COMLine::MODEM_CONTROL, 0x1E); // Set in loopback mode, test the serial chip
        write(COMLine::DATA, 0xAE);    // Test serial chip (send byte 0xAE and check if serial returns same byte)

        if(inb(port + 0) != 0xAE) {
            klog<LogLevel::ERROR>("Serial") << "Serial port doesn't work!";
        }

        // If serial is not faulty set it in normal operation mode
        // (not-loopback with IRQs enabled and OUT#1 and OUT#2 bits enabled)
        write(COMLine::MODEM_CONTROL, 0x0F);
        klog<LogLevel::ERROR>("Serial") << "Serial port initialised";
    }

    void
    COMPort::write(COMLine line, std::uint8_t data) noexcept  {
        writeIO(static_cast<std::uint16_t>(line), data);
    }

    void
    COMPort::writeToVM(std::string_view data) noexcept {
        for (char c : data) {
            write(COMLine::DATA, static_cast<std::uint8_t>(c));
        }
    }

} // namespace Serial
