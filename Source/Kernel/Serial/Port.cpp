/*
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 *
 * Copyright (C) 2021 Tristan Gerritsen
 * All Rights Reserved.
 */

#include "Kernel/IO.hpp"
#include "Kernel/Serial/Port.hpp"

namespace Serial {
    bool
    Port::isBlocking() noexcept {
        return (inb(intPort() + 5) & 0x20) == 0;
    }

    void
    Port::writeIO(std::uint16_t outputLine, std::uint8_t data) noexcept {
        while (isBlocking());

        outb(intPort() + static_cast<std::uint16_t>(outputLine), data);
    }

} // namespace Serial

