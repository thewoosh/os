#include "Kernel/Runtime/Crash.hpp"

#include "Kernel/Runtime/Debug.hpp"
#include "Kernel/Runtime/StackTrace.hpp"
#include "Kernel/Print.hpp"

#define CRASH_COMPACT

namespace kernel {

    void
    crash(std::string_view message, std::string_view file_name, std::size_t line_number,
          std::string_view function_name, CrashSource crashSource) {
        // TODO: Debug
        g_textModeBufferView.disable_print_to_screen();
#ifndef CRASH_COMPACT
        if (g_textModeBufferView.current_row() + 10 > 80)
            g_textModeBufferView.clear();
#endif

#ifndef CRASH_COMPACT
        klog<LogLevel::CRITICAL>("CrashReporter") << "====================";
#endif
        klog<LogLevel::CRITICAL>("CrashReporter") << "Kernel crash! :(";
#ifndef CRASH_COMPACT
        klog<LogLevel::CRITICAL>("CrashReporter") << "";
#endif
        if (crashSource != CrashSource::ASSERT) {
            klog<LogLevel::CRITICAL>("CrashReporter") << "Message: " << message;

            klog<LogLevel::CRITICAL>("CrashReporter") << "File: " << file_name << ":"
                    << TextModeBufferViewWriter::Decimal << line_number;
            klog<LogLevel::CRITICAL>("CrashReporter") << "Function: " << function_name;
        }

#ifndef CRASH_COMPACT
        klog<LogLevel::CRITICAL>("CrashReporter") << "";
#endif
        klog<LogLevel::CRITICAL>("CrashReporter") << "Stack Trace:";
        bool doBreak = false;
        traverse_stack_trace([&] (const StackFrameInformation &frame) {
            if (frame.function_name == "long_mode_entrypoint")
                doBreak = true;
            if (doBreak)
                return;

            klog<LogLevel::CRITICAL>("CrashReporter") << "  #" << TextModeBufferViewWriter::Decimal << frame.stack_number
                    << " " << frame.function_name << " (" << TextModeBufferViewWriter::Hexadecimal << frame.rip << ")";
        });

#ifndef CRASH_COMPACT
        klog<LogLevel::CRITICAL>("CrashReporter") << "";
        klog<LogLevel::CRITICAL>("CrashReporter") << "Halting!";
#endif

        BREAKPOINT();

        while (true) {
            asm("hlt");
        }
    }

} // namespace kernel
