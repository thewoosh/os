//#define SYMBOLIZER_DEBUG
//#define DEMANGLER_DEBUG
//#define DEMANGER_ERRORS

#include "Include/Kernel/Runtime/Symbolizer.hpp"

#include <Core/Pointer.hpp>
#include <Core/StackVector.hpp>

#include "Kernel/Memory/MemoryManager.hpp"
#include "Kernel/Memory/Page.hpp"
#include "Kernel/Specifications/ELF/ELF.hpp"
#include "Kernel/Specifications/Multiboot/Tags.hpp"

namespace kernel {

    static const multiboot2::ELFSymbolsTag *s_symbols_tag;
    static const elf64::SectionHeader *s_elf_symtab;
    static const elf64::SectionHeader *s_elf_strtab;
    static const elf64::SectionHeader *s_elf_shstrtab;

    void
    symbolizer::ensureELFHeaderIsMapped() {
        const kernel::elf64::SectionHeader *elfSections[3]{s_elf_symtab, s_elf_strtab, s_elf_shstrtab};
        for (const auto *elfSection : elfSections) {
            if (!elfSection)
                continue;
            const auto sectionBegin = page_floor(reinterpret_cast<void *>(elfSection->sh_addr));
            const auto sectionEnd = page_ceil(core::advance_pointer_with_byte_offset(sectionBegin, elfSection->sh_size));
            for (auto *page = sectionBegin; page < sectionEnd; page = core::advance_pointer_with_byte_offset(page, kPageSize)) {
                MemoryManager::the().map(VirtualAddress{page}, page, MemoryManager::MAP_FLAG_NON_WRITABLE);
            }
        }
    }

    void
    symbolizer::set_multiboot_tag(const multiboot2::ELFSymbolsTag *tag) {
        s_symbols_tag = tag;

        const auto *elf_sections = reinterpret_cast<const elf64::SectionHeader *>(&s_symbols_tag->sections_begin);
        s_elf_strtab = &elf_sections[s_symbols_tag->shndx];

        // TODO assert that the section size = 0x40 (ELF64)

        for (std::size_t section_id = 0; section_id < s_symbols_tag->num; ++section_id) {
            const auto &section = *elf_sections;

            if (section.sh_type == elf64::SectionType::SYMTAB)
                s_elf_symtab = &section;
            else if (section.sh_type == elf64::SectionType::STRTAB && (&section) != s_elf_strtab)
                s_elf_shstrtab = &section;

#ifdef SYMBOLIZER_DEBUG
            klog<LogLevel::DEBUG>("Symbolizer") << "Section of type " << static_cast<std::uint64_t>(section.sh_type) << " of ptr "
                    << reinterpret_cast<std::uint64_t>(&section) << " s_elf_shtrbtab=" << reinterpret_cast<std::uint64_t>(s_elf_shstrtab);
#endif

            elf_sections = reinterpret_cast<const elf64::SectionHeader *>(
                    reinterpret_cast<const std::uint8_t *>(elf_sections) + s_symbols_tag->entsize
            );
        }
    }

    [[nodiscard]] static std::string_view
    get_string_by_index(std::size_t index) {
        if (!s_elf_strtab)
            return "<symbolizer_not_initialized>";

//        klog<LogLevel::DEBUG>("Symbolizer") << "Index: " << index;

        const auto try_strtab = [=] (const elf64::SectionHeader *section) -> std::string_view {
            const auto *begin = reinterpret_cast<const char *>(section->sh_addr);
            const auto *const end = begin + section->sh_size;
//            klog<LogLevel::DEBUG>("Symbolizer") << "Section=" << reinterpret_cast<std::uint64_t>(section)
//                    << " begin=" << reinterpret_cast<std::uint64_t>(begin)
//                    << " end=" << reinterpret_cast<std::uint64_t>(end)
//                    << " index=" << index;
            if (begin + index < end) {
//                klog<LogLevel::DEBUG>("Symbolizer") << "A___1";
                return begin + index;
            }
//            klog<LogLevel::DEBUG>("Symbolizer") << "A___2";
            return {};
        };

        if (auto result = try_strtab(s_elf_strtab); !result.empty())
            return result;

        if (s_elf_shstrtab)
            if (auto result = try_strtab(s_elf_shstrtab); !result.empty())
                return result;

        return "<invalid_string>";
    }

    std::string_view
    symbolizer::get_name_of_function(void *address) {
        if (!s_elf_symtab || !s_elf_strtab)
            return "<symbolizer_not_initialized>";

        if (reinterpret_cast<std::uint64_t>(address) < 0x100000)
            return "<invalid_address>";

        const elf64::Symbol *symbol_closest_to = nullptr;
        std::int64_t symbol_closest_to_diff = 0xFFFFFFFFFFFFFF;

        const auto *symbols = reinterpret_cast<const elf64::Symbol *>(s_elf_symtab->sh_addr);
        const auto num_entries = static_cast<std::size_t>(s_elf_symtab->sh_size / sizeof(elf64::Symbol));
        [[maybe_unused]] std::size_t closest_to_index = 0;
        for (std::size_t i = 0; i < num_entries; ++i) {
            const auto &symbol = symbols[i];
            const auto *symbol_begin = reinterpret_cast<void *>(symbol.st_value);
//            const auto *symbol_end = static_cast<const void *>(static_cast<const std::uint8_t *>(symbol_begin) + symbol.st_size);

            const auto difference_from_begin = reinterpret_cast<std::int64_t>(address) - reinterpret_cast<std::int64_t>(symbol_begin);

            if (difference_from_begin < 0)
                continue;

            if (!symbol_closest_to || difference_from_begin < symbol_closest_to_diff || symbol_closest_to_diff < 0) {
                symbol_closest_to = &symbol;
                symbol_closest_to_diff = difference_from_begin;
                closest_to_index = i;
            }

//            if (address >= symbol_begin && address <= symbol_end) {
//                klog<LogLevel::DEBUG>("Symbolizer") << "Address " << reinterpret_cast<std::uint64_t>(address) << " is withing bounds "
//                        << reinterpret_cast<std::uint64_t>(symbol_begin) << " and " << reinterpret_cast<std::uint64_t>(symbol_end)
//                        << " and symbol is of size " << symbol.st_size;
//                return get_string_by_index(symbol.st_name);
//            }
        }

        if (!symbol_closest_to)
            return "<unknown_symbol>";
#ifdef SYMBOLIZER_DEBUG
        klog<LogLevel::DEBUG>("Symbolizer") << "Symbol Closest To: " << reinterpret_cast<std::uint64_t>(symbol_closest_to)
                << " with offset: " << static_cast<std::uint64_t>(symbol_closest_to_diff) << " and index: " << closest_to_index;
#endif
        return demangle(get_string_by_index(symbol_closest_to->st_name));
    }

    inline constexpr std::size_t SymbolizerNameSize = 256;

    [[nodiscard]] static std::string_view
    translate_type(const char **it, const char *end) {
        if (*it >= end) {
#ifdef DEMANGLER_DEBUG
            kputs("ERROR!DemanglerOutOfBounds!TranslateType");
#endif
            return {};
        }

        std::string_view type;
        switch (**it) {
            case 'v': type = "void"; break;
            case 'w': type = "wchar_t"; break;
            case 'b': type = "bool"; break;
            case 'c': type = "char"; break;
            case 'a': type = "signed char"; break;
            case 'h': type = "unsigned char"; break;
            case 's': type = "short"; break;
            case 't': type = "unsigned short"; break;
            case 'i': type = "int"; break;
            case 'j': type = "unsigned int"; break;
            case 'l': type = "long"; break;
            case 'm': type = "unsigned long"; break;
            case 'x': type = "long long"; break;
            case 'y': type = "unsigned long long"; break;
            case 'n': type = "int128_t"; break;
            case 'o': type = "uint128_t"; break;
            case 'f': type = "float"; break;
            case 'd': type = "double"; break;
            case 'e': type = "long double"; break;
            case 'g': type = "float128_t"; break;
            case 'z': type = "..."; break;
            default:
                return {};
        }

        ++*it;
        return type;
    }

    //
    // C++ Itanium Demangler
    // https://itanium-cxx-abi.github.io/cxx-abi/abi.html
    //
    std::string_view
    symbolizer::demangle(std::string_view name) {
#ifdef MANGLER_DEBUG
#define DEMANGLER_NO_END(tag) if (it == end) { kputs(tag); return name; }
#else
#define DEMANGLER_NO_END(tag) if (it == end) return name;
#endif

#ifdef DEMANGLER_DEBUG
        klog<LogLevel::DEBUG>("Demangler") << "Input: \"" << name << "\"";
#endif
        if (!name.starts_with("_Z")) {
//            kputs("demangle-no-prefix");
            return name;
        }

        const auto end = name.end();
        auto it = name.begin() + 2;

        auto consume_number = [&] () -> std::size_t {
            std::size_t value = 0;
            while (it != end && *it >= '0' && *it <= '9') {
                value *= 10;
                value += static_cast<std::uint8_t>(*it - '0');
                ++it;
            }
            return value;
        };

        if (it == end) {
            kputs("demangler-reached-end-before-the-fun-could-begin");
            return name;
        }

        const bool isNested = *it == 'N';
        if (isNested)
            ++it;

        //
        // Qualifiers
        //

        bool has_restrict = false, has_volatile = false, has_const = false;
        if (it != end && *it == 'r') {
            has_restrict = true;
            ++it;
        }
        if (it != end && *it == 'V') {
            has_volatile = true;
            ++it;
        }
        if (it != end && *it == 'K') {
            has_const = true;
            ++it;
        }

        //
        // Nesting
        //
        core::StackVector<std::string_view, 25> nests;
        core::StackVector<core::StackVector<char, 40>, 25> template_per_nest;
        while (true) {
            if (it == end)
                return name;
            auto &template_string = template_per_nest.emplace_back();

            if (*it == 'C') {
                ++it;
                if (it == end)
                    return name;
                if (*it == '1') {
                    ++it;

                    nests.push_back("(ctor)");
                } else if (*it == '2') {
                    ++it;
                    nests.push_back("(base_ctor)");
                } else if (*it == '3') {
                    ++it;
                    nests.push_back("(comp_alloc_ctor)");
                } else if (*it == 'I' && it + 1 != end && *(it + 1) == '1') {
                    it += 2;
                    nests.push_back("(comp_inherit_ctor)");
                } else if (*it == 'I' && it + 1 != end && *(it + 1) == '2') {
                    it += 2;
                    nests.push_back("(base_inherit_ctor)");
                } else {
#ifdef DEMANGER_ERRORS
                    klog<LogLevel::DEBUG>("Demangler") << "Invalid C* type: " << std::string_view{it, 1};
#endif
                    return name;
                }
            } else if (*it == 'D') {
                ++it;
                if (it == end)
                    return name;
                if (*it == '0') {
                    ++it;
                    nests.push_back("(dtor)");
                } else if (*it == '1') {
                    ++it;
                    nests.push_back("(complete_dtor)");
                } else if (*it == '2') {
                    ++it;
                    nests.push_back("(base_dtor)");
                } else {
#ifdef DEMANGER_ERRORS
                    klog<LogLevel::DEBUG>("Demangler") << "Invalid D* type: " << std::string_view{it, 1};
#endif
                    return name;
                }
            } else {
                auto number = consume_number();
                if (number == 0)
                    break;
                if (static_cast<std::size_t>(end - it) < number) {
#ifdef DEMANGER_ERRORS
                    kputs("demangle-nest-name-to-large");
#endif
                    return name;
                }
                nests.push_back(std::string_view(it, it + number));
                it += number;
            }

            //
            // Templates
            //
            if (it != end && *it == 'I') {
                ++it;
                bool first{true};
                while (it < end) {
#ifdef DEMANGER_ERRORS
                    const auto begin_loop = it;
#endif
                    if (*it == 'E') {
                        ++it;
                        break;
                    }

                    if (first)
                        first = false;
                    else
                        template_string.push_back_range(std::string_view{", "});

                    if (*it == 'L') {
                        ++it;

                        auto type = translate_type(&it, end);
                        if (type.empty()) {
#ifdef DEMANGER_ERRORS
                            kputs("demangle-invalid-template-arg-type-constant");
                            klog<LogLevel::DEBUG>("Demangler") << "End: " << it;
#endif
                            template_string.push_back_range(type);
                            continue;
                        }
                        ++it;

                        template_string.push_back_range(type);
                        template_string.push_back('=');

                        while (it != end && *it >= '0' && *it <= '9')
                            template_string.push_back(*it++);

                        if (it == end || *it != 'E') {
#ifdef DEMANGER_ERRORS
                            kputs("demangle-invalid-template-constant-end");
                            klog<LogLevel::DEBUG>("Demangler") << "End: " << it;
#endif
                            return name;
                        }
                        ++it;

                        continue;
                    }

                    auto type = translate_type(&it, end);
                    if (!type.empty()) {
                        ++it;
                        template_string.push_back_range(type);
                        continue;
                    }
#ifdef DEMANGER_ERRORS
                    kputs("demangle-invalid-template-arg");
                    klog<LogLevel::DEBUG>("Demangler") << "It: " << it << " BeginLoop: " << begin_loop;
#endif
                    return name;
                }
            }
        }

        if (isNested && nests.empty()) {
#ifdef DEMANGER_ERRORS
            kputs("demangle-no-nests");
#endif
            return name;
        }

        // 'E' means end of nested name
        if (it == end) {
#ifdef DEMANGER_ERRORS
            kputs("demangle-nested-no-end");
#endif
            return name;
        }

        if (isNested) {
            if (*it != 'E') {
#ifdef DEMANGER_ERRORS
                kputs("demangle-nested-no-end");
                klog<LogLevel::DEBUG>("Demangler") << "End: " << it;
#endif
                return name;
            }
            ++it;
        }

        //
        // Return Type
        //
        if (it == end) {
#ifdef DEMANGER_ERRORS
            kputs("demangle-no-ret");
#endif
            return name;
        }

        static core::StackVector<char, SymbolizerNameSize> output_string{};
        output_string.clear();

        std::size_t pointersInReturnType{0};
        while (it != end && *it == 'P') {
            ++it;
            ++pointersInReturnType;
        }

        if (*it == 'F') {
            ++it;
            DEMANGLER_NO_END("demangle-ret-function-after-F")
            {
                const auto returnTypeOfReturnTypeFunction = translate_type(&it, end);
                if (returnTypeOfReturnTypeFunction.empty()) {
#ifdef DEMANGER_ERRORS
                    kputs("demangle-ret-function-ret-type");
                    klog<LogLevel::DEBUG>("Demangler") << "End: " << it;
#endif
                    return name;
                }
                output_string.push_back_range(returnTypeOfReturnTypeFunction);
                output_string.push_back('(');
                while (it < end) {
                    DEMANGLER_NO_END("demangle-ret-function-2")
                    const bool isRef = *it == 'R';
                    if (isRef) ++it;

                }
            }
            DEMANGLER_NO_END("demangle-ret-function-before-R-and-or-E")
            if (*it == 'R') {
                output_string.push_back('&');
                ++it;
            }
            DEMANGLER_NO_END("demangle-ret-function-before-E-end")
            if (*it != 'E') {
#ifdef DEMANGER_ERRORS
                kputs("demangle-ret-function-no-end");
                klog<LogLevel::DEBUG>("Demangler") << "End: " << it;
#endif
                return name;
            }
        } else {
            const auto returnType = translate_type(&it, end);
            if (returnType.empty()) {
#ifdef DEMANGER_ERRORS
                kputs("demangle-ret");
                klog<LogLevel::DEBUG>("Demangler") << "End: " << it;
#endif
                return name;
            }

            output_string.push_back_range(returnType);
        }

        if (pointersInReturnType)
            while (pointersInReturnType--)
                output_string.push_back('*');
        ++it;
        output_string.push_back(' ');

        bool first{true};
        std::size_t nest_id = 0;
        for (const auto &nest : nests) {
            if (first)
                first = false;
            else
                output_string.push_back_range(std::string_view{"::"});
            output_string.push_back_range(nest);

            if (!template_per_nest[nest_id].empty()) {
                output_string.push_back('<');
                output_string.push_back_range(template_per_nest[nest_id]);
                output_string.push_back('>');
            }
            ++nest_id;
        }

        output_string.push_back('(');
        // TODO
        output_string.push_back(')');

        if (has_const) output_string.push_back_range(" const");
        if (has_restrict) output_string.push_back_range(" restrict");
        if (has_volatile) output_string.push_back_range(" volatile");

        return {output_string.begin(), output_string.end()};
    }

} // namespace kernel
