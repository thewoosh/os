// This file provides the C++ ABI functions Clang/GCC need.

#include "Kernel/Print.hpp"
#include "Kernel/Runtime/Crash.hpp"

__extension__ typedef int __guard __attribute__((mode(__DI__)));

extern "C" int
__cxa_guard_acquire (__guard *);

extern "C" void
__cxa_guard_release (__guard *);

extern "C" void
__cxa_guard_abort (__guard *);

extern "C" int
__cxa_guard_acquire (__guard *g) {
    return !*reinterpret_cast<char *>(g);
}

extern "C" void
__cxa_guard_release (__guard *g) {
    *reinterpret_cast<char *>(g) = 1;
}

extern "C" void
__cxa_guard_abort (__guard *) {
}



extern "C"
void __stack_chk_fail() {
    klog<LogLevel::INFO>("StackProtector") << "__stack_chk_fail() called";
    CRASH("Stack smashing detected");
}

extern "C" void
__cxa_pure_virtual() {
    klog<LogLevel::INFO>("CXA") << "__cxa_pure_virtual() called";
    CRASH("Virtual Method Table invalid!");
}
