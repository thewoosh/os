#define ACPI_ALLOW_DUMP
#define SHOW_PRINT_OVERLOADS

#include <Core/Lambda.hpp>
#include <Core/Void.hpp>

#include "Kernel/ACPI/AML.hpp"
#include "Kernel/ACPI/AML/Interpreter.hpp"
#include "Kernel/ACPI/AML/Method.hpp"
#include "Kernel/ACPI/DSDT.hpp"
#include "Kernel/ACPI/Interface.hpp"
#include "Kernel/ACPI/MADT.hpp"
#include "Kernel/ACPI/MCFG.hpp"
#include "Kernel/CPU/CPU.hpp"
#include "Kernel/CPU/PIC8259.hpp"
#include "Kernel/CPUInformation.hpp"
#include "Kernel/CPU/APIC.hpp"
#include "Kernel/Device/Drive/AHCI/AHCI.hpp"
#include "Kernel/Device/Input/PS2/PS2Controller.hpp"
#include "Kernel/Device/SystemClassification.hpp"
#include "Kernel/Memory/Alloc.hpp"
#include "Kernel/Memory/MemoryMapRegion.hpp"
#include "Kernel/Memory/PageTableEntry.hpp"
#include "Kernel/Memory/PageDirectoryTable.hpp"
#include "Kernel/PCI/PCIMain.hpp"
#include "Kernel/PCI/ProgrammingInterface.hpp"
#include "Kernel/PCI/SubclassCode.hpp"
#include "Kernel/Print.hpp"
#include "Kernel/Resources/PCScreenFont.hpp"
#include "Kernel/Runtime/Crash.hpp"
#include "Kernel/Runtime/Debug.hpp"
#include "Kernel/Serial/COMPort.hpp"
#include "Kernel/Specifications/UEFI/UEFIManager.hpp"
#include "Kernel/Task/TaskManager.hpp"

#include "Include/Kernel/Memory/MemoryManager.hpp"

extern "C" {
#include <Kernel/Specifications/Multiboot/multiboot2.h>
}

#include "Kernel/ACPI/AML/Package.hpp"
#include "Kernel/ACPI/RSDT.hpp"
#include "Kernel/ACPI/XSDT.hpp"
#include "Kernel/Memory/PageFrameAllocator.hpp"
#include "Kernel/Memory/VirtualAddress.hpp"
#include "Kernel/PCI/ACPIRoutingTableEntry.hpp"
#include <Kernel/Specifications/Multiboot/MultibootInformation.hpp>
#include <Kernel/Specifications/Multiboot/Tags.hpp>
#include "Include/Kernel/Runtime/Symbolizer.hpp"

extern "C"
const void *
multiboot_header_start;

extern "C"
const void *
multiboot_information_ptr;

extern "C" std::uint64_t *null_pointer;
kernel::acpi::Interface s_acpiInterface{kernel::acpi::Interface::Type::RSDT, nullptr};

extern "C" core::Void stack_bottom;
extern "C" core::Void stack_top;
extern "C" core::Void kernel_code_begin;
extern "C" core::Void kernel_code_end;
extern "C" core::Void kernel_readonly_begin;
extern "C" core::Void kernel_readonly_end;
extern "C" core::Void kernel_section_begin;
extern "C" core::Void kernel_section_end;

extern "C" void *paging_pml4;
extern "C" void *paging_pdp_first;
extern "C" void *paging_pd_first;
extern "C" void *paging_pt_first;

kernel::PCIMain *g_pci;

kernel::SystemClassification system_classification{};

struct {
    const kernel::acpi::FADT::Base *fadt;
    const kernel::acpi::MADT *madt;
} g_acpiData;

struct ACPIInitializationResult {
    std::vector<kernel::pci::ACPIRoutingTableEntry> routingTable;
};

void
determine_system_type_using_rsdt_oem_id(std::string_view oem_id) {
    using namespace kernel;

    if (system_classification.virtualization_software != VirtualizationSoftwareType::UNKNOWN)
        return;

    if (oem_id == "VBOX  ") {
        klog<LogLevel::DEBUG>("SystemClassification") << "Hypervisor found: Oracle VM VirtualBox";
        system_classification.virtualization_software = VirtualizationSoftwareType::VIRTUAL_BOX;
    } else if (oem_id == "BOCHS ") {
        klog<LogLevel::DEBUG>("SystemClassification") << "Hypervisor found: Bochs";
        system_classification.virtualization_software = VirtualizationSoftwareType::BOCHS;
    } else if (oem_id == "ALASKA") {
        klog<LogLevel::DEBUG>("SystemClassification") << "Real system found: GIGABYTE(R) motherboard";
        system_classification.virtualization_software = VirtualizationSoftwareType::NONE;
    } else {
        klog<LogLevel::DEBUG>("SystemClassification") << "Unknown oem_id=\"" << oem_id << "\"";
    }
}

void
load_acpi_dsdt(const kernel::acpi::DSDT &dsdt, ACPIInitializationResult &initializationResult) {
    using namespace kernel;

    klog<LogLevel::DEBUG>("ACPI") << "DSDT signature: " << std::string_view{dsdt.header.signature, 4};
    CHECK((std::string_view{dsdt.header.signature, 4}) == "DSDT");

    klog<LogLevel::DEBUG>("Main/ACPI") << "Launching AML parser for DSDT";

    acpi::aml::Parser aml_parser{&dsdt, dsdt.header.length};
    const auto aml_parse_result = aml_parser.run();
    klog<LogLevel::DEBUG>("Main/ACPI(AML)") << "AML Returned: " << acpi::aml::to_string(aml_parse_result)
                                            << " (" << static_cast<std::size_t>(aml_parse_result) << ")";

    auto *prt = aml_parser.findObject(R"(\._SB_.PCI0._PRT)");

    // TODO debug
    if (prt == nullptr)
        return;

    using namespace acpi::aml;

    Interpreter interpreter{aml_parser};
    auto result = interpreter.invokeMethod(static_cast<Method *>(const_cast<AMLObject *>(prt)));
    CHECK(result);
    CHECK(result->objectType() == kernel::acpi::aml::AMLObject::Type::PACKAGE);


    const auto &package = static_cast<const Package &>(*result);
    klog<LogLevel::DEBUG>("main/PRT invoker") << package.elements().size();
    CHECK(!package.elements().empty());

    ASSERT(initializationResult.routingTable.empty());
    initializationResult.routingTable.reserve(package.elements().size());

    for (const auto &abstractEntry : package.elements()) {
//        klog<LogLevel::DEBUG>("main/PRT invoker") << "Type of " << i << ": "
//                << toString(package.elements()[i]->objectType());

        ASSERT(abstractEntry);
        ASSERT(abstractEntry->objectType() == kernel::acpi::aml::AMLObject::Type::PACKAGE);

        const auto &entry = static_cast<const Package &>(*abstractEntry);
        CHECK(entry.elements().size() >= 4);

        CHECK(entry.elements()[0]);
        CHECK(entry.elements()[0]->objectType() == kernel::acpi::aml::AMLObject::Type::CONST_NUMBER);
        const auto address = static_cast<const ConstNumber &>(*entry.elements()[0]).value();
        ASSERT(address <= 0xFFFFFFFF);

        CHECK(entry.elements()[1]);
        CHECK(entry.elements()[1]->objectType() == kernel::acpi::aml::AMLObject::Type::CONST_NUMBER);
        const auto pin = static_cast<const ConstNumber &>(*entry.elements()[1]).value();
        ASSERT(pin <= 0xFF);

        CHECK(entry.elements()[2]);
        CHECK(entry.elements()[2]->objectType() == kernel::acpi::aml::AMLObject::Type::NAME_STRING);
        const auto &third = static_cast<const NameString &>(*entry.elements()[2]).value();
        const std::string_view source{third.c_str(), third.length()};

        CHECK(entry.elements()[3]);
        CHECK(entry.elements()[3]->objectType() == kernel::acpi::aml::AMLObject::Type::CONST_NUMBER);
        const auto sourceIndex = static_cast<const ConstNumber &>(*entry.elements()[3]).value();
        ASSERT(sourceIndex <= 0xFFFFFFFF);

        klog<LogLevel::DEBUG>("PRT Entry") << address << " " << pin << " " << source << " " << sourceIndex;

        initializationResult.routingTable.push_back({
                static_cast<std::uint32_t>(address),
                static_cast<std::uint8_t>(pin),
                std::string(source),
                static_cast<std::uint32_t>(sourceIndex)
        });
    }
}

[[nodiscard]] ACPIInitializationResult
prepareACPI(const kernel::multiboot2::MultibootInformation &multiboot_info) {
    ACPIInitializationResult result{};

    using namespace kernel;

    const acpi::RSDT *rsdt_address = nullptr;
    const acpi::XSDT *xsdt_address = nullptr;

    if (auto *old_rsdp_tag = multiboot_info.find_tag<kernel::multiboot2::ACPIOldRSDPTag>()) {
        rsdt_address = reinterpret_cast<const acpi::RSDT *>(old_rsdp_tag->descriptor.rsdt_address);
    } else if (auto *new_rsdp_tag = multiboot_info.find_tag<kernel::multiboot2::ACPINewRSDPTag>()) {
        xsdt_address = reinterpret_cast<const acpi::XSDT *>(new_rsdp_tag->descriptor.xsdt_address);
    } else {
        klog<LogLevel::ERROR>("Multiboot") << "Neither RSDP New/Old Tag Found!";
        return result;
    }

    auto begin_acpi = page_floor(xsdt_address ? reinterpret_cast<std::uint64_t>(xsdt_address) : reinterpret_cast<std::uint64_t>(rsdt_address));
    klog<LogLevel::DEBUG>("main") << "RSDT_Pointer_Floored = " << begin_acpi;

    VirtualAddress begin_acpi_virtual_address{reinterpret_cast<void *>(begin_acpi)};
    klog<LogLevel::DEBUG>("VirtualAddress") << "PML4E=" << begin_acpi_virtual_address.pml4e()
                                            << " PDPE=" << begin_acpi_virtual_address.pdpe()
                                            << " PDE=" << begin_acpi_virtual_address.pde()
                                            << " PTE=" << begin_acpi_virtual_address.pte()
                                            << " PPO=" << begin_acpi_virtual_address.physical_page_offset();

//    // assume that the PDPE is 0
//    page_table_l3[address.pde()] = reinterpret_cast<std::uint32_t *>(
//            reinterpret_cast<std::uint64_t>(page_table_l2_extension_1) | 0b11);
    bool found_acpi_memory_map_entry = false;
    std::size_t acpi_region_size = 0;
    MemoryMapRegionManager::forEachRegion(core::lambda([&] (const MemoryMapRegion &region) {
//        klog<LogLevel::DEBUG>("ACPI Preparation") << "Needle=" << std::uint64_t(begin_acpi_virtual_address.address())
//                << " Begin=" << entry.base_addr << " Size=" << entry.length << " End=" << (entry.base_addr + entry.length);
        if (region.contains(begin_acpi_virtual_address.address())) {
            if (found_acpi_memory_map_entry)
                CRASH("Found overlapping ACPI memory map regions!");
            found_acpi_memory_map_entry = true;

            if (page_floor(region.begin()) != region.begin())
                CRASH("ACPI memory map region isn't floored to page");

            if (region.type() != MemoryMapRegion::Type::AVAILABLE && region.type() != MemoryMapRegion::Type::UNUSABLE) {
                begin_acpi_virtual_address = VirtualAddress{const_cast<void *>(region.begin())};
                acpi_region_size = region.size();
                klog<LogLevel::DEBUG>("ACPI Preparation") << "Found ACPI inside region of type " << toString(region.type());
            } else {
                klog<LogLevel::CRITICAL>("ACPI Preparation") << "Incorrect type of MemoryMap::Entry wherein the ACPI lays: "
                        << static_cast<std::uint64_t>(region.type());
                CRASH("Incorrect type of MemoryMap::Entry");
            }
        }
    }));

    if (!found_acpi_memory_map_entry)
        CRASH("Couldn't find the memory map region wherein the ACPI is laid out");

    klog<LogLevel::DEBUG>("ACPI Preparation") << "IdentityMapping the ACPI region";
    for (auto *acpi_page = static_cast<std::uint8_t *>(begin_acpi_virtual_address.address());
         acpi_page < static_cast<std::uint8_t *>(begin_acpi_virtual_address.address()) + acpi_region_size;
         acpi_page += kPageSize) {
        // TODO when we're already here, why not map the pages as readonly an NX?
        //      I don't know if it is really necessary, since the region is
        //      guaranteed to be reserved for ACPI, but it might still be a good
        //      idea!
        MemoryManager::the().map(VirtualAddress{acpi_page}, acpi_page, MemoryManager::MAP_FLAG_NON_WRITABLE);
    }

//    for (std::size_t i = 0; i < 512; ++i) {
//        auto value = page_table_l3[i];
//        if (value != nullptr)
//            klog<LogLevel::DEBUG>("PTL3") << "#" << i << " = " << reinterpret_cast<std::uint64_t>(value)
//                    << *reinterpret_cast<const kernel::PageTableEntry *>(&value);
//    }

//    for (std::size_t i = 0; i < 512; ++i) {
//        kernel::PageTableEntry entry{
//            .present = true,
//            .read_write = false,
//            .larger_pages = false,
//            .address = (begin_acpi + i * 0x1000) >> 12,
//        };
//        if (i == 0)
//            klog<LogLevel::DEBUG>("PTECreation") << "" << entry
//                    << "\n" << entry;
//        page_table_l2_extension_1[i] = *reinterpret_cast<std::uint64_t *>(&entry);
//    }

//    page_table_l2_extension_1[0] = reinterpret_cast<std::uint64_t>(page_table_l1_extension_1) | 0b10000011;
//    for (std::size_t i = 0; i < 512; ++i) {
//        page_table_l1_extension_1[i] = reinterpret_cast<std::uint64_t>(rsdt_pointer_floored + i * 0x1000) | 0b10000011;
//    }

//    klog<LogLevel::DEBUG>("ACPI_Paging") << "ptl2e1[" << address.pte() << "] = "
//            << page_table_l2_extension_1[address.pte()]
//            << " @ " << reinterpret_cast<std::uint64_t>(page_table_l2_extension_1[address.pte()]);

//    klog<LogLevel::DEBUG>("ACPI_Paging") << "page_table_l2_extension_1=" << reinterpret_cast<std::uint64_t>(page_table_l2_extension_1);
//    klog<LogLevel::DEBUG>("ACPI_Paging") << "First=" << page_table_l2_extension_1[0] << " Last=" << page_table_l2_extension_1[511];

    klog<LogLevel::DEBUG>("ACPI") << "RSDT/XSDT @ " << begin_acpi << " XSDT=" << reinterpret_cast<std::uint64_t>(xsdt_address)
                                  << " RSDT=" << reinterpret_cast<std::uint64_t>(rsdt_address);

    if (xsdt_address) {
        klog<LogLevel::DEBUG>("ACPI") << "XSDT has " << ((xsdt_address->header.length - sizeof(xsdt_address->header)) / 4) << " entries";
    } else {
        klog<LogLevel::DEBUG>("ACPI") << "RSDT has " << ((rsdt_address->header.length - sizeof(rsdt_address->header)) / 4) << " entries";
    }

    s_acpiInterface = {
            xsdt_address ? acpi::Interface::Type::XSDT : acpi::Interface::Type::RSDT,
            xsdt_address ? static_cast<const void *>(xsdt_address) : static_cast<const void *>(rsdt_address)
    };

    switch (s_acpiInterface.type()) {
        case kernel::acpi::Interface::Type::RSDT:
            klog<LogLevel::DEBUG>("OEMTableID(RSDT)") << std::string_view{s_acpiInterface.rsdt()->header.oem_table_id, 8};
            determine_system_type_using_rsdt_oem_id(std::string_view{s_acpiInterface.rsdt()->header.oem_id, 6});
            break;
        case kernel::acpi::Interface::Type::XSDT:
            klog<LogLevel::DEBUG>("OEMTableID(XSDT)") << std::string_view{s_acpiInterface.rsdt()->header.oem_table_id, 8};
            determine_system_type_using_rsdt_oem_id(std::string_view{s_acpiInterface.xsdt()->header.oem_id, 6});
            break;
        default:
            CRASH("Illegal ACPI interface type");
    }

    // Print all ACPI tables for debugging purposes
    {
        auto stream = klog<LogLevel::DEBUG>("ACPI");
        stream << "Tables Present: ";
        bool flag = false;
        s_acpiInterface.for_each_entry([&](const acpi::SDTHeader &header) {
            if (flag)
                stream << ", ";
            else
                flag = true;
            stream << std::string_view{header.signature, 4};
        });
    }

    const auto *fadt = reinterpret_cast<const kernel::acpi::FADT *>(s_acpiInterface.find("FACP"));
    if (!fadt) {
        klog<LogLevel::ERROR>("ACPI") << "FADT/FACP Not Found";
        BREAKPOINT();
        BREAKPOINT();
        BREAKPOINT();
    }

    klog<LogLevel::DEBUG>("ACPI-FADT") << "FADT Present with length=" << fadt->header.length << " and pm_profile=" << static_cast<std::uint8_t>(fadt->base().preferred_power_management_profile);
    fadt->header.dump();
    g_acpiData.fadt = &fadt->base();
    g_acpiData.madt = reinterpret_cast<const acpi::MADT *>(s_acpiInterface.find("APIC"));

//    acpi_interface.for_each_entry([] (const acpi::SDTHeader &header) {
//        klog<LogLevel::DEBUG>("ACPI") << "SDTHeader points to " << reinterpret_cast<std::uint64_t>(&header);
//        klog<LogLevel::DEBUG>("ACPI") << "SDTHeader signature=\"" << std::string_view(header.signature, 4)
//                << "\" oem_id=\"" << std::string_view(header.oem_id, 6) << "\"";
//
//    });

    klog<LogLevel::DEBUG>("ACPI") << "Finding DSDT...";
    const auto &dsdt = *fadt->dsdt();
    klog<LogLevel::DEBUG>("ACPI") << "Found DSDT @ " << std::uint64_t(&dsdt);
    klog<LogLevel::DEBUG>("ACPI") << "header.length=" << std::uint64_t(dsdt.header.length);
    klog<LogLevel::DEBUG>("ACPI") << "body length=" << std::uint64_t(dsdt.header.length - sizeof(dsdt.header));
    klog<LogLevel::DEBUG>("ACPI") << "acpi_region_start=" << std::uint64_t(begin_acpi_virtual_address.address());
    klog<LogLevel::DEBUG>("ACPI") << "acpi_region_size=" << acpi_region_size;

//    reinterpret_cast<char *>(fadt->base().dsdt)[0] = 'W';

    const auto dsdt_validation = dsdt.header.validate();
    if (!dsdt_validation.empty()) {
        klog<LogLevel::DEBUG>("ACPI") << "DSDT Validation Result: " << dsdt_validation;
        dsdt.header.dump();

        if (dsdt_validation == "SIGNATURE_IS_NULL") {
            klog<LogLevel::DEBUG>("ACPI") << "Searching for correct DSDT location...";
            for (auto *ptr = static_cast<const char *>(begin_acpi_virtual_address.address());
                 ptr < static_cast<const char *>(begin_acpi_virtual_address.address()) + acpi_region_size - 4;
                 ++ptr) {
                if (std::string_view{ptr, 4} == "DSDT") {
                    klog<LogLevel::DEBUG>("ACPI") << "Found DSDT @ " << std::uint64_t(ptr);
                    reinterpret_cast<const acpi::DSDT *>(ptr)->header.dump();
                }
            }

            klog<LogLevel::DEBUG>("ACPI") << "Finished searching for correct DSDT location";
        }

        CRASH("Invalid ACPI DSDT table");
    }
    load_acpi_dsdt(dsdt, result);

#if 0
    TaskManager task_manager{};
    task_manager.launch();

    device::device::ahci::AHCIManager ahci_manager;
    ahci_manager.launch();
#endif

    return result;
}

void
runSelfTests() {
    klog<LogLevel::INFO>("NullCheck") << "Begin";
    klog<LogLevel::INFO>("NullCheck") << "Char at 0x0=" << *reinterpret_cast<std::uint8_t *>(null_pointer);
    klog<LogLevel::INFO>("NullCheck") << "End";

    klog<LogLevel::INFO>("KMallocCheck") << "Begin";
    auto *data = malloc(1);
    klog<LogLevel::INFO>("KMallocCheck") << "AllocatedPointer=" << reinterpret_cast<std::uint64_t>(data);
    klog<LogLevel::INFO>("KMallocCheck") << "  *data=" << *reinterpret_cast<std::uint8_t *>(data);
    klog<LogLevel::INFO>("KMallocCheck") << "  Setting To 0x69";
    *reinterpret_cast<std::uint8_t *>(data) = 0x69;
    klog<LogLevel::INFO>("KMallocCheck") << "  *data=" << *reinterpret_cast<std::uint8_t *>(data);
    klog<LogLevel::INFO>("KMallocCheck") << "  Freeing";
    free(data);
    data = malloc(1);
    klog<LogLevel::INFO>("KMallocCheck") << "  Again Allocating, pointer=" << reinterpret_cast<std::uint64_t>(data);
    free(data);
    klog<LogLevel::INFO>("KMallocCheck") << "End";
}

void
initializePCI(const ACPIInitializationResult &acpiInitializationResult) {
    using namespace kernel;

    const auto *mcfg = reinterpret_cast<const acpi::MCFG *>(s_acpiInterface.find("MCFG"));
    klog<LogLevel::DEBUG>("MCFG") << "@ " << std::size_t(mcfg);
    core::VariableView<const pci::ACPIRoutingTableEntry> routingTable{
        acpiInitializationResult.routingTable.begin(),
        acpiInitializationResult.routingTable.size()
    };

    auto *pci = new PCIMain{};
    g_pci = pci;
    if (mcfg ? !pci->initializeExpress(mcfg, routingTable) : !pci->initializeConventional(routingTable)) {
        klog<LogLevel::CRITICAL>("PCI") << "Failed to initialize! :-(";
        delete pci;
        return;
    }

    pci->forEachDevice([] (const pci::AbstractDevice &device) {
        if (device.getClassCode() == kernel::pci::ClassCode::NETWORK_CONTROLLER) {
            klog<LogLevel::DEBUG>("PCI") << "Network Adapter";
            klog<LogLevel::DEBUG>("PCI") << "Vendor: " << device.getVendorName() << " " << std::size_t(device.getVendorID());
            klog<LogLevel::DEBUG>("PCI") << "Device: " << device.getDeviceName() << " " << std::size_t(device.getDeviceID());
            klog<LogLevel::DEBUG>("PCI") << "Subclass: " << device.getSubclassName() << " " << std::size_t(device.getSubclassCode());
            klog<LogLevel::DEBUG>("PCI") << "PI: " << device.getProgrammingInterfaceName() << " " << std::size_t(device.getProgrammingInterfaceCode());
//            while (true)
//                asm("hlt");
        }
    });
}

void
prepareUEFI(const kernel::multiboot2::MultibootInformation &multibootInformation) {
    using namespace kernel;
    const auto *estpTag = multibootInformation.find_tag<multiboot2::EFI64BitSystemTablePointerTag>();
    if (!estpTag)
        return;

    auto *uefiManager = new uefi::UEFIManager(reinterpret_cast<uefi::EFISystemTable *>(estpTag->pointer));
    VERIFY(uefiManager->launch());
}

extern "C" void
kernel_entrypoint() {
    using namespace kernel;

//    klog<LogLevel::INFO>("KernelStage") << "Querying Multiboot2 information";
    const auto *mb_header = reinterpret_cast<const multiboot_header *>(&multiboot_header_start);
//    klog<LogLevel::DEBUG>("Multiboot") << "Pointer: " << reinterpret_cast<std::uint64_t>(mb_header);

    if (mb_header->magic != MULTIBOOT2_HEADER_MAGIC) {
        g_textModeBufferView.setPrintToScreenAvailable(true);
        g_textModeBufferView.enable_print_to_screen();
        klog<LogLevel::ERROR>("Multiboot") << "Magic incorrect: " << mb_header->magic;
        return;
    }

    //    klog<LogLevel::INFO>("KernelStage") << "Querying CPU Information";
    const auto info = Kernel::ProduceCPUInformation();

//    klog<LogLevel::INFO>("CPU") << "Vendor: "
//        << std::string_view(std::data(info.vendor), std::size(info.vendor));
//
//    klog<LogLevel::INFO>("CPU") << "Processor Name: "
//        << std::string_view(std::data(info.processorName), std::size(info.processorName));
    const auto is_bochs = std::string_view(info.processorName.data()) == "EMU_BOCHS";

    Serial::COMPort com1{Serial::PortName::COM1};
    if (!is_bochs)
        g_textModeBufferView.setHostVMCOMPort(&com1);

    kputs("Copyright (C) 2021 - 2022 Tristan Gerritsen\n");
    kputs("All Rights Reserved\n");

    klog<LogLevel::DEBUG>("Interrupts") << "Loading IDT...";
    Kernel::LoadInterruptDescriptorTable();

    klog<LogLevel::DEBUG>("Multiboot") << "multiboot_information_ptr: " << reinterpret_cast<std::uint64_t>(multiboot_information_ptr);

    //
    // Mapping the Multiboot Information Structure.
    //
    // At this point, we don't have information about the structure of the
    // memory. This is because the multiboot information hasn't been
    // retrieved. Therefore we cannot use the MemoryManager to map the
    // structure.
    //
    // To solve this, we use early pages to identity map the multiboot
    // information pointer.
    //

    // TODO we should probably zero out the early pages of the PFA.
    PageFrameAllocator::earlyIdentityMapPage(multiboot_information_ptr);

    // The page is now mapped, and we can use the pointer to initialize
    // this structure:
    multiboot2::MultibootInformation multiboot_info{multiboot_information_ptr};

    // We've mapped the first page of the multiboot information, but now we need
    // to map the whole thing!
    for (auto address = page_floor(std::size_t(multiboot_information_ptr) + kPageSize);
            address < page_ceil(std::size_t(multiboot_information_ptr) + multiboot_info.size());
            address += kPageSize) {
        PageFrameAllocator::earlyIdentityMapPage(reinterpret_cast<const void *>(address));
    }

    g_textModeBufferView.setPrintToScreenAvailable(multiboot_info.find_tag<kernel::multiboot2::FramebufferInfoTag>() != nullptr);
    g_textModeBufferView.enable_print_to_screen();

    klog<LogLevel::DEBUG>("Kernel") << "Begin=" << std::size_t(&kernel_section_begin) << " End=" << std::size_t(&kernel_section_end);
    klog<LogLevel::DEBUG>("Stack") << "Bottom @ " << std::size_t(&stack_bottom) << " top @ " << std::size_t(&stack_top);

    klog<LogLevel::DEBUG>("KernelEntry") << "Begin: " << reinterpret_cast<std::uint64_t>(&kernel_section_begin)
            << " End: " << reinterpret_cast<std::uint64_t>(&kernel_section_end);

    klog<LogLevel::DEBUG>("Table") << "paging_pml4 " << reinterpret_cast<std::uint64_t>(&paging_pml4);
    klog<LogLevel::DEBUG>("Table") << "paging_pdp_first " << reinterpret_cast<std::uint64_t>(&paging_pdp_first);
    klog<LogLevel::DEBUG>("Table") << "paging_pd_first " << reinterpret_cast<std::uint64_t>(&paging_pd_first);
    klog<LogLevel::DEBUG>("Table") << "paging_pt_first " << reinterpret_cast<std::uint64_t>(&paging_pt_first);

    // Temp disable the "print to screen" functionality to save time
    g_textModeBufferView.disable_print_to_screen();
    multiboot_info.dump_all();
    kernel::symbolizer::set_multiboot_tag(multiboot_info.find_tag<kernel::multiboot2::ELFSymbolsTag>());
    g_textModeBufferView.enable_print_to_screen();

    const auto *efiMemoryMapTag = multiboot_info.find_tag<multiboot2::EFIMemoryMapTag>();
    if (efiMemoryMapTag != nullptr) {
        uefi::MemoryMap memoryMap{};
        memoryMap.size = efiMemoryMapTag->tag.size - sizeof(*efiMemoryMapTag);
        memoryMap.map = reinterpret_cast<const uefi::EFIMemoryDescriptor *>(&efiMemoryMapTag->efiMemoryMap);
        memoryMap.descriptorSize = efiMemoryMapTag->descriptor_size;
        memoryMap.descriptorVersion = efiMemoryMapTag->descriptor_version;
        MemoryMapRegionManager::initialize(memoryMap);
    } else {
        auto *memory_map_tag = multiboot_info.find_tag<kernel::multiboot2::MemoryMapTag>();
        CHECK(memory_map_tag != nullptr);
        MemoryMapRegionManager::initialize(*memory_map_tag);
    }

    const MemoryMapRegion *biggest_region{};
    MemoryMapRegionManager::forEachRegion(core::lambda([&] (const MemoryMapRegion &region) {
        if (region.type() == kernel::MemoryMapRegion::Type::AVAILABLE) {
            if (!biggest_region || biggest_region->size() < region.size())
                biggest_region = &region;
        }
    }));

    CHECK(biggest_region != nullptr);
    klog<LogLevel::DEBUG>("Memory") << "Biggest region is of size " << biggest_region->size();

    klog<LogLevel::DEBUG>("PageFrameAllocatorInit") << "Calculating block";
    auto *pfa_block_begin = static_cast<std::uint8_t *>(const_cast<void *>(biggest_region->begin()));
    auto pfa_block_size = biggest_region->size();
    auto *pfa_block_end = pfa_block_begin + pfa_block_size;

//    klog<LogLevel::DEBUG>("PFA") << "B=" << reinterpret_cast<std::uint64_t>(pfa_block_begin)
//            << " E=" << reinterpret_cast<std::uint64_t>(pfa_block_end);

    struct BlockedRegion {
        std::string_view name;
        const std::uint8_t *begin;
        std::uint8_t *end;
    };

    const std::array<BlockedRegion, 2> blocked_regions{{
            {"KernelImage", reinterpret_cast<const std::uint8_t *>(&kernel_section_begin), reinterpret_cast<std::uint8_t *>(&kernel_section_end)},
            {"Multiboot2 Information", multiboot_info.ptr(), const_cast<std::uint8_t *>(multiboot_info.ptr() + multiboot_info.size())}
    }};

    for (const auto &blocked_region : blocked_regions) {
        klog<LogLevel::DEBUG>("BlockedRegion") << blocked_region.name << " " << std::size_t(blocked_region.begin)
                << " " << std::size_t(blocked_region.end);
        if (blocked_region.begin > pfa_block_begin && blocked_region.end < pfa_block_end) {
            const auto decrease = static_cast<std::size_t>(blocked_region.begin - pfa_block_begin);
            klog<LogLevel::DEBUG>("BlockRegions") << "Region named \"" << blocked_region.name
                    << "\" caused the pfa_block_size to be decreased by " << decrease;
            pfa_block_size -= decrease;
            pfa_block_begin = reinterpret_cast<std::uint8_t *>(blocked_region.end);
        }
    }
//    klog<LogLevel::DEBUG>("KS") << "B=" << reinterpret_cast<std::uint64_t>(&kernel_section_begin)
//            << " E=" << reinterpret_cast<std::uint64_t>(&kernel_section_end);
//    klog<LogLevel::DEBUG>("PFA") << "B=" << reinterpret_cast<std::uint64_t>(pfa_block_begin)
//            << " E=" << reinterpret_cast<std::uint64_t>(pfa_block_end);

    klog<LogLevel::DEBUG>("PageFrameAllocatorInit") << "Initializing";
    kernel::PageFrameAllocator pageFrameAllocator{static_cast<void *>(pfa_block_begin), pfa_block_size};

    klog<LogLevel::DEBUG>("MemoryManager") << "Initializing";
    kernel::MemoryManager memoryManager{&pageFrameAllocator};

    //
    // Make sure the full Multiboot Header is mapped
    //
    const auto mbEnd = page_ceil(core::advance_pointer_with_byte_offset(multiboot_information_ptr, multiboot_info.size()));
    for (auto *page = const_cast<void *>(page_floor(multiboot_information_ptr)); page < mbEnd;
            page = core::advance_pointer_with_byte_offset(page, kPageSize)) {
        memoryManager.map(VirtualAddress{reinterpret_cast<void *>(page)}, page, MemoryManager::MAP_FLAG_NON_WRITABLE);
    }

    //
    // Make sure the ELF header is mapped
    //
    symbolizer::ensureELFHeaderIsMapped();

    //
    // Make sure kernel code is mapped
    //
    for (auto *page = static_cast<void *>(page_floor(&kernel_section_begin)); page != page_ceil(&kernel_section_end);
            page = core::advance_pointer_with_byte_offset(page, kPageSize)) {
        std::size_t flags{};

        if (page >= &kernel_code_begin && page <= &kernel_code_end)
            ; // TODO set NX to false, otherwise true

        if (page >= &kernel_readonly_begin && page <= &kernel_readonly_end)
            flags |= MemoryManager::MAP_FLAG_NON_WRITABLE;

        memoryManager.map(VirtualAddress{page}, page, flags);
    }

    //
    // From here on, paging is set up and mapping is available.
    //

    //
    // UEFI
    //
    prepareUEFI(multiboot_info);

    //
    // ACPI
    //
    auto acpiInitializationResult = prepareACPI(multiboot_info);

    //
    // Self Tests
    //
    runSelfTests();

    //
    // Interrupt Controllers
    //
    if (g_acpiData.madt == nullptr) {
        klog<LogLevel::DEBUG>("8259 PIC") << "Initializing...";
        PIC8259 pic8259{};
        pic8259.initialize(0x50, 0x58);
    } else {
        klog<LogLevel::DEBUG>("APIC") << "Initializing...";
        asm("cli");
        PIC8259 pic8259{};
        pic8259.disable();

        APIC apic{};
        VERIFY(apic.initialize(g_acpiData.madt));
    }

    //
    // PCI
    //
    initializePCI(acpiInitializationResult);

    if (!g_acpiData.fadt || g_acpiData.fadt->bootArchitectureFlags.has8042PS2Controller) {
        device::input::ps2::PS2Controller ps2Controller{};
        if (ps2Controller.isDualChannel())
            klog<LogLevel::DEBUG>("ignore") << "PS/2 Controller is dual channel";
    } else
        klog<LogLevel::WARNING>("PS/2") << "Not available!";

    BREAKPOINT();
}
