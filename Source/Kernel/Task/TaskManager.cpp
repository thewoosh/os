#define AMD64_SHOW_GDT_ENTRY_STREAM_INSERTION_OPERATOR

#include "Kernel/Task/TaskManager.hpp"
#include "Source/Implementation/amd64/TSS.hpp"

extern "C"
void flush_tss();

namespace kernel {

    void
    TaskManager::launch() {
        setup_gdt();
        setup_tss();
    }

    extern "C" struct [[gnu::packed]] {
        std::uint32_t length;
        void *pointer;
    } gdt64_pointer;

    void
    TaskManager::setup_gdt() {
        m_gdt[0] = {};

        m_gdt[k_ring_0_code_index] = {
                .limit_low = 0x1000,
                .base_low = 0x0,
                .base_high = 0x0,
                .accessed = false,
                .read_write = false,
                .conforming_expand_down = false,
                .is_code = true,
                .is_code_or_data_segment = true,
                .descriptor_privilege_level = PrivilegeLevel::RING_0,
                .present = true,
                .limit_high = 0,
                .available = false,
                .long_mode = true,
                .big = false,
                .use_4k_page_addressing = false,
                .base_high_2 = 0
        };

        m_gdt[k_ring_0_data_index] = {
                .limit_low = 0x0,
                .base_low = 0x0,
                .base_high = 0x0,
                .accessed = false,
                .read_write = false,
                .conforming_expand_down = false,
                .is_code = true,
                .is_code_or_data_segment = true,
                .descriptor_privilege_level = PrivilegeLevel::RING_0,
                .present = true,
                .limit_high = 0,
                .available = false,
                .long_mode = true,
                .big = false,
                .use_4k_page_addressing = false,
                .base_high_2 = 0
        };

        m_gdt[k_ring_3_code_index] = {
                .limit_low = 0xFFFF,
                .base_low = 0,
                .accessed = false,
                .read_write = true,
                .conforming_expand_down = false,
                .is_code = true,
                .is_code_or_data_segment = true,
                .descriptor_privilege_level = PrivilegeLevel::RING_3,
                .present = true,
                .limit_high = 0xF,
                .available = true,
                .long_mode = true,
                .big = false,
                .use_4k_page_addressing = false,
                .base_high_2 = 0,
        };

        m_gdt[k_ring_3_data_index] = m_gdt[k_ring_3_code_index];
        m_gdt[k_ring_3_data_index].is_code = false;

        gdt64_pointer.length = sizeof(m_gdt);
        gdt64_pointer.pointer = m_gdt.data();

        klog<LogLevel::DEBUG>("Task Manager") << "If you see this, the GDT is still OK!";
    }

    void
    TaskManager::setup_tss() {
//        flush_tss();
    }

} // namespace kernel
