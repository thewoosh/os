#define SHOW_PRINT_OVERLOADS
//#define PAGE_HIERARCHY_TRAVERSER_WITH_DEBUGGING
#include "Kernel/Memory/PageFrameAllocator.hpp"

#include <Core/Pointer.hpp>
#include <Core/StackVector.hpp>

#include "Kernel/Assert.hpp"
#include "Kernel/Memory/MemoryManager.hpp"
#include "Kernel/Memory/Page.hpp"
#include "Kernel/Memory/PageDirectoryTable.hpp"
#include "Kernel/Memory/PageHierarchyTraverser.hpp"
#include "Kernel/Memory/VirtualAddress.hpp"
#include "Kernel/Runtime/Crash.hpp"

//#define PAGE_FRAME_ALLOCATOR_DEBUG
//#define PAGE_FRAME_ALLOCATOR_DEBUG_ALLOCATION_BLOCK_INSTALLATION
//#define PAGE_FRAME_ALLOCATOR_DEBUG_CONSTRUCTOR
//#define PAGE_FRAME_ALLOCATOR_ALLOCATE
//#define PAGE_FRAME_ALLOCATOR_SET_BITMAP
//#define PAGE_FRAME_ALLOCATOR_GET_BITMAP
//#define PAGE_FRAME_ALLOCATOR_FREE_PAGE
//#define PAGE_FRAME_ALLOCATOR_PRE_MAP_WITH_DEBUGGING
//#define PAGE_FRAME_ALLOCATOR_DEBUG_EARLY_PAGE_ALLOCATOR
//#define PAGE_FRAME_ALLOCATOR_DEBUG_UNMAP_EXTRANEOUS_PAGES_FROM_THE_BEGINNING
//#define PAGE_FRAME_ALLOCATOR_LOCK_PAGE

#ifdef PAGE_FRAME_ALLOCATOR_DEBUG
#include "Kernel/Print.hpp"
#endif // PAGE_FRAME_ALLOCATOR_DEBUG

namespace kernel {

    extern "C" PageDirectoryTable paging_pml4;
    extern "C" PageDirectoryTable paging_pdp_first;
    extern "C" PageDirectoryTable paging_pd_first;
    extern "C" PageDirectoryTable paging_pt_first;
    extern "C" PageDirectoryTable paging_pdp_allocation_block;
    extern "C" PageDirectoryTable paging_pd_allocation_block;
    extern "C" PageDirectoryTable paging_pt_allocation_block;
    extern "C" const std::uint8_t stack_bottom;

    // FIXME: this should be solved by the prekernel, which sets the PFA
    //        correctly, because then the paging isn't already enabled.
    [[gnu::aligned(4096)]] static core::StackVector<std::array<std::uint8_t, kPageSize>, 10> s_earlyPages{};

    // This function makes sure that in the early page frame allocator (of the
    // constructor) can use those pages at the beginning.
    void
    install_allocation_block(void *address) {
#ifdef PAGE_FRAME_ALLOCATOR_DEBUG_ALLOCATION_BLOCK_INSTALLATION
        klog<LogLevel::DEBUG>("PFA/IAB/Pre") << "paging_pml4 @ " << reinterpret_cast<std::uint64_t>(&paging_pml4);
        klog<LogLevel::DEBUG>("PFA/IAB/Pre") << "paging_pdp_first @ " << reinterpret_cast<std::uint64_t>(&paging_pdp_first);
        klog<LogLevel::DEBUG>("PFA/IAB/Pre") << "paging_pd_first @ " << reinterpret_cast<std::uint64_t>(&paging_pd_first);
        klog<LogLevel::DEBUG>("PFA/IAB/Pre") << "paging_pt_first @ " << reinterpret_cast<std::uint64_t>(&paging_pt_first);
        klog<LogLevel::DEBUG>("PFA/IAB/Pre") << "paging_pdp_allocation_block @ " << reinterpret_cast<std::uint64_t>(&paging_pdp_allocation_block);
        klog<LogLevel::DEBUG>("PFA/IAB/Pre") << "paging_pd_allocation_block @ " << reinterpret_cast<std::uint64_t>(&paging_pd_allocation_block);
        klog<LogLevel::DEBUG>("PFA/IAB/Pre") << "paging_pt_allocation_block @ " << reinterpret_cast<std::uint64_t>(&paging_pt_allocation_block);
        klog<LogLevel::DEBUG>("PFA/IAB/Pre") << "stack_bottom @ " << reinterpret_cast<std::uint64_t>(&stack_bottom);
#endif

        VirtualAddress virtual_address{address};
#ifdef PAGE_FRAME_ALLOCATOR_DEBUG_ALLOCATION_BLOCK_INSTALLATION
        klog<LogLevel::DEBUG>("PFA/IAB") << "Address: PML4E=" << virtual_address.pml4e()
                                         << " PDPE=" << virtual_address.pdpe() << " PDE=" << virtual_address.pde()
                                         << " PTE=" << virtual_address.pte() << " PPO="
                                         << virtual_address.physical_page_offset();
#endif

        PageDirectoryTable *pdp;
        {
            auto &pml4_entry = paging_pml4[static_cast<std::uint16_t>(virtual_address.pml4e())];
            if (pml4_entry.address) {
                pdp = reinterpret_cast<decltype(pdp)>(pml4_entry.get());
#ifdef PAGE_FRAME_ALLOCATOR_DEBUG_ALLOCATION_BLOCK_INSTALLATION
                klog<LogLevel::INFO>("PFA/IAB") << "PML4E(PDP) Already Present";
#endif
            } else {
                paging_pdp_allocation_block = {};
                pdp = &paging_pdp_allocation_block;
                pml4_entry.setAddress(reinterpret_cast<std::uint64_t>(&paging_pdp_allocation_block) >> 12);
                pml4_entry.present = true;
                pml4_entry.read_write = true;
#ifdef PAGE_FRAME_ALLOCATOR_DEBUG_ALLOCATION_BLOCK_INSTALLATION
                klog<LogLevel::INFO>("PFA/IAB") << "PML4E(PDP) Newly: " << reinterpret_cast<PageTableEntry &>(pml4_entry)
                        << " pdp=" << reinterpret_cast<std::uint64_t>(pdp);
#endif
            }
        }

        PageDirectoryTable *pd;
        {
            auto &pdp_entry = (*pdp)[static_cast<std::uint16_t>(virtual_address.pdpe())];
            if (pdp_entry.address) {
                pd = reinterpret_cast<decltype(pd)>(pdp_entry.get());
#ifdef PAGE_FRAME_ALLOCATOR_DEBUG_ALLOCATION_BLOCK_INSTALLATION
                klog<LogLevel::WARNING>("PFA/IAB") << "PDPE(PD) Already Present";
#endif
            } else {
                paging_pd_allocation_block = {};
                pd = &paging_pd_allocation_block;
                pdp_entry.setAddress(reinterpret_cast<std::uint64_t>(&paging_pd_allocation_block) >> 12);
                pdp_entry.present = true;
                pdp_entry.read_write = true;
#ifdef PAGE_FRAME_ALLOCATOR_DEBUG_ALLOCATION_BLOCK_INSTALLATION
                klog<LogLevel::INFO>("PFA/IAB") << "PDPE(PD) Newly: " << reinterpret_cast<PageTableEntry &>(pdp_entry)
                        << " pd=" << reinterpret_cast<std::uint64_t>(pd);
#endif
            }
        }

        auto &pd_entry = (*pd)[static_cast<std::uint16_t>(virtual_address.pde())];
        if (!pd_entry.address) {
            pd_entry.setAddress(reinterpret_cast<std::uint64_t>(&paging_pt_allocation_block) >> 12);
            pd_entry.present = true;
            pd_entry.read_write = true;

#ifdef PAGE_FRAME_ALLOCATOR_DEBUG_ALLOCATION_BLOCK_INSTALLATION
            klog<LogLevel::INFO>("PFA/IAB") << "PDE(PT) Newly: " << reinterpret_cast<PageTableEntry &>(pd_entry)
                    << " pt=" << reinterpret_cast<std::uint64_t>(&paging_pt_allocation_block);
#endif

//            for (std::uint16_t i = 0; i < 512; ++i) {
//                auto &page_entry = paging_pt_allocation_block[i];
//                auto currentVirtualAddress = VirtualAddress::from_indices(
//                        static_cast<std::uint16_t>(virtual_address.pml4e()), static_cast<std::uint16_t>(virtual_address.pdpe()),
//                        static_cast<std::uint16_t>(virtual_address.pde()), i, 0);
//                page_entry.setAddress(std::uint64_t(currentVirtualAddress.address()) >> 12);
//                page_entry.present = true;
//                page_entry.read_write = true;
//
//#ifdef PAGE_FRAME_ALLOCATOR_DEBUG_ALLOCATION_BLOCK_INSTALLATION
//                if (i == 0 || i == 511)
//                    klog<LogLevel::DEBUG>("PFA/IAB/PT") << virtual_address.pml4e() << " " << virtual_address.pdpe()
//                        << " " << virtual_address.pde() << " " << i << " = " << reinterpret_cast<const PageTableEntry &>(page_entry);
//#endif
//            }
        } else {
#ifdef PAGE_FRAME_ALLOCATOR_DEBUG_ALLOCATION_BLOCK_INSTALLATION
            klog<LogLevel::WARNING>("PFA/IAB") << "PDE(PT) Already Installed? Address=" << reinterpret_cast<std::uint64_t>(address)
                    << " pd_entry = " << reinterpret_cast<const PageTableEntry &>(pd_entry);
#endif
        }
    }

    // The `install_allocation_block()` identity-maps the first 512 pages of
    // the memory chunk, and we most likely don't need all of them. To make
    // the MemoryManager::map() function work properly, we need to unmap
    // those.
    void
    unmapExtraneousPagesFromTheBeginning(void *chunkBegin, void *freeChunkBegin) {
#ifdef PAGE_FRAME_ALLOCATOR_DEBUG_UNMAP_EXTRANEOUS_PAGES_FROM_THE_BEGINNING
        klog<LogLevel::DEBUG>("PageFrameAllocator")
                << "Unmapping the previously premature mapped pages that we don't need";
#endif // PAGE_FRAME_ALLOCATOR_DEBUG_UNMAP_EXTRANEOUS_PAGES_FROM_THE_BEGINNING

        if (core::advance_pointer_with_byte_offset(chunkBegin, kPageSize) > freeChunkBegin)
            return;

        auto crashingPageAllocator = [] () -> void * {
            CHECK_NOT_REACHED();
            return nullptr;
        };

        VirtualAddress baseVirtualAddress{freeChunkBegin};
        for (std::uint16_t i = 0; i < 512; ++i) {
            auto *pdp = page_hierarchy_traverser::get_pdp(baseVirtualAddress, crashingPageAllocator);
            auto *pd = page_hierarchy_traverser::get_pd(pdp, baseVirtualAddress, crashingPageAllocator);
            auto *pt = page_hierarchy_traverser::get_pt(pd, baseVirtualAddress, crashingPageAllocator);
            auto &pageEntry = pt->operator[](i);
            // Reset.
            pageEntry = {};
        }
    }

    void
    PageFrameAllocator::earlyIdentityMapPage(const void *address) {
        VirtualAddress virtualAddress{const_cast<void *>(address)};

        klog<LogLevel::DEBUG>("PFA (Early)") << "Mapping " << std::size_t(address) << " with indices " <<
                virtualAddress.pml4e() << " " << virtualAddress.pdpe() << " " << virtualAddress.pde() << " " <<
                virtualAddress.pte();

        PageDirectoryTable *pdp;
        {
            auto &pml4_entry = paging_pml4[static_cast<std::uint16_t>(virtualAddress.pml4e())];
            if (!pml4_entry.address) {
                auto &earlyPage = s_earlyPages.emplace_back();
                pml4_entry.setAddress(reinterpret_cast<std::uint64_t>(earlyPage.data()) >> 12);
                pml4_entry.present = true;
                pml4_entry.read_write = true;
            }
            pdp = reinterpret_cast<decltype(pdp)>(pml4_entry.get());
        }

        PageDirectoryTable *pd;
        {
            auto &pdp_entry = (*pdp)[static_cast<std::uint16_t>(virtualAddress.pdpe())];
            if (!pdp_entry.address) {
                auto &earlyPage = s_earlyPages.emplace_back();
                pdp_entry.setAddress(reinterpret_cast<std::uint64_t>(earlyPage.data()) >> 12);
                pdp_entry.present = true;
                pdp_entry.read_write = true;
            }
            pd = reinterpret_cast<decltype(pd)>(pdp_entry.get());
        }

        PageDirectoryTable *pt;
        {
            auto &pd_entry = (*pd)[static_cast<std::uint16_t>(virtualAddress.pde())];
            if (!pd_entry.address) {
                auto &earlyPage = s_earlyPages.emplace_back();
                pd_entry.setAddress(reinterpret_cast<std::uint64_t>(earlyPage.data()) >> 12);
                pd_entry.present = true;
                pd_entry.read_write = true;
            }
            pt = reinterpret_cast<decltype(pt)>(pd_entry.get());
        }

        auto &entry = (*pt)[static_cast<std::uint16_t>(virtualAddress.pte())];
        if (!entry.address) {
            entry.setAddress(reinterpret_cast<std::uint64_t>(address) >> 12);
            entry.present = true;
            entry.read_write = true;
        }
    }

    PageFrameAllocator::PageFrameAllocator(void *in_memory_begin, std::size_t size) {
        const auto memoryBeginCeiled = page_ceil(static_cast<std::uint8_t *>(in_memory_begin));

        //
        // Make sure we can read this block
        //
        install_allocation_block(memoryBeginCeiled);

        auto *memory_begin = memoryBeginCeiled;
        auto *const memory_end = page_floor(static_cast<std::uint8_t *>(in_memory_begin) + size);
        m_chunk_end = memory_end;


#ifdef PAGE_FRAME_ALLOCATOR_DEBUG_CONSTRUCTOR
        klog<LogLevel::DEBUG>("PageFrameAllocator") << "InMemBegin: " << reinterpret_cast<std::uint64_t>(in_memory_begin)
                        << " InMemSize: " << size << " ActualBegin=" << std::uint64_t(memory_begin) << " ActualSize=" << std::uint64_t(memory_end - memory_begin)
                        << " ActualEnd=" << std::uint64_t(memory_end);
#endif

//#ifdef PAGE_FRAME_ALLOCATOR_DEBUG_CONSTRUCTOR
//        klog<LogLevel::DEBUG>("PageFrameAllocator/MemoryCheck") << "Checking Read: "
//                << *reinterpret_cast<std::uint8_t *>(memory_begin);
//#endif
//        *reinterpret_cast<std::uint8_t *>(memory_begin) = 0x69;
//#ifdef PAGE_FRAME_ALLOCATOR_DEBUG_CONSTRUCTOR
//        klog<LogLevel::DEBUG>("PageFrameAllocator/MemoryCheck") << "Checking Write: " << *reinterpret_cast<std::uint16_t *>(memory_begin);
//#endif
//        *reinterpret_cast<std::uint8_t *>(memory_begin) = 0;

        const auto bitset_size = size / 4096;
        auto *bitmap_begin = page_floor(memory_end - bitset_size);
#ifdef PAGE_FRAME_ALLOCATOR_DEBUG_CONSTRUCTOR
        klog<LogLevel::DEBUG>("PFA/bitmap") << "bitset_size=" << bitset_size << " bitmap_begin=" << std::uint64_t(bitmap_begin)
                << " meaning bitmap_end=" << (std::uint64_t(bitmap_begin) + bitset_size);
#endif
        m_extended_bitset = {bitmap_begin, bitset_size};

        auto temporary_page_allocator = [&] () -> void * {
            if (!s_earlyPages.full()) {
#ifdef PAGE_FRAME_ALLOCATOR_DEBUG_EARLY_PAGE_ALLOCATOR
                klog<LogLevel::DEBUG>("PFA/Early") << "Yes! Got an early page: #"
                        << TextModeBufferViewWriter::Decimal << s_earlyPages.size();
#endif
                auto *page = s_earlyPages.emplace_back().data();
                CHECK(isPageAligned(page));
                return page;
            }

            auto *page = memory_begin;
#ifdef PAGE_FRAME_ALLOCATOR_DEBUG_EARLY_PAGE_ALLOCATOR
            klog<LogLevel::DEBUG>("PFA/Early") << "Yes! Got a page from the memory chunk: #"
                    << TextModeBufferViewWriter::Decimal << (std::size_t(memory_begin - page_ceil(static_cast<std::uint8_t *>(in_memory_begin))) / kPageSize)
                    << " starting @ " << TextModeBufferViewWriter::Hexadecimal << std::size_t(page);
#endif

            memory_begin += kPageSize;
            if (memory_begin >= memory_end)
                CRASH("TemporaryPageFrameAllocator allocated too much!");

            size -= kPageSize;
            return page;
        };

        auto temporary_mapper = [&] (VirtualAddress virtual_address, void *physical_address) {
#ifdef PAGE_FRAME_ALLOCATOR_PRE_MAP_WITH_DEBUGGING
            klog<LogLevel::DEBUG>("PageFrameAllocator/TemporaryMapper") << "Mapping " << reinterpret_cast<std::uint64_t>(virtual_address.address())
                    << " to " << reinterpret_cast<std::uint64_t>(physical_address);
#endif // PAGE_FRAME_ALLOCATOR_PRE_MAP_WITH_DEBUGGING

            auto *pdp = page_hierarchy_traverser::get_pdp(virtual_address, temporary_page_allocator);
            auto *pd = page_hierarchy_traverser::get_pd(pdp, virtual_address, temporary_page_allocator);
            auto *pt = page_hierarchy_traverser::get_pt(pd, virtual_address, temporary_page_allocator);

            auto &pe = (*pt)[static_cast<std::uint16_t>(virtual_address.pte())];
            pe.setAddress(reinterpret_cast<std::uint64_t>(physical_address) >> 12);
            pe.present = true;
            pe.present = true;
            pe.read_write = true;

#ifdef PAGE_FRAME_ALLOCATOR_PRE_MAP_WITH_DEBUGGING
            klog<LogLevel::DEBUG>("PageFrameAllocator/TemporaryMapper") << "PTE " << virtual_address.pte()
                    << " @ " << reinterpret_cast<std::uint64_t>(&pe) << " has been set to " << reinterpret_cast<std::uint64_t>(pe.get())
                    << " (internally " << pe.address << ")";
#endif // PAGE_FRAME_ALLOCATOR_PRE_MAP_WITH_DEBUGGING
        };

#ifdef PAGE_FRAME_ALLOCATOR_DEBUG_CONSTRUCTOR
        klog<LogLevel::DEBUG>("PageFrameAllocator") << "IdentityMapping bitmap_begin to bitmap_end";
#endif
        g_textModeBufferView.disable_print_to_screen();
        for (auto *ptr = bitmap_begin; ptr < bitmap_begin + bitset_size; ptr += kPageSize) {
            temporary_mapper(VirtualAddress{ptr}, ptr);
        }
        g_textModeBufferView.enable_print_to_screen();


#ifdef PAGE_FRAME_ALLOCATOR_DEBUG_CONSTRUCTOR
        klog<LogLevel::DEBUG>("PageFrameAllocator") << "memset'ing the bitmap that we just mapped to 0";
        auto table = MemoryManager::find_table(VirtualAddress{bitmap_begin});
        klog<LogLevel::DEBUG>("PFA") << "Table @ " << std::size_t(table) << " val " << *table;
#endif
        std::memset(bitmap_begin, 0, bitset_size);

#ifdef PAGE_FRAME_ALLOCATOR_DEBUG_CONSTRUCTOR
        klog<LogLevel::DEBUG>("PageFrameAllocator") << "Initialization OK, continuing with locking the extended bitset buffer";
#endif // PAGE_FRAME_ALLOCATOR_DEBUG_CONSTRUCTOR
        static_cast<void>(
                lock_pages_size(page_floor(m_extended_bitset.data()), m_extended_bitset.buffer_size())
        );

        /**
         * This function pre-allocates all the page tables (PML4, PDP, PD & PT) so
         * they are always available.
         *
         * FIXME: This function shouldn't be necessary, and should be removed when
         *        on-demand page frame allocation is enabled.
         */
        klog<LogLevel::DEBUG>("PageFrameAllocator") << "Initialize a bunch of PageTables";

        g_textModeBufferView.disable_print_to_screen();
        CHECK(page_floor(memory_begin) == memory_begin);
        CHECK(page_floor(memory_end) == memory_end);

        for (void *page = memory_begin; page < memory_end;
                page = core::advance_pointer_with_byte_offset(page, kPageSize)) {
            VirtualAddress virtual_address{page};
            auto *pdp = page_hierarchy_traverser::get_pdp(virtual_address, temporary_page_allocator);
            auto *pd = page_hierarchy_traverser::get_pd(pdp, virtual_address, temporary_page_allocator);
            auto *pt = page_hierarchy_traverser::get_pt(pd, virtual_address, temporary_page_allocator);
            auto &pte = pt->operator[](virtual_address.pte());
            pte.present = true;
            pte.read_write = true;
            pte.setAddress(std::size_t(virtual_address.address()) >> 12);
        }

        g_textModeBufferView.enable_print_to_screen();

        m_chunk_begin = memory_begin;
        m_free_memory = size;
        m_used_memory = 0;

        unmapExtraneousPagesFromTheBeginning(memoryBeginCeiled, m_chunk_begin);

#ifdef PAGE_FRAME_ALLOCATOR_DEBUG_CONSTRUCTOR
        klog<LogLevel::DEBUG>("PageFrameAllocator") << "Finished initialising! MemoryBegin=" << std::size_t(m_chunk_begin);
#endif // PAGE_FRAME_ALLOCATOR_DEBUG_CONSTRUCTOR
    }

    void *
    PageFrameAllocator::allocate() {
        for (std::size_t index = 0; index < bitmap_count(); ++index) {
            void *address = m_chunk_begin + index * kPageSize;
#ifdef PAGE_FRAME_ALLOCATOR_ALLOCATE_VERBOSE
            klog<LogLevel::DEBUG>("PageFrameAllocator") << "AllocateEntry: " << reinterpret_cast<std::uint64_t>(address)
                    << " which is by index " << index;
#endif
            if (lock_page(address) != LockResult::OK)
                continue;
#ifdef PAGE_FRAME_ALLOCATOR_ALLOCATE
            klog<LogLevel::DEBUG>("PageFrameAllocator") << "Found: " << reinterpret_cast<std::uint64_t>(address)
                    << " which is by index " << index << " * " << kPageSize << " of chunk_begin " << reinterpret_cast<std::uint64_t>(m_chunk_begin);
#endif
//            if (address == reinterpret_cast<void *>(0x200000)) {
//                VirtualAddress virtualAddress{address};
//                auto *pdp = page_hierarchy_traverser::get_pdp(address, [&] { return allocate_or_crash(); });
//                auto *pd = page_hierarchy_traverser::get_pd(pdp, address, [&] { return allocate_or_crash(); });
//                auto *pt = page_hierarchy_traverser::get_pt(pd, address, [&] { return allocate_or_crash(); });
//                klog<LogLevel::DEBUG>("0x2000000000") << "pt[" << virtualAddress.pte() << "] " <<
//                        reinterpret_cast<const PageTableEntry &>(pt->operator[](virtualAddress.pte()));
//            }

            return address;
        }

        klog<LogLevel::WARNING>("PageFrameAllocator") << "No space left; nullptr";
        return nullptr;
    }

    void *
    PageFrameAllocator::allocate_or_crash() {
        if (auto *address = allocate())
            return address;
        CRASH("No free pages left!");
    }

    void
    PageFrameAllocator::free_page(void *address) {
        const auto index = calculate_index(address);
        if (!get_bitmap(index)) {
#ifdef PAGE_FRAME_ALLOCATOR_FREE_PAGE
            klog<LogLevel::WARNING>("PageFrameAllocator") << "Page starting @ " << std::size_t(address) << " indexed by "
                    << index << " of " << bitmap_count() << " not allocated, but tried to free";
#endif
            return;
        }
#ifdef PAGE_FRAME_ALLOCATOR_FREE_PAGE
        klog<LogLevel::DEBUG>("PageFrameAllocator") << "Free page starting @ " << std::size_t(address);
#endif

        set_bitmap(index, false);
        m_free_memory += kPageSize;
        m_used_memory -= kPageSize;
    }

    PageFrameAllocator::LockResult
    PageFrameAllocator::lock_page(void *address) {
        if (static_cast<decltype(m_chunk_begin)>(address) < m_chunk_begin) {
#ifdef PAGE_FRAME_ALLOCATOR_LOCK_PAGE
            klog<LogLevel::WARNING>(__PRETTY_FUNCTION__) << "Address too low: " << std::uint64_t(address);
#endif
            return LockResult::INPUT_ADDRESS_TOO_LOW;
        }
        if (static_cast<decltype(m_chunk_end)>(address) > m_chunk_end) {
#ifdef PAGE_FRAME_ALLOCATOR_LOCK_PAGE
            klog<LogLevel::WARNING>(__PRETTY_FUNCTION__) << "Address too high: " << std::uint64_t(address);
#endif
            return LockResult::INPUT_ADDRESS_TOO_HIGH;
        }

//        klog<LogLevel::DEBUG>("PageFrameAllocator") << "Locking page " << reinterpret_cast<std::uint64_t>(address);
        const auto index = calculate_index(address);
        if (get_bitmap(index)) {
//#ifdef PAGE_FRAME_ALLOCATOR_LOCK_PAGE
//            g_textModeBufferView.disable_print_to_screen();
//            klog<LogLevel::DEBUG>("PageFrameAllocator") << "Locking page starting @ " << std::size_t(address)
//                    << " indexed by " << index << " of " << bitmap_count() << " refused since it is already locked!";
//            g_textModeBufferView.enable_print_to_screen();
//#endif
            return LockResult::ALREADY_LOCKED;
        }
#ifdef PAGE_FRAME_ALLOCATOR_LOCK_PAGE
        g_textModeBufferView.disable_print_to_screen();
        klog<LogLevel::DEBUG>("PageFrameAllocator") << "Locked page starting @ " << std::size_t(address)
                << " indexed by " << index << " of " << bitmap_count();
        g_textModeBufferView.enable_print_to_screen();
#endif

        set_bitmap(index, true);
        m_free_memory -= kPageSize;
        m_used_memory += kPageSize;

        return LockResult::OK;
    }

    void
    PageFrameAllocator::set_bitmap(std::size_t index, bool value) {
#ifdef PAGE_FRAME_ALLOCATOR_HAS_BEGIN_BITSET
        if (index < m_begin_bitset.count())
            m_begin_bitset.set(index, value);
        m_extended_bitset.set(index - m_begin_bitset.count(), value);
#else
#ifdef PAGE_FRAME_ALLOCATOR_SET_BITMAP
        klog<LogLevel::DEBUG>("set_bitmap") << "index=" << index << " #byte=" << (index / 8) << " #bit=" << (index % 8) << " to " << (value ? "TRUE" : "FALSE")
                << " data_at_byte=" << m_extended_bitset.data()[(index / 8)] << " data_at=" << std::size_t(&m_extended_bitset.data()[(index / 8)]);
#endif
        m_extended_bitset.set(index, value);
#endif
    }

    bool
    PageFrameAllocator::get_bitmap(std::size_t index) const {
#ifdef PAGE_FRAME_ALLOCATOR_HAS_BEGIN_BITSET
        if (index < m_begin_bitset.count())
            return m_begin_bitset[index];
        return m_extended_bitset[index - m_begin_bitset.count()];
#else
#ifdef PAGE_FRAME_ALLOCATOR_GET_BITMAP
        klog<LogLevel::DEBUG>("get_bitmap") << "index=" << index << " #byte=" << (index / 8) << " #bit=" << (index % 8)
                << " data_at_byte=" << m_extended_bitset.data()[(index / 8)] << " data_at=" << std::size_t(&m_extended_bitset.data()[(index / 8)]);
#endif
        return m_extended_bitset[index];
#endif
    }

    std::size_t
    PageFrameAllocator::bitmap_count() const {
        return m_extended_bitset.count()
#ifdef PAGE_FRAME_ALLOCATOR_HAS_BEGIN_BITSET
                + m_begin_bitset.count()
#endif
        ;
    }

} // namespace kernel
