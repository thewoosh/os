#define MEMORY_MAP_IMPLEMENTATION
#include "Kernel/Memory/MemoryMapRegion.hpp"

#include <Core/StackVector.hpp>

#include "Kernel/Assert.hpp"
#include "Kernel/Specifications/UEFI/EFIMemoryDescriptor.hpp"
#include "Kernel/Specifications/UEFI/EFIMemoryType.hpp"
#include "Kernel/Specifications/UEFI/MemoryMap.hpp"
#include "Kernel/Memory/Page.hpp"

namespace kernel {

    static core::StackVector<MemoryMapRegion, 256> s_memoryMapRegions;

    void
    MemoryMapRegionManager::forEachRegion(void callback (const MemoryMapRegion &)) {
        if (s_memoryMapRegions[0].type() != MemoryMapRegion::Type::FIRST_MAP_MAGIC)
            return;

        for (const auto &region : s_memoryMapRegions) {
            if (region.type() == MemoryMapRegion::Type::FIRST_MAP_MAGIC)
                continue;
            callback(region);
        }
    }

    MemoryMapRegion
    MemoryMapRegionManager::getRegion(const void *physicalAddress) {
        if (s_memoryMapRegions[0].type() != MemoryMapRegion::Type::FIRST_MAP_MAGIC)
            return {};

        for (const auto &region : s_memoryMapRegions) {
//            klog<LogLevel::DEBUG>(__PRETTY_FUNCTION__) << "Contains: " << std::size_t(region.begin()) << " to "
//                    << std::size_t(region.end()) << " " << toString(region.type());
//            klog<LogLevel::DEBUG>(__PRETTY_FUNCTION__) << "Contains: " << std::size_t(physicalAddress);
            if (region.contains(physicalAddress)) {
                return region;
            }
        }

        return {};
    }

    void
    MemoryMapRegionManager::initialize(const uefi::MemoryMap &memoryMap) {
        s_memoryMapRegions = {};

        s_memoryMapRegions.push_back(MemoryMapRegion{MemoryMapRegion::Type::FIRST_MAP_MAGIC, nullptr, 0});

        constexpr const MemoryMapRegion nullRegion{MemoryMapRegion::Type::UNUSABLE, nullptr, kPageSize};

        CHECK(!nullRegion.contains(memoryMap.map));
        CHECK(memoryMap.descriptorSize != 0);
        CHECK(memoryMap.size > memoryMap.descriptorSize);

        if (memoryMap.descriptorSize != sizeof(uefi::EFIMemoryDescriptor)) {
            klog<LogLevel::WARNING>("MemoryMapRegionManager") << "Given EFIMemoryMap has a different DescriptorSize than ours!"
                    << " given=" << TextModeBufferViewWriter::Decimal << memoryMap.descriptorSize
                    << " ours=" << sizeof(uefi::EFIMemoryDescriptor) << " givenVersion=" << memoryMap.descriptorVersion;
        }

        for (std::size_t offset{}; offset < memoryMap.size; offset += memoryMap.descriptorSize) {
            const auto &descriptor = *core::advance_pointer_with_byte_offset(memoryMap.map, offset);
            CHECK(descriptor.numberOfPages != 0);

            auto type = MemoryMapRegion::Type::UNKNOWN;
            switch (descriptor.memoryType) {
                case uefi::EFIMemoryType::RESERVED_MEMORY_TYPE:
                    type = MemoryMapRegion::Type::RESERVED;
                    break;
                case uefi::EFIMemoryType::LOADER_CODE:
                    type = MemoryMapRegion::Type::KERNEL_CODE;
                    break;
                case uefi::EFIMemoryType::LOADER_DATA:
                    type = MemoryMapRegion::Type::UEFI_POOL_MEMORY_KERNEL;
                    break;
                case uefi::EFIMemoryType::BOOT_SERVICES_CODE:
                    type = MemoryMapRegion::Type::UEFI_CODE_BOOT_SERVICES;
                    break;
                case uefi::EFIMemoryType::BOOT_SERVICES_DATA:
                    type = MemoryMapRegion::Type::UEFI_POOL_MEMORY_BOOT_SERVICES;
                    break;
                case uefi::EFIMemoryType::RUNTIME_SERVICES_CODE:
                    type = MemoryMapRegion::Type::UEFI_CODE_RUNTIME_SERVICES;
                    break;
                case uefi::EFIMemoryType::RUNTIME_SERVICES_DATA:
                    type = MemoryMapRegion::Type::UEFI_POOL_MEMORY_RUNTIME_SERVICES;
                    break;
                case uefi::EFIMemoryType::CONVENTIONAL_MEMORY:
                    type = MemoryMapRegion::Type::AVAILABLE;
                    break;
                case uefi::EFIMemoryType::UNUSABLE_MEMORY:
                    type = MemoryMapRegion::Type::UNUSABLE;
                    break;
                case uefi::EFIMemoryType::ACPI_RECLAIMABLE_MEMORY:
                    type = MemoryMapRegion::Type::ACPI_RECLAIMABLE;
                    break;
                case uefi::EFIMemoryType::ACPI_MEMORY_NVS:
                    type = MemoryMapRegion::Type::ACPI_NVS;
                    break;
                case uefi::EFIMemoryType::MEMORY_MAPPED_IO:
                    type = MemoryMapRegion::Type::MEMORY_MAPPED_IO;
                    break;
                case uefi::EFIMemoryType::MEMORY_MAPPED_IO_PORT_SPACE:
                    type = MemoryMapRegion::Type::MEMORY_MAPPED_IO_PORT_SPACE;
                    break;
                case uefi::EFIMemoryType::PAL_CODE:
                    type = MemoryMapRegion::Type::FIRMWARE;
                    break;
                case uefi::EFIMemoryType::PERSISTENT_MEMORY:
                    type = MemoryMapRegion::Type::AVAILABLE;
                    break;
                case uefi::EFIMemoryType::UNACCEPTED_MEMORY_TYPE:
                    type = MemoryMapRegion::Type::UNKNOWN;
                    break;
                default:
                    klog<LogLevel::DEBUG>("MemoryMapRegion") << "Unknown EFI_MEMORY_TYPE: " << std::size_t(descriptor.memoryType);
            }

            s_memoryMapRegions.push_back({type, descriptor.physicalStart, descriptor.numberOfPages * 4096});
        }
    }

    void
    MemoryMapRegionManager::initialize(const multiboot2::MemoryMapTag &tag) {
        s_memoryMapRegions = {};
        s_memoryMapRegions.push_back(MemoryMapRegion{MemoryMapRegion::Type::FIRST_MAP_MAGIC, nullptr, 0});

        tag.for_each_entry([&] (const multiboot2::MemoryMapTag::Entry &entry) {
            auto type = MemoryMapRegion::Type::UNKNOWN;
            switch (entry.type) {
                case multiboot2::MemoryMapTag::Entry::Type::AVAILABLE:
                    type = MemoryMapRegion::Type::AVAILABLE;
                    break;
                case multiboot2::MemoryMapTag::Entry::Type::BADRAM:
                    type = MemoryMapRegion::Type::UNUSABLE;
                    break;
                case multiboot2::MemoryMapTag::Entry::Type::ACPI_RECLAIMABLE:
                    type = MemoryMapRegion::Type::ACPI_RECLAIMABLE;
                    break;
                case multiboot2::MemoryMapTag::Entry::Type::PRESERVE_HIBERNATION:
                case multiboot2::MemoryMapTag::Entry::Type::RESERVED:
                    type = MemoryMapRegion::Type::UNKNOWN;
                    break;
            }

            s_memoryMapRegions.push_back({type, reinterpret_cast<void *>(entry.base_addr), entry.length});
        });
    }

} // namespace kernel
