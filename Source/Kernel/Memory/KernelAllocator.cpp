#include <algorithm>
#include <numeric_limits>
#include <type_traits>

#include <cstddef>
#include <cstdint>

#include <Core/IteratorDecision.hpp>
#include <Core/Pointer.hpp>
#include <Kernel/Assert.hpp>
#include <Kernel/Print.hpp>

#include "Kernel/Runtime/Crash.hpp"

#define KERNEL_ALLOCATOR_USE_TAG_MAGICS
//#define KERNEL_ALLOCATOR_DEBUG

using KernelAllocatorMagicType = std::uint64_t;

inline constexpr KernelAllocatorMagicType k_allocator_data_magic = 0x6480648064806480;

#ifdef KERNEL_ALLOCATOR_USE_TAG_MAGICS
inline constexpr KernelAllocatorMagicType k_allocator_tag_magic = 0x0468046804680468;
inline constexpr KernelAllocatorMagicType k_allocator_tag_magic_of_previously_available = 0x1029384761029384;
#endif

struct KernelAllocatorTag {
#ifdef KERNEL_ALLOCATOR_USE_TAG_MAGICS
    KernelAllocatorMagicType magic;
#endif

    std::uint32_t size;
    bool free;

    [[nodiscard]] inline void *
    chunk_begin() {
        return core::advance_pointer_with_byte_offset(static_cast<void *>(this), sizeof(KernelAllocatorTag));
    }

    [[nodiscard]] inline const void *
    chunk_begin() const {
        return core::advance_pointer_with_byte_offset(static_cast<const void *>(this), sizeof(KernelAllocatorTag));
    }
};

static struct {

    std::uint64_t magic;
    void *begin;
    std::size_t size;
    std::size_t used;

    [[nodiscard]] inline void *
    end() const {
        return core::advance_pointer_with_byte_offset(this->begin, this->size);
    }

    [[nodiscard]] inline KernelAllocatorTag *
    first_tag() const {
        if (magic != k_allocator_data_magic)
            return nullptr;

        return static_cast<KernelAllocatorTag *>(begin);
    }

    template <typename Callback>
    inline void
    for_each_chunk(Callback callback) {
        CHECK(magic == k_allocator_data_magic);

        const void *const end = core::advance_pointer_with_byte_offset(begin, size);
        void *position = begin;

        KernelAllocatorTag *previousTag{nullptr};

#ifdef KERNEL_ALLOCATOR_DEBUG
        klog<LogLevel::DEBUG>("KernelAllocator") << "for_each_chunk: Begin=" << std::size_t(begin) << " End=" << std::size_t(end);
#endif

        while (position < end) {
            auto *const tag = static_cast<KernelAllocatorTag *>(position);
#ifdef KERNEL_ALLOCATOR_DEBUG
            klog<LogLevel::DEBUG>("KernelAllocator") << "for_each_chunk: position=" << std::size_t(position)
                    << " chunk_begin=" << std::size_t(tag->chunk_begin())
                    << " chunk_size=" << tag->size
                    << " chunk_end=" << (std::size_t(tag->chunk_begin()) + tag->size)
                    << " free=" << (tag->free ? "true" : "false");
#endif

#ifdef KERNEL_ALLOCATOR_USE_TAG_MAGICS
//#ifdef KERNEL_ALLOCATOR_DEBUG
//            CHECK(tag->magic == k_allocator_tag_magic);
//#else
            if (tag->magic != k_allocator_tag_magic) {
                klog<LogLevel::ERROR>("KernelAllocator") << "for_each_chunk: Tag @ " << std::size_t(tag)
                        << " has invalid magic: " << std::size_t(tag->magic);
                if (previousTag)
                    klog<LogLevel::INFO>("KernelAllocator/InvalidMagic") << "Previous tag chunk @ "
                            << std::size_t(previousTag->chunk_begin()) << " with size " << previousTag->size
                            << " and data " << std::string_view{static_cast<const char *>(previousTag->chunk_begin()), previousTag->size};
                else
                    klog<LogLevel::INFO>("KernelAllocator/InvalidMagic") << "This tag is the first one, so it couldn't "
                            "be caused by a OOB write from the previous tag.";
                KernelAllocatorMagicType valToCheck = k_allocator_tag_magic;
                {
                    auto stream = klog<LogLevel::DEBUG>("StreamBefore");
                    for (auto i = -long(sizeof(k_allocator_tag_magic)); i < 0; i++) {
                        stream << reinterpret_cast<std::uint8_t *>(&tag->magic)[i] << " " << std::string_view{&reinterpret_cast<char *>(&tag->magic)[i], 1} << " ";
                    }
                }
                for (std::size_t i = 0; i < sizeof(k_allocator_tag_magic); ++i) {
                    const auto actual = reinterpret_cast<std::uint8_t *>(&tag->magic)[i];
                    const auto shouldBe = reinterpret_cast<std::uint8_t *>(&valToCheck)[i];
                    if (actual != shouldBe)
                        klog<LogLevel::INFO>("KernelAllocator/InvalidMagic") << "Index " << i << " @ " << std::size_t(&reinterpret_cast<std::uint8_t *>(&tag->magic)[i])
                                << " is incorrect: " << actual
                                << " or " << std::string_view{reinterpret_cast<const char *>(&actual), 1}
                                << " should be " << shouldBe;
                }
                CRASH("Incorrect tag magic! Something is very messed up!");
            }
//#endif // !KERNEL_ALLOCATOR_DEBUG
#endif // KERNEL_ALLOCATOR_USE_TAG_MAGICS

            if (tag->size == 0) {
                // TODO dumb workaround: find out the reason this happens in the first place.
                if (std::size_t(end) - std::size_t(tag)
                > 0x200
                //!= sizeof(KernelAllocatorTag)
                ) {
                    klog<LogLevel::WARNING>("KernelAllocator") << "Chunk has size of 0, chunk @ " << std::size_t(tag)
                            << " WholeAllocationBlock{begin=" << std::size_t(begin) << ", end=" << std::size_t(end) << "}"
                            << " OffsetEnd=";
                }
                break;
            }

            if (core::advance_pointer_with_byte_offset(tag->chunk_begin(), tag->size) > end) {
                klog<LogLevel::WARNING>("KernelAllocator") << "Chunk has size that exceeds the bounds of the whole allocation block."
                        << " ChunkEnd=" << std::uint64_t(core::advance_pointer_with_byte_offset(tag->chunk_begin(), tag->size))
                        << " ChunkSize=" << tag->size
                        << " AllocationBlockEnd=" << std::uint64_t(end)
                        << " AllocationBlockSize=" << this->size;
                return;
            }

#define FOR_EACH_CHUNK_INVOCATION callback(*tag)
            if constexpr (std::is_same_v<decltype(FOR_EACH_CHUNK_INVOCATION), core::IterationDecision>) {
                if (FOR_EACH_CHUNK_INVOCATION == core::IterationDecision::BREAK)
                    break;
            } else {
                FOR_EACH_CHUNK_INVOCATION;
            }

            previousTag = tag;
            position = core::advance_pointer_with_byte_offset(tag->chunk_begin(), tag->size);
        }
    }

} s_allocator_data;

namespace kernel {

    void
    set_allocatable_memory(void *begin, std::size_t size) {
        s_allocator_data = {
                .magic = k_allocator_data_magic,
                .begin = begin,
                .size = size,
        };

        CHECK(size < 0xFFFFFFFF);

        auto *first_tag = s_allocator_data.first_tag();
        CHECK(first_tag != nullptr);
        first_tag->size = static_cast<decltype(KernelAllocatorTag::size)>(size) - sizeof(KernelAllocatorTag);
        first_tag->free = true;

#ifdef KERNEL_ALLOCATOR_USE_TAG_MAGICS
        first_tag->magic = k_allocator_tag_magic;
#endif
    }

} // namespace kernel

static void
combineAdjacentTags(KernelAllocatorTag *targetTag) {
#ifdef KERNEL_ALLOCATOR_DEBUG
    klog<LogLevel::DEBUG>("KernelAllocator") << "combineAdjacentTags: request begin for tag @ " << std::size_t(targetTag);
#endif // KERNEL_ALLOCATOR_DEBUG

    KernelAllocatorTag *farthestLeft = nullptr;
    KernelAllocatorTag *farthestRight = nullptr;
    bool rightHasStopped = false;

    s_allocator_data.for_each_chunk([&] (KernelAllocatorTag &otherTag) {
        if (&otherTag == targetTag)
            return;

        if (!otherTag.free) {
            if (&otherTag < targetTag)
                farthestLeft = nullptr;
            else
                rightHasStopped = true;
            return;
        }

        if (&otherTag < targetTag && !farthestLeft)
            farthestLeft = &otherTag;
        if (!rightHasStopped && &otherTag > targetTag && (!farthestRight || farthestRight < &otherTag))
            farthestRight = &otherTag;
    });

    if (!farthestLeft)
        farthestLeft = targetTag;
    if (!farthestRight)
        farthestRight = targetTag;

    if (farthestLeft == farthestRight) {
#ifdef KERNEL_ALLOCATOR_DEBUG
        klog<LogLevel::DEBUG>("KernelAllocator") << "combineAdjacentTags: not possible because adjacent are both nonfree";
#endif
        return;
    }

    ASSERT(farthestLeft < farthestRight);

#ifdef KERNEL_ALLOCATOR_DEBUG
    const auto previousSize = targetTag->size;

    klog<LogLevel::DEBUG>("KernelAllocator") << "left @ " << std::size_t(farthestLeft) << ", right @ " << std::size_t(farthestRight);
#endif // KERNEL_ALLOCATOR_DEBUG

    const auto start = farthestLeft->chunk_begin();
    const auto end = core::advance_pointer_with_byte_offset(farthestRight->chunk_begin(), farthestRight->size);
    const auto size = static_cast<const std::uint8_t *>(end) - static_cast<const std::uint8_t *>(start);

#ifdef KERNEL_ALLOCATOR_DEBUG
    klog<LogLevel::DEBUG>("KernelAllocator") << "Replacing start @ " << std::size_t(start) << ", end @ " << std::size_t(end);
#endif // KERNEL_ALLOCATOR_DEBUG

    CHECK(size > 0);
    ASSERT(size < std::numeric_limits<decltype(farthestLeft->size)>::max());

#ifdef KERNEL_ALLOCATOR_USE_TAG_MAGICS
    s_allocator_data.for_each_chunk([&] (KernelAllocatorTag &otherTag) {
        if (&otherTag > start && &otherTag < start) {
#ifdef KERNEL_ALLOCATOR_DEBUG
            klog<LogLevel::DEBUG>("KernelAllocator") << "combineAdjacentTags: tag @ " << std::size_t(targetTag) << " made obsolete";
#endif // KERNEL_ALLOCATOR_DEBUG
            ASSERT(otherTag.free);
            otherTag.magic = k_allocator_tag_magic_of_previously_available;
        }
    });
#endif

    farthestLeft->size = static_cast<decltype(farthestLeft->size)>(size);
#ifdef KERNEL_ALLOCATOR_DEBUG
    klog<LogLevel::DEBUG>("KernelAllocator") << "combineAdjacentTags: " << previousSize << " to " << std::size_t(size)
            << " @ " << std::size_t(farthestLeft);
#endif // KERNEL_ALLOCATOR_DEBUG
}

extern "C" void *
malloc(const std::size_t requested_size) {
    if (requested_size == 0)
        return nullptr;

#ifdef KERNEL_ALLOCATOR_DEBUG
    klog<LogLevel::DEBUG>("KernelAllocator") << "malloc(" << requested_size << "): Request Begin";
#endif

#ifdef KERNEL_ALLOCATOR_USE_TAG_MAGICS
    if (s_allocator_data.magic != k_allocator_data_magic) {
        klog<LogLevel::ERROR>("KernelAllocator") << "malloc(" << requested_size << "): Not initialized!";
        return nullptr;
    }
#endif

    if (requested_size > std::numeric_limits<decltype(KernelAllocatorTag::size)>::max()) {
        klog<LogLevel::ERROR>("KernelAllocator") << "malloc(" << requested_size << "): Size is ridiculously large";
        ASSERT(false);
        return nullptr;
    }

    if (requested_size > s_allocator_data.size) {
        klog<LogLevel::ERROR>("KernelAllocator") << "malloc(" << requested_size << "): Size is larger than the whole allocation block";
        return nullptr;
    }

    bool best_tag_finalized = false;
    KernelAllocatorTag *best_tag = nullptr;
    KernelAllocatorTag *tag_after_best_tag = nullptr;
    s_allocator_data.for_each_chunk([&] (const KernelAllocatorTag &chunk_tag) {
        if (best_tag && !tag_after_best_tag)
            tag_after_best_tag = const_cast<KernelAllocatorTag *>(&chunk_tag);

        if (best_tag_finalized)
            return core::IterationDecision::BREAK;

#ifdef KERNEL_ALLOCATOR_DEBUG
        klog<LogLevel::ERROR>("KernelAllocator") << "malloc(" << requested_size << "): Chunk of size " << chunk_tag.size
                << (chunk_tag.free ? " is free" : " is NOT free");
#endif

        if (!chunk_tag.free || chunk_tag.size < requested_size) {
#ifdef KERNEL_ALLOCATOR_DEBUG
            klog<LogLevel::INFO>("KernelAllocator") << "    Chunk is not free / too small: " << (!chunk_tag.free ? "NF" : "FF")
                        << " " << ((chunk_tag.size < requested_size) ? "TS" : "NS");
#endif
            return core::IterationDecision::CONTINUE;
        }

        if (chunk_tag.size == requested_size) {
#ifdef KERNEL_ALLOCATOR_DEBUG
            klog<LogLevel::INFO>("KernelAllocator") << "    Chunk is selected since it is the exact size";
#endif
            best_tag = const_cast<KernelAllocatorTag *>(&chunk_tag);
            best_tag_finalized = true;
            tag_after_best_tag = nullptr;
        } else if (!best_tag || chunk_tag.size < best_tag->size) {
#ifdef KERNEL_ALLOCATOR_DEBUG
            if (!best_tag)
                klog<LogLevel::INFO>("KernelAllocator") << "    Chunk is a candidate since we have no best_tag";
            else if (chunk_tag.size < best_tag->size)
                klog<LogLevel::INFO>("KernelAllocator") << "    Chunk is a candidate since the chunk is smaller than the current best_tag";
#endif
            best_tag = const_cast<KernelAllocatorTag *>(&chunk_tag);
            tag_after_best_tag = nullptr;
        }
#ifdef KERNEL_ALLOCATOR_DEBUG
        else {
            klog<LogLevel::INFO>("KernelAllocator") << "    Chunk is ignored since it isn't better";
        }
#endif

        return core::IterationDecision::CONTINUE;
    });

    if (!best_tag) {
#ifdef KERNEL_ALLOCATOR_DEBUG
        klog<LogLevel::DEBUG>("KernelAllocator") << "malloc(" << requested_size << ") Couldn't find chunk!";
#endif
        return nullptr;
    }

#ifdef KERNEL_ALLOCATOR_DEBUG
    klog<LogLevel::DEBUG>("KernelAllocator") << "malloc(" << requested_size << "): Found chunk starting @ "
            << std::size_t(best_tag->chunk_begin());
#endif

#ifdef KERNEL_ALLOCATOR_USE_TAG_MAGICS
    best_tag->magic = k_allocator_tag_magic;
#endif
    best_tag->free = false;

#ifdef KERNEL_ALLOCATOR_DEBUG
    klog<LogLevel::DEBUG>("KernelAllocator") << "2022-02-13T10:51 tag_after_best_tag @ " << std::uint64_t(tag_after_best_tag);
#endif

    // The size of the tag + the size of the chunk
    std::size_t next_tag_full_size = 0;

    auto *best_tag_chunk_end = core::advance_pointer_with_byte_offset(best_tag->chunk_begin(), requested_size);
    bool install_next_tag{true};
    if (best_tag->size > requested_size + sizeof(KernelAllocatorTag)) {
        next_tag_full_size = best_tag->size - requested_size;
#ifdef KERNEL_ALLOCATOR_DEBUG
        klog<LogLevel::DEBUG>("KernelAllocator") << "Initial next_tag_full_size=" << next_tag_full_size
                << " with: " << best_tag->size << " - " << requested_size;
#endif
    } else
        install_next_tag = false;

    if (tag_after_best_tag) {
        if (!tag_after_best_tag->free) {
            if ((static_cast<const std::uint8_t *>(s_allocator_data.end()) - static_cast<const std::uint8_t *>(best_tag_chunk_end)) <= long(sizeof(KernelAllocatorTag)))
                install_next_tag = false;
        } else {
            const auto *tag_after_best_tag_chunk_end =
                    core::advance_pointer_with_byte_offset(tag_after_best_tag->chunk_begin(), tag_after_best_tag->size);
            const auto *best_tag_cut_off_end = core::advance_pointer_with_byte_offset(best_tag->chunk_begin(), requested_size);
            next_tag_full_size = std::size_t(tag_after_best_tag_chunk_end) - std::size_t(best_tag_cut_off_end);
            if (next_tag_full_size != 0)
                install_next_tag = true;

#ifdef KERNEL_ALLOCATOR_DEBUG
            klog<LogLevel::DEBUG>("KernelAllocator") << "2022-02-12T20:20 next_tag_full_size=" << next_tag_full_size
                    << " => " << std::size_t(tag_after_best_tag_chunk_end) << " - " << std::size_t(best_tag_cut_off_end);
#endif
            tag_after_best_tag->free = false;
            tag_after_best_tag->size = 0x69696969;
#ifdef KERNEL_ALLOCATOR_USE_TAG_MAGICS
            tag_after_best_tag->magic = k_allocator_tag_magic_of_previously_available;
#endif
        }
    }

    if (install_next_tag) {
        if (next_tag_full_size <= sizeof(KernelAllocatorTag)) {
            klog<LogLevel::DEBUG>("KernelAllocator") << "Best Tag: " << std::size_t(best_tag);
            klog<LogLevel::DEBUG>("KernelAllocator") << "Used: " << s_allocator_data.used << " Free: " << s_allocator_data.size - s_allocator_data.used;
            klog<LogLevel::DEBUG>("KernelAllocator") << "FAB:  Begin: " << std::size_t(s_allocator_data.begin) << " End: " << std::size_t(s_allocator_data.end());
        }
        ASSERT(next_tag_full_size > sizeof(KernelAllocatorTag));

#ifdef KERNEL_ALLOCATOR_DEBUG
        klog<LogLevel::DEBUG>("KernelAllocator") << "Installing next tag @ " << std::size_t(best_tag_chunk_end);
#endif
        auto *next_tag = static_cast<KernelAllocatorTag *>(best_tag_chunk_end);
#ifdef KERNEL_ALLOCATOR_USE_TAG_MAGICS
        next_tag->magic = k_allocator_tag_magic;
#endif
        next_tag->free = true;

        best_tag->size = static_cast<decltype(best_tag->size)>(requested_size);

#ifdef KERNEL_ALLOCATOR_DEBUG
        klog<LogLevel::DEBUG>("KernelAllocator") << "next_tag_full_size=" << next_tag_full_size;
#endif
        ASSERT(next_tag_full_size < std::numeric_limits<decltype(next_tag->size)>::max());
        next_tag->size = static_cast<decltype(next_tag->size)>(next_tag_full_size - sizeof(*next_tag));
        ASSERT(next_tag->size != 0);
    }
#ifdef KERNEL_ALLOCATOR_DEBUG
    else
        klog<LogLevel::WARNING>("KernelAllocator") << "Next tag isn't selected";
#endif // KERNEL_ALLOCATOR_DEBUG

#ifdef KERNEL_ALLOCATOR_DEBUG
    klog<LogLevel::DEBUG>("KernelAllocator") << "Malloc Succeeded! Tag @ " << std::size_t(best_tag) << " Chunk @ " << std::size_t(best_tag->chunk_begin());
#endif

    s_allocator_data.used += best_tag->size;
    return best_tag->chunk_begin();
}

extern "C" void
free(void *pointer) {
#ifdef KERNEL_ALLOCATOR_DEBUG
    klog<LogLevel::DEBUG>("KernelAllocator") << "free(" << std::size_t(pointer) << "): Request Begin";
#endif

    if (pointer == nullptr || std::size_t(pointer) < sizeof(KernelAllocatorTag)) {
#ifdef KERNEL_ALLOCATOR_DEBUG
        klog<LogLevel::DEBUG>("KernelAllocator") << "free(" << std::size_t(pointer) << "): nullptr: early return";
#endif
        return;
    }

    if (s_allocator_data.magic != k_allocator_data_magic) {
        klog<LogLevel::ERROR>("KernelAllocator") << "free(" << std::size_t(pointer) << "): Not initialized!";
        return;
    }

    auto tag = core::advance_pointer_with_byte_offset(static_cast<KernelAllocatorTag *>(pointer), -sizeof(KernelAllocatorTag));

    if (tag < s_allocator_data.begin || tag >= s_allocator_data.end()) {
        klog<LogLevel::ERROR>("KernelAllocator") << "free(" << std::size_t(pointer) << "): Out of bounds: ["
                << std::size_t(s_allocator_data.begin) << ", " << std::size_t(s_allocator_data.end()) << "]";
        return;
    }

#ifdef KERNEL_ALLOCATOR_USE_TAG_MAGICS
    if (tag->magic != k_allocator_tag_magic) {
        klog<LogLevel::ERROR>("KernelAllocator") << "free(" << std::size_t(pointer) << "): Invalid magic: "
                << tag->magic;
        return;
    }
#endif

    if (tag->free) {
        klog<LogLevel::ERROR>("KernelAllocator") << "free(" << std::size_t(pointer) << "): Already free";
        CRASH("fucky ou");
        return;
    }

    tag->free = true;
    CHECK(s_allocator_data.used >= tag->size);

    combineAdjacentTags(tag);

#ifdef KERNEL_ALLOCATOR_DEBUG
    klog<LogLevel::DEBUG>("KernelAllocator") << "Free Succeeded!";
#endif
}

void *
operator new(std::size_t size) {
    auto *ptr = malloc(size);
//    klog<LogLevel::CRITICAL>("operator new") << std::size_t(ptr) << " to " << (std::size_t(ptr) + size)
//            << " for " << size << " bytes";

    if (!ptr) {
        klog<LogLevel::CRITICAL>("KernelAllocator") << "Failed to allocate " << size;
        CRASH("operator new() failed");
    }

    return ptr;
}

void *
operator new[](std::size_t size) {
    auto *ptr = malloc(size);
//    klog<LogLevel::CRITICAL>("operator new[]") << std::size_t(ptr) << " to " << (std::size_t(ptr) + size)
//            << " for " << size << " bytes";

    if (!ptr) {
        klog<LogLevel::CRITICAL>("KernelAllocator") << "Failed to allocate " << size;
        CRASH("operator new[]() failed");
    }

    return ptr;
}

void
operator delete(void *pointer) {
    free(pointer);
}

void
operator delete[](void *pointer) {
    free(pointer);
}
