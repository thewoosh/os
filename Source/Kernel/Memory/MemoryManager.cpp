//#define PAGE_HIERARCHY_TRAVERSER_WITH_DEBUGGING
//#define MEMORY_MANAGER_DEBUG
//#define MEMORY_MANAGER_FIND_TABLE_DEBUG
//#define MEMORY_MANAGER_DEBUG_ALLOCATE
//#define MEMORY_MANAGER_ENABLE_NULLPTR_TEST
//#define MEMORY_MANAGER_MAP

#define SHOW_PRINT_OVERLOADS
#include "Kernel/Memory/MemoryManager.hpp"

#include <array>

#include <cstdint>

#include <Core/Pointer.hpp>

#include "Kernel/Assert.hpp"
#include "Kernel/CPU/CPU.hpp"
#include "Kernel/Runtime/Crash.hpp"
#include "Kernel/Runtime/Debug.hpp"
#include "Kernel/Memory/Alloc.hpp"
#include "Kernel/Memory/MemoryMapRegion.hpp"
#include "Kernel/Memory/PageFrameAllocator.hpp"
#include "Kernel/Memory/PageHierarchyTraverser.hpp"
#include "Kernel/Memory/PageTableEntry.hpp"
#include "Kernel/Memory/PageDirectoryTable.hpp"
#include "Kernel/Memory/PageDirectoryPointerTable.hpp"
#include "Kernel/Memory/SIUnits.hpp"
#include "Kernel/Memory/VirtualAddress.hpp"
#include "Kernel/Print.hpp"
#include "Kernel/Specifications/Multiboot/Tags.hpp"
#include "Kernel/Memory/VirtualAddress.hpp"

namespace kernel {

    MemoryManager *MemoryManager::s_instance;

    extern "C" void *paging_pml4;
    extern "C" void *paging_pdp_first;
    extern "C" void *paging_pd_first;
    extern "C" PageDirectoryTable paging_pt_first;

    inline constexpr std::size_t k_kernel_allocator_chunk_size = 3 * MiB;

    MemoryManager::MemoryManager(PageFrameAllocator *pageFrameAllocator)
            : m_pageFrameAllocator(pageFrameAllocator) {
        s_instance = this;

        //
        // Mark the NULL-page as illegal.
        //

        auto *table = find_table(VirtualAddress::null());
#ifdef MEMORY_MANAGER_ENABLE_NULLPTR_TEST
        klog<LogLevel::DEBUG>("Table") << "for nullptr is " << reinterpret_cast<std::uint64_t>(table);
        klog<LogLevel::DEBUG>("Table") << "paging_pml4 " << reinterpret_cast<std::uint64_t>(paging_pml4);
        klog<LogLevel::DEBUG>("Table") << "paging_pdp_first " << reinterpret_cast<std::uint64_t>(paging_pdp_first);
        klog<LogLevel::DEBUG>("Table") << "paging_pd_first " << reinterpret_cast<std::uint64_t>(paging_pd_first);
        klog<LogLevel::DEBUG>("Table") << "paging_pt_first " << reinterpret_cast<std::uint64_t>(&paging_pt_first);

        klog<LogLevel::DEBUG>("Table") << "paging_pml4 " << reinterpret_cast<std::uint64_t>(&paging_pml4);
        klog<LogLevel::DEBUG>("Table") << "paging_pdp_first " << reinterpret_cast<std::uint64_t>(&paging_pdp_first);
        klog<LogLevel::DEBUG>("Table") << "paging_pd_first " << reinterpret_cast<std::uint64_t>(&paging_pd_first);
        klog<LogLevel::DEBUG>("Table") << "paging_pt_first " << reinterpret_cast<std::uint64_t>(&paging_pt_first);
#endif
        if (reinterpret_cast<std::uint64_t>(table) <= 0x100)
            CRASH("static null/zero table is nullptr according to the PML map");

#ifdef MEMORY_MANAGER_ENABLE_NULLPTR_TEST
        *reinterpret_cast<std::uint8_t *>(table->get()) = 0x69;
        klog<LogLevel::DEBUG>("Table2") << "nullptr = " << reinterpret_cast<std::uint64_t>(table->get());
        klog<LogLevel::DEBUG>("Table2") << "*nullptr = " << *reinterpret_cast<std::uint8_t *>(table->get());
        klog<LogLevel::DEBUG>("Table2") << "nullptr = " << *reinterpret_cast<PageTableEntry *>(table);
#endif

        table->accessed = false;
        cpu::write_cr3(cpu::read_cr3());
        cpu::native_flush_tlb_single(0x0);

        //
        // This piece of code doesn't work because it triggers an immediate PF
        // for address 0x28 :(
        //
//        table->present = false;
//        write_cr3(read_cr3());
//        native_flush_tlb_single(0x0);

        klog<LogLevel::DEBUG>("MemoryManager") << "Allocating Mapped Chunk for KernelAllocator";
        set_allocatable_memory(allocate_or_crash(k_kernel_allocator_chunk_size / kPageSize),
                               k_kernel_allocator_chunk_size);
    }

    void *
    MemoryManager::allocate_or_crash(std::size_t number_of_pages) {
        if (number_of_pages == 0)
            return nullptr;

        void *begin = nullptr;
        for (std::size_t i = 0; i < number_of_pages; ++i) {
#ifdef MEMORY_MANAGER_DEBUG_ALLOCATE
            klog<LogLevel::DEBUG>("MemoryAllocator") << "Alloc " << i << " of " << number_of_pages;
#endif
            auto *page_begin = m_pageFrameAllocator->allocate_or_crash();
            if (i == 0)
                begin = page_begin;
            map(VirtualAddress{page_begin}, page_begin);
        }

        return begin;
    }

    void
    MemoryManager::free_pages(void *begin, std::size_t number_of_consecutive_pages) {
        m_pageFrameAllocator->free_pages_count(begin, number_of_consecutive_pages);
    }

    // NOTE: this function might be used in stacktraces for early kernel crashes
    //       (page faults), so we can't use the PageFrameAllocator/MemoryManager.
    SomePageTableEntry<void> *
    MemoryManager::find_table(VirtualAddress virtual_address) {
#if defined(MEMORY_MANAGER_FIND_TABLE_DEBUG) || defined(MEMORY_MANAGER_DEBUG)
        klog<LogLevel::DEBUG>(__PRETTY_FUNCTION__) << "";
#endif
        PageDirectoryTable *pdp;
        {
            auto &pde = PageDirectoryPointerTable::the()[static_cast<std::uint16_t>(virtual_address.pml4e())];
#if defined(MEMORY_MANAGER_FIND_TABLE_DEBUG) || defined(MEMORY_MANAGER_DEBUG)
            klog<LogLevel::DEBUG>(">") << "pde: " << reinterpret_cast<PageTableEntry &>(pde)
                    << " @ " << reinterpret_cast<std::uint64_t>(&pde);
#endif
            if (!pde.address)
                return reinterpret_cast<SomePageTableEntry<void> *>(0x10);
            if (!pde.present)
                return reinterpret_cast<SomePageTableEntry<void> *>(0x11);
            pdp = pde.get();
        }

        PageDirectoryTable *pd;
        {
            auto &pde = (*pdp)[static_cast<std::uint16_t>(virtual_address.pdpe())];
#if defined(MEMORY_MANAGER_FIND_TABLE_DEBUG) || defined(MEMORY_MANAGER_DEBUG)
            klog<LogLevel::DEBUG>(">") << "pde: " << reinterpret_cast<PageTableEntry &>(pde)
                    << " @ " << reinterpret_cast<std::uint64_t>(&pde);
#endif
            if (!pde.address)
                return reinterpret_cast<SomePageTableEntry<void> *>(0x20);
            if (!pde.present)
                return reinterpret_cast<SomePageTableEntry<void> *>(0x21);
            pd = reinterpret_cast<PageDirectoryTable *>(pde.get());
        }

        PageDirectoryTable *pt;
        {
            auto &pde = (*pd)[static_cast<std::uint16_t>(virtual_address.pde())];
#if defined(MEMORY_MANAGER_FIND_TABLE_DEBUG) || defined(MEMORY_MANAGER_DEBUG)
            klog<LogLevel::DEBUG>(">") << "pde: " << reinterpret_cast<PageTableEntry &>(pde)
                    << " @ " << reinterpret_cast<std::uint64_t>(&pde);
#endif
            if (!pde.address)
                return reinterpret_cast<SomePageTableEntry<void> *>(0x30);
            if (!pde.present)
                return reinterpret_cast<SomePageTableEntry<void> *>(0x31);
            pt = reinterpret_cast<PageDirectoryTable *>(pde.get());
        }

        return reinterpret_cast<SomePageTableEntry<void> *>(
                &(*pt)[static_cast<std::uint16_t>(virtual_address.pte())]
        );
    }

    PageDirectoryTable *
    MemoryManager::get_pdp(VirtualAddress virtual_address) {
        return page_hierarchy_traverser::get_pdp(virtual_address, [&] () {
            return m_pageFrameAllocator->allocate_or_crash();
        });
    }

    PageDirectoryTable *
    MemoryManager::get_pd(PageDirectoryTable *pdp, VirtualAddress virtual_address) {
        return page_hierarchy_traverser::get_pd(pdp, virtual_address, [&] () {
            return m_pageFrameAllocator->allocate_or_crash();
        });
    }

    PageDirectoryTable *
    MemoryManager::get_pt(PageDirectoryTable *pd, VirtualAddress virtual_address) {
        return page_hierarchy_traverser::get_pt(pd, virtual_address, [&] () {
            return m_pageFrameAllocator->allocate_or_crash();
        });
    }

    void
    MemoryManager::map(VirtualAddress virtual_address, void *physical_address, std::size_t flags) {
#if !defined(MEMORY_MANAGER_MAP) && !defined(MEMORY_MANAGER_DEBUG)
        if (flags & MAP_FLAG_DEBUG) {
#endif
        klog<LogLevel::DEBUG>("MemoryManager") << "Mapping "
                                               << reinterpret_cast<std::uint64_t>(virtual_address.address())
                                               << " to " << reinterpret_cast<std::uint64_t>(physical_address)
                                               << " ind " << virtual_address.pml4e() << " " << virtual_address.pdpe() << " " << virtual_address.pde()
                                                          << " " << virtual_address.pte() << " " << virtual_address.physical_page_offset();
#if !defined(MEMORY_MANAGER_MAP) && !defined(MEMORY_MANAGER_DEBUG)
        }
#endif
        auto *pdp = get_pdp(virtual_address);
        auto *pd = get_pd(pdp, virtual_address);
        auto *pt = get_pt(pd, virtual_address);

        auto &pe = (*pt)[static_cast<std::uint16_t>(virtual_address.pte())];
#if defined(MEMORY_MANAGER_MAP) || defined(MEMORY_MANAGER_DEBUG)
        if (pe.address != 0)
            klog<LogLevel::INFO>("MemoryManager") << "map @ " << std::uint64_t(virtual_address.address()) <<
                    " was already " << std::uint64_t(pe.address << 12) << " => " << std::size_t(pe.get())
                    << " indices " << virtual_address.pml4e() << " " << virtual_address.pdpe() << " " << virtual_address.pde()
                    << " " << virtual_address.pte() << " " << virtual_address.physical_page_offset();
#endif

        CHECK(pe.address == 0 || (pe.address << 12) == std::uint64_t(virtual_address.address()));

        pe.setAddress(reinterpret_cast<std::uint64_t>(physical_address) >> 12);
        pe.present = true;
        pe.read_write = !(flags & MAP_FLAG_NON_WRITABLE);

        pe.cache_disabled = flags & MAP_FLAG_CACHE_DISABLED;

#if !defined(MEMORY_MANAGER_MAP) && !defined(MEMORY_MANAGER_DEBUG)
        if (flags & MAP_FLAG_DEBUG) {
#endif
        klog<LogLevel::DEBUG>(__PRETTY_FUNCTION__) << "PTE " << virtual_address.pte()
                << " @ " << reinterpret_cast<std::uint64_t>(&pe) << " setting to " << reinterpret_cast<std::uint64_t>(pe.get())
                << " internally " << pe.address;
#if !defined(MEMORY_MANAGER_MAP) && !defined(MEMORY_MANAGER_DEBUG)
        }
#endif
    }

    PageDirectoryPointerTable &
    PageDirectoryPointerTable::the() {
        return *reinterpret_cast<PageDirectoryPointerTable *>(cpu::read_cr3());
    }

    PageTableEntry *
    PageDirectoryPointerTable::find(VirtualAddress address) const {
        if (address.pml4e() != 0 || address.pdpe() >= 512 || address.pde() >= 512 || address.pte() >= 512)
            return nullptr;

        auto &page_directory = m_data[address.pdpe()];
        auto *page_directory_pointer = page_directory.get();
        if (page_directory_pointer == nullptr)
            return nullptr;

#if defined(MEMORY_MANAGER_PAGE_DIRECTORY_POINTER_TABLE_FIND) || defined(MEMORY_MANAGER_DEBUG)
        klog<LogLevel::DEBUG>(__PRETTY_FUNCTION__) << "" << BitPrinter(*reinterpret_cast<std::uint64_t *>(page_directory_pointer));

#endif
        auto &page_table = (*page_directory_pointer)[static_cast<std::uint16_t>(address.pde())];

        auto *page_table_pointer = page_table.get();
        if (page_table_pointer == nullptr)
            return nullptr;

        return reinterpret_cast<PageTableEntry *>(page_table_pointer);
    }

    void
    MemoryManager::prepareMap(VirtualAddress virtualAddress) {
        auto *pdp = get_pdp(virtualAddress);
        auto *pd = get_pd(pdp, virtualAddress);
        auto *pt = get_pt(pd, virtualAddress);

        g_textModeBufferView.disable_print_to_screen();
        klog<LogLevel::DEBUG>("PrepareMap") << "for " << std::size_t(virtualAddress.address())
                << " " << virtualAddress.pml4e()
                << " " << virtualAddress.pdpe()
                << " " << virtualAddress.pde()
                << " " << virtualAddress.pte();
        g_textModeBufferView.enable_print_to_screen();

        static_cast<void>(pt);
    }

} // namespace kernel

void
dump_page_structure() {
    using namespace kernel;
    using namespace kernel::page_hierarchy_traverser;

    for (std::uint16_t pdpt_index = 0; pdpt_index < 512; ++pdpt_index) {
        const auto &pdp = get_pdp_by_index(pdpt_index);

        if (pdpt_index == 0 || pdpt_index == 511 || get_pdp_by_index(pdpt_index - 1).present != pdp.present) {
            if (pdpt_index > 1 && get_pdp_by_index(pdpt_index - 2).present == pdp.present)
                klog<LogLevel::DEBUG>("dump_page_structure") << "...";

            klog<LogLevel::DEBUG>("dump_page_structure") << "PDP " << TextModeBufferViewWriter::Decimal << pdpt_index
                    << TextModeBufferViewWriter::Hexadecimal << ": " << reinterpret_cast<const kernel::PageTableEntry &>(pdp);
        }

        if (!pdp.present || !pdp.address)
            continue;

        for (std::uint16_t pdp_index = 0; pdp_index < 512; ++pdp_index) {
            const auto &pdp_entry = get_pd_by_index(pdpt_index, pdp_index);

            if (pdp_index == 0 || pdp_index == 511 || get_pd_by_index(pdpt_index, pdp_index - 1).present != pdp_entry.present) {
                if (pdp_index > 1 && get_pd_by_index(pdpt_index, pdp_index - 2).present == pdp_entry.present)
                    klog<LogLevel::DEBUG>("dump_page_structure") << "    ...";

                klog<LogLevel::DEBUG>("dump_page_structure") << "    PD " << TextModeBufferViewWriter::Decimal << pdp_index
                        << TextModeBufferViewWriter::Hexadecimal << ": " << reinterpret_cast<const kernel::PageTableEntry &>(pdp_entry);
            }

            if (!pdp_entry.present)
                continue;

            for (std::uint16_t pd_index = 0; pd_index < 512; ++pd_index) {
                const auto &pd_entry = get_pt_by_index(pdpt_index, pdp_index, pd_index);
                const bool pd_entry_previous_presence = get_pt_by_index(pdpt_index, pdp_index, pd_index - 1).present;

                if (pd_index == 0 || pd_index == 511 || pd_entry_previous_presence != pd_entry.present) {
                    if (pd_index > 1 && get_pt_by_index(pdpt_index, pdp_index, pd_index - 2).present == pd_entry_previous_presence)
                        klog<LogLevel::DEBUG>("dump_page_structure") << "        ...";

                    klog<LogLevel::DEBUG>("dump_page_structure") << "        PT " << TextModeBufferViewWriter::Decimal << pd_index
                            << TextModeBufferViewWriter::Hexadecimal << ": " << reinterpret_cast<const kernel::PageTableEntry &>(pd_entry);
                }

                if (!pd_entry.present)
                    continue;

                for (std::uint16_t pt_index = 0; pt_index < 512; ++pt_index) {
                    const auto &pt_entry = get_page_by_index(pdpt_index, pdp_index, pd_index, pt_index);
                    const bool pt_entry_previous_presence = get_pt_by_index(pdpt_index, pdp_index, pt_index - 1).present;

                    if (pt_index == 0 || pt_index == 511 || pt_entry_previous_presence != pt_entry.present) {
                        if (pt_index > 1 && get_page_by_index(pdpt_index, pdp_index, pd_index, pt_index - 2).present == pt_entry_previous_presence)
                            klog<LogLevel::DEBUG>("dump_page_structure") << "            ...";
                        auto virtual_address = VirtualAddress::from_indices(pdpt_index, pdp_index, pd_index, pt_index, 0);

                        klog<LogLevel::DEBUG>("dump_page_structure") << "            Page " << TextModeBufferViewWriter::Decimal << pt_index
                                << TextModeBufferViewWriter::Hexadecimal << ": " << reinterpret_cast<const kernel::PageTableEntry &>(pt_index)
                                << std::uint64_t(virtual_address.address());
                    }
                }
            }
        }
    }
}
