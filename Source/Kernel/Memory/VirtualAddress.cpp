#define SHOW_PRINT_OVERLOADS
#include "Kernel/Memory/VirtualAddress.hpp"

#include "Kernel/Memory/PageTableEntry.hpp"
#include "Kernel/Print.hpp"

namespace kernel {

    constexpr std::uint64_t k_address_mask = 0b1111111111111111111111111111111111111111111111111111000000000000;

    extern "C" std::uint64_t ***paging_pml4[1024];

    [[nodiscard]] static const PageTableEntry *
    get_page_table(const void *table, std::size_t index, std::string_view name, bool debug) {
        if (!table)
            return nullptr;

        auto &entry_ref = reinterpret_cast<const std::uint64_t **>(const_cast<void *>(table))[index];
        auto entry = entry_ref;
        const auto entry_masked = reinterpret_cast<std::uint64_t>(entry) & k_address_mask;
        if (debug) {
            klog<LogLevel::DEBUG>("GetPageTable") << name << ": " << reinterpret_cast<std::uint64_t>(entry)
                << " @ " << reinterpret_cast<std::uint64_t>(&entry_ref)
                << " or " << entry_masked;
            if (entry_masked == 0) {
                klog<LogLevel::CRITICAL>("GetPageTable") << name << " missing: "
                        << *reinterpret_cast<const PageTableEntry *>(&entry);
            } else {
                klog<LogLevel::DEBUG>("GetPageTable") << name << " present: "
                        << *reinterpret_cast<const PageTableEntry *>(&entry);
            }
        }
        return reinterpret_cast<const PageTableEntry *>(entry_masked);
    }

    const PageTableEntry *
    VirtualAddress::pdp_table() const {
        //return reinterpret_cast<const PageTableEntry *>(page_table_l4[pdp_entry()]);
        return get_page_table(paging_pml4, pml4e(), "PDPE", debug);
    }

    const PageTableEntry *
    VirtualAddress::pd_table() const {
        return get_page_table(pdp_table(), pdpe(), "PDE", debug);
    }

    const PageTableEntry *
    VirtualAddress::pt_table() const {
        return get_page_table(pd_table(), pde(), "PTE", debug);
    }

} // namespace kernel
