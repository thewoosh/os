#include "Kernel/Assert.hpp"

#include "Include/Kernel/Runtime/Crash.hpp"
#include "Kernel/Print.hpp"

namespace kernel {

    void
    assert::do_assert(std::string_view type_of_check, std::string_view file, std::size_t line,
                      std::string_view function_name, std::string_view expression) {
        klog<LogLevel::CRITICAL>(type_of_check) << "Failed for: " << expression;
        klog<LogLevel::CRITICAL>(type_of_check) << "  In file " << file << ":" << TextModeBufferViewWriter::Decimal << line;
        klog<LogLevel::CRITICAL>(type_of_check) << "  In function " << function_name;
        CRASH_FROM_ASSERT(type_of_check);
    }

} // namespace kernel
