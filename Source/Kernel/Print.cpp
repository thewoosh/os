/*
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 *
 * Copyright (C) 2021 Tristan Gerritsen
 * All Rights Reserved.
 */

#include "Include/Kernel/Print.hpp"

#include <array>
#include <numeric_limits>

#include "Kernel/Assert.hpp"
#include "Kernel/Serial/COMPort.hpp"

TextModeBufferView<80, 25> g_textModeBufferView;

void
kputhexnumber(std::uint64_t value, HexnumberPrefix prefix) {
    constexpr const std::array digits{
            '0', '1', '2', '3',
            '4', '5', '6', '7',
            '8', '9', 'A', 'B',
            'C', 'D', 'E', 'F'
    };

    if (prefix == HexnumberPrefix::YES) {
        kputchar('0');
        kputchar('x');
    }

    if (value == 0) {
        kputchar('0');
        return;
    }

    int width = 1;
    std::size_t cursor = 0x0F;
    while (value > cursor) {
        ++width;
        cursor *= 0x10;
        cursor += 0x0F;
    }

    while (width-- > 0) {
        kputchar(digits[(value >> (width * 4)) & 0xF]);
    }
}

void
kputdecnumber(std::uint64_t value, std::size_t width) {
    std::size_t n_width = 1;
    std::size_t i = 9;
    while (value > i && i < std::numeric_limits<std::uint64_t>::max()) {
        ++n_width;
        i *= 10;
        i += 9;
    }

    if (width != 0) {
        for (std::size_t printed = 0; n_width + printed < width; ++printed) {
            kputchar('0');
        }
    }

    std::array<char, 128> buffer;
    CHECK(n_width < buffer.size());

    buffer[n_width] = '\0';
    for (i = n_width; i > 0; --i) {
        buffer[i - 1] = static_cast<char>((value % 10) + '0');
        value /= 10;
    }
    kputs(std::string_view{buffer.data(), n_width});
}

TextModeBufferViewWriter &
TextModeBufferViewWriter::operator<<(std::uint64_t value) {
    switch (m_number_mode) {
        case Hexadecimal:
            kputhexnumber(value);
            break;
        case Decimal:
            kputdecnumber(value);
            break;
    }

//    std::array<char, 8> output{};
//
//    std::size_t i{};
//    while (value != 0) {
//        const auto part = value % digits.size();
//        output[i++] = digits[part];
//        value = value / digits.size();
//    }
//
//    bool flag{false};
//    for (i = 8; i > 0; --i) {
//        const auto val = output[i - 1];
//
//        if (!flag && (val == '\0' || val == '0'))
//            continue;
//
//        kputchar(val);
//        flag = true;
//    }
//
//    if (!flag)
//        kputs("(invalid,flag=false)");

    return *this;
}

void
kputchar(char character) {
    g_textModeBufferView.printCharacter(character);

    auto *port = g_textModeBufferView.hostVMCOMPort();
    if (port != nullptr) {
        port->write(Serial::COMLine::DATA, static_cast<uint8_t>(character));
    }
}
