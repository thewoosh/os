/*
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 *
 * Copyright (C) 2021 Tristan Gerritsen
 * All Rights Reserved.
 */

#include "Kernel/CPUInformation.hpp"

namespace Kernel {

    extern "C" void
    cpuid_vendor_identification(char *dest);

    extern "C" void
    cpuid_processor_name(char *dest);

    extern "C" void
    cpuid_get_features(std::uint32_t *ecx, std::uint32_t *edx);

    CPUInformation
    ProduceCPUInformation() noexcept {
        CPUInformation value{};

        cpuid_vendor_identification(std::data(value.vendor));
        cpuid_processor_name(std::data(value.processorName));
        cpuid_get_features(&value.featureECX, &value.featureEDX);

        return value;
    }

} // namespace Kernel
