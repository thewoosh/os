/*
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 *
 * Copyright (C) 2021 Tristan Gerritsen
 * All Rights Reserved.
 */

#include "Include/Kernel/CPU/CPU.hpp"

#include <array>
#include <algorithm>

#include <cstdint>

#include <Kernel/Print.hpp>
#include "Kernel/Runtime/Crash.hpp"
#include "CPU/InterruptHandlers.hpp"
#include "CPU/InterruptNumbers.hpp"

namespace Kernel {

    /**
     * The TrapFrame is a C-style way of accessing the pushed registers.
     */
    using kernel::TrapFrame;

    struct [[gnu::packed]] IDTLocation {
        std::uint16_t  limit;
        std::uint32_t *base;
    };

    struct [[gnu::packed]] InterruptDescriptor {
    private:
        [[nodiscard]]
        InterruptDescriptor(const void *function_ptr, std::uint8_t ist)
                : offset_1(reinterpret_cast<std::uint64_t>(function_ptr) & 0xffff)
                , ist(ist)
                , offset_2((reinterpret_cast<std::uint64_t>(function_ptr) >> 16) & 0xffff)
                , offset_3((reinterpret_cast<std::uint64_t>(function_ptr) >> 32) & 0xffff) {
        }

    public:
        InterruptDescriptor() = default;
        InterruptDescriptor(InterruptDescriptor &&) = default;
        InterruptDescriptor(const InterruptDescriptor &) = default;
        InterruptDescriptor &operator=(InterruptDescriptor &&) = default;
        InterruptDescriptor &operator=(const InterruptDescriptor &) = default;

        InterruptDescriptor(void (function)(const TrapFrame *), std::uint8_t ist)
                : InterruptDescriptor(reinterpret_cast<const void *>(function), ist) {
        }

        InterruptDescriptor(void (function)(const TrapFrame *, kernel::InterruptErrorCode), std::uint8_t ist)
                : InterruptDescriptor(reinterpret_cast<const void *>(function), ist) {
        }

        std::uint16_t offset_1{};

        // Selector in GDT
        std::uint16_t selector{0x08};

        // Interrupt Stack Table offset (TSS)
        std::uint8_t ist{};

        // How to handle the interrupt
        std::uint8_t typeAttribute{0x8e};

        std::uint16_t offset_2{};
        std::uint32_t offset_3{};

        std::uint32_t zero{0};

    };

    std::array<InterruptDescriptor, 256> interruptDescriptorTable{};

    /**
     * The location is the descriptor of the table itself. It describes where
     * the table lives, and how big the table is.
     *
     * The base pointer is set at runtime, by LoadInterruptDescriptorTable().
     */
    IDTLocation location{sizeof(interruptDescriptorTable), nullptr};

    /**
     * External assembly function, defined in:
     *   Source/Implementation/.../InterruptDescriptorTable.asm
     */
    extern "C"
    void LoadIDT(const IDTLocation *);

    void LoadInterruptDescriptorTable() noexcept {
        for (std::size_t i = 0; i < std::size(interruptDescriptorTable); ++i) {
            interruptDescriptorTable[i] = InterruptDescriptor(&KERNEL_IRS_ENTRYPOINT_FUNCTION_NAME(generic), 0);
        }

#define DEFINE_FALLBACK(number) interruptDescriptorTable[number] = InterruptDescriptor(&KERNEL_IRS_ENTRYPOINT_FUNCTION_NAME(fallback_##number), 0);
        ITERATE_FALLBACKS(DEFINE_FALLBACK)

#define REGISTER_GENERIC_INTERRUPT(name) // nothing to do

#define REGISTER_INTERRUPT(name) \
        interruptDescriptorTable[::kernel::interrupt_number::name] = \
                InterruptDescriptor(&KERNEL_IRS_ENTRYPOINT_FUNCTION_NAME(name), 0); \
        klog<LogLevel::DEBUG>("RegisterInterrupt") << "name=\"" #name "\" number=" << ::kernel::interrupt_number::name << " proc=" << reinterpret_cast<std::uint64_t>(&KERNEL_IRS_ENTRYPOINT_FUNCTION_NAME(name));

        KERNEL_ITERATE_IRS(REGISTER_GENERIC_INTERRUPT, REGISTER_INTERRUPT, REGISTER_INTERRUPT)
#undef REGISTER_INTERRUPT

//        interruptDescriptorTable[0] = InterruptDescriptor(&KERNEL_IRS_ENTRYPOINT_FUNCTION_NAME(divide_by_zero), 0);
//        interruptDescriptorTable[3] = InterruptDescriptor(&KERNEL_IRS_ENTRYPOINT_FUNCTION_NAME(breakpoint), 0);
//        interruptDescriptorTable[14] = InterruptDescriptor(&KERNEL_IRS_ENTRYPOINT_FUNCTION_NAME(page_fault), 0);

        location.base = reinterpret_cast<std::uint32_t *>(std::data(interruptDescriptorTable));
        klog<LogLevel::DEBUG>("RegisterInterrupt") << "location=" << reinterpret_cast<std::uint64_t>(&location);
        klog<LogLevel::DEBUG>("RegisterInterrupt") << "location.base=" << reinterpret_cast<std::uint64_t>(location.base);
        LoadIDT(&location);
    }

} // namespace Kernel
