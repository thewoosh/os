#include "Kernel/PCI/ExpressDevice.hpp"

#include "Kernel/Assert.hpp"
#include "Kernel/Print.hpp"
#include "Kernel/PCI/BaseAddressRegister.hpp"
#include "Kernel/PCI/Capability.hpp"
#include "Kernel/PCI/CommandRegister.hpp"
#include "Kernel/PCI/Header.hpp"
#include "Kernel/PCI/HeaderType.hpp"
#include "Kernel/PCI/MSI.hpp"
#include "Kernel/PCI/PCIPort.hpp"
#include "Kernel/PCI/ProgrammingInterface.hpp"

namespace kernel::pci {

    struct StatusRegisterValue {
        [[nodiscard]] inline constexpr explicit
        StatusRegisterValue(std::uint16_t value)
                : m_value(value) {
        }

        [[nodiscard]] inline constexpr bool
        hasCapabilities() const {
            return m_value & 0x4;
        }

    private:
        std::uint16_t m_value;
    };

    ExpressDevice::ExpressDevice(void *segmentBaseAddress, std::uint8_t bus, std::uint8_t slot, std::uint8_t function)
            : AbstractDevice(bus, slot, function)
            , m_deviceAddress(calculateAddress(segmentBaseAddress, bus, slot, function)) {
        initializeHeader();
    }

    void
    ExpressDevice::forEachCapability(void (*callback)(const Capability &)) const {
        if (m_header.headerType != HeaderType::NORMAL) {
            // TODO other headers have capabilities too
            return;
        }

        if (!readStatusRegister().hasCapabilities())
            return;

        static_cast<void>(callback);
        std::uint8_t offset = readConfigWord(0x34) & 0xFC;

        while (offset != 0) {
            auto word = readConfigWord(offset);
            const auto capabilityType = static_cast<CapabilityType>(word & 0xFF);
            const auto offsetToNext = static_cast<std::uint8_t>((word & 0xFF00) >> 8);
            const auto size = offsetToNext - offset;

            switch (capabilityType) {
                case CapabilityType::MESSAGE_SIGNAL_INTERRUPTS: {
                    const std::uint16_t messageControl = size > 2 ? readConfigWord(offset + 2) : 0;
                    const auto messageAddressLow = size > 4 ? readConfigDWord(offset + 4) : 0;
                    const auto messageAddressHigh = size > 8 ? readConfigDWord(offset + 8) : 0;
                    const std::uint16_t messageData = size > 12 ? readConfigWord(offset + 12) : 0;
                    // offset + 14 is reserved
                    const auto mask = size > 16 ? readConfigDWord(offset + 16) : 0;
                    const auto pending = size > 20 ? readConfigDWord(offset + 20) : 0;

                    callback(MSI{offsetToNext, messageControl, messageAddressLow, messageAddressHigh, messageData,
                                 mask, pending});
                    break;
                }
                default:
                    callback(Capability{sizeof(Capability), capabilityType, offsetToNext});
                    break;
            }
            offset = offsetToNext;
        }
    }

    BaseAddressRegister
    ExpressDevice::getBaseAddressRegister0() {
        switch (m_header.headerType) {
            case HeaderType::NORMAL:
            case HeaderType::PCI_TO_PCI_BRIDGE:
                return {this, readConfigDWord(0x10)};
            case HeaderType::PCI_TO_CARD_BUS_BRIDGE:
                break;
        }

        return {nullptr, {}};
    }

    BaseAddressRegister
    ExpressDevice::getBaseAddressRegister1() {
        switch (m_header.headerType) {
            case HeaderType::NORMAL:
            case HeaderType::PCI_TO_PCI_BRIDGE:
                return {this, readConfigDWord(0x14)};
            case HeaderType::PCI_TO_CARD_BUS_BRIDGE:
                break;
        }

        return {nullptr, {}};
    }

    BaseAddressRegister
    ExpressDevice::getBaseAddressRegister2() {
        switch (m_header.headerType) {
            case HeaderType::NORMAL:
                return {this, readConfigDWord(0x18)};
            case HeaderType::PCI_TO_PCI_BRIDGE:
            case HeaderType::PCI_TO_CARD_BUS_BRIDGE:
                break;
        }

        return {nullptr, {}};
    }

    BaseAddressRegister
    ExpressDevice::getBaseAddressRegister3() {
        switch (m_header.headerType) {
            case HeaderType::NORMAL:
                return {this, readConfigDWord(0x1C)};
            case HeaderType::PCI_TO_PCI_BRIDGE:
            case HeaderType::PCI_TO_CARD_BUS_BRIDGE:
                break;
        }

        return {nullptr, {}};
    }

    BaseAddressRegister
    ExpressDevice::getBaseAddressRegister4() {
        switch (m_header.headerType) {
            case HeaderType::NORMAL:
                return {this, readConfigDWord(0x20)};
            case HeaderType::PCI_TO_PCI_BRIDGE:
            case HeaderType::PCI_TO_CARD_BUS_BRIDGE:
                break;
        }

        return {nullptr, {}};
    }

    BaseAddressRegister
    ExpressDevice::getBaseAddressRegister5() {
        switch (m_header.headerType) {
            case HeaderType::NORMAL:
                return {this, readConfigDWord(0x24)};
            case HeaderType::PCI_TO_PCI_BRIDGE:
            case HeaderType::PCI_TO_CARD_BUS_BRIDGE:
                break;
        }

        return {nullptr, {}};
    }

    bool
    ExpressDevice::hasCapabilities() const {
        return m_header.headerType == HeaderType::NORMAL
               && readStatusRegister().hasCapabilities()
               && (readConfigWord(0x34) & 0xFC) != 0;
    }

    StatusRegisterValue
    ExpressDevice::readStatusRegister() const {
        return StatusRegisterValue{readConfigWord(0x4)};
    }

} // namespace kernel::pci
