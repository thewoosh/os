#include "Kernel/PCI/PCIMain.hpp"

#include <utility>

#include <Core/Lambda.hpp>
#include <Core/Pointer.hpp>

#include "Kernel/PCI/ACPIRoutingTableEntry.hpp"
#include "Kernel/PCI/DeviceID.hpp"
#include "Kernel/PCI/VendorID.hpp"

#include "Kernel/ACPI/MCFG.hpp"
#include "Kernel/Device/Display/VMware/SVGA_II/Manager.hpp"
#include "Kernel/Device/Drive/AHCI/AHCI.hpp"
#include "Kernel/Device/Network/RTL8139/Manager.hpp"
#include "Kernel/Device/Network/RTL8169/Manager.hpp"
#include "Kernel/IO.hpp"
#include "Kernel/Memory/MemoryManager.hpp"
#include "Kernel/Memory/MemoryMapRegion.hpp"
#include "Kernel/Memory/Page.hpp"
#include "Kernel/Print.hpp"
#define PCI_REQUEST_CAPABILITY_NAMES
#include "Kernel/PCI/Capability.hpp"
#include "Kernel/PCI/ClassCode.hpp"
#include "Kernel/PCI/ProgrammingInterface.hpp"
#include "Kernel/PCI/SubclassCode.hpp"
#include "Kernel/PCI/PCIPort.hpp"

namespace kernel {

    struct PCIExpressEntry {
        const acpi::MCFG::Entry &entry;
    };

    void
    PCIMain::checkDeviceIdentifiers() const {
        bool foundUnknownDevice = false;
        forEachDevice(core::lambda([&] (const pci::AbstractDevice &device) {
//            klog<LogLevel::DEBUG>("PCI") << "Bus=" << bus << " Slot=" << slot << " Func=" << function;
            auto deviceName = device.getDeviceName();

            if (!std::empty(deviceName)) {
                auto stream = klog<LogLevel::DEBUG>("PCI!");
                stream << "Device: " << device.getVendorName() << "  " << deviceName;
                auto tmp = device.getClassName();
                if (tmp.empty())
                    stream << "  " << static_cast<std::size_t>(device.getClassCode());
                else
                    stream << "  " << tmp;

                tmp = device.getSubclassName();
                if (tmp.empty())
                    stream << "  " << static_cast<std::size_t>(device.getSubclassCode());
                else
                    stream << "  " << tmp;

                tmp = device.getProgrammingInterfaceName();
                if (tmp.empty())
                    stream << "  " << static_cast<std::size_t>(device.getProgrammingInterfaceCode());
                else
                    stream << "  " << tmp;
//                 << " " << device.getRevisionID();
            } else {
                foundUnknownDevice = true;
                return;
            }

            if (device.hasCapabilities()) {
                auto stream = klog<LogLevel::DEBUG>("PCI!Cap");
                device.forEachCapability(core::lambda([&](const pci::Capability &capability) {
                    stream << pci::pciCapabilityNames[std::size_t(capability.type())] << " ";
                }));
            }

//            klog<LogLevel::DEBUG>("PCI") << "ClassID: " << entry.classID;
//            klog<LogLevel::DEBUG>("PCI") << "SubclassID: " << entry.subclassID;
        }));

        if (foundUnknownDevice) {
            auto stream = klog<LogLevel::DEBUG>("PCI?");
            stream << "Unknown devices: ";
            forEachDevice([](const pci::AbstractDevice &device) {
                if (std::empty(device.getDeviceName())) {
                    if (device.getVendorName().empty())
                        kputhexnumber(static_cast<std::uint16_t>(device.getVendorID()));
                    else
                        kputs(device.getVendorName());

                    kputchar(' ');
                    kputhexnumber(static_cast<std::uint16_t>(device.getDeviceID()));

                    kputchar(' ');
                    kputs(device.getSubclassName());

                    kputs("    ");
                }
            });
        }
    }

    void
    PCIMain::forEachDevice(DeviceEnumerationCallbackType callback) {
        for (auto &device : m_conventionalDevices) {
            callback(device);
        }
        for (auto &device : m_expressDevices) {
            callback(device);
        }
    }

    void
    PCIMain::forEachDevice(ConstDeviceEnumerationCallbackType callback) const {
        for (const auto &device : m_conventionalDevices) {
            callback(device);
        }
        for (const auto &device : m_expressDevices) {
            callback(device);
        }
    }

    void
    PCIMain::informDeviceDrivers() {
        forEachDevice(core::lambda([&] (pci::AbstractDevice &device) {
            if (device.getClassCode() == pci::ClassCode::MASS_STORAGE_CONTROLLER
                && device.getSubclassCode() == pci::SubclassCode::MSA_SERIAL_ATA_CONTROLLER
                && device.getProgrammingInterfaceCode() == pci::ProgrammingInterface::MSA_SERIAL_ATA_CONTROLLER_AHCI_1_0) {
                auto *manager = new device::drive::ahci::AHCIManager{};
                manager->launch(&device);
                return;
            }

            if (device.getVendorID() == pci::VendorID::VMWARE
                    && device.getDeviceID() == pci::DeviceID::VMWARE_SVGA_II_ADAPTER) {
                auto *manager = new device::display::vm_svga_ii::Manager{};
                manager->launch(&device);
                return;
            }

            if ((device.getVendorID() == pci::VendorID::REALTEK
                    && (device.getDeviceID() == pci::DeviceID::REALTEK_RTL8111_PCIE_GIGABIT_ETHERNET_CONTROLLER_A
                        || device.getDeviceID() == pci::DeviceID::REALTEK_RTL8111_PCIE_GIGABIT_ETHERNET_CONTROLLER_B
                        || device.getDeviceID() == pci::DeviceID::REALTEK_RTL8169_GIGABIT_ETHERNET_CONTROLLER))
                    || (device.getVendorID() == pci::VendorID::LINKSYS
                            && device.getDeviceID() == pci::DeviceID::LINKSYS_GIGABIT_NETWORK_ADAPTER)
                    || (device.getVendorID() == pci::VendorID::US_ROBOTICS
                            && device.getDeviceID() == pci::DeviceID::US_ROBOTICS_USR997902_PCI_NETWORK_CARD)) {
                auto *manager = new device::network::rtl8169::Manager{};
                manager->launch(&device);
                return;
            }

            if (device.getVendorID() == pci::VendorID::REALTEK && device.getDeviceID() == pci::DeviceID::REALTEK_RTL8139_ETHERNET_ADAPTER) {
                auto *manager = new device::network::rtl8139::Manager{};
                manager->launch(&device);
                return;
            }
        }));
    }

    bool
    PCIMain::initializeConventional(core::VariableView<const pci::ACPIRoutingTableEntry> routingTable) {
        for (std::uint16_t bus = 0; bus < 256; ++bus) {
            for (std::uint16_t slot = 0; slot < 256; ++slot) {
                for (std::uint8_t function = 0; function < 8; ++function) {
                    pci::ConventionalDevice device{static_cast<std::uint8_t>(bus), static_cast<std::uint8_t>(slot), function};
                    if (device.getVendorID() == pci::VendorID::INVALID)
                        continue;

                    klog<LogLevel::DEBUG>("Device") << device.getDeviceName() << " > " << std::size_t(device.header().headerType)
                            << " @ " << bus << " " << slot << " " << function;

                    auto stream = klog<LogLevel::DEBUG>("PCIConventionalDevice");
                    stream << "Found PRT Entry!";
                    for (const auto &routingTableEntry : routingTable) {
                        if (bus == 0 && routingTableEntry.isAddress(slot)) {
                            const std::array names{"INTA", "INTB", "INTC", "INTD"};
                            stream << " " << names[routingTableEntry.pin];
//                            klog<LogLevel::DEBUG>("PCIConventionalDevice") << routingTableEntry.source.c_str();
//                            klog<LogLevel::DEBUG>("PCIConventionalDevice") << routingTableEntry.sourceIndex;
                        }
                    }
                    stream.finish();

                    m_conventionalDevices.push_back(std::move(device));
//                callback(entry, bus, slot, function);
                }
            }
        }

        checkDeviceIdentifiers();
        informDeviceDrivers();
        return true;
    }

    bool
    PCIMain::initializeExpress(const acpi::MCFG *mcfg, core::VariableView<const pci::ACPIRoutingTableEntry> routingTable) {
        klog<LogLevel::DEBUG>("PCIExpress") << "OEM ID is " << std::string_view{mcfg->header.oem_id, 6};
        klog<LogLevel::DEBUG>("PCIExpress") << "OEM Table ID is " << std::string_view{mcfg->header.oem_table_id, 8};
        klog<LogLevel::DEBUG>("PCIExpress") << "Size of table is " << TextModeBufferViewWriter::Decimal << mcfg->header.length;
        klog<LogLevel::DEBUG>("PCIExpress") << "Size of header is " << TextModeBufferViewWriter::Decimal << sizeof(mcfg->header);
        klog<LogLevel::DEBUG>("PCIExpress") << "Minus reserved of 8";
        klog<LogLevel::DEBUG>("PCIExpress") << "Entry size is " << TextModeBufferViewWriter::Decimal << sizeof(acpi::MCFG::Entry);
        klog<LogLevel::DEBUG>("PCIExpress") << "Leaves the entry count of " << TextModeBufferViewWriter::Decimal << mcfg->entryCount();
        const auto *entries = &mcfg->firstEntry;
        for (std::size_t entryIndex = 0; entryIndex < mcfg->entryCount(); ++entryIndex) {
            const auto &entry = entries[entryIndex];
            klog<LogLevel::DEBUG>("PCIExpress") << "Entry #" << TextModeBufferViewWriter::Decimal << entryIndex;
            klog<LogLevel::DEBUG>("PCIExpress") << "    Base Address: " << std::size_t(entry.baseAddress);
            klog<LogLevel::DEBUG>("PCIExpress") << "        Within region of type " << toString(MemoryMapRegionManager::getRegion(entry.baseAddress).type());
            klog<LogLevel::DEBUG>("PCIExpress") << "    Segment Group Number: " << entry.pciSegmentGroupNumber;
            klog<LogLevel::DEBUG>("PCIExpress") << "    Start Bus: " << entry.startPCIBus;
            klog<LogLevel::DEBUG>("PCIExpress") << "    End Bus: " << entry.endPCIBus;

            if (!loadPCIExpressEntry({entry}, routingTable)) {
                klog<LogLevel::ERROR>("PCIExpress") << "Failed to load entry #" << TextModeBufferViewWriter::Decimal << entryIndex;
                return false;
            }
        }
        klog<LogLevel::DEBUG>("PCIExpress") << "Last dword: " << *core::advance_pointer_with_byte_offset(reinterpret_cast<const std::uint32_t *>(mcfg), mcfg->header.length - 4);

        checkDeviceIdentifiers();
        informDeviceDrivers();
        return true;
    }

    bool
    PCIMain::loadPCIExpressEntry(const PCIExpressEntry &info, core::VariableView<const pci::ACPIRoutingTableEntry> routingTable) {
        if (!info.entry.baseAddress) {
            klog<LogLevel::ERROR>("PCIExpress") << "Invalid base address: " << std::size_t(info.entry.baseAddress);
            return false;
        }

        CHECK(info.entry.startPCIBus == 0);
        for (std::uint16_t bus = info.entry.startPCIBus; bus <= info.entry.endPCIBus; ++bus) {
            for (std::uint16_t slot = 0; slot < 32; ++slot) {
                for (std::uint8_t function = 0; function < 8; ++function) {
                    // The configuration space is exactly 4096 bytes, thus one page! ^_^
                    static_assert(kPageSize == 4096);
                    auto *address = pci::ExpressDevice::calculateAddress(info.entry.baseAddress, std::uint8_t(bus), std::uint8_t(slot), function);
                    for (const auto &routingTableEntry : routingTable) {
                        if (std::size_t(address) == std::size_t(routingTableEntry.address)) {
                            klog<LogLevel::DEBUG>("PCI") << "Found PRT Entry!";
                            klog<LogLevel::DEBUG>("PCI") << routingTableEntry.address;
                            klog<LogLevel::DEBUG>("PCI") << routingTableEntry.pin;
                            klog<LogLevel::DEBUG>("PCI") << routingTableEntry.source.c_str();
                            klog<LogLevel::DEBUG>("PCI") << routingTableEntry.sourceIndex;
                            break;
                        }
                    }

                    ASSERT(page_floor(address) == address);
                    MemoryManager::the().map(VirtualAddress{address}, address, MemoryManager::MAP_FLAG_CACHE_DISABLED);

                    pci::ExpressDevice device{info.entry.baseAddress, static_cast<std::uint8_t>(bus), static_cast<std::uint8_t>(slot), function};
                    if (device.getVendorID() == pci::VendorID::INVALID) {
                        // TODO properly unmap here
                        MemoryManager::the().map(VirtualAddress{address}, nullptr, MemoryManager::MAP_FLAG_NON_WRITABLE);
                        continue;
                    }

#if 0
                    klog<LogLevel::DEBUG>("PCIExpressDevice") << device.getVendorName() << " > "
                            << std::size_t(device.getVendorID()) << " > " << device.getDeviceName() << " > "
                            << std::size_t(device.getDeviceID()) << " > " << std::size_t(device.header().headerType);
#endif
                    m_expressDevices.push_back(std::move(device));
//                callback(entry, bus, slot, function);
                }
            }
        }
        return true;
    }

} // namespace kernel
