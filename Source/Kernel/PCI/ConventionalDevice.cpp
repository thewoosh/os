#include "Kernel/PCI/ConventionalDevice.hpp"

#include "Kernel/Assert.hpp"
#include "Kernel/IO.hpp"
#include "Kernel/Print.hpp"
#include "Kernel/PCI/BaseAddressRegister.hpp"
#include "Kernel/PCI/Capability.hpp"
#include "Kernel/PCI/CommandRegister.hpp"
#include "Kernel/PCI/Header.hpp"
#include "Kernel/PCI/HeaderType.hpp"
#include "Kernel/PCI/MSI.hpp"
#include "Kernel/PCI/PCIPort.hpp"
#include "Kernel/PCI/ProgrammingInterface.hpp"

namespace kernel::pci {

    struct StatusRegisterValue {
        [[nodiscard]] inline constexpr explicit
        StatusRegisterValue(std::uint16_t value)
                : m_value(value) {
        }

        [[nodiscard]] inline constexpr bool
        hasCapabilities() const {
            return m_value & 0x4;
        }

    private:
        std::uint16_t m_value;
    };

    ConventionalDevice::ConventionalDevice(std::uint8_t bus, std::uint8_t slot, std::uint8_t function)
            : AbstractDevice(bus, slot, function) {
        initializeHeader();
    }

    void
    ConventionalDevice::forEachCapability(void (*callback)(const Capability &)) const {
        if (m_header.headerType != HeaderType::NORMAL) {
            // TODO other headers have capabilities too
            return;
        }

        if (!readStatusRegister().hasCapabilities())
            return;

        static_cast<void>(callback);
        std::uint8_t offset = readConfigWord(0x34) & 0xFC;

        while (offset != 0) {
            auto word = readConfigWord(offset);
            const auto capabilityType = static_cast<CapabilityType>(word & 0xFF);
            const auto offsetToNext = static_cast<std::uint8_t>((word & 0xFF00) >> 8);
            const auto size = offsetToNext - offset;

            switch (capabilityType) {
                case CapabilityType::MESSAGE_SIGNAL_INTERRUPTS: {
                    const std::uint16_t messageControl = size > 2 ? readConfigWord(offset + 2) : 0;
                    const auto messageAddressLow = size > 4 ? readConfigDWord(offset + 4) : 0;
                    const auto messageAddressHigh = size > 8 ? readConfigDWord(offset + 8) : 0;
                    const std::uint16_t messageData = size > 12 ? readConfigWord(offset + 12) : 0;
                    // offset + 14 is reserved
                    const auto mask = size > 16 ? readConfigDWord(offset + 16) : 0;
                    const auto pending = size > 20 ? readConfigDWord(offset + 20) : 0;

                    callback(MSI{offsetToNext, messageControl, messageAddressLow, messageAddressHigh, messageData,
                                 mask, pending});
                    break;
                }
                default:
                    callback(Capability{sizeof(Capability), capabilityType, offsetToNext});
                    break;
            }
            offset = offsetToNext;
        }
    }

    BaseAddressRegister
    ConventionalDevice::getBaseAddressRegister0() {
        switch (m_header.headerType) {
            case HeaderType::NORMAL:
            case HeaderType::PCI_TO_PCI_BRIDGE:
                return {this, readConfigDWord(0x10)};
            case HeaderType::PCI_TO_CARD_BUS_BRIDGE:
                break;
        }

        return {nullptr, {}};
    }

    BaseAddressRegister
    ConventionalDevice::getBaseAddressRegister1() {
        switch (m_header.headerType) {
            case HeaderType::NORMAL:
            case HeaderType::PCI_TO_PCI_BRIDGE:
                return {this, readConfigDWord(0x14)};
            case HeaderType::PCI_TO_CARD_BUS_BRIDGE:
                break;
        }

        return {nullptr, {}};
    }

    BaseAddressRegister
    ConventionalDevice::getBaseAddressRegister2() {
        switch (m_header.headerType) {
            case HeaderType::NORMAL:
                return {this, readConfigDWord(0x18)};
            case HeaderType::PCI_TO_PCI_BRIDGE:
            case HeaderType::PCI_TO_CARD_BUS_BRIDGE:
                break;
        }

        return {nullptr, {}};
    }

    BaseAddressRegister
    ConventionalDevice::getBaseAddressRegister3() {
        switch (m_header.headerType) {
            case HeaderType::NORMAL:
                return {this, readConfigDWord(0x1C)};
            case HeaderType::PCI_TO_PCI_BRIDGE:
            case HeaderType::PCI_TO_CARD_BUS_BRIDGE:
                break;
        }

        return {nullptr, {}};
    }

    BaseAddressRegister
    ConventionalDevice::getBaseAddressRegister4() {
        switch (m_header.headerType) {
            case HeaderType::NORMAL:
                return {this, readConfigDWord(0x20)};
            case HeaderType::PCI_TO_PCI_BRIDGE:
            case HeaderType::PCI_TO_CARD_BUS_BRIDGE:
                break;
        }

        return {nullptr, {}};
    }

    BaseAddressRegister
    ConventionalDevice::getBaseAddressRegister5() {
        switch (m_header.headerType) {
            case HeaderType::NORMAL:
                return {this, readConfigDWord(0x24)};
            case HeaderType::PCI_TO_PCI_BRIDGE:
            case HeaderType::PCI_TO_CARD_BUS_BRIDGE:
                break;
        }

        return {nullptr, {}};
    }

    bool
    ConventionalDevice::hasCapabilities() const {
        return m_header.headerType == HeaderType::NORMAL
               && readStatusRegister().hasCapabilities()
               && (readConfigWord(0x34) & 0xFC) != 0;
    }

    std::uint8_t
    ConventionalDevice::readConfigByte(std::uint16_t offset) const {
        setConfigAddress(offset);
        return inb(ports::PCIConfigData + (offset & 3));
    }

    std::uint32_t
    ConventionalDevice::readConfigDWord(std::uint16_t offset) const {
        setConfigAddress(offset);
        return inl(ports::PCIConfigData);
    }

    std::uint16_t
    ConventionalDevice::readConfigWord(std::uint16_t offset) const {
        setConfigAddress(offset);
        return inw(ports::PCIConfigData + (offset & 2));
    }

    StatusRegisterValue
    ConventionalDevice::readStatusRegister() const {
        return StatusRegisterValue{readConfigWord(0x4)};
    }

    void
    ConventionalDevice::setConfigAddress(std::uint16_t offset) const {
        ASSERT(offset < 256);
        outl(ports::PCIConfigAddress, createAddress(m_bus, m_slot, m_function) | offset);
    }

    void
    ConventionalDevice::writeConfigByte(std::uint16_t offset, std::uint8_t value) {
        ASSERT(offset < 256);
        outl(ports::PCIConfigAddress, createAddress(m_bus, m_slot, m_function) | (offset & 0xFC));
        outb(ports::PCIConfigData, value);
    }

    void
    ConventionalDevice::writeConfigDWord(std::uint16_t offset, std::uint32_t value) {
        ASSERT(offset < 256);
        outl(ports::PCIConfigAddress, createAddress(m_bus, m_slot, m_function) | (offset & 0xFC));
        outl(ports::PCIConfigData, value);
    }

    void
    ConventionalDevice::writeConfigWord(std::uint16_t offset, std::uint16_t value) {
        ASSERT(offset < 256);
        outl(ports::PCIConfigAddress, createAddress(m_bus, m_slot, m_function) | (offset & 0xFC));
        outw(ports::PCIConfigData + (offset & 2), value);
    }

} // namespace kernel::pci
