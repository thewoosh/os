#include "Kernel/PCI/Device.hpp"

#include "Kernel/Assert.hpp"
#include "Kernel/PCI/BaseAddressRegister.hpp"
#include "Kernel/PCI/Capability.hpp"
#include "Kernel/PCI/ClassCode.hpp"
#include "Kernel/PCI/CommandRegister.hpp"
#include "Kernel/PCI/DeviceID.hpp"
#include "Kernel/PCI/ProgrammingInterface.hpp"
#include "Kernel/PCI/SubclassCode.hpp"
#include "Kernel/PCI/VendorID.hpp"

namespace kernel::pci {

    std::string_view
    AbstractDevice::getClassName() const {
        switch (getClassCode()) {
#define REGISTER_CLASS_CODE(name, id) case ClassCode::name: return #name;
            PCI_ITERATE_CLASS_CODES(REGISTER_CLASS_CODE)
#undef REGISTER_CLASS_CODE
        }

        return {};
    }

    std::string_view
    AbstractDevice::getDeviceName() const {
        switch (getVendorID()) {
            case VendorID::INVALID:
                return "<invalid>";

#define REGISTER_DEVICE_ID(enumeration, id, product_name) case DeviceID::enumeration: return product_name;
#define REGISTER_VENDOR(vendor, id, human_name) \
        case VendorID::vendor: \
            switch (getDeviceID()) { \
                PCI_DEVICE_ID_ITERATE_##vendor(REGISTER_DEVICE_ID) \
                default: return {}; \
            } \
            break;

                PCI_ITERATE_VENDOR_IDS(REGISTER_VENDOR)
#undef REGISTER_VENDOR
        }

        return {};
    }

    std::string_view
    AbstractDevice::getProgrammingInterfaceName() const {
#define REGISTER_PROGRAMMING_INTERFACE(enumeration, id, description) \
            case ProgrammingInterface::enumeration: return description;

        if (getClassCode() == ClassCode::MASS_STORAGE_CONTROLLER) {
            const auto subClassCode = getSubclassCode();
            if (subClassCode == SubclassCode::MSA_IDE_CONTROLLER) {
                switch (getProgrammingInterfaceCode()) {
                    PCI_ITERATE_PROGRAMMING_INTERFACES_FOR_MSA_IDE_CONTROLLER(REGISTER_PROGRAMMING_INTERFACE)
                    default:
                        return "Other";
                }
            }

            if (subClassCode == SubclassCode::MSA_SERIAL_ATA_CONTROLLER) {
                switch (getProgrammingInterfaceCode()) {
                    PCI_ITERATE_PROGRAMMING_INTERFACES_FOR_MSA_SERIAL_ATA_CONTROLLER(REGISTER_PROGRAMMING_INTERFACE)
                    default:
                        return "Other";
                }
            }
        }

        return {};
    }

    std::string_view
    AbstractDevice::getSubclassName() const {
        switch (getClassCode()) {
#define REGISTER_SUBCLASS_CODE(subcode, id, name) case SubclassCode::subcode: return name;
#define REGISTER_CLASS_CODE(code, id) \
        case ClassCode::code: \
            switch (getSubclassCode()) {\
                PCI_ITERATE_SUBCLASS_CODES_BASE_##code(REGISTER_SUBCLASS_CODE) \
                default: return {}; \
            }
            PCI_ITERATE_CLASS_CODES(REGISTER_CLASS_CODE)
#undef REGISTER_CLASS_CODE
#undef REGISTER_SUBCLASS_CODE
        }

        return {};
    }

    std::string_view
    AbstractDevice::getVendorName() const {
        switch (getVendorID()) {
#define REGISTER_VENDOR(name, id, human_name) case VendorID::name: return human_name;
            PCI_ITERATE_VENDOR_IDS(REGISTER_VENDOR)
#undef REGISTER_VENDOR

            case VendorID::INVALID:
                return "INVALID";
        }

        return {};
    }

    void
    AbstractDevice::initializeHeader() {
        m_header.vendorID = static_cast<VendorID>(readConfigWord(0));
        if (m_header.vendorID == kInvalidVendorID)
            return;
        m_header.deviceID = static_cast<DeviceID>(readConfigWord(2));

        auto word = readConfigWord(0x8);
        m_header.programmingInterface = static_cast<ProgrammingInterface>((word >> 8) & 0xFF);
        m_header.revisionID = word & 0xFF;

        word = readConfigWord(0xA);
        m_header.classID = static_cast<ClassCode>((word >> 8) & 0xFF);
        m_header.subclassID = static_cast<SubclassCode>(word & 0xFF);
    }

    CommandRegister
    AbstractDevice::readCommandRegister() const {
        union {
            std::uint16_t raw;
            CommandRegister commandRegister;
        } data{.raw = readConfigWord(0x8)};
        return data.commandRegister;
    }

    void
    AbstractDevice::writeCommandRegister(CommandRegister commandRegister) {
        union {
            std::uint16_t raw;
            CommandRegister commandRegister;
        } data{.commandRegister = commandRegister};
        writeConfigWord(0x8, data.raw);
    }

} // namespace kernel::pci
