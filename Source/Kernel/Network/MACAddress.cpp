#include "Kernel/Network/MACAddress.hpp"

#include "Kernel/Print.hpp"

namespace kernel {

    TextModeBufferViewWriter &
    operator<<(TextModeBufferViewWriter &stream, const MACAddress &macAddress) {
        auto printPart = [&] (std::uint8_t digit) {
            if (digit < 0x10)
                kputchar('0');
            kputhexnumber(digit, HexnumberPrefix::NO);
        };

        printPart(macAddress[0]);
        kputchar(':');
        printPart(macAddress[1]);
        kputchar(':');
        printPart(macAddress[2]);
        kputchar(':');
        printPart(macAddress[3]);
        kputchar(':');
        printPart(macAddress[4]);
        kputchar(':');
        printPart(macAddress[5]);

        return stream;
    }

} // namespace kernel
