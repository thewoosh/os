#define MEMORY_MAP_IMPLEMENTATION

#include "Kernel/Specifications/UEFI/UEFIManager.hpp"

#include <Core/Pointer.hpp>

#include "Kernel/Print.hpp"
#include "Kernel/Memory/MemoryManager.hpp"
#include "Kernel/Memory/MemoryMapRegion.hpp"
#include "Kernel/Memory/Page.hpp"
#include "Kernel/Specifications/UEFI/EFISimpleTextOutputProtocol.hpp"
#include "Kernel/Specifications/UEFI/EFISystemTable.hpp"
#include "Kernel/Specifications/UEFI/Signature.hpp"

namespace kernel::uefi {

    bool
    UEFIManager::launch() {
        CHECK(m_efiSystemTable != nullptr);
        if (!mapSystemTableMemory()) {
            klog<LogLevel::ERROR>("UEFIManager") << "Failed to map EFISystemTable";
            return false;
        }

        if (m_efiSystemTable->header.signature != Signature::SYSTEM_TABLE) {
            klog<LogLevel::ERROR>("UEFIManager") << "EFISystemTable signature incorrect!";
            return false;
        }

        if (m_efiSystemTable->conOut == nullptr) {
            klog<LogLevel::WARNING>("UEFIManager") << "Unfortunately, EFISystemTable::conOut is not available :(";
            return true;
        }

        // TODO ConOut is most likely mapped in a different memory region
        klog<LogLevel::ERROR>("UEFIManager") << "EFISystemTable: " << std::size_t(m_efiSystemTable);
        klog<LogLevel::ERROR>("UEFIManager") << "ConOut: " << std::size_t(m_efiSystemTable->conOut);
        auto &conOut = *m_efiSystemTable->conOut;
        EFI_ASSERT(conOut.clearScreen());
        EFI_ASSERT(conOut.outputString(u"Hello World from UEFIManager::launch()"));

        return true;
    }

    bool
    UEFIManager::mapSystemTableMemory() {
        klog<LogLevel::DEBUG>("UEFIManager") << "EFISystemTable @ " << std::size_t(m_efiSystemTable);
        const auto region = MemoryMapRegionManager::getRegion(m_efiSystemTable);
        if (!region.contains(m_efiSystemTable)) {
            klog<LogLevel::ERROR>("UEFIManager::mapSystemTableMemory") << "Failed to locate EFI system table";
            return false;
        }

        for (auto *page = static_cast<std::uint8_t *>(region.begin()); page < region.end(); page += kPageSize) {
            MemoryManager::the().map(VirtualAddress{page}, page, MemoryManager::MAP_FLAG_CACHE_DISABLED);
        }
        return true;
    }

} // namespace kernel::uefi
