#include "Kernel/Device/Network/RTL8169/Manager.hpp"

#include "Kernel/IO.hpp"
#include "Kernel/Print.hpp"
#include "Kernel/PCI/BaseAddressRegister.hpp"
#include "Kernel/PCI/Device.hpp"

namespace kernel::device::network::rtl8169 {

    void
    Manager::getMACAddress() {
        klog<LogLevel::DEBUG>("Device/Network/RTL8169") << "BAR0: " << m_pciDevice->getBaseAddressRegister0().raw_value();
        klog<LogLevel::DEBUG>("Device/Network/RTL8169") << "BAR1: " << m_pciDevice->getBaseAddressRegister1().raw_value();
        klog<LogLevel::DEBUG>("Device/Network/RTL8169") << "BAR2: " << m_pciDevice->getBaseAddressRegister2().raw_value();
        klog<LogLevel::DEBUG>("Device/Network/RTL8169") << "BAR3: " << m_pciDevice->getBaseAddressRegister3().raw_value();
        klog<LogLevel::DEBUG>("Device/Network/RTL8169") << "BAR4: " << m_pciDevice->getBaseAddressRegister4().raw_value();
        klog<LogLevel::DEBUG>("Device/Network/RTL8169") << "BAR5: " << m_pciDevice->getBaseAddressRegister5().raw_value();
        if (std::size_t(m_pciDevice->getVendorID()) != 0)
            return;
        for (std::uint8_t i = 0; i < 6; ++i) {
            m_macAddress[i] = inb(m_pciDevice->hasCapabilities() + 1);
        }
    }

    void
    Manager::launch(pci::AbstractDevice *device) {
        m_pciDevice = device;

        getMACAddress();
    }

} // namespace kernel::device::network::rtl8169
