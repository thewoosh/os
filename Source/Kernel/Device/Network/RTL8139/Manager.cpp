#define SHOW_PRINT_OVERLOADS
#include "Kernel/Device/Network/RTL8139/Manager.hpp"

#include <Core/Lambda.hpp>
#include <Core/StackVector.hpp>

#include "Kernel/IO.hpp"
#include "Kernel/Network/ARP/HardwareType.hpp"
#include "Kernel/Network/DHCP/Header.hpp"
#include "Kernel/Network/DHCP/MessageType.hpp"
#include "Kernel/Network/DHCP/Options.hpp"
#include "Kernel/Network/IP/v4/Packet.hpp"
#include "Kernel/Network/UDP/Segment.hpp"
#include "Kernel/Network/UDP/Ports.hpp"
#include "Kernel/Print.hpp"
#include "Kernel/PCI/BaseAddressRegister.hpp"
#include "Kernel/PCI/Device.hpp"
#include "Kernel/PCI/MSI.hpp"

//
// Information:
//     Realtek DataSheet - http://realtek.info/pdf/rtl8139d.pdf
//
namespace kernel::device::network::rtl8139 {

    inline constexpr const std::string_view kLogTag = "Device/Net/RTL8139";

    inline constexpr const std::uint16_t kRegisterMac0 = 0x00;
    inline constexpr const std::uint16_t kRegisterMulticast0 = 0x09;
    inline constexpr const std::uint16_t kRegisterTransmitStatus0 = 0x10;
    inline constexpr const std::uint16_t kRegisterTransmitStart0 = 0x20;
    inline constexpr const std::uint16_t kRegisterReceiveBufferStart = 0x30;
    inline constexpr const std::uint16_t kRegisterCommand = 0x37;
    inline constexpr const std::uint16_t kRegisterInterruptMask = 0x3C;
    inline constexpr const std::uint16_t kRegisterInterruptStatus = 0x3E;
    inline constexpr const std::uint16_t kRegisterReceiveConfiguration = 0x44;
    inline constexpr const std::uint16_t kRegisterConfig0 = 0x51;
    inline constexpr const std::uint16_t kRegisterConfig1 = 0x52;

    inline constexpr const std::size_t kTimeoutReset = 10000000;

    void
    Manager::configurePCI() {
        m_ioAddress = std::uint16_t(m_pciDevice->getBaseAddressRegister0().raw_value()) - 1;
        m_pciDevice->enableBusMastering();

        bool foundMSI = false;
        m_pciDevice->forEachCapability(core::lambda([&] (const pci::Capability &capability) {
            if (capability.type() == pci::CapabilityType::MESSAGE_SIGNAL_INTERRUPTS) {
                foundMSI = true;
                CHECK(capability.size() >= sizeof(pci::MSI));
                const auto &msi = static_cast<const pci::MSI &>(capability);
                klog<LogLevel::DEBUG>(kLogTag) << "MSI.messageControl = " << msi.messageControl();
                klog<LogLevel::DEBUG>(kLogTag) << "MSI.messageAddressLow = " << msi.messageAddressLow();
                klog<LogLevel::DEBUG>(kLogTag) << "MSI.messageAddressHigh = " << msi.messageAddressHigh();
                klog<LogLevel::DEBUG>(kLogTag) << "MSI.messageData = " << msi.messageData();
                klog<LogLevel::DEBUG>(kLogTag) << "MSI.mask = " << msi.mask();
                klog<LogLevel::DEBUG>(kLogTag) << "MSI.pending = " << msi.pending();
            }
        }));

//        CHECK(foundMSI);
    }

    void
    Manager::getMACAddress() {
        for (std::uint8_t i = 0; i < 6; ++i)
            m_macAddress[i] = inb(m_ioAddress + kRegisterMac0 + i);

        klog<LogLevel::DEBUG>("Device/Net/RTL8139") << "MAC Address: " << m_macAddress;
    }

    void
    Manager::initializeReceiveBuffer() {
        // TODO resize instead of reserve
        m_receiveBuffer.reserve(8192 + 16);
        outl(m_ioAddress + kRegisterReceiveBufferStart, std::uint32_t(std::size_t(m_receiveBuffer.data())));
    }

    void
    Manager::launch(pci::AbstractDevice *device) {
        m_deviceStatus = DeviceStatus::INITIALIZING;

        m_pciDevice = device;
        configurePCI();

        // Turn On
        outb(m_ioAddress + kRegisterConfig1, 0);

        reset();
        if (m_deviceStatus == DeviceStatus::INITIALIZATION_TIMEOUT)
            return;

        getMACAddress();
        initializeReceiveBuffer();

        // Initialize Interrupts
        outw(m_ioAddress + kRegisterInterruptMask, 0x0005);

        // Configure Receive Buffer
        outl(m_ioAddress + kRegisterReceiveConfiguration, 0xf | (1 << 7));

        // Enable Receive and Transmitter
        outb(m_ioAddress + kRegisterCommand, 0x0C);

        m_deviceStatus = DeviceStatus::INITIALIZED;
        klog<LogLevel::DEBUG>(kLogTag) << "Done initializing!";

        struct Data {
            dhcp::Header header;
            dhcp::MessageTypeOption type{dhcp::MessageType::DISCOVER};
            dhcp::EndOption endOption;
        };

        Data data{
            .header {
                .opCode = dhcp::OpCode::REQUEST,
                .hardwareAddressType = arp::HardwareTypeShort::ETHERNET,
                .hardwareAddressLength = 6,
                .hops = 0,
                .transactionID = 0x64806480, // TODO must be random!
                .secondsSinceProcessBegin = 0,
                .flags{
                    .broadcast = false,
                    .mustBeZero = 0
                },
                .clientIPAddress = ip::v4::IPAddress::fromSystem(0x00000000),
                .yourIPAddress = ip::v4::IPAddress::fromSystem(0x00000000),
                .serverIPAddress = ip::v4::IPAddress::fromSystem(0x00000000),
                .relayAgentIPAddress = ip::v4::IPAddress::fromSystem(0x00000000),
                .clientHardwareAddress = {
                        0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
                        m_macAddress[0],
                        m_macAddress[1],
                        m_macAddress[2],
                        m_macAddress[3],
                        m_macAddress[4],
                        m_macAddress[5],
                },
                .serverHostName = {},
                .bootFileName = {},
            }
        };

        ip::v4::Packet<udp::Segment<Data>> packet{
            .header {
                .version = 4,
                .internetHeaderLength = sizeof(ip::v4::Header) / 4,
                .differentialServices = ip::v4::DifferentialServices::NORMAL,
                .totalLength = std::uint16_t(sizeof(ip::v4::Header) + sizeof(udp::Segment<Data>)),
                .identification = 0, // ??
                .flags = 0,
                .fragmentOffset = 0, // TODO
                .timeToLive = 0x80,
                .protocol = ip::IPProtocol::UDP,
                .headerChecksum = 0, // !
                .sourceAddress = {0, 0, 0, 0},
                .destinationAddress = {255, 255, 255, 255},
            },
            .segment {
                .header {
                    .sourcePort = udp::ports::DHCPClient,
                    .destinationPort = udp::ports::DHCPServer,
                    .length = sizeof(udp::Header) + sizeof(data),
                    .checksum = 0, // !
                },
                .data = &data
            }
        };

        VERIFY(send({reinterpret_cast<const std::uint8_t *>(&packet), sizeof(packet)}));
    }

    void
    Manager::reset() {
        outb(m_ioAddress + kRegisterCommand, 1 << 4);

        std::size_t counter{0};
        while ((inb(m_ioAddress + kRegisterCommand) & 0x10) != 0) {
            // TODO Wait() here.

            if (++counter == kTimeoutReset) {
                klog<LogLevel::ERROR>(kLogTag) << "RESET timed out!";
                m_deviceStatus = DeviceStatus::INITIALIZATION_TIMEOUT;
                return;
            }
        }
    }

    bool
    Manager::send(core::RegionPointer<const std::uint8_t> data) {
        klog<LogLevel::DEBUG>(kLogTag) << "Sending " << TextModeBufferViewWriter::Decimal << data.size() << " bytes";

#define bool unsigned int
        struct StatusBits {
            std::uint16_t size : 12;
            bool own : 1;
            bool transmitFifoUnderrun : 1;
            bool transmitOK : 1;
            std::uint8_t earlyThreshold : 5;
            bool reserved0 : 1;
            bool reserved1 : 1;
            std::uint8_t numberOfCollisionCount : 4;
            bool cdHeartBeat : 1;
            bool outOfWindowCollision : 1;
            bool transmitAbort : 1;
            bool carrierSenseLost : 1;
        };
#undef bool
        static_assert(sizeof(StatusBits) == 4);

        union {
            std::uint32_t raw;
            StatusBits bits;

            void
            load(std::uint16_t ioAddress) {
                raw = inl(ioAddress + kRegisterTransmitStatus0);
            }

        } status{};
        status.load(m_ioAddress);

        if (!status.bits.own) {
            klog<LogLevel::ERROR>(kLogTag) << "Device isn't reset correctly, descriptor 0 is still owned by device!";
            return true; // TODO false
        }

#define COMP(ent) klog<LogLevel::DEBUG>(kLogTag) << #ent << ": " << status.bits.ent;
        COMP(carrierSenseLost)
        COMP(transmitAbort)
        COMP(outOfWindowCollision)
        COMP(cdHeartBeat)
        COMP(numberOfCollisionCount)
        COMP(reserved0)
        COMP(reserved1)
        COMP(earlyThreshold)
        COMP(transmitOK)
        COMP(transmitFifoUnderrun)
        COMP(own)
        COMP(size)
#undef COMP

        outl(m_ioAddress + kRegisterTransmitStart0, std::uint32_t(std::size_t(&*data.begin())));
        status.bits.size = static_cast<std::uint16_t>(data.size());
        status.bits.own = false;
        outl(m_ioAddress + kRegisterTransmitStatus0, status.raw);

        // TODO here the round robin counter is incremented, after we sent data

        klog<LogLevel::DEBUG>(kLogTag) << status.raw;
        klog<LogLevel::DEBUG>(kLogTag) << "Written to buffer, waiting now...";

        while (true) {
            auto newly = decltype(status){};
            newly.load(m_ioAddress);
            if (newly.raw != status.raw) {
                klog<LogLevel::DEBUG>(kLogTag) << newly.raw;
#define COMP(ent) if (newly.bits.ent != status.bits.ent) klog<LogLevel::DEBUG>(kLogTag) << #ent << " changed: " << status.bits.ent << " now " << newly.bits.ent;
                COMP(carrierSenseLost)
                COMP(transmitAbort)
                COMP(outOfWindowCollision)
                COMP(cdHeartBeat)
                COMP(numberOfCollisionCount)
                COMP(reserved0)
                COMP(reserved1)
                COMP(earlyThreshold)
                COMP(transmitOK)
                COMP(transmitFifoUnderrun)
                COMP(own)
                COMP(size)
            }
            status = newly;
            if (status.bits.own)
                break;
        }

        klog<LogLevel::DEBUG>(kLogTag) << "Sent!";
        return true;
    }

} // namespace kernel::device::network::rtl8139
