#include "Kernel/Device/Drive/AHCI/AHCI.hpp"

#include "Kernel/Assert.hpp"
#include "Kernel/Device/Drive/AHCI/FrameInformationStructure.hpp"
#include "Kernel/Device/Drive/AHCI/HostBusAdapter.hpp"
#include "Kernel/Memory/MemoryManager.hpp"
#include "Kernel/Print.hpp"
#include "Kernel/PCI/BaseAddressRegister.hpp"
#include "Kernel/PCI/Capability.hpp"

namespace kernel::device::drive::ahci {

    void
    AHCIManager::checkPort(volatile HostBusAdapterPort &port) {
        if (port.sataStatus.deviceDetection != SATAStatus::DeviceDetection::DEVICE_PRESENT_AND_PHY_COMMUNICATION_ESTABLISHED) {
//            klog<LogLevel::DEBUG>("AHCI/CheckPort") << "  Device Not Present";
            // Device not present
            return;
        }

        if (port.sataStatus.interfacePowerManagement != SATAStatus::InterfacePowerManagement::ACTIVE) {
//            klog<LogLevel::DEBUG>("AHCI/CheckPort") << "  Device Not Active";
            // Device not active
            return;
        }

        klog<LogLevel::DEBUG>("AHCI/CheckPort") << "  Device Signature: " << toString(port.signature);
    }

    void
    AHCIManager::checkPorts() {
        auto portsImplemented = m_hbaMemory->hostControl.portsImplemented;
//        std::uint32_t i{};
        for (auto &port : m_hbaMemory->ports) {
            if (portsImplemented & 0x1) {
//                klog<LogLevel::DEBUG>("AHCI/CheckPorts") << "Index " << TextModeBufferViewWriter::Decimal << i++;
                checkPort(port);
            }

            portsImplemented >>= 1;
        }
    }

    void
    AHCIManager::launch(pci::AbstractDevice *device) {
        m_pciDevice = device;

        // TODO map ABAR as uncacheable

        m_pciDevice->forEachCapability([] (const pci::Capability &capability) {
            klog<LogLevel::DEBUG>("AHCIManager") << "PCIDeviceCapability: " << std::size_t(capability.type());
        });

        auto abar = m_pciDevice->getBaseAddressRegister5();
        CHECK(abar);
        auto address = abar.get();
        MemoryManager::the().map(VirtualAddress{address}, address, MemoryManager::MAP_FLAG_CACHE_DISABLED);

        m_hbaMemory = abar.get<HostBusAdapterMemory>();
        klog<LogLevel::DEBUG>("AHCIManager") << "HBA Version: " << toString(m_hbaMemory->hostControl.version);

        checkPorts();
    }

} // namespace kernel::device::drive::ahci
