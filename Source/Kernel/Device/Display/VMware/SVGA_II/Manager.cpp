#include "Kernel/Device/Display/VMware/SVGA_II/Manager.hpp"

#include "Kernel/Device/Display/VMware/SVGA_II/Registers.hpp"
#include "Kernel/Device/Display/VMware/SVGA_II/Version.hpp"
#include "Kernel/IO.hpp"
#include "Kernel/PCI/BaseAddressRegister.hpp"
#include "Kernel/PCI/Device.hpp"
#include "Kernel/Print.hpp"

namespace kernel::device::display::vm_svga_ii {

    inline constexpr std::uint16_t kPortOffsetIndex = 0x0000;
    inline constexpr std::uint16_t kPortOffsetValue = 0x0001;
    inline constexpr std::uint16_t kPortOffsetBIOS = 0x0002;
    inline constexpr std::uint16_t kPortOffsetIRQStatus = 0x0008;

    bool
    Manager::determineVersion() {
        constexpr const Version versions[] {Version::VERSION_2, Version::VERSION_1, Version::VERSION_0};
        for (auto version : versions) {
            writeRegister(Register::ID, std::uint32_t(version));
            if (Version(readRegister(Register::ID)) == version) {
                m_version = version;
                return true;
            }
        }
        return false;
    }

    void
    Manager::launch(pci::AbstractDevice *device) {
        m_pciDevice = device;

        klog<LogLevel::DEBUG>("VMware SVGA2") << "DeviceID: " << std::size_t(device->getDeviceID());

        klog<LogLevel::DEBUG>("VMware SVGA II") << "BAR0: " << m_pciDevice->getBaseAddressRegister0().raw_value();
        m_bar0 = std::uint16_t(m_pciDevice->getBaseAddressRegister0().raw_value());
        klog<LogLevel::DEBUG>("VMware SVGA II") << "BAR0: " << m_bar0;
        klog<LogLevel::DEBUG>("VMware SVGA II") << "BAR1: " << m_pciDevice->getBaseAddressRegister1().raw_value();

        if (!determineVersion()) {
            klog<LogLevel::ERROR>("VMware SVGA2") << "Failed to detect version";
            return;
        }

        klog<LogLevel::DEBUG>("VMware SVGA II") << "WIDTH: " << TextModeBufferViewWriter::Decimal << readRegister(Register::WIDTH);
    }

    std::uint32_t
    Manager::readRegister(Register reg) {
        ASSERT(m_bar0 != 0);
        outl(m_bar0 + kPortOffsetIndex, static_cast<std::uint32_t>(reg));
        return inl(m_bar0 + kPortOffsetValue);
    }

    void
    Manager::writeRegister(Register reg, std::uint32_t value) {
        ASSERT(m_bar0 != 0);
        outl(m_bar0 + kPortOffsetIndex, static_cast<std::uint32_t>(reg));
        outl(m_bar0 + kPortOffsetValue, value);
    }

} // namespace kernel::device::display::vm_svga_ii
