#include "Kernel/Device/Input/PS2/PS2Controller.hpp"

#include "Kernel/Assert.hpp"
#include "Kernel/IO.hpp"
#include "Kernel/Print.hpp"

namespace kernel::device::input::ps2 {

    enum class PS2Command : std::uint8_t {
        READ_BYTE_0 = 0x20,
        WRITE_BYTE = 0x60,
        DISABLE_SECOND_PS2_PORT = 0xA7,
        ENABLE_SECOND_PS2_PORT = 0xA8,
        TEST_SECOND_PS2_PORT = 0xA9,
        TEST_PS2_CONTROLLER = 0xAA,
        TEST_FIRST_PS2_PORT = 0xAB,
        DIAGNOSTIC_DUMP = 0xAC,
        DISABLE_FIRST_PS2_PORT = 0xAD,
        ENABLE_FIRST_PS2_PORT = 0xAE,
        READ_CONTROLLER_INPUT_PORT = 0xC0,
        COPY_BITS_0_TO_3_TO_STATUS_4_TO_7 = 0xC1,
        COPY_BITS_4_TO_7_TO_STATUS_4_TO_7 = 0xC2,
        READ_CONTROLLER_OUTPUT_PORT = 0xD0,
        WRITE_NEXT_BYTE_TO_CONTROLLER_OUTPUT_PORT = 0xD1,
        WRITE_NEXT_BYTE_TO_FIRST_PS2_PORT_OUTPUT_BUFFER = 0xD2,
        WRITE_NEXT_BYTE_TO_SECOND_PS2_PORT_OUTPUT_BUFFER = 0xD2,
        WRITE_NEXT_BYTE_TO_SECOND_PS2_PORT_INPUT_BUFFER = 0xD3,
        // OUTPUT LINES
    };

    struct [[gnu::packed]] FirstConfigurationByte {
        bool firstPortInterrupt : 1;
        bool secondPortInterrupt : 1;
        bool systemFlag : 1;
        bool shouldBeZero : 1;
        bool firstPortClock : 1;
        bool secondPortClock : 1;
        bool firstPortTranslation : 1;
        bool mustBeZero : 1;
    };

    template <typename T>
    [[nodiscard]] T
    readPortDataTyped() {
        union {
            T result;
            std::uint8_t buffer;
        } data {.buffer = PS2Controller::readPortData()};
        return data.result;
    }

    static_assert(sizeof(FirstConfigurationByte) == 1);

    PS2Controller::PS2Controller() {
        klog<LogLevel::DEBUG>("PS/2") << "Initializing";

        // TODO 1. Make sure USB support is enabled
        // TODO 2. Make sure the PS/2 controller exists

        // 3. Disable devices
        writePortCommand(PS2Command::DISABLE_FIRST_PS2_PORT);
        writePortCommand(PS2Command::DISABLE_SECOND_PS2_PORT);

        // 4. Flush The Output Buffer
        static_cast<void>(readPortData());

        // 5. Set the Controller Configuration Byte
        writePortCommand(PS2Command::READ_BYTE_0);
        auto controllerConfigurationByte = readPortDataTyped<FirstConfigurationByte>();
        m_isDualChannel = controllerConfigurationByte.secondPortClock;
        controllerConfigurationByte.firstPortInterrupt = false;
        controllerConfigurationByte.secondPortInterrupt = false;
        controllerConfigurationByte.firstPortTranslation = false;
        writePortCommand(PS2Command::WRITE_BYTE, controllerConfigurationByte);

        // 6. Perform Controller Self Test
        writePortCommand(PS2Command::TEST_PS2_CONTROLLER);
        CHECK(readPortData() == 0x55);
        writePortCommand(PS2Command::WRITE_BYTE, controllerConfigurationByte);

        // 7. Verify that a 2nd Channel exists
        if (m_isDualChannel) {
            writePortCommand(PS2Command::ENABLE_SECOND_PS2_PORT);
            controllerConfigurationByte = readPortDataTyped<FirstConfigurationByte>();
            m_isDualChannel = !controllerConfigurationByte.secondPortClock; // TODO - is this correct?
            if (m_isDualChannel)
                writePortCommand(PS2Command::DISABLE_SECOND_PS2_PORT);
        }

        // 8. Perform Interface Tests
    }

    std::uint8_t
    PS2Controller::readPortData() {
        while (!readPortStatus().inputBufferAvailable) {
            // wait.
        }
        return inb(0x60);
    }

    PS2Controller::StatusRegister
    PS2Controller::readPortStatus() {
        union {
            std::uint8_t numeric;
            StatusRegister status;
        } data {.numeric = inb(0x64)};
        return data.status;
    }

    void
    PS2Controller::writePortData(std::uint8_t data) {
        while (!readPortStatus().outputBufferAvailable) {
            // wait.
        }
        outb(0x60, data);
    }

    void
    PS2Controller::writePortCommand(std::uint8_t data) {
        while (!readPortStatus().outputBufferAvailable) {
            // wait.
        }
        outb(0x64, data);
    }

} // namespace kernel::device::input::ps2
