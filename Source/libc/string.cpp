/*
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 *
 * Copyright (C) 2021 Tristan Gerritsen
 * All Rights Reserved.
 */

#define FREESIA_STRING_NO_INLINE
#include <string.h>

#include <cstddef>
#include "Kernel/Print.hpp"
#include "Kernel/Runtime/Crash.hpp"
#include "Kernel/Assert.hpp"

#ifdef FREESIA_CSTRING_DEBUG
#   include "Kernel/Print.hpp"
#   include "Kernel/Runtime/Crash.hpp"
#endif

extern "C" void *
memcpy(void *inDst, const void *inSrc, size_t len) __FREESIA_NOEXCEPT {
    if (len == 0) {
#ifdef FREESIA_CSTRING_DEBUG
        klog<LogLevel::WARNING>("memcpy") << "Length is zero! inDst=" << std::size_t(inDst) << " inSrc=" << std::size_t(inSrc);
#endif
        return inDst;
    }

    if (0x2C3940 >= std::size_t(inDst) && 0x2C3940 <= std::size_t(inDst) + len)
        klog<LogLevel::CRITICAL>("memcpy") << "Found the trouble maker";

#ifdef FREESIA_CSTRING_DEBUG
    if (std::size_t(inDst) > 0xC0000 || std::size_t(inSrc) > 0xC0000)
        klog<LogLevel::CRITICAL>(__PRETTY_FUNCTION__) << "inDst=" << std::size_t(inDst) << " inSrc=" << std::size_t(inSrc) << " len=" << len;
#endif

    auto *dst = reinterpret_cast<char *>(inDst);
    auto *src = reinterpret_cast<const char *>(inSrc);

    do {
        *dst = *src;

        ++dst;
        ++src;
        --len;
    } while (len > 0);

    return inDst;
}

extern "C" void *
memset(void *s, int c, size_t n) __FREESIA_NOEXCEPT {
    auto *data = reinterpret_cast<unsigned char *>(s);

//    static bool inMemset = false;
//    const auto shouldPrint = n == 0x6FEF && !inMemset;
//    if (shouldPrint)
//        g_textModeBufferView.disable_print_to_screen();
    for (size_t i = 0; i < n; ++i) {
//        if (shouldPrint) {
//            inMemset = true;
//            klog<LogLevel::DEBUG>("memset") << "data=" << std::size_t(data) << " c=" << std::size_t(c) << " n=" << n
//                                            << " i=" << i << " addr=" << std::size_t(&data[i]);
//            inMemset = false;
//        }
        data[i] = static_cast<unsigned char>(c);
    }
//    if (shouldPrint)
//        g_textModeBufferView.enable_print_to_screen();

    return s;
}

extern "C" std::size_t
strlen(const char *data) __FREESIA_NOEXCEPT {
    std::size_t ret{};

    while (*data++ != '\0') {
        ret++;
    }

    return ret;
}

extern "C" int
strncmp(const char *left, const char *right, std::size_t size) __FREESIA_NOEXCEPT {
    ASSERT(left != nullptr);
    ASSERT(right != nullptr);

    if (size == 0)
        return 0;

    do {
        if (*left != *right++)
            return *reinterpret_cast<const unsigned char *>(left) - *reinterpret_cast<const unsigned char *>(--right);

        VERIFY(*left++ != 0);
    } while (--size != 0);

    return 0;
}
