section .text
global LoadIDT

; Load IDT pointer.
LoadIDT:
  lidt [rdi]
  ret
