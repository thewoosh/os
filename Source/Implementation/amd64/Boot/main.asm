global boot_entrypoint
global multiboot_information_ptr
global paging_pml4
global paging_pdp_first
global paging_pd_first
global paging_pt_first
global paging_pdp_allocation_block
global paging_pd_allocation_block
global paging_pt_allocation_block
global stack_bottom
global stack_top
global gdt64_pointer

extern long_mode_entrypoint
extern multiboot_header_start
extern _init

section .text
bits 32
boot_entrypoint:
	XCHG bx, bx
	mov esp, stack_top
	mov [multiboot_information_ptr], ebx

	call check_multiboot

	call _init

	call check_cpuid
	call check_long_mode

	call setup_page_tables
	call enable_paging
	call enable_sse

	lgdt [gdt64.pointer]
	jmp gdt64.code_segment:long_mode_entrypoint

	hlt

check_multiboot:
	cmp eax, 0x36d76289
	jne .no_multiboot
	ret
.no_multiboot:
	mov al, "M"
	jmp error

check_cpuid:
	pushfd
	pop eax
	mov ecx, eax
	xor eax, 1 << 21
	push eax
	popfd
	pushfd
	pop eax
	push ecx
	popfd
	cmp eax, ecx
	je .no_cpuid
	ret
.no_cpuid:
	mov al, "C"
	jmp error

check_long_mode:
	mov eax, 0x80000000
	cpuid
	cmp eax, 0x80000001
	jb .no_long_mode

	mov eax, 0x80000001
	cpuid
	test edx, 1 << 29
	jz .no_long_mode

	ret
.no_long_mode:
	mov al, "L"
	jmp error

setup_page_tables:
	mov eax, paging_pdp_first
	or eax, 0b11 ; present, writable
	mov [paging_pml4], eax

	mov eax, paging_pd_first
	or eax, 0b11 ; present, writable
	mov [paging_pdp_first], eax

	mov eax, paging_pt_first
	or eax, 0b11 ; present, writable
	mov [paging_pd_first], eax

    mov eax, paging_pt_second
    or eax, 0b11 ; present, writable
    mov [paging_pd_first + 8], eax

	mov ecx, 0 ; counter
	; skip page 0 as that one is the NULL/zero page, which we don't want to map.

; This should 'allocate' 2 200 KiB blocks for the kernel to live in,
; it is reasonable now to think that the kernel won't take up more than
; 400 KiB.
.loop:
	mov eax, 0x1000 ; 4KiB
	mul ecx
	or eax, 0b11 ; present, writable

	; since paging_pt_second is directly after paging_pt_first, this is ok:
	mov [paging_pt_first + ecx * 8], eax

	inc ecx ; increment counter
	cmp ecx, 1024 ; checks if the whole table is mapped
	jne .loop ; if not, continue

	ret

enable_paging:
	; pass page table location to cpu
	mov eax, paging_pml4
	mov cr3, eax

	; enable PAE
	mov eax, cr4
	or eax, 1 << 5
	mov cr4, eax

	; enable long mode
	mov ecx, 0xC0000080
	rdmsr
	or eax, 1 << 8
	wrmsr

	; enable paging
	mov eax, cr0
	or eax, 1 << 31
	mov cr0, eax

	ret

enable_sse:
    mov eax, 0x1
    cpuid
    test edx, 1 << 25
    jz .end

    mov eax, cr0
    ; clear coprocessor emulation CR0.EM
    and ax, 0xFFFB
    ; set coprocessor monitoring  CR0.MP
    or ax, 0x2
    mov cr0, eax

    mov eax, cr4
    ; set CR4.OSFXSR and CR4.OSXMMEXCPT at the same time
    or ax, 3 << 9
    mov cr4, eax

.end:
    ret

error:
	; print "ERR: X" where X is the error code
	mov dword [0xb8000], 0x4f524f45
	mov dword [0xb8004], 0x4f3a4f52
	mov dword [0xb8008], 0x4f204f20
	mov byte  [0xb800a], al
	hlt

section .bss
align 4096
paging_pml4:
	resb 4096
paging_pdp_first:
	resb 4096
paging_pd_first:
	resb 4096
paging_pt_first:
    resb 4096
paging_pt_second:
    resb 4096
paging_pdp_allocation_block:
    resb 4096
paging_pd_allocation_block:
    resb 4096
paging_pt_allocation_block:
    resb 4096
stack_bottom:
	resb 4096 * 64
stack_top:

section .data
multiboot_information_ptr:
    dq 0
global null_pointer
null_pointer:
    dq 0

align 4
section .rodata
gdt64:
	dq 0 ; zero entry
.code_segment: equ $ - gdt64
;	dq (1 << 43) | (1 << 44) | (1 << 47) | (1 << 53) ; code segment
    dd 0x00001000
	dd 0x00209800
.data_segment: equ $ - gdt64
    dd 0x00000000
    dd 0x00209800
.pointer:
gdt64_pointer
	dw $ - gdt64 - 1 ; length
	dq gdt64 ; address