global long_mode_entrypoint
global flush_tss
extern kernel_entrypoint

section .text
bits 64
long_mode_entrypoint:
    ; load null into all data segment registers
    mov ax, 0
    mov ss, ax
    mov ds, ax
    mov es, ax
    mov fs, ax
    mov gs, ax

    call kernel_entrypoint

    hlt

flush_tss:
    mov ax, (5 * 8) | 0 ; fifth 8-byte selector, symbolically OR-ed with 0 to set the RPL (requested privilege level).
    ltr ax
    ret