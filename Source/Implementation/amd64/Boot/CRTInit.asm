bits 32

section .init
global _init
; type _init, @function
_init:
	push ebp
	mov ebp, esp
	; The compiler will but the contents of the constructors here

section .fini
global _fini
; type _fini, @function
_fini:
	push ebp
	mov ebp, esp
    ; The compiler will but the contents of the destructors here