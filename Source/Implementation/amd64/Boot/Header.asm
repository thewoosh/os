section .multiboot_header
global multiboot_header_start

multiboot_header_start:
header_start:
    ; Magic
	dd 0xe85250d6

	; Architecture: 32-bit protected mode i386
	dd 0

	; Header Length
	dd header_end - header_start


	; Checksum (result should be 0)
	dd 0x100000000 - (0xe85250d6 + 0 + (header_end - header_start))

	; Tags Section
	; Information Request
information_request_start:
	dw 1 ; type
	dw 1 ; flags: bit1=optional(1 = not optional)
	dd (information_request_end - information_request_start)

	dd  4 ; basic memory information
	dd  6 ; memory map
	dd  9 ; ELF symbols of the ELF kernel
	dd 12 ; EFI 64-bit system table pointer

information_request_end:

	; NULL tag specifies end
	dw 0
	dw 0
	dd 8
header_end: