#pragma once

#include <cstdint>

#ifdef AMD64_SHOW_GDT_ENTRY_STREAM_INSERTION_OPERATOR
#include "Kernel/Print.hpp"
#endif

namespace kernel {

    enum class PrivilegeLevel {
        RING_0,
        RING_1,
        RING_2,
        RING_3,
    };

    struct [[gnu::packed]] GDTEntry {
        std::uint16_t limit_low;
        std::uint16_t base_low;
        std::uint8_t base_high;
        bool accessed : 1;
        bool read_write : 1;
        bool conforming_expand_down : 1;
        bool is_code : 1; // Otherwise, data
        bool is_code_or_data_segment : 1;
        PrivilegeLevel descriptor_privilege_level : 2;
        bool present : 1;
        std::uint8_t limit_high : 4;
        bool available : 1;
        bool long_mode : 1;
        bool big : 1;
        bool use_4k_page_addressing : 1;
        std::uint8_t base_high_2 : 8;
    };

#define AMD64_SHOW_GDT_ENTRY_STREAM_INSERTION_OPERATOR
#ifdef AMD64_SHOW_GDT_ENTRY_STREAM_INSERTION_OPERATOR
    inline TextModeBufferViewWriter &
    operator<<(TextModeBufferViewWriter &stream, const GDTEntry &entry) {
#define B(v) (entry.v ? "true" : "false")

        stream << "{\n.limit_low = " << entry.limit_low << ", "
                << "\n.base_low = " << entry.base_low << ", "
                << "\n.base_high = " << entry.base_high << ", "
                << "\n.accessed = " << B(accessed) << ", "
                << "\n.read_write = " << B(read_write) << ", "
                << "\n.conforming_expand_down = " << B(conforming_expand_down) << ", "
                << "\n.is_code = " << B(is_code) << ", "
                << "\n.is_code_or_data_segment = " << B(is_code_or_data_segment) << ", "
                << "\n.descriptor_privilege_level = PrivilegeLevel::RING_" << TextModeBufferViewWriter::Decimal
                << static_cast<std::uint8_t>(entry.descriptor_privilege_level) << ", "
                << "\n.present = " << B(present) << ", "
                << "\n.limit_high = " << entry.limit_high << ", "
                << "\n.available = " << B(available) << ", "
                << "\n.long_mode = " << B(long_mode) << ", "
                << "\n.big = " << B(big) << ", "
                << "\n.use_4k_page_addressing = " << B(use_4k_page_addressing) << ", "
                << "\n.base_high_2 = " << entry.base_high_2
                << "\n}";

        return stream;
    }
#endif

    static_assert(sizeof(GDTEntry) == 8);

} // namespace kernel
