#pragma once

#include <cstdint>

namespace kernel {

    // Reference:
    //     AMD64 Architecture Programmer's Manual, Volume 2: System Programming - 24593
    //         12.2.5 64-Bit Task State Segment
    //         Figure 12-8. Long Mode TSS Format
    struct [[gnu::packed]] TSSEntry {
        std::uint32_t reserved0;
        std::uint64_t rsp0;
        std::uint64_t rsp1;
        std::uint64_t rsp2;
        std::uint64_t reserved1;
        std::uint64_t ist1;
        std::uint64_t ist2;
        std::uint64_t ist3;
        std::uint64_t ist4;
        std::uint64_t ist5;
        std::uint64_t ist6;
        std::uint64_t ist7;
        std::uint64_t reserved2;
        std::uint16_t reserved3;
        std::uint16_t io_map_base_address;
    };

    static_assert(sizeof(TSSEntry) == 0x68);

} // namespace kernel
