; used in Source/Implementation/Kernel/CPUInformation.cpp
; https://www.amd.com/system/files/TechDocs/25481.pdf

global cpuid_vendor_identification
global cpuid_processor_name
global cpuid_get_features

cpuid_vendor_identification:
    mov eax, 0 ; pull vendor info
    cpuid
    mov     [rdi], ebx
    mov     [rdi + 4], edx
    mov     [rdi + 8], ecx
    ret

cpuid_processor_name:
    mov eax, 0x80000002
    cpuid
    mov [rdi], eax
    mov [rdi + 4], ebx
    mov [rdi + 8], ecx
    mov [rdi + 12], edx

    mov eax, 0x80000003
    cpuid
    mov [rdi + 16], eax
    mov [rdi + 20], ebx
    mov [rdi + 24], ecx
    mov [rdi + 28], edx

    mov eax, 0x80000004
    cpuid
    mov [rdi + 32], eax
    mov [rdi + 36], ebx
    mov [rdi + 40], ecx
    mov [rdi + 44], edx

    ret

; void
; cpuid_get_features(std::uint32_t *ecx, std::uint32_t *edx);
cpuid_get_features:
    mov eax, 0x1
    cpuid
    mov [edi], ecx
    mov [esi], edx
    ret
