add_custom_target(compiler_flags)

set(COMPILER_FLAGS
        -fno-omit-frame-pointer
#        -fstack-protector-all
        -fno-stack-protector

        -nostdlib
        -ffreestanding
        -nostdinc
        -nostdinc++

        -fno-exceptions
        -fno-rtti
        -O2
)
