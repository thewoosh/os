/*
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 *
 * Copyright (C) 2021 Tristan Gerritsen
 * All Rights Reserved.
 */

#pragma once

/**
 * Format: YYYYMMDD
 */
#define __FREESIA__ 20210607

#if !defined(__LP64__) || __LP64__ != 1
#   error "Incorrect data model."
#endif

#ifndef __FREESIA_NODISCARD
#   if defined(__cplusplus) && __cplusplus >= 201703
#      define __FREESIA_NODISCARD [[nodiscard]]
#   elif defined(__GNUC__)
#       define __FREESIA_NODISCARD __attribute__((warn_unused_result))
#   else
#       define __FREESIA_NODISCARD
#   endif
#endif

#ifndef __FREESIA_NOEXCEPT
#   if defined(__cplusplus) && __cplusplus >= 201103L
#      define __FREESIA_NOEXCEPT noexcept
#   elif defined(__GNUC__)
#       define __FREESIA_NOEXCEPT __attribute__((nothrow))
#   else
#       define __FREESIA_NOEXCEPT
#   endif
#endif

#ifdef FREESIA_USE_RECOMMENDED_NODISCARD
#   define __FREESIA_RECOMMENDED_NODISCARD __FREESIA_NODISCARD
#else
#   define __FREESIA_RECOMMENDED_NODISCARD
#endif
