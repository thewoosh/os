/*
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 *
 * Copyright (C) 2021 Tristan Gerritsen
 * All Rights Reserved.
 */

#pragma once

#include <Freesia/Common.h>
#include <stddef.h>

#ifdef __cplusplus
extern "C" {
#endif

//#define FREESIA_STRING_NO_INLINE
#ifdef FREESIA_STRING_NO_INLINE

/**
 * Copy `n` times `c` into `s`.
 *
 * @returns s
 */
void *
memset(void *s, int c, size_t n) __FREESIA_NOEXCEPT;

#else // defined(FREESIA_STRING_NO_INLINE)

/**
 * Copy `n` times `c` into `s`.
 *
 * @returns s
 */
[[gnu::always_inline]] inline void *
memset(void *s, int c, size_t n) __FREESIA_NOEXCEPT {
    auto *data = reinterpret_cast<unsigned char *>(s);

    for (size_t i = 0; i < n; ++i) {
        data[i] = static_cast<unsigned char>(c);
    }

    return s;
}

#endif // !defined(FREESIA_STRING_NO_INLINE)

/**
 * Copy a block of `length` of `src` into `dst`.
 *
 * @returns dst
 */
void *
memcpy(void *dst, const void *src, size_t length) __FREESIA_NOEXCEPT;

/**
 * @returns the length of str
 */
__FREESIA_NODISCARD size_t
strlen(const char *str) __FREESIA_NOEXCEPT;

__FREESIA_NODISCARD int
strcmp(const char *, const char *) __FREESIA_NOEXCEPT;

__FREESIA_NODISCARD int
strncmp(const char *, const char *, size_t) __FREESIA_NOEXCEPT;

#ifdef __cplusplus
};
#endif
