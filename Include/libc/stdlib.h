/*
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 *
 * Copyright (C) 2021 Tristan Gerritsen
 * All Rights Reserved.
 */

#pragma once

#include <Freesia/Common.h>
#include <stddef.h>

#ifdef __cplusplus
extern "C" {
#endif

__FREESIA_NODISCARD void *
malloc(size_t n);

void
free(void *ptr);

#ifdef __cplusplus
};
#endif
