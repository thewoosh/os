/*
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 *
 * Copyright (C) 2021 Tristan Gerritsen
 * All Rights Reserved.
 */

#pragma once

#include <Freesia/Common.h>

#ifdef __cplusplus
#   define NULL nullptr
#else
#   define NULL ((void *)0)
#endif

typedef unsigned long int   size_t;
typedef   signed long int   ssize_t;

/**
 * The difference between two pointers
 * => decltype(p1 - p2);
 * => decltype(reinterpret_cast<int *>(0x1) - reinterpret_cast<int *>(0x2));
 */
typedef ssize_t             ptrdiff_t;

#define __FREESIA_OFFSETOF_RETURN_TYPE unsigned int *
#ifdef __cplusplus
#   define offsetof(TYPE, MEMBER) __builtin_offsetof(TYPE, MEMBER)
//        ((size_t) &((TYPE *)0)->MEMBER)//static_cast<__FREESIA_OFFSETOF_RETURN_TYPE>(&reinterpret_cast<TYPE *>(nullptr)->MEMBER);
#else
#   define offsetof(TYPE, MEMBER) ((__FREESIA_OFFSETOF_RETURN_TYPE) &((TYPE *)0)->MEMBER)
#endif
