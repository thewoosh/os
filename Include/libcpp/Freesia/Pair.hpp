#pragma once

#ifndef __FREESIA_UTILITY
#    include <utility>
#endif

namespace std {

    template<typename FirstType, typename SecondType>
    struct pair {
        FirstType first;
        SecondType second;


        __FREESIA_NODISCARD inline constexpr
        pair() __FREESIA_NOEXCEPT = default;

        __FREESIA_NODISCARD inline constexpr
        pair(pair &&) __FREESIA_NOEXCEPT = default;

        __FREESIA_NODISCARD inline constexpr
        pair(const pair &) __FREESIA_NOEXCEPT = default;


        inline constexpr pair &
        operator=(pair &&) __FREESIA_NOEXCEPT = default;

        inline constexpr pair &
        operator=(const pair &) __FREESIA_NOEXCEPT = default;


        __FREESIA_NODISCARD inline constexpr
        pair(FirstType &&first, SecondType &&second) __FREESIA_NOEXCEPT
                : first(move(first))
                , second(move(second)) {
        }

        __FREESIA_NODISCARD inline constexpr
        pair(const FirstType &first, const SecondType &second) __FREESIA_NOEXCEPT
                : first(first)
                , second(second) {
        }
    };

} // namespace std
