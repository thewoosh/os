/*
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 *
 * Copyright (C) 2021 Tristan Gerritsen
 * All Rights Reserved.
 */

#pragma once

namespace std {

    template <typename Type>
    __FREESIA_NODISCARD inline constexpr Type
    min(Type &&first, Type &&second) noexcept {
        return first < second ? first : second;
    }

    template <typename Type>
    __FREESIA_NODISCARD inline constexpr Type
    max(Type &&first, Type &&second) noexcept {
        return first > second ? first : second;
    }

    template <class _InputIterator, class _Predicate>
    __FREESIA_RECOMMENDED_NODISCARD constexpr bool
    all_of(_InputIterator first, _InputIterator last, _Predicate pred)
            noexcept(noexcept(pred(first))) {

        for (_InputIterator it = first; it != last; ++it) {
            if (!pred(it)) {
                return false;
            }
        }

        return true;
    }

    template <class _InputIterator, class _Predicate>
    __FREESIA_RECOMMENDED_NODISCARD constexpr bool
    any_of(_InputIterator first, _InputIterator last, _Predicate pred)
            noexcept(noexcept(pred(first))) {

        for (_InputIterator it = first; it != last; ++it) {
            if (pred(it)) {
                return true;
            }
        }

        return false;
    }

    template <class _InputIterator, class _Predicate>
    __FREESIA_RECOMMENDED_NODISCARD constexpr bool
    none_of(_InputIterator first, _InputIterator last, _Predicate pred)
            noexcept(noexcept(pred(first))) {

        for (_InputIterator it = first; it != last; ++it) {
            if (pred(it)) {
                return false;
            }
        }

        return true;
    }

    template<class _InputIterator, class _Function>
    constexpr _Function
    for_each(_InputIterator first, _InputIterator last, _Function func)
            noexcept(noexcept(func(first))) {

        for (_InputIterator it = first; it != last; ++it) {
            func(it);
        }

        return func;
    }

    template<class _InputIterator, class Size, class _Function>
    constexpr _Function
    for_each_n(_InputIterator first, Size n, _Function func)
            noexcept(noexcept(func(first))) {

        for (Size i = 0; i < n; ++i) {
            static_cast<void>(i);
            func(first++);
        }

        return func;
    }

    template<class _InputIterator, class _Type>
    __FREESIA_RECOMMENDED_NODISCARD constexpr _InputIterator
    find(_InputIterator first, _InputIterator last, const _Type &value)
            noexcept(noexcept(first++) && noexcept(*first) && noexcept(value == value)) {

        for (_InputIterator it = first; it != end; ++it) {
           if (*it == value) {
               return it;
           }
        }

        return last;
    }

    template<class _InputIterator, class _Predicate>
    __FREESIA_RECOMMENDED_NODISCARD constexpr _InputIterator
    find_if(_InputIterator first, _InputIterator last, _Predicate pred)
            noexcept(noexcept(pred(first))) {

        for (_InputIterator it = first; it != end; ++it) {
            if (pred(it)) {
                return it;
            }
        }

        return last;
    }

    template<class _InputIterator, class _Predicate>
    __FREESIA_RECOMMENDED_NODISCARD constexpr _InputIterator
    find_if_not(_InputIterator first, _InputIterator last, _Predicate pred)
            noexcept(noexcept(pred(first))) {

        for (_InputIterator it = first; it != end; ++it) {
            if (!pred(it)) {
                return it;
            }
        }

        return last;
    }

} // namespace std

