/*
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 *
 * Copyright (C) 2021 Tristan Gerritsen
 * All Rights Reserved.
 */

#pragma once

namespace std {

    template<class _InputIterator, class _OutputIterator>
    constexpr _OutputIterator
    copy(_InputIterator inputFirst, _InputIterator inputLast, _OutputIterator outputFirst)
            noexcept(noexcept(*inputFirst = *outputFirst)) {

        while (inputFirst != inputLast) {
            *outputFirst++ = *inputFirst++;
        }

        return outputFirst;
    }

    template<class _InputIterator, class _ValueType>
    constexpr void
    fill(_InputIterator first, _InputIterator last, const _ValueType &value)
            noexcept(noexcept(*first = value)) {

        for (_InputIterator it = first; it != last; ++it) {
            *it = value;
        }
    }

} // namespace std

