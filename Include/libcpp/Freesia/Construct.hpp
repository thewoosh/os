#pragma once

#include <utility>
#include <new>

namespace std {

    template<class Type, class... Args>
    inline constexpr Type *
    construct_at(Type *ptr, Args &&...args) __FREESIA_NOEXCEPT {
        return new(static_cast<void *>(ptr)) Type(std::forward<Args>(args)...);
    }

} // namespace std
