/*
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 *
 * Copyright (C) 2021 Tristan Gerritsen
 * All Rights Reserved.
 */

#pragma once

#include <cstddef>

#include "Common.hpp"

namespace std {

    template <class _Type, std::size_t _Size>
    [[nodiscard]] __FREESIA_ALWAYS_INLINE constexpr bool
    empty(const _Type (&)[_Size]) noexcept {
        return _Size == 0;
    }

    template <class _Type, std::size_t _Size>
    [[nodiscard]] __FREESIA_ALWAYS_INLINE constexpr std::size_t
    size(const _Type (&)[_Size]) noexcept {
        return _Size;
    }

#define __FREESIA_DEFINE_ITERATOR_UTILITY(name)          \
    /* non-const variant */                              \
    template <typename _Type>                            \
    [[nodiscard]] __FREESIA_ALWAYS_INLINE constexpr auto \
    name(_Type &_in) -> decltype(_in.name()) {           \
        return _in.name();                               \
    }                                                    \
    /* const variant */                                  \
    template <typename _Type>                            \
    [[nodiscard]] __FREESIA_ALWAYS_INLINE constexpr auto \
    name(const _Type &_in) -> decltype(_in.name()) {     \
        return _in.name();                               \
    }


    __FREESIA_DEFINE_ITERATOR_UTILITY(data)
    __FREESIA_DEFINE_ITERATOR_UTILITY(empty)
    __FREESIA_DEFINE_ITERATOR_UTILITY(size)

    __FREESIA_DEFINE_ITERATOR_UTILITY(begin)
    __FREESIA_DEFINE_ITERATOR_UTILITY(end)

    __FREESIA_DEFINE_ITERATOR_UTILITY(cbegin)
    __FREESIA_DEFINE_ITERATOR_UTILITY(cend)

    __FREESIA_DEFINE_ITERATOR_UTILITY(rbegin)
    __FREESIA_DEFINE_ITERATOR_UTILITY(rend)

    __FREESIA_DEFINE_ITERATOR_UTILITY(crbegin)
    __FREESIA_DEFINE_ITERATOR_UTILITY(crend)

} // namespace std
