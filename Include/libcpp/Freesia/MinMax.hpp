#pragma once

#include "Common.hpp"

namespace std {

    template<typename Type>
    __FREESIA_ALWAYS_INLINE constexpr const Type &
    max(const Type &a, const Type &b) {
        return (b > a) ? b : a;
    }

    template<typename Type>
    __FREESIA_ALWAYS_INLINE constexpr const Type &
    min(const Type &a, const Type &b) {
        return (b < a) ? b : a;
    }

} // namespace std
