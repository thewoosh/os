#pragma once

#define __FREESIA_UTILITY

#include <Freesia/Common.hpp>

#include <type_traits>

namespace std {

    template<typename T>
    [[nodiscard]] [[gnu::always_inline]] constexpr T &&
    forward(std::remove_reference_t<T> &t) __FREESIA_NOEXCEPT {
        return static_cast<T &&>(t);
    }

    template<typename T>
    [[nodiscard]] [[gnu::always_inline]] constexpr T &&
    forward(std::remove_reference_t<T> &&t) __FREESIA_NOEXCEPT {
        return static_cast<T &&>(t);
    }

    template<typename T>
    [[nodiscard]] [[gnu::always_inline]] typename remove_reference<T>::type &&
    move(T &&t) __FREESIA_NOEXCEPT {
        return static_cast<typename remove_reference<T>::type &&>(t);
    }

} // namespace std

#include <Freesia/Pair.hpp>
