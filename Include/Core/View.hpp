#pragma once

#include <cstddef>

#ifndef CORE_VIEW_WITHOUT_BOUNDS_CHECK
#include "Kernel/Assert.hpp"
#endif

namespace core {

    template<typename Type, std::size_t Size>
    struct View {
        [[nodiscard]] inline constexpr explicit
        View(Type *begin)
                : m_begin(begin) {
        }

        template <std::size_t Index>
        [[nodiscard]] inline constexpr Type &
        at() {
            static_assert(Index < Size);
            return m_begin[Index];
        }

        template <std::size_t Index>
        [[nodiscard]] inline constexpr const Type &
        at() const {
            static_assert(Index < Size);
            return m_begin[Index];
        }

        [[nodiscard]] inline constexpr Type &
        at_runtime(std::size_t index) {
#ifndef CORE_VIEW_WITHOUT_BOUNDS_CHECK
            CHECK(index < Size);
#endif
            return m_begin[index];
        }

        [[nodiscard]] inline constexpr const Type &
        at_runtime(std::size_t index) const {
#ifndef CORE_VIEW_WITHOUT_BOUNDS_CHECK
            CHECK(index < Size);
#endif
            return m_begin[index];
        }

    private:
        Type *m_begin;
    };

} // namespace core