#pragma once

#include <cstddef> // for std::size_t

namespace core::constant_size {

    template<typename FirstType, typename... OtherTypes>
    struct biggest {
        static const std::size_t size =
                sizeof(FirstType) > biggest<OtherTypes...>::size ? sizeof(FirstType) : biggest<OtherTypes...>::size;
    };

    template<typename F>
    struct biggest<F>  {
        static const std::size_t size = sizeof(F);
    };

} // namespace core::constant_size
