#pragma once

#include <utility>
#include <type_traits>

namespace core {

    template <class Function>
    struct LambdaTraits
            : LambdaTraits<decltype(&Function::operator())> {
    };

    template <typename Function, typename ReturnType, typename... Args>
    struct LambdaTraits<ReturnType (Function::*)(Args...)>
            : LambdaTraits<ReturnType (Function::*)(Args...) const> {
    };

    template <class Function, class ReturnType, class... Args>
    struct LambdaTraits<ReturnType(Function::*)(Args...) const> {
        using pointer = typename std::add_pointer<ReturnType(Args...)>::type;

        [[nodiscard]] static pointer 
        lambda(Function &&function) {
            static Function func = std::forward<Function>(function);
            return [](Args... args) {
                return func(std::forward<Args>(args)...);
            };
        }
    };

    template <class Function>
    inline typename LambdaTraits<Function>::pointer
    lambda(Function &&function) {
        return LambdaTraits<Function>::lambda(std::forward<Function>(function));
    }

} // namespace core