#pragma once

#include <cstddef>

#include "Kernel/Assert.hpp"

namespace core {

    template <typename T>
    struct RegionPointer {
        struct ConstIterator {
            [[nodiscard]] inline constexpr
            ConstIterator(const RegionPointer<T> &parent, std::size_t index)
                    : m_parent(&parent)
                    , m_index(index) {
            }

            inline constexpr
            ConstIterator(ConstIterator &&) noexcept = default;
            inline constexpr
            ConstIterator(const ConstIterator &) noexcept = default;
            inline constexpr ConstIterator &
            operator=(ConstIterator &&) noexcept = default;
            inline constexpr ConstIterator &
            operator=(const ConstIterator &) noexcept = default;

            [[nodiscard]] inline constexpr std::size_t
            base() const {
                return m_index;
            }

            inline constexpr ConstIterator &
            operator++() {
                ++m_index;
                CHECK(m_index <= m_parent->size());
                return *this;
            }

            inline constexpr ConstIterator &
            operator--() {
                CHECK(m_index > 0);
                --m_index;
                return *this;
            }

            [[nodiscard]] inline constexpr ConstIterator
            operator+(std::size_t size) {
                CHECK(m_index + size <= m_parent->size());
                return {*m_parent, m_index + size};
            }

            [[nodiscard]] inline constexpr ConstIterator
            operator-(std::size_t size) {
                CHECK(m_index >= size);
                return {*m_parent, m_index - size};
            }

            [[nodiscard]] inline constexpr ConstIterator
            operator+(const ConstIterator &other) {
                CHECK(m_parent == other.m_parent);
                CHECK(m_index + other.m_index < m_parent->size());
                return {*m_parent, m_index + other.m_index};
            }

            [[nodiscard]] inline constexpr ConstIterator
            operator-(const ConstIterator &other) {
                CHECK(m_parent == other.m_parent);
                CHECK(m_index >= other.m_index);
                return {*m_parent, m_index - other.m_index};
            }

            inline constexpr ConstIterator &
            operator+=(std::size_t size) {
                CHECK(m_index + size <= m_parent->size());
                m_index += size;
                return *this;
            }

            inline constexpr ConstIterator &
            operator-=(std::size_t size) {
                CHECK(long(m_index - size) >= 0);
                CHECK(long(m_index - size) < m_parent->size());
                m_index -= size;
                return *this;
            }

            inline constexpr const T &
            operator*() const {
                return m_parent->operator[](m_index);
            }

            inline constexpr const T *
            operator->() const {
                return &m_parent->operator[](m_index);
            }

            inline constexpr const T &
            operator[](std::size_t value) const {
                return m_parent->operator[](m_index + value);
            }

            [[nodiscard]] inline constexpr bool
            operator==(const ConstIterator &other) const {
                return *m_parent == *other.m_parent && m_index == other.m_index;
            }

            [[nodiscard]] inline constexpr bool
            operator<(const ConstIterator &other) const {
                CHECK(*m_parent == *other.m_parent);
                return m_index < other.m_index;
            }

            [[nodiscard]] inline constexpr bool
            operator>(const ConstIterator &other) const {
                CHECK(*m_parent == *other.m_parent);
                return m_index > other.m_index;
            }

            template<typename OutputType>
            inline void
            copy_with_assertion(OutputType &output) const {
                CHECK(m_index + sizeof(OutputType) < m_parent->size());
                std::memcpy(&output, &m_parent->operator[](m_index), sizeof(OutputType));
            }

            template<typename OutputType>
            [[nodiscard]] inline bool
            copy_with_status(OutputType &output) const {
                if (m_index + sizeof(OutputType) > m_parent->size())
                    return false;

                std::memcpy(&output, &m_parent->operator[](m_index), sizeof(OutputType));
                return true;
            }

        private:
            const RegionPointer<T> *m_parent;
            std::size_t m_index;
        };

        [[nodiscard]] inline constexpr
        RegionPointer(T *begin, std::size_t size)
                : m_begin(begin)
                , m_size(size) {
        }

        [[nodiscard]] inline constexpr bool
        operator==(const RegionPointer &other) const {
            return m_size == other.m_size && m_begin == other.m_begin;
        }

        [[nodiscard]] inline constexpr T &
        operator*() {
            return *m_begin;
        }

        [[nodiscard]] inline constexpr const T &
        operator*() const {
            return *m_begin;
        }

        [[nodiscard]] inline constexpr T *
        operator->() {
            return m_begin;
        }

        [[nodiscard]] inline constexpr const T *
        operator->() const {
            return m_begin;
        }

        [[nodiscard]] inline T &
        operator[](std::size_t index) {
            CHECK(index < m_size);
            return m_begin[index];
        }

        [[nodiscard]] inline const T &
        operator[](std::size_t index) const {
            CHECK(index < m_size);
            return m_begin[index];
        }

        [[nodiscard]] inline constexpr ConstIterator
        begin() const {
            return {*this, 0};
        }

        [[nodiscard]] inline constexpr ConstIterator
        end() const {
            return {*this, m_size};
        }

        [[nodiscard]] inline constexpr std::size_t
        size() const {
            return m_size;
        }

    private:
        T *m_begin;
        std::size_t m_size;
    };

} // namespace core