#pragma once

#include <cstddef>

#ifndef CORE_VARIABLE_VIEW_WITHOUT_BOUNDS_CHECK
#include "Kernel/Assert.hpp"
#endif

namespace core {

    template<typename Type>
    struct VariableView {
        [[nodiscard]] inline constexpr explicit
        VariableView(Type *begin, std::size_t size)
                : m_begin(begin)
                , m_size(size) {
        }

        [[nodiscard]] inline constexpr Type &
        at_runtime(std::size_t index) {
#ifndef CORE_VARIABLE_VIEW_WITHOUT_BOUNDS_CHECK
            CHECK(index < m_size);
#endif
            return m_begin[index];
        }

        [[nodiscard]] inline constexpr const Type &
        at_runtime(std::size_t index) const {
#ifndef CORE_VARIABLE_VIEW_WITHOUT_BOUNDS_CHECK
            CHECK(index < m_size);
#endif
            return m_begin[index];
        }

        [[nodiscard]] inline constexpr const Type *
        begin() const {
            return m_begin;
        }

        [[nodiscard]] inline constexpr const Type *
        end() const {
            return m_begin + m_size;
        }

    private:
        Type *m_begin;
        std::size_t m_size;
    };

} // namespace core