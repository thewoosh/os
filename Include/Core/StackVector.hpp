#pragma once

#include <utility>

#include <cstddef>
#include <cstring>

#include <Freesia/Construct.hpp>

#ifndef CORE_STACK_VECTOR_NO_CRASH_ON_OVERFLOW
#    include <Kernel/Runtime/Crash.hpp>
#endif

namespace core {

    template <typename Contained, std::size_t Size>
    class StackVector {
        static_assert(Size > 0);

    public:
        [[nodiscard]] inline constexpr auto &
        operator[](std::size_t index) {
            return m_data[index];
        }

        [[nodiscard]] inline constexpr const auto &
        operator[](std::size_t index) const {
            return m_data[index];
        }

        [[nodiscard]] inline constexpr std::size_t
        size() const {
            return m_count;
        }

        [[nodiscard]] inline constexpr std::size_t
        max_size() const {
            return Size;
        }

        [[nodiscard]] inline constexpr auto &
        front() {
            return m_data[0];
        }

        [[nodiscard]] inline constexpr const auto &
        front() const {
            return m_data[0];
        }

        [[nodiscard]] inline constexpr auto &
        back() {
            return m_data[m_count - 1];
        }

        [[nodiscard]] inline constexpr const auto &
        back() const {
            return m_data[m_count - 1];
        }

        inline constexpr void
        push_back(Contained &&element) {
            if (m_count + 1 == Size) {
#ifndef CORE_STACK_VECTOR_NO_CRASH_ON_OVERFLOW
                CRASH("StackVector overflow");
#endif
                return;
            }
            std::construct_at(&m_data[m_count], std::move(element));
            m_count++;
        }

        inline constexpr void
        push_back(const Contained &element) {
            if (m_count + 1 == Size) {
#ifndef CORE_STACK_VECTOR_NO_CRASH_ON_OVERFLOW
                CRASH("StackVector overflow");
#endif
                return;
            }
            m_data[m_count++] = element;
        }

        inline constexpr void
        pop_back() {
            if (m_count == 0)
                return;
            m_data[m_count - 1].~Contained();
            std::memset(&m_data[m_count - 1], 0, sizeof(Contained));
            --m_count;
        }

        inline constexpr Contained &
        emplace_back() {
            if (m_count + 1 == Size) {
#ifndef CORE_STACK_VECTOR_NO_CRASH_ON_OVERFLOW
                CRASH("StackVector overflow");
#endif
                return back();
            }
            std::construct_at(&m_data[m_count]);
            return m_data[m_count++];
        }

        inline constexpr void
        push_back_range(const auto &element) {
            for (const auto &entry : element)
                push_back(entry);
        }

        [[nodiscard]] inline constexpr Contained *
        begin() {
            return m_data;
        }

        [[nodiscard]] inline constexpr Contained *
        end() {
            return &m_data[m_count];
        }

        [[nodiscard]] inline constexpr const Contained *
        begin() const {
            return m_data;
        }

        [[nodiscard]] inline constexpr const Contained *
        end() const {
            return &m_data[m_count];
        }

        [[nodiscard]] inline constexpr const Contained *
        cbegin() const {
            return m_data;
        }

        [[nodiscard]] inline constexpr const Contained *
        cend() const {
            return &m_data[m_count];
        }

        inline void
        clear() {
            std::memset(m_data, 0, Size);
            m_count = 0;
        }

        [[nodiscard]] inline constexpr bool
        empty() const {
            return m_count == 0;
        }

        [[nodiscard]] inline constexpr bool
        full() const {
            return m_count == Size - 1;
        }

    private:
        Contained m_data[Size]{};
        std::size_t m_count{0};
    };

} // namespace core