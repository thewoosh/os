#pragma once

#include <cstddef>

namespace core {

    /**
     * Advance the pointer with byte offset. When using a pointer type which
     * pointee has a length other than 1, it can be a pain to decrease/increase
     * the pointer with byte offsets instead of the type offsets.
     *
     * For example:
     *   A pointer to `std::uint32_t` might need to move 2 bytes. When using ptr+2
     *   the pointer doesn't get moved 2 bytes, but 8 (2 * sizeof(std::uint32_t))
     *   bytes.
     *
     * Note that this function cannot be `constexpr`, because we use a union.
     */
    template <typename PointerType, typename SizeType>
    [[nodiscard]] [[gnu::always_inline]] inline PointerType *
    advance_pointer_with_byte_offset(PointerType *ptr, SizeType advance) {
        union {
            PointerType *pointer;
            std::size_t integer;
        } data{
            .pointer = ptr
        };

        data.integer += std::size_t(advance);

        return data.pointer;
    }

    [[nodiscard]] inline bool
    does_pointer_lay_within(const void *pointer, const void *region_begin, std::size_t region_size) {
        return std::size_t(pointer) >= std::size_t(region_begin)
            && std::size_t(pointer) < (std::size_t(region_begin) + region_size);
    }

} // namespace core