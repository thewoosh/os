#pragma once

namespace core {

    /**
     * This class can be used to replicate the 'break' and 'continue' keywords
     * in for-loops, when using Functors with for_each() functions.
     */
    enum class IterationDecision {

        // Stop the iteration.
        BREAK,

        // Continue the iteration. If the current position is the last, this
        // decision has the same effect as a BREAK.
        CONTINUE,

    };

} // namespace core