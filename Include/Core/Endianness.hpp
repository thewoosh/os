#pragma once

#include <bit>

namespace core {

    // ntohs & htons, ntohl & htonl equivalent
    template <typename T>
    [[nodiscard]] inline constexpr T
    endian_swap_network_and_host(T t) {
        if constexpr (std::endian::native == std::endian::big)
            return t;
        else
            return std::byteswap(t);
    }

} // namespace core
