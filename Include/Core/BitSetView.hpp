#pragma once

#include <cstddef>
#include <cstdint>

namespace core {

    struct BitSetView {
        BitSetView() = default;
        BitSetView(BitSetView &&) = default;
        BitSetView(const BitSetView &) = default;
        BitSetView &operator=(BitSetView &&) = default;
        BitSetView &operator=(const BitSetView &) = default;

        [[nodiscard]] inline constexpr
        BitSetView(std::uint8_t *data, std::size_t size_in_bytes)
                : m_data(data)
                , m_size_in_bytes(size_in_bytes) {
        }

        [[nodiscard]] [[gnu::always_inline]] inline constexpr bool
        operator[](std::size_t index) const {
            return m_data[index / 8] & (1 << (index % 8));
        }

        [[nodiscard]] inline constexpr std::size_t
        buffer_size() const {
            return m_size_in_bytes;
        }

        [[nodiscard]] inline constexpr std::size_t
        count() const {
            return m_size_in_bytes * 8;
        }

        [[nodiscard]] inline constexpr std::uint8_t *
        data() const {
            return m_data;
        }

        inline constexpr void
        set(std::size_t index, bool value) const {
            const auto bit = 1 << (index % 8);
            m_data[index / 8] &= ~bit;

            if (value)
                m_data[index / 8] |= bit;
        }

    private:
        std::uint8_t *m_data;
        std::size_t m_size_in_bytes;
    };

} // namespace core