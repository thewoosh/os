#pragma once

namespace core {

    struct Void {
        Void() = delete;
        Void(const Void &) = delete;
        Void(Void &&) = delete;
        ~Void() = delete;

        const void *
        operator->() = delete;

        const void *
        operator*() = delete;

        auto
        operator<=>(const auto &) = delete;
    };

} // namespace core