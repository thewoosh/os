#pragma once

namespace kernel {

    class MemoryManager;

    extern MemoryManager *g_memory_manager;

} // namespace kernel
