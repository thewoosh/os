#pragma once

#include <cstdint>

namespace kernel::pc_screen_font {

    using ColorType = std::uint32_t;
    using DrawPixelCallback = void (*)(std::uint32_t x, std::uint32_t y, ColorType);

    namespace colors {

        inline constexpr const ColorType kBlack{0};
        inline constexpr const ColorType kWhite{0xFFFFFFFF};

    } // namespace colors

    [[nodiscard]] bool
    initialize(std::uint32_t width, std::uint32_t height, DrawPixelCallback);

    void
    renderCharacter(ColorType, char);

} // namespace kernel::pc_screen_font
