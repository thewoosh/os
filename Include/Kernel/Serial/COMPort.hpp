/**
 * Copyright (C) 2021 Tristan Gerritsen <tristan-legal@outlook.com>
 * All Rights Reserved
 *
 * SPDX-License-Identifier: BSD-3-Clause
 */

#pragma once

#include <string_view>

#include "Include/Kernel/Serial/Port.hpp"

namespace Serial {

    class COMPort
            : public Port {
    public:
        [[nodiscard]] explicit
        COMPort(PortName portName) noexcept;

        void
        write(COMLine line, std::uint8_t data) noexcept;

        void
        writeToVM(std::string_view) noexcept;
    };

} // namespace Serial
