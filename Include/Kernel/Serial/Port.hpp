/*
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 *
 * Copyright (C) 2021 Tristan Gerritsen
 * All Rights Reserved.
 */

#pragma once

#include <cstdint>

#include "Source/Kernel/Serial/COMLine.hpp"

namespace Serial {

    enum class PortName {
        COM1 = 0x3F8,
        COM2 = 0x2F8,
        COM3 = 0x3E8,
        COM4 = 0x2E8
    };

    class Port {
        PortName m_name;

    protected:
        [[nodiscard]] inline constexpr explicit
        Port(PortName name) noexcept
                : m_name(name) {
        }

        [[nodiscard]] inline constexpr std::uint16_t
        intPort() const noexcept {
            return static_cast<std::uint16_t>(m_name);
        }

        [[nodiscard]] bool
        isBlocking() noexcept;

        void
        writeIO(std::uint16_t line, std::uint8_t data) noexcept;
    };

} // namespace Serial
