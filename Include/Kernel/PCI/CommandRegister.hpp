#pragma once

namespace kernel::pci {

    struct CommandRegister {
    public:
        bool ioSpace : 1;
        bool memorySpace : 1;
        bool busMaster : 1;
        bool specialCycles : 1;
        bool memoryWriteAndInvalidateEnable : 1;
        bool vgaPaletteSnoop : 1;
        bool parityErrorResponse : 1;

        // Make the reserved fields private instead of deprecated to allow the
        // copy constructor to work...
    private:
        [[maybe_unused]] bool reserved0 : 1;

    public:
        bool serrEnable : 1;
        bool fastBackToBackEnable : 1;
        bool interruptDisable : 1;

    private:
        [[maybe_unused]] bool reserved1 : 1;
        [[maybe_unused]] bool reserved2 : 1;
        [[maybe_unused]] bool reserved3 : 1;
        [[maybe_unused]] bool reserved4 : 1;
        [[maybe_unused]] bool reserved5 : 1;
    };

    static_assert(sizeof(CommandRegister) == 2);

} // namespace kernel::pci
