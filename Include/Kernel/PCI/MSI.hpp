#pragma once

#include "Kernel/PCI/Capability.hpp"

namespace kernel::pci {

    struct [[gnu::packed]] MSI final
            : public Capability {

        [[nodiscard]] inline constexpr
        MSI(std::uint8_t offsetToNext, std::uint16_t messageControl, std::uint32_t messageAddressLow,
            std::uint32_t messageAddressHigh, std::uint16_t messageData, std::uint32_t mask, std::uint32_t pending)
                : Capability(sizeof(MSI), CapabilityType::MESSAGE_SIGNAL_INTERRUPTS, offsetToNext)
                , m_messageControl(messageControl)
                , m_messageAddressLow(messageAddressLow)
                , m_messageAddressHigh(messageAddressHigh)
                , m_messageData(messageData)
                , m_mask(mask)
                , m_pending(pending) {
        }

        [[nodiscard]] inline constexpr std::uint16_t
        messageControl() const {
            return m_messageControl;
        }

        [[nodiscard]] inline constexpr std::uint32_t
        messageAddressLow() const {
            return m_messageAddressLow;
        }

        [[nodiscard]] inline constexpr std::uint32_t
        messageAddressHigh() const {
            return m_messageAddressHigh;
        }

        [[nodiscard]] inline constexpr std::uint16_t
        messageData() const {
            return m_messageData;
        }

        [[nodiscard]] inline constexpr std::uint32_t
        mask() const {
            return m_mask;
        }

        [[nodiscard]] inline constexpr std::uint32_t
        pending() const {
            return m_pending;
        }

    private:
        std::uint16_t m_messageControl;
        std::uint32_t m_messageAddressLow;
        std::uint32_t m_messageAddressHigh;
        std::uint16_t m_messageData;
        std::uint32_t m_mask;
        std::uint32_t m_pending;
    };

} // namespace kernel::pci
