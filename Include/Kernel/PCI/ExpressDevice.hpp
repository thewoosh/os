#pragma once

#include "Kernel/PCI/Device.hpp"

namespace kernel::pci {

    struct StatusRegisterValue;

    class ExpressDevice final
            : public AbstractDevice {
    public:
        [[nodiscard]] inline static constexpr void *
        calculateAddress(void *segmentBaseAddress, std::uint8_t bus, std::uint8_t slot, std::uint8_t function) {
            return static_cast<std::uint8_t *>(segmentBaseAddress) + (((bus * 256) + (slot * 8) + function) * 4096);
        }

        [[nodiscard]]
        ExpressDevice() = default;

        [[nodiscard]]
        ExpressDevice(void *segmentBaseAddress, std::uint8_t bus, std::uint8_t slot, std::uint8_t function);

        void
        forEachCapability(void (*)(const Capability &)) const override;

        [[nodiscard]] BaseAddressRegister
        getBaseAddressRegister0() override;

        [[nodiscard]] BaseAddressRegister
        getBaseAddressRegister1() override;

        [[nodiscard]] BaseAddressRegister
        getBaseAddressRegister2() override;

        [[nodiscard]] BaseAddressRegister
        getBaseAddressRegister3() override;

        [[nodiscard]] BaseAddressRegister
        getBaseAddressRegister4() override;

        [[nodiscard]] BaseAddressRegister
        getBaseAddressRegister5() override;

        [[nodiscard]] bool
        hasCapabilities() const override;

        template <typename T>
        [[nodiscard]] inline const T &
        read(std::uint16_t offset) const {
            return *static_cast<const T *>(static_cast<void *>(&static_cast<std::uint8_t *>(m_deviceAddress)[offset]));
        }

        [[nodiscard]] inline std::uint8_t
        readConfigByte(std::uint16_t offset) const override {
            return read<std::uint8_t>(offset);
        }

        [[nodiscard]] inline std::uint32_t
        readConfigDWord(std::uint16_t offset) const override {
            return read<std::uint32_t>(offset);
        }

        [[nodiscard]] inline std::uint16_t
        readConfigWord(std::uint16_t offset) const override {
            return read<std::uint16_t>(offset);
        }

        template <typename T>
        inline void
        write(std::uint16_t offset, const T &data) const {
            *static_cast<T *>(static_cast<void *>(&static_cast<std::uint8_t *>(m_deviceAddress)[offset])) = data;
        }

        inline void
        writeConfigByte(std::uint16_t offset, std::uint8_t value) override {
            write(offset, value);
        }

        inline void
        writeConfigDWord(std::uint16_t offset, std::uint32_t value) override {
            // TODO ensure endianness (since PCI is little-endian)
            write(offset, value);
        }

        inline void
        writeConfigWord(std::uint16_t offset, std::uint16_t value) override {
            // TODO ensure endianness (since PCI is little-endian)
            write(offset, value);
        }

    private:
        void *m_deviceAddress{nullptr};

        [[nodiscard]] StatusRegisterValue
        readStatusRegister() const;
    };

} // namespace kernel::pci
