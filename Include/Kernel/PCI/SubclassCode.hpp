#pragma once

#include <cstdint>

#include "ClassCode.hpp"

namespace kernel::pci {

    enum class SubclassCode
            : std::uint8_t {

#define PCI_ITERATE_SUBCLASS_CODES_BASE_UNCLASSIFIED(REGISTER_SUBCLASS_CODE) \
        REGISTER_SUBCLASS_CODE(UNCLASSIFIED_NON_VGA_COMPATIBLE_UNCLASSIFIED_DEVICE, 0x0, "Non-VGA-Compatible Unclassified Device") \
        REGISTER_SUBCLASS_CODE(UNCLASSIFIED_VGA_COMPATIBLE_UNCLASSIFIED_DEVICE, 0x1, "VGA-Compatible Unclassified Device") \


#define PCI_ITERATE_SUBCLASS_CODES_BASE_MASS_STORAGE_CONTROLLER(REGISTER_SUBCLASS_CODE) \
        REGISTER_SUBCLASS_CODE(MSA_SCSI_BUS_CONTROLLER, 0x0, "SCSI Bus Controller") \
        REGISTER_SUBCLASS_CODE(MSA_IDE_CONTROLLER, 0x1, "IDE Controller") \
        REGISTER_SUBCLASS_CODE(MSA_FLOPPY_DISK, 0x2, "Floppy Disk Controller") \
        REGISTER_SUBCLASS_CODE(MSA_IPI_BUS_CONTROLLER, 0x3, "IPI Bus Controller") \
        REGISTER_SUBCLASS_CODE(MSA_RAID_CONTROLLER, 0x4, "RAID Controller") \
        REGISTER_SUBCLASS_CODE(MSA_ATA_CONTROLLER, 0x5, "ATA Controller") \
        REGISTER_SUBCLASS_CODE(MSA_SERIAL_ATA_CONTROLLER, 0x6, "Serial ATA Controller") \
        REGISTER_SUBCLASS_CODE(MSA_SERIAL_ATTACHED_SCSI_CONTROLLER, 0x7, "Serial Attached SCSI Controller") \
        REGISTER_SUBCLASS_CODE(MSA_NON_VOLATILE_MEMORY_CONTROLLER, 0x8, "Non-Volatile Memory Controller") \
        REGISTER_SUBCLASS_CODE(MSA_OTHER, 0x80, "Other") \


#define PCI_ITERATE_SUBCLASS_CODES_BASE_NETWORK_CONTROLLER(REGISTER_SUBCLASS_CODE) \
        REGISTER_SUBCLASS_CODE(NETWORK_CONTROLLER_ETHERNET, 0x0, "Ethernet Controller") \
        REGISTER_SUBCLASS_CODE(NETWORK_CONTROLLER_TOKEN_RING, 0x1, "Token Ring Controller") \
        REGISTER_SUBCLASS_CODE(NETWORK_CONTROLLER_FDDI, 0x2, "FDDI Controller") \
        REGISTER_SUBCLASS_CODE(NETWORK_CONTROLLER_ATM, 0x3, "ATM Controller") \
        REGISTER_SUBCLASS_CODE(NETWORK_CONTROLLER_ISDN, 0x4, "ISDN Controller") \
        REGISTER_SUBCLASS_CODE(NETWORK_CONTROLLER_WORLD_FLIP, 0x5, "WorldFlip Controller") \
        REGISTER_SUBCLASS_CODE(NETWORK_CONTROLLER_PICMG_2_14_MULTI_COMPUTING, 0x6, "PICMG 2.14 Multi Computing Controller") \
        REGISTER_SUBCLASS_CODE(NETWORK_CONTROLLER_INFINIBAND, 0x7, "Infiniband Controller") \
        REGISTER_SUBCLASS_CODE(NETWORK_CONTROLLER_FABRIC, 0x8, "Fabric Controller") \
        REGISTER_SUBCLASS_CODE(NETWORK_CONTROLLER_OTHER, 0x80, "Other") \


#define PCI_ITERATE_SUBCLASS_CODES_BASE_DISPLAY_CONTROLLER(REGISTER_SUBCLASS_CODE) \
        REGISTER_SUBCLASS_CODE(DISPLAY_CONTROLLER_VGA_COMPATIBLE, 0x0, "VGA Compatible") \
        REGISTER_SUBCLASS_CODE(DISPLAY_CONTROLLER_XGA, 0x1, "XGA Compatible") \
        REGISTER_SUBCLASS_CODE(DISPLAY_CONTROLLER_3D_NON_VGA_COMPATIBLE, 0x3, "3D Controller (Not VGA-Compatible)") \
        REGISTER_SUBCLASS_CODE(DISPLAY_CONTROLLER_OTHER, 0x80, "Other") \


#define PCI_ITERATE_SUBCLASS_CODES_BASE_MULTIMEDIA_CONTROLLER(REGISTER_SUBCLASS_CODE) \
        REGISTER_SUBCLASS_CODE(MULTIMEDIA_CONTROLLER_MULTIMEDIA_VIDEO, 0x0, "Multimedia Video Controller") \
        REGISTER_SUBCLASS_CODE(MULTIMEDIA_CONTROLLER_MULTIMEDIA_AUDIO, 0x1, "Multimedia Audio Controller") \
        REGISTER_SUBCLASS_CODE(MULTIMEDIA_CONTROLLER_COMPUTER_TELEPHONY_DEVICE, 0x2, "Computer Telephony Device") \
        REGISTER_SUBCLASS_CODE(MULTIMEDIA_CONTROLLER_AUDIO_DEVICE, 0x3, "Audio Device") \
        REGISTER_SUBCLASS_CODE(MULTIMEDIA_CONTROLLER_OTHER, 0x80, "Other") \


#define PCI_ITERATE_SUBCLASS_CODES_BASE_MEMORY_CONTROLLER(REGISTER_SUBCLASS_CODE) \
        REGISTER_SUBCLASS_CODE(MEMORY_CONTROLLER_RAM, 0x0, "RAM Controller") \
        REGISTER_SUBCLASS_CODE(MEMORY_CONTROLLER_FLASH, 0x1, "Flash Controller") \
        REGISTER_SUBCLASS_CODE(MEMORY_CONTROLLER_OTHER, 0x80, "Other") \


#define PCI_ITERATE_SUBCLASS_CODES_BASE_BRIDGE(REGISTER_SUBCLASS_CODE) \
        REGISTER_SUBCLASS_CODE(BRIDGE_HOST, 0x0, "Host Bridge") \
        REGISTER_SUBCLASS_CODE(BRIDGE_ISA, 0x1, "ISA Bridge") \
        REGISTER_SUBCLASS_CODE(BRIDGE_EISA, 0x2, "EISA Bridge") \
        REGISTER_SUBCLASS_CODE(BRIDGE_MSCA, 0x3, "MCA Bridge") \
        REGISTER_SUBCLASS_CODE(BRIDGE_PCI_TO_PCI, 0x4, "PCI-to-PCI Bridge") \
        REGISTER_SUBCLASS_CODE(BRIDGE_PCMCIA, 0x5, "PCMCIA Bridge") \
        REGISTER_SUBCLASS_CODE(BRIDGE_NUBUS, 0x6, "NuBus Bridge") \
        REGISTER_SUBCLASS_CODE(BRIDGE_CARDBUS, 0x7, "CardBus Bridge") \
        REGISTER_SUBCLASS_CODE(BRIDGE_RACEWAY, 0x8, "RACEway Bridge") \
        REGISTER_SUBCLASS_CODE(BRIDGE_PCI_TO_PCI_SECOND, 0x9, "PCI-to-PCI (Second) Bridge") \
        REGISTER_SUBCLASS_CODE(BRIDGE_INFINIBAND_TO_PCI_HOST, 0xA, "InfiniBand-to-PCI Host Bridge") \
        REGISTER_SUBCLASS_CODE(BRIDGE_OTHER, 0x80, "Other") \


#define PCI_ITERATE_SUBCLASS_CODES_BASE_SIMPLE_COMMUNICATION_CONTROLLER(REGISTER_SUBCLASS_CODE) \
        REGISTER_SUBCLASS_CODE(SCM_SERIAL_CONTROLLER, 0x0, "Serial Controller") \
        REGISTER_SUBCLASS_CODE(SCM_PARALLEL_CONTROLLER, 0x1, "Parallel Controller") \
        REGISTER_SUBCLASS_CODE(SCM_MUTLIPORT_SERIAL_CONTROLLER, 0x2, "Multiport Serial Controller") \
        REGISTER_SUBCLASS_CODE(SCM_MODEM, 0x3, "Modem") \
        REGISTER_SUBCLASS_CODE(SCM_IEEE_488_1_2_GPIB_CONTROLLER, 0x4, "IEEE 488.1/2 (GPIB) Controller") \
        REGISTER_SUBCLASS_CODE(SCM_SMART_CARD_CONTROLLER, 0x5, "Smart Card Controller") \
        REGISTER_SUBCLASS_CODE(SCM_OTHER, 0x80, "Other") \


#define PCI_ITERATE_SUBCLASS_CODES_BASE_BASE_SYSTEM_PERIPHERAL(REGISTER_SUBCLASS_CODE) \
        REGISTER_SUBCLASS_CODE(BSP_PIC, 0x0, "PIC") \
        REGISTER_SUBCLASS_CODE(BSP_DMA_CONTROLLER, 0x1, "DMA Controller") \
        REGISTER_SUBCLASS_CODE(BSP_TIMER, 0x2, "Timer") \
        REGISTER_SUBCLASS_CODE(BSP_RTC_CONTROLLER, 0x3, "RTC Controller") \
        REGISTER_SUBCLASS_CODE(BSP_PCI_HOT_PLUG_CONTROLLER, 0x4, "PCI Hot-Plug Controller") \
        REGISTER_SUBCLASS_CODE(BSP_SD_HOST_CONTROLLER, 0x5, "SD Host Controller") \
        REGISTER_SUBCLASS_CODE(BSP_IOMMU, 0x6, "I/O MMU") \
        REGISTER_SUBCLASS_CODE(BSP_OTHER, 0x80, "Other") \


#define PCI_ITERATE_SUBCLASS_CODES_BASE_INPUT_DEVICE_CONTROLLER(REGISTER_SUBCLASS_CODE) \
        REGISTER_SUBCLASS_CODE(IDC_KEYBOARD_CONTROLLER, 0x0, "Keyboard Controller") \
        REGISTER_SUBCLASS_CODE(IDC_DIGITIZER_PEN, 0x1, "Digitizer Pen") \
        REGISTER_SUBCLASS_CODE(IDC_MOUSE_CONTROLLER, 0x2, "Mouse Controller") \
        REGISTER_SUBCLASS_CODE(IDC_SCANNER_CONTROLLER, 0x3, "Scanner Controller") \
        REGISTER_SUBCLASS_CODE(IDC_GAMEPORT_CONTROLLER, 0x4, "Gameport Controller") \
        REGISTER_SUBCLASS_CODE(IDC_OTHER, 0x80, "Other") \


#define PCI_ITERATE_SUBCLASS_CODES_BASE_DOCKING_STATION(REGISTER_SUBCLASS_CODE) \
        REGISTER_SUBCLASS_CODE(DOCKING_STATION_GENERIC, 0x0, "Generic") \
        REGISTER_SUBCLASS_CODE(DOCKING_STATION_OTHER, 0x80, "Other") \


#define PCI_ITERATE_SUBCLASS_CODES_BASE_PROCESSOR(REGISTER_SUBCLASS_CODE) \
        REGISTER_SUBCLASS_CODE(PROCESSOR_386, 0x0, "386") \
        REGISTER_SUBCLASS_CODE(PROCESSOR_486, 0x1, "486") \
        REGISTER_SUBCLASS_CODE(PROCESSOR_PENTIUM, 0x2, "Pentium") \
        REGISTER_SUBCLASS_CODE(PROCESSOR_PENTIUM_PRO, 0x3, "Pentium Pro") \
        REGISTER_SUBCLASS_CODE(PROCESSOR_ALPHA, 0x10, "Alpha") \
        REGISTER_SUBCLASS_CODE(PROCESSOR_POWERPC, 0x20, "PowerPC") \
        REGISTER_SUBCLASS_CODE(PROCESSOR_MIPS, 0x30, "MIPS") \
        REGISTER_SUBCLASS_CODE(PROCESSOR_CO_PROCESSOR, 0x40, "Co-Processor") \
        REGISTER_SUBCLASS_CODE(PROCESSOR_OTHER, 0x80, "Other") \


#define PCI_ITERATE_SUBCLASS_CODES_BASE_SERIAL_BUS_CONTROLLER(REGISTER_SUBCLASS_CODE) \
        REGISTER_SUBCLASS_CODE(SBC_FIRE_WIRE_CONTROLLER, 0x0, "FireWire (IEEE 1394) Controller") \
        REGISTER_SUBCLASS_CODE(SBC_ACCESS_BUS_CONTROLLER, 0x1, "ACCESS Bus Controller") \
        REGISTER_SUBCLASS_CODE(SBC_SSA, 0x2, "Serial Storage Architecture (SSA)") \
        REGISTER_SUBCLASS_CODE(SBC_USB_CONTROLLER, 0x3, "USB Controller") \
        REGISTER_SUBCLASS_CODE(SBC_FIBRE_CHANNEL, 0x4, "Fibre Channel") \
        REGISTER_SUBCLASS_CODE(SBC_SMBUS_CONTROLLE, 0x5, "SMBUS Controller") \
        REGISTER_SUBCLASS_CODE(SBC_INFINIBAND_CONTROLLER, 0x6, "InfiniBand Controller") \
        REGISTER_SUBCLASS_CODE(SBC_IPMI_INTERFACE, 0x7, "IPMI Interface") \
        REGISTER_SUBCLASS_CODE(SBC_SERCOS_INTERFACE, 0x8, "SERCOS Interface (IEC 61491)") \
        REGISTER_SUBCLASS_CODE(SBC_CANBUS_CONTROLLER, 0x9, "CANbus Controller") \
        REGISTER_SUBCLASS_CODE(SBC_OTHER, 0x80, "Other") \


#define PCI_ITERATE_SUBCLASS_CODES_BASE_WIRELESS_CONTROLLER(REGISTER_SUBCLASS_CODE) \
        REGISTER_SUBCLASS_CODE(WIRELESS_CONTROLLER_IRDA_COMPATIBLE, 0x0, "iRDA Compatible Controller") \
        REGISTER_SUBCLASS_CODE(WIRELESS_CONTROLLER_CONSUMER_IR, 0x1, "Consumer IR Controller") \
        REGISTER_SUBCLASS_CODE(WIRELESS_CONTROLLER_RF, 0x10, "RF Controller") \
        REGISTER_SUBCLASS_CODE(WIRELESS_CONTROLLER_BLUETOOTH, 0x11, "Bluetooth Controller") \
        REGISTER_SUBCLASS_CODE(WIRELESS_CONTROLLER_BROADBAND, 0x12, "Broadband Controller") \
        REGISTER_SUBCLASS_CODE(WIRELESS_CONTROLLER_ETHERNET_802_1A, 0x20, "Ethernet Controller (802.1a)") \
        REGISTER_SUBCLASS_CODE(WIRELESS_CONTROLLER_ETHERNET_802_1B, 0x21, "Ethernet Controller (802.1b)") \
        REGISTER_SUBCLASS_CODE(WIRELESS_CONTROLLER_OTHER, 0x80, "Other") \


#define PCI_ITERATE_SUBCLASS_CODES_BASE_INTELLIGENT_CONTROLLER(REGISTER_SUBCLASS_CODE) \
        REGISTER_SUBCLASS_CODE(INTELLIGENT_CONTROLLER_I20, 0x0, "I20") \


#define PCI_ITERATE_SUBCLASS_CODES_BASE_SATELLITE_COMMUNICATION_CONTROLLER(REGISTER_SUBCLASS_CODE) \
        REGISTER_SUBCLASS_CODE(SCC_TV_CONTROLLER, 0x1, "Satellite TV Controller") \
        REGISTER_SUBCLASS_CODE(SCC_AUDIO_CONTROLLER, 0x2, "Satellite Audio Controller") \
        REGISTER_SUBCLASS_CODE(SCC_VIDIO_CONTROLLER, 0x3, "Satellite Video Controller") \
        REGISTER_SUBCLASS_CODE(SCC_DATA_CONTROLLER, 0x4, "Satellite Data Controller") \


#define PCI_ITERATE_SUBCLASS_CODES_BASE_ENCRYPTION_CONTROLLER(REGISTER_SUBCLASS_CODE) \
        REGISTER_SUBCLASS_CODE(ENCRYPTION_CONTROLLER_NETWORK_AND_COMPUTING, 0x0, "Network and Computing Encryption/Decryption") \
        REGISTER_SUBCLASS_CODE(ENCRYPTION_CONTROLLER_ENTERTAINMENT, 0x10, "Entertainment Encryption/Decryption") \
        REGISTER_SUBCLASS_CODE(ENCRYPTION_CONTROLLER_OTHER, 0x80, "Other") \


#define PCI_ITERATE_SUBCLASS_CODES_BASE_SIGNAL_PROCESSING_CONTROLLER(REGISTER_SUBCLASS_CODE) \
        REGISTER_SUBCLASS_CODE(SPC_DPIO_MODULES, 0x0, "DPIO Modules") \
        REGISTER_SUBCLASS_CODE(SPC_PERFORMANCE_COUNTERS, 0x1, "Performance Counters") \
        REGISTER_SUBCLASS_CODE(SPC_COMMUNICATION_SYNCHRONIZER, 0x10, "Communication Synchronizer") \
        REGISTER_SUBCLASS_CODE(SPC_SIGNAL_PROCESSING_MANAGEMENT, 0x20, "Signal Processing Management") \
        REGISTER_SUBCLASS_CODE(SPC_OTHER, 0x80, "Other") \


#define PCI_ITERATE_SUBCLASS_CODES_BASE_PROCESSING_ACCELERATOR(REGISTER_SUBCLASS_CODE)


#define PCI_ITERATE_SUBCLASS_CODES_BASE_NON_ESSENTIAL_INSTRUMENTATION(REGISTER_SUBCLASS_CODE)


#define PCI_ITERATE_SUBCLASS_CODES_BASE_CO_PROCESSOR(REGISTER_SUBCLASS_CODE)



#define REGISTER_SUBCLASS_CODE(subcode, id, name) subcode = id,
#define REGISTER_CLASS_CODE(code, id) PCI_ITERATE_SUBCLASS_CODES_BASE_##code(REGISTER_SUBCLASS_CODE)
        PCI_ITERATE_CLASS_CODES(REGISTER_CLASS_CODE)
#undef REGISTER_CLASS_CODE
#undef REGISTER_SUBCLASS_CODE
    };


} // namespace kernel::pci
