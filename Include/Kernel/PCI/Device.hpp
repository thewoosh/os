#pragma once

#include <string_view>

#include <cstdint>

#include "Kernel/PCI/CommandRegister.hpp"
#include "Kernel/PCI/Forward.hpp"
#include "Kernel/PCI/Header.hpp"

namespace kernel { enum class InterruptSource; }

namespace kernel::pci {

    struct Capability;

    class AbstractDevice {
    public:
        AbstractDevice() = default;
        virtual ~AbstractDevice() = default;

        inline void
        enableBusMastering() {
            auto commandRegister = readCommandRegister();
            if (commandRegister.busMaster)
                return;

            commandRegister.busMaster = true;
            writeCommandRegister(commandRegister);
        }

        virtual void
        forEachCapability(void (*)(const Capability &)) const = 0;

        [[nodiscard]] virtual BaseAddressRegister
        getBaseAddressRegister0() = 0;

        [[nodiscard]] virtual BaseAddressRegister
        getBaseAddressRegister1() = 0;

        [[nodiscard]] virtual BaseAddressRegister
        getBaseAddressRegister2() = 0;

        [[nodiscard]] virtual BaseAddressRegister
        getBaseAddressRegister3() = 0;

        [[nodiscard]] virtual BaseAddressRegister
        getBaseAddressRegister4() = 0;

        [[nodiscard]] virtual BaseAddressRegister
        getBaseAddressRegister5() = 0;

        [[nodiscard]] inline constexpr ClassCode
        getClassCode() const {
            return m_header.classID;
        }

        [[nodiscard]] std::string_view
        getClassName() const;

        [[nodiscard]] inline constexpr DeviceID
        getDeviceID() const {
            return m_header.deviceID;
        }

        [[nodiscard]] std::string_view
        getDeviceName() const;

        [[nodiscard]] inline constexpr ProgrammingInterface
        getProgrammingInterfaceCode() const {
            return m_header.programmingInterface;
        }

        [[nodiscard]] std::string_view
        getProgrammingInterfaceName() const;

        [[nodiscard]] inline constexpr std::uint8_t
        getRevisionID() const {
            return m_header.revisionID;
        }

        [[nodiscard]] inline constexpr SubclassCode
        getSubclassCode() const {
            return m_header.subclassID;
        }

        [[nodiscard]] std::string_view
        getSubclassName() const;

        [[nodiscard]] inline constexpr VendorID
        getVendorID() const {
            return m_header.vendorID;
        }

        [[nodiscard]] std::string_view
        getVendorName() const;

        /**
         * Returns true if the device supports and has capabilities.
         */
        [[nodiscard]] virtual bool
        hasCapabilities() const = 0;

        [[nodiscard]] inline constexpr const Header &
        header() const {
            return m_header;
        }

        [[nodiscard]] CommandRegister
        readCommandRegister() const;

        [[nodiscard]] virtual std::uint8_t
        readConfigByte(std::uint16_t offset) const = 0;

        [[nodiscard]] virtual std::uint32_t
        readConfigDWord(std::uint16_t offset) const = 0;

        [[nodiscard]] virtual std::uint16_t
        readConfigWord(std::uint16_t offset) const = 0;

        void
        writeCommandRegister(CommandRegister);

        virtual void
        writeConfigByte(std::uint16_t offset, std::uint8_t value) = 0;

        virtual void
        writeConfigDWord(std::uint16_t offset, std::uint32_t value) = 0;

        virtual void
        writeConfigWord(std::uint16_t offset, std::uint16_t value) = 0;

        bool (*onInterrupt)(InterruptSource, std::uint16_t){};

    protected:
        [[nodiscard]] inline constexpr
        AbstractDevice(std::uint8_t bus, std::uint8_t slot, std::uint8_t function)
                : m_bus(bus), m_slot(slot), m_function(function) {
        }

        void
        initializeHeader();

        pci::Header m_header{};
        std::uint8_t m_bus{};
        std::uint8_t m_slot{};
        std::uint8_t m_function{};
    };

} // namespace kernel::pci
