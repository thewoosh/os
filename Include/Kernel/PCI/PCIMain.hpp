#pragma once

#include <Core/StackVector.hpp>
#include <Core/VariableView.hpp>

#include <cstdint>

#include "Kernel/PCI/ConventionalDevice.hpp"
#include "Kernel/PCI/ExpressDevice.hpp"
#include "Kernel/PCI/Header.hpp"

namespace kernel {

    namespace acpi { struct MCFG; }
    namespace pci { struct ACPIRoutingTableEntry; }
    struct PCIExpressEntry;

    class PCIMain {
    public:
        [[nodiscard]] bool
        initializeConventional(core::VariableView<const pci::ACPIRoutingTableEntry> routingTable);

        [[nodiscard]] bool
        initializeExpress(const acpi::MCFG *, core::VariableView<const pci::ACPIRoutingTableEntry> routingTable);

        using DeviceEnumerationCallbackType = void (*)(pci::AbstractDevice &);
        using ConstDeviceEnumerationCallbackType = void (*)(const pci::AbstractDevice &);

        void
        forEachDevice(DeviceEnumerationCallbackType);

        void
        forEachDevice(ConstDeviceEnumerationCallbackType) const;

    private:
        void
        checkDeviceIdentifiers() const;

        void
        informDeviceDrivers();

        [[nodiscard]] bool
        loadPCIExpressEntry(const PCIExpressEntry &, core::VariableView<const pci::ACPIRoutingTableEntry> routingTable);

        core::StackVector<pci::ConventionalDevice, 256> m_conventionalDevices{};
        core::StackVector<pci::ExpressDevice, 256> m_expressDevices{};
    };

} // namespace kernel
