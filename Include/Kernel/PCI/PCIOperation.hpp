/**
 * Copyright (C) 2021 Tristan Gerritsen <tristan-legal@outlook.com>
 * All Rights Reserved
 *
 * SPDX-License-Identifier: BSD-3-Clause
 */

#pragma once

#include <cstdint>

namespace Kernel::PCI {

    enum class Operation {
        VENDOR_ID = 0x0,
        STATUS    = 0x1,
        DEVICE_ID = 0x2,
        CLASS_ID  = 0xA,
    };

} // namespace Kernel::PCI
