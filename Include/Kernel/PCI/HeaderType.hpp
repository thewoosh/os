#pragma once

#include <cstdint>

namespace kernel::pci {

    enum class HeaderType
            : std::uint8_t {
        NORMAL = 0x0,
        PCI_TO_PCI_BRIDGE = 0x01,
        PCI_TO_CARD_BUS_BRIDGE = 0x02,
    };

} // namespace kernel::pci
