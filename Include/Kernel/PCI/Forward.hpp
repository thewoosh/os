#pragma once

#include <cstdint>

namespace kernel::pci {

    enum class ClassCode : std::uint8_t;
    enum class DeviceID : std::uint16_t;
    enum class HeaderType : std::uint8_t;
    enum class ProgrammingInterface : std::uint8_t;
    enum class SubclassCode : std::uint8_t;
    enum class VendorID : std::uint16_t;

    inline constexpr VendorID kInvalidVendorID = static_cast<VendorID>(0xFFFF);

    struct BaseAddressRegister;
    struct Entry;

} // namespace kernel::pci
