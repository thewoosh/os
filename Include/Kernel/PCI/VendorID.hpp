#pragma once

#include <cstdint>

namespace kernel::pci {

    enum class VendorID
            : std::uint16_t {
        INVALID = 0xFFFF,

#define PCI_ITERATE_VENDOR_IDS(REGISTER_VENDOR) \
        REGISTER_VENDOR(AMD, 0x1022, "AMD") \
        REGISTER_VENDOR(AMD_ATI, 0x1002, "AMD [ATI]") \
        REGISTER_VENDOR(APPLE, 0x106b, "Apple") \
        REGISTER_VENDOR(BROADCOM_LSI, 0x1000, "Broadcom / LSI") \
        REGISTER_VENDOR(ENSONIQ, 0x1274, "Ensoniq") \
        REGISTER_VENDOR(INNOTEK, 0x80ee, "InnoTek (VirtualBox)") \
        REGISTER_VENDOR(INTEL, 0x8086, "Intel") \
        REGISTER_VENDOR(IBM, 0x1014, "IBM") \
        REGISTER_VENDOR(LINKSYS, 0x1737, "Linksys") \
        REGISTER_VENDOR(NVIDIA, 0x10de, "Nvidia") \
        REGISTER_VENDOR(QEMU, 0x1234, "QEMU") \
        REGISTER_VENDOR(REALTEK, 0x10ec, "Realtek Semiconductor") \
        REGISTER_VENDOR(SUN_ORACLE, 0x108e, "Sun (Oracle)") \
        REGISTER_VENDOR(US_ROBOTICS, 0x16ec, "U.S. Robotics") \
        REGISTER_VENDOR(VMWARE, 0x15ad, "VMware") \

#define REGISTER_VENDOR(name, id, _) name = id,
        PCI_ITERATE_VENDOR_IDS(REGISTER_VENDOR)
#undef REGISTER_VENDOR
    };

} // namespace kernel::pci
