#pragma once

#include "Kernel/PCI/Device.hpp"
#include "Kernel/PCI/Header.hpp"

namespace kernel::pci {

    struct StatusRegisterValue;

    class ConventionalDevice final
            : public AbstractDevice {
    public:
        [[nodiscard]]
        ConventionalDevice() = default;

        [[nodiscard]]
        ConventionalDevice(std::uint8_t bus, std::uint8_t slot, std::uint8_t function);

        [[nodiscard]] inline static constexpr std::uint32_t
        createAddress(std::uint8_t bus, std::uint8_t slot, std::uint8_t function) {
            return static_cast<std::uint32_t>(
                    (static_cast<std::uint32_t>(bus) << 16)
                    | (static_cast<std::uint32_t>(slot) << 11)
                    | (static_cast<std::uint32_t>(function) << 8)
                    | static_cast<std::uint32_t>(0x80000000)
            );
        }

        void
        forEachCapability(void (*)(const Capability &)) const override;

        [[nodiscard]] BaseAddressRegister
        getBaseAddressRegister0() override;

        [[nodiscard]] BaseAddressRegister
        getBaseAddressRegister1() override;

        [[nodiscard]] BaseAddressRegister
        getBaseAddressRegister2() override;

        [[nodiscard]] BaseAddressRegister
        getBaseAddressRegister3() override;

        [[nodiscard]] BaseAddressRegister
        getBaseAddressRegister4() override;

        [[nodiscard]] BaseAddressRegister
        getBaseAddressRegister5() override;

        [[nodiscard]] bool
        hasCapabilities() const override;

        [[nodiscard]] std::uint8_t
        readConfigByte(std::uint16_t offset) const override;

        [[nodiscard]] std::uint32_t
        readConfigDWord(std::uint16_t offset) const override;

        [[nodiscard]] std::uint16_t
        readConfigWord(std::uint16_t offset) const override;

        void
        writeConfigByte(std::uint16_t offset, std::uint8_t value) override;

        void
        writeConfigDWord(std::uint16_t offset, std::uint32_t value) override;

        void
        writeConfigWord(std::uint16_t offset, std::uint16_t value) override;

    private:
        void
        setConfigAddress(std::uint16_t offset) const;

        [[nodiscard]] StatusRegisterValue
        readStatusRegister() const;
    };

} // namespace kernel::pci
