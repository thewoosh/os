/**
 * Copyright (C) 2021 Tristan Gerritsen <tristan-legal@outlook.com>
 * All Rights Reserved
 *
 * SPDX-License-Identifier: BSD-3-Clause
 */

#pragma once

#include <string_view>

#include <cstdint>

#include "Kernel/PCI/Forward.hpp"

namespace kernel::pci {

    struct Header {
        VendorID vendorID{kInvalidVendorID};
        DeviceID deviceID;
        ClassCode classID;
        SubclassCode subclassID;
        ProgrammingInterface programmingInterface;
        std::uint8_t revisionID;
        std::uint8_t bist;
        HeaderType headerType;
        std::uint8_t latencyTimer;
        std::uint8_t cacheLineSize;

        [[nodiscard]] std::string_view
        getDeviceName() const;

        [[nodiscard]] std::string_view
        getProgrammingInterfaceName() const;

        [[nodiscard]] std::string_view
        getSubclassName() const;

        [[nodiscard]] std::string_view
        getVendorName() const;
    };

} // namespace kernel::pci
