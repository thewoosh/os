#pragma once

#include <cstdint>

#include "Kernel/Assert.hpp"

namespace kernel::pci {

    class AbstractDevice;

    struct BaseAddressRegister {
        [[nodiscard]] inline constexpr
        BaseAddressRegister(AbstractDevice *device, std::uint32_t value)
                : m_device(device)
                , m_value(value) {
        }

        [[nodiscard]] inline constexpr explicit
        operator bool() const {
            return m_device != nullptr;
        }

        [[nodiscard]] inline constexpr AbstractDevice *
        device() const {
            return m_device;
        }

        template<typename PointerType = void>
        [[nodiscard]] inline PointerType *
        get() const {
            const auto toPointer = [] (std::uint64_t value) {
                return static_cast<PointerType *>(reinterpret_cast<void *>(value));
            };
            if (m_value & 0b1)
                return toPointer(m_value & 0xFFFFFFFC);
            if ((m_value & 0b110) == 0)
                return toPointer(m_value & 0xFFFFFFF0);
            CHECK_NOT_REACHED();
            return nullptr;
        }

        [[nodiscard]] inline constexpr auto
        raw_value() const {
            return m_value;
        }

    private:
        AbstractDevice *m_device;
        std::uint32_t m_value;
    };

} // namespace kernel::pci
