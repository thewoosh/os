#pragma once

#include <string>

#include <cstdint>

namespace kernel::pci {

    struct ACPIRoutingTableEntry {
        std::uint32_t address;
        std::uint8_t pin;
        std::string source;
        std::uint32_t sourceIndex;

        [[nodiscard]] inline constexpr bool
        isAddress(std::uint16_t device) const {
            return address >> 16 == device;
        }
    };

} // namespace kernel::pci
