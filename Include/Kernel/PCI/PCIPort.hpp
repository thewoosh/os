/**
 * Copyright (C) 2021 Tristan Gerritsen <tristan-legal@outlook.com>
 * All Rights Reserved
 *
 * SPDX-License-Identifier: BSD-3-Clause
 */

#pragma once

#include <cstdint>

namespace kernel::pci::ports {

    constexpr const std::uint16_t PCIConfigAddress = 0xCF8;
    constexpr const std::uint16_t PCIConfigData = 0xCFC;

} // namespace kernel::pci::ports
