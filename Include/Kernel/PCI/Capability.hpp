#pragma once

#include <cstddef>
#include <cstdint>

namespace kernel::pci {

    enum class CapabilityType
            : std::uint8_t {
        RESERVED = 0x00,
        PCI_POWER_MANAGEMENT_INTERFACE = 0x01,
        ACCELERATED_GRAPHICS_PORT = 0x02,
        VITAL_PRODUCT_DATA = 0x03,
        SLOT_IDENTIFICATION = 0x04,
        MESSAGE_SIGNAL_INTERRUPTS = 0x05,

        // CompactPCI Hot Swap Specification PICMG 2.1, R1.0 available at
        // http://www.picmg.org
        COMPACT_PCI_HOT_SWAP = 0x06,

        PCI_X = 0x07,

        // For details, refer to the HyperTransport I/O Link Specification
        // available at http://www.hypertransport.org.
        HYPER_TRANSPORT = 0x08,

        VENDOR_SPECIFIC = 0x09,
        DEBUG_PORT = 0x0A,
        COMPACT_PCI_CENTRAL_RESOURCE_CONTROL = 0x0B,
        PCI_HOT_PLUG = 0x0C,
        PCI_BRIDGE_SUBSYSTEM_VENDOR_ID = 0x0D,
        AGP_8X = 0x0E,
        SECURE_DEVICE = 0x0F,
        PCI_EXPRESS = 0x10,
        MSI_X = 0x11,

        SATA_DATA_INDEX = 0x12,
    };

#ifdef PCI_REQUEST_CAPABILITY_NAMES
    constexpr const char *pciCapabilityNames[] = {
            "RESERVED",
            "PCI_POWER_MANAGEMENT_INTERFACE",
            "ACCELERATED_GRAPHICS_PORT",
            "VITAL_PRODUCT_DATA",
            "SLOT_IDENTIFICATION",
            "MESSAGE_SIGNAL_INTERRUPTS",
            "COMPACT_PCI_HOT_SWAP",
            "PCI_X",
            "HYPER_TRANSPORT",
            "VENDOR_SPECIFIC",
            "DEBUG_PORT",
            "COMPACT_PCI_CENTRAL_RESOURCE_CONTROL",
            "PCI_HOT_PLUG",
            "PCI_BRIDGE_SUBSYSTEM_VENDOR_ID",
            "AGP_8X",
            "SECURE_DEVICE",
            "PCI_EXPRESS",
            "MSI_X",
            "SATA_DATA_INDEX",
            "(reserved)",
            "(reserved)",
            "(reserved)",
            "(reserved)",
            "(reserved)",
            "(reserved)",
            "(reserved)",
            "(reserved)",
            "(reserved)",
            "(reserved)",
            "(reserved)",
            "(reserved)",
            "(reserved)",
            "(reserved)",
            "(reserved)",
            "(reserved)",
            "(reserved)",
            "(reserved)",
            "(reserved)",
            "(reserved)",
            "(reserved)",
            "(reserved)",
    };
#endif // PCI_REQUEST_CAPABILITY_NAMES

    struct Capability {
        [[nodiscard]] inline constexpr
        Capability(std::size_t size, CapabilityType type, std::uint8_t offsetToNext)
                : m_size(size)
                , m_type(type)
                , m_offsetToNext(offsetToNext) {
        }

        [[nodiscard]] inline constexpr std::uint8_t
        offsetToNext() const {
            return m_offsetToNext;
        }

        [[nodiscard]] inline constexpr std::size_t
        size() const {
            return m_size;
        }

        [[nodiscard]] inline constexpr CapabilityType
        type() const {
            return m_type;
        }

    private:
        // Size of this structure
        std::size_t m_size;
        CapabilityType m_type;

        // Offset to the next capability
        std::uint8_t m_offsetToNext;
    };

} // namespace kernel::pci
