#pragma once

#include <cstdint>

#include "SubclassCode.hpp"

namespace kernel::pci {

    enum class ProgrammingInterface
            : std::uint8_t {

#define PCI_ITERATE_PROGRAMMING_INTERFACES_FOR_MSA_IDE_CONTROLLER(REGISTER_PROGRAMMING_INTERFACE) \
        REGISTER_PROGRAMMING_INTERFACE(MSA_IDE_CONTROLLER_ISA_COMPAT_MODE_ONLY, 0x00, \
                "ISA Compatibility mode-only controller") \
        REGISTER_PROGRAMMING_INTERFACE(MSA_IDE_CONTROLLER_PCI_NATIVE_MODE_ONLY, 0x05, \
                "PCI native mode-only controller") \
        REGISTER_PROGRAMMING_INTERFACE(MSA_IDE_CONTROLLER_ISA_COMPAT_SUPPORTS_BOTH_CHANNELS_PCI_NATIVE_MODE, 0x0A, \
                "ISA Compatibility mode controller, supports both channels switched to PCI native mode") \
        REGISTER_PROGRAMMING_INTERFACE(MSA_IDE_CONTROLLER_PCI_NATIVE_SUPORTS_BOTH_CHANNELS_ISA_COMPAT_MODE, 0x0F, \
                "PCI native mode controller, supports both channels switched to ISA compatibility mode") \
        REGISTER_PROGRAMMING_INTERFACE(MSA_IDE_CONTROLLER_ISA_COMPAT_MODE_ONLY_WITH_BUS_MASTERING, 0x80, \
                "ISA Compatibility mode-only controller, supports bus mastering") \
        REGISTER_PROGRAMMING_INTERFACE(MSA_IDE_CONTROLLER_ISA_COMPAT_SUPPORTS_BOTH_CHANNELS_PCI_NATIVE_MODE_WITH_BUS_MASTERING, 0x8A, \
                "ISA Compatibility mode controller, supports both channels switched to PCI native mode, supports bus mastering") \
        REGISTER_PROGRAMMING_INTERFACE(MSA_IDE_CONTROLLER_PCI_NATIVE_SUPORTS_BOTH_CHANNELS_ISA_COMPAT_MODE_WITH_BUS_MASTERING, 0x8F, \
                "PCI native mode controller, supports both channels switched to ISA compatibility mode, supports bus mastering") \


#define PCI_ITERATE_PROGRAMMING_INTERFACES_FOR_MSA_SERIAL_ATA_CONTROLLER(REGISTER_PROGRAMMING_INTERFACE) \
        REGISTER_PROGRAMMING_INTERFACE(MSA_SERIAL_ATA_CONTROLLER_VENDOR_SPECIFIC_INTERFACE, 0x00, "Vendor Specific Interface") \
        REGISTER_PROGRAMMING_INTERFACE(MSA_SERIAL_ATA_CONTROLLER_AHCI_1_0, 0x01, "AHCI 1.0") \
        REGISTER_PROGRAMMING_INTERFACE(MSA_SERIAL_ATA_CONTROLLER_SERIAL_STORAGE_BUS, 0x02, "Serial Storage Bus") \


#define REGISTER_PROGRAMMING_INTERFACE(enumeration, id, description) enumeration = id,
        PCI_ITERATE_PROGRAMMING_INTERFACES_FOR_MSA_IDE_CONTROLLER(REGISTER_PROGRAMMING_INTERFACE)
        PCI_ITERATE_PROGRAMMING_INTERFACES_FOR_MSA_SERIAL_ATA_CONTROLLER(REGISTER_PROGRAMMING_INTERFACE)
#undef REGISTER_PROGRAMMING_INTERFACE
    };


} // namespace kernel::pci
