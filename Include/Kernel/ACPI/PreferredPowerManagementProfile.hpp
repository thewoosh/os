#pragma once

#include <cstdint>

namespace kernel::acpi {

    //
    // Preferred_PM_Profile of FADT
    // ACPI 6.3 page 127
    //
    enum class PreferredPowerManagementProfile : std::uint8_t {
        UNSPECIFIED = 0,
        DESKTOP = 1,
        MOBILE = 2,
        WORKSTATION = 3,
        ENTERPRISE_SERVER = 4,
        SOHO_SERVER = 5,
        APPLIANCE_PC = 6,
        PERFORMANCE_SERVER =  7,
        TABLET = 8,

        // Rest is reserved
    };

} // namespace kernel::acpi