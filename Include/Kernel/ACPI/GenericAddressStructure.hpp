#pragma once

#include <cstdint>

#include "AccessSizeType.hpp"
#include "AddressSpaceType.hpp"

namespace kernel::acpi {

    struct [[gnu::packed]] GenericAddressStructure {
        AddressSpaceType address_space;
        std::uint8_t bit_width;
        std::uint8_t bit_offset;
        AccessSizeType access_size;
        std::uint64_t address;
    };

} // namespace kernel::acpi