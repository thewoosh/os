#pragma once

#include "Kernel/Assert.hpp"
#include "SDTHeader.hpp"

#include <cstdint>

namespace kernel::acpi {

    struct [[gnu::packed]] MCFG {
        struct [[gnu::packed]] Entry {
            void *baseAddress;
            std::uint16_t pciSegmentGroupNumber;
            std::uint8_t startPCIBus;
            std::uint8_t endPCIBus;

            [[deprecated("Reserved")]] std::uint32_t reserved;
        };
        static_assert(sizeof(Entry) == 16);

        SDTHeader header;
        [[deprecated("Reserved")]] std::uint64_t reserved;
        Entry firstEntry;

        [[nodiscard]] inline std::size_t
        entryCount() const {
            return (header.length - sizeof(header)) / sizeof(Entry);
        }
    };

} // namespace kernel::acpi