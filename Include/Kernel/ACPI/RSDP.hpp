#pragma once

#include <cstdint>

namespace kernel::acpi {

    struct [[gnu::packed]] RSDPDescriptor10 {
        char signature[8];
        std::uint8_t checksum;
        char oem_id[6];
        std::uint8_t revision;
        std::uint32_t rsdt_address;
    };

    struct [[gnu::packed]] RSDPDescriptor20 {
        RSDPDescriptor10 first_gen;

        std::uint32_t length;
        std::uint64_t xsdt_address;
        std::uint8_t extended_checksum;
        std::uint8_t reserved[3];
    };

} // namespace kernel::acpi