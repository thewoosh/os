#pragma once

#include "SDTHeader.hpp"

#include <cstdint>

namespace kernel::acpi {

    struct [[gnu::packed]] XSDT {
        SDTHeader header;
        std::uint64_t sub_sdts;
    };

} // namespace kernel::acpi