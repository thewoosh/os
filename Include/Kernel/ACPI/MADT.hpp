#pragma once

#include <cstdint>

#include "SDTHeader.hpp"

namespace kernel::acpi {

    //
    // Interrupt Controller Structure Types
    //
    // ACPI 6.3 table 5-45 page 152
    //
    enum class InterruptControllerStructureType : std::uint8_t {
        PROCESSOR_LOCAL_APIC = 0,
        IOAPIC = 1,
        INTERRUPT_SOURCE_OVERRIDE = 2,
        NON_MASKABLE_INTERRUPT_SOURCE = 3,
        LOCAL_APIC_NMI = 4,
        LOCAL_APIC_ADDRESS_OVERRIDE = 5,
        IO_SAPIC = 6,
        LOCAL_SAPIC = 7,
        PLATFORM_INTERRUPT_SOURCES = 8,
        PROCESSOR_LOCAL_X2APIC = 9,
        LOCAL_X2APIC = 0xA,
        GIC_CPU_INTERFACE = 0xB,
        GIC_DISTRIBUTOR = 0xC,
        GIC_MSI_FRAME = 0xD,
        GIC_REDISTRIBUTOR = 0xE,
        GIC_INTERRUPT_TRANSLATION_SERVICE = 0xF,
        MULTIPROCESSOR_WAKEUP = 0x10,
    };

    struct [[gnu::packed]] MADTCommonHeader {
        InterruptControllerStructureType type;
        std::uint8_t length;
    };

    //
    // Processor Local APIC Structure
    //
    // ACPI 6.3 table 5-46 page 154
    //
    struct [[gnu::packed]] ProcessorLocalAPICStructure {
        MADTCommonHeader header;
        std::uint8_t acpiProcessorUID;
        std::uint8_t apicID;
        std::uint32_t flags; // LocalAPICFlags
    };

    //
    // Local APIC Flags
    //
    // ACPI 6.3 table 5-47 page 154
    //
    enum class LocalAPICFlags : std::uint8_t {
        ENABLED = 1 << 0,
        ONLINE_CAPABLE = 1 << 1,
    };

    //
    // I/O APIC Structure
    //
    // ACPI 6.3 table 5-48 page 154-155
    //
    struct [[gnu::packed]] IOAPICStructure {
        MADTCommonHeader header;
        std::uint8_t ioAPICID;
        std::uint8_t reserved;
        std::uint32_t ioAPICAddress;
        std::uint32_t globalSystemInterruptBase;
    };

    //
    // Multiple APIC Description Table
    //
    struct [[gnu::packed]] MADT {
        SDTHeader header;
        std::uint32_t localAPICAddress;
        std::uint32_t flags;

        MADTCommonHeader firstRecord;
    };

} // namespace kernel::acpi