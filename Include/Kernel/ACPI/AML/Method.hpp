#pragma once

#include <string>
#include <string_view>

#include "AMLObject.hpp"
#include "MethodFlags.hpp"

namespace kernel::acpi::aml {

    struct Method final
            : public AMLObject, public AMLScope {
        [[nodiscard]] inline constexpr explicit
        Method(std::string &&name, MethodFlags flags)
                : AMLObject(AMLObject::Type::METHOD)
                , AMLScope(std::move(name), this, nullptr)
                , m_flags(flags) {
        }

        Method(Method &&) = default;
        Method(const Method &) = delete;

        [[nodiscard]] inline constexpr MethodFlags
        flags() const {
            return m_flags;
        }

        [[nodiscard]] inline const AMLScope *
        asScope() const final {
            return this;
        }

    private:
        MethodFlags m_flags;
    };

} // namespace kernel::acpi::aml
