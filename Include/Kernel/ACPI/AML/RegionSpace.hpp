#pragma once

#include <cstdint>

namespace kernel::acpi::aml {

    // https://uefi.org/specs/ACPI/6.4/20_AML_Specification/AML_Specification.html#regionspace
    enum class RegionSpace
            : std::uint8_t {
        SystemMemory = 0x00,
        SystemIO = 0x01,
        PCI_Config = 0x02,
        EmbeddedControl = 0x03,
        SMBus = 0x04,
        SystemCMOS = 0x05,
        PciBarTarget = 0x06,
        IPMI = 0x07,
        GeneralPurposeIO = 0x08,
        GeneralSerialBus = 0x09,
        PCC = 0x0A,
        // OEM Defined
    };

} // namespace kernel::acpi::aml
