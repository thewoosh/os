#pragma once

#include <vector>

#include "AMLObject.hpp"

namespace kernel::acpi::aml {

    struct BufferData
            : public AMLObject {
        [[nodiscard]] inline explicit
        BufferData(std::unique_ptr<AMLObject> &&size = nullptr, std::vector<char> &&data = {})
                : AMLObject(AMLObject::Type::BUFFER)
                , m_size(std::move(size))
                , m_data(std::forward<std::vector<char>>(data)) {
        }

        BufferData(BufferData &&) = default;

        [[nodiscard]] inline const std::vector<char> &
        data() const {
            return m_data;
        }

        [[nodiscard]] inline const AMLObject &
        size() const {
            return *m_size;
        }

    private:
        std::unique_ptr<AMLObject> m_size;
        std::vector<char> m_data;
    };

} // namespace kernel::acpi::aml
