#pragma once

#include <cstdint>
#include <cstdlib>

#include "AMLObject.hpp"

namespace kernel::acpi::aml {

    struct Statement
            : public AMLObject {
        enum class Type {
#define ACPI_AML_ITERATE_STATEMENT_TYPES(REGISTER_TYPE) \
            REGISTER_TYPE(BREAK) \
            REGISTER_TYPE(CONTINUE) \
            REGISTER_TYPE(IF_ELSE) \
            REGISTER_TYPE(RETURN) \
            REGISTER_TYPE(WHILE) \

#define REGISTER_TYPE(type) type,
            ACPI_AML_ITERATE_STATEMENT_TYPES(REGISTER_TYPE)
#undef REGISTER_TYPE
        };

        [[nodiscard]] inline explicit
        Statement(Type type)
                : AMLObject(AMLObject::Type::STATEMENT)
                , m_type(type) {
        }

        [[nodiscard]] inline constexpr Type
        type() const {
            return m_type;
        }

    private:
        Type m_type;
    };

    [[nodiscard]] inline constexpr std::string_view
    toString(Statement::Type type) {
        switch (type) {
#define REGISTER_TYPE(type) case Statement::Type::type: return #type;
            ACPI_AML_ITERATE_STATEMENT_TYPES(REGISTER_TYPE)
#undef REGISTER_TYPE
        }

        return "<invalid_type>";
    }

    struct IfElseStatement final
            : public Statement, public AMLScope {
        [[nodiscard]] inline explicit
        IfElseStatement(std::unique_ptr<AMLObject> &&condition)
                : Statement(Statement::Type::IF_ELSE)
                , AMLScope(std::string("INTERNAL_IF_ELSE_ROOT_STATEMENT"), this, nullptr)
                , m_condition(std::move(condition)) {
        }

        [[nodiscard]] inline const AMLScope *
        asScope() const final {
            return this;
        }

        [[nodiscard]] inline const AMLObject *
        condition() const {
            return m_condition.get();
        }

        [[nodiscard]] inline constexpr AMLScope &
        trueScope() {
            return m_trueScope;
        }

        [[nodiscard]] inline constexpr const AMLScope &
        trueScope() const {
            return m_trueScope;
        }

        [[nodiscard]] inline constexpr AMLScope &
        falseScope() {
            return m_falseScope;
        }

        [[nodiscard]] inline constexpr const AMLScope &
        falseScope() const {
            return m_falseScope;
        }

    private:
        std::unique_ptr<AMLObject> m_condition;
        AMLScope m_trueScope{std::string("INTERNAL_IF_ELSE_TRUE_STATEMENT"), nullptr, this};
        AMLScope m_falseScope{std::string("INTERNAL_IF_ELSE_FALSE_STATEMENT"), nullptr, this};
    };

    struct ReturnStatement final
            : public Statement {
        [[nodiscard]] inline explicit
        ReturnStatement(std::unique_ptr<AMLObject> &&result)
                : Statement(Statement::Type::RETURN)
                , m_result(std::move(result)) {
        }

        [[nodiscard]] inline const AMLObject &
        result() const {
            return *m_result;
        }

    private:
        std::unique_ptr<AMLObject> m_result;
    };

    struct WhileStatement final
            : public Statement, public AMLScope {
        [[nodiscard]] inline explicit
        WhileStatement(std::unique_ptr<AMLObject> &&condition)
                : Statement(Statement::Type::WHILE)
                , AMLScope(std::string("INTERNAL_WHILE_STATEMENT"), this, nullptr)
                , m_condition(std::move(condition)) {
        }

        [[nodiscard]] inline const AMLScope *
        asScope() const final {
            return this;
        }

        [[nodiscard]] inline const AMLObject &
        condition() const {
            return *m_condition;
        }

    private:
        std::unique_ptr<AMLObject> m_condition;
    };

} // namespace kernel::acpi::aml
