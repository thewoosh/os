#pragma once

#include <cstdint>

namespace kernel::acpi::aml {

    enum class OpCode
            : std::uint8_t {
        NullChar = 0x00,
        ZeroOp = 0x00,
        OneOp = 0x01,

        AliasOp = 0x06,

        NameOp = 0x08,

        BytePrefix = 0x0A,
        WordPrefix = 0x0B,
        DWordPrefix = 0x0C,
        StringPrefix = 0x0D,
        QWordPrefix = 0x0E,

        ScopeOp = 0x10,
        BufferOp = 0x11,
        PackageOp = 0x12,

        MethodOp = 0x14,

        ExtOpPrefix = 0x5B,

        Local0Op = 0x60,
        Local1Op = 0x61,
        Local2Op = 0x62,
        Local3Op = 0x63,
        Local4Op = 0x64,
        Local5Op = 0x65,
        Local6Op = 0x66,
        Local7Op = 0x67,
        Arg0Op = 0x68,
        Arg1Op = 0x69,
        Arg2Op = 0x6A,
        Arg3Op = 0x6B,
        Arg4Op = 0x6C,
        Arg5Op = 0x6D,
        Arg6Op = 0x6E,

        StoreOp = 0x70,

        AddOp = 0x72,

        SubtractOp = 0x74,
        IncrementOp = 0x75,

        ShiftLeftOp = 0x79,
        ShiftRightOp = 0x7A,
        AndOp = 0x7B,

        OrOp = 0x7D,

        DerefOfOp = 0x83,

        NotifyOp = 0x86,
        SizeofOp = 0x87,
        IndexOp = 0x88,

        CreateDWordFieldOp = 0x8A,
        CreateWordFieldOp = 0x8B,
        CreateByteFieldOp = 0x8C,
        CreateBitFieldOp = 0x8D,
        ObjectTypeOp = 0x8E,
        CreateQWordFieldOp = 0x8F,

        LandOp = 0x90,
        LorOp = 0x91,
        LnotOp = 0x92,
        LequalOp = 0x93,
        LgreaterOp = 0x94,
        LlessOp = 0x95,
        ToBufferOp = 0x96,

        ToHexStringOp = 0x98,

        ContinueOp = 0x9F,

        IfOp = 0xA0,
        ElseOp = 0xA1,
        WhileOp = 0xA2,
        NoopOp = 0xA3,
        ReturnOp = 0xA4,
        BreakOp = 0xA5,

        BreakPointOp = 0xCC,

        OnesOp = 0xFF,
    };

    enum class ExtendedOpCode
            : std::uint8_t {
        MutexOp = 0x01,

        StallOp = 0x21,
        SleepOp = 0x22,
        AcquireOp = 0x23,
        SignalOp = 0x24,

        ResetOp = 0x26,
        ReleaseOp = 0x27,

        DebugOp = 0x31,
        FatalOp = 0x32,

        OpRegionOp = 0x80,
        FieldOp = 0x81,
        DeviceOp = 0x82,
        // deprecated, see https://uefi.org/sites/default/files/resources/ACPI_Spec_6_3_A_Oct_6_2020.pdf
        ProcessorOp = 0x83,
    };

} // namespace kernel::acpi::aml
