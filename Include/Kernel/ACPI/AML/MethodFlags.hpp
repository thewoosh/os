#pragma once

#include <cstdint>

namespace kernel::acpi::aml {

    // https://uefi.org/specs/ACPI/6.4/20_AML_Specification/AML_Specification.html#methodflags
    struct [[gnu::packed]] MethodFlags {
        [[nodiscard]] inline constexpr std::uint8_t
        argumentCount() const {
            return m_arg_count;
        }

        [[nodiscard]] inline constexpr bool
        is_serialized() const {
            return m_is_serialized;
        }

        [[nodiscard]] inline constexpr std::uint8_t
        sync_level() const {
            return m_sync_level;
        }

    private:
        std::uint8_t m_arg_count : 3;
        bool m_is_serialized : 1;
        std::uint8_t m_sync_level : 4;
    };

    static_assert(sizeof(MethodFlags) == 1);

} // namespace kernel::acpi::aml