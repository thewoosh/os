#pragma once

#include <freesia_fwd>
#include <memory>

#include <cstdint>

namespace kernel::acpi::aml {

    struct AMLObject;
    struct DataRefObject;
    struct Expression;
    struct IfElseStatement;
    struct Method;
    struct Parser;
    struct WhileStatement;

    struct MethodContext;

    class Interpreter {
    public:
        [[nodiscard]] inline constexpr explicit
        Interpreter(Parser &parser)
                : m_parser(parser) {
        }

        [[nodiscard]] std::unique_ptr<AMLObject>
        invokeExpression(MethodContext &, const Expression *);

        void
        invokeIfElseStatement(MethodContext &, const IfElseStatement &);

        [[nodiscard]] std::unique_ptr<AMLObject>
        invokeMethod(Method *);

        void
        invokeStatementOrExpression(MethodContext &, const AMLObject &);

        void
        invokeWhileStatement(MethodContext &, const WhileStatement &);

        [[nodiscard]] std::unique_ptr<AMLObject>
        resolveObjectToValue(MethodContext &, const AMLObject &);

        [[nodiscard]] std::optional<std::uint64_t>
        resolveTermArgToInteger(MethodContext &, const AMLObject &);

    private:
        [[nodiscard]] bool
        areValuesEqual(MethodContext &, const AMLObject &, const AMLObject &);

        [[nodiscard]] std::unique_ptr<AMLObject>
        copyObject(const AMLObject &);

        [[nodiscard]] std::unique_ptr<AMLObject>
        invokeLOROTExpression(MethodContext &, const Expression *, auto operator_);

        [[nodiscard]] std::unique_ptr<AMLObject> *
        resolveSuperNameToReference(MethodContext &, const AMLObject &);

        /* discardable */ std::unique_ptr<DataRefObject>
        store(MethodContext &, const AMLObject &superName, std::unique_ptr<AMLObject> &&value);

        [[maybe_unused]] Parser &m_parser;
    };

} // namespace kernel::acpi::aml
