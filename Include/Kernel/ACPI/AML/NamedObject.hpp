#pragma once

#include <string>

#include "AMLObject.hpp"

namespace kernel::acpi::aml {

    // NamedObj := DefBankField | DefCreateBitField | DefCreateByteField
    //           | DefCreateDWordField | DefCreateField | DefCreateQWordField
    //           | DefCreateWordField | DefDataRegion | DefExternal | DefOpRegion
    //           | DefPowerRes | DefThermalZone
    // https://uefi.org/specs/ACPI/6.4/20_AML_Specification/AML_Specification.html#named-objects-encoding
    struct NamedObject
            : public AMLObject {
        [[nodiscard]] inline constexpr
        NamedObject(AMLObject::Type objectType, std::string &&name)
                : AMLObject(objectType)
                , m_name(std::move(name)) {
        }

        [[nodiscard]] inline constexpr const std::string &
        name() const {
            return m_name;
        }

    private:
        std::string m_name;
    };

    template<AMLObject::Type ObjectType>
    struct CreateIntegerField final
            : public NamedObject {
        [[nodiscard]] inline
        CreateIntegerField(std::string &&name, std::unique_ptr<AMLObject> &&sourceBuff,
                           std::unique_ptr<AMLObject> &&byteIndex)
                : NamedObject(ObjectType, std::move(name))
                , m_sourceBuff(std::move(sourceBuff))
                , m_byteIndex(std::move(byteIndex)) {
        }

        [[nodiscard]] inline const AMLObject &
        sourceBuff() const {
            return *m_sourceBuff;
        }

        [[nodiscard]] inline const AMLObject &
        byteIndex() const {
            return *m_byteIndex;
        }

    private:
        std::unique_ptr<AMLObject> m_sourceBuff;
        std::unique_ptr<AMLObject> m_byteIndex;
    };

    struct ProcessorDefinition
            : public AMLObject, public AMLScope {
        [[nodiscard]] inline constexpr
        ProcessorDefinition(std::string &&name, std::uint8_t procID, std::uint32_t pblkAddress, std::uint8_t pblkLength)
                : AMLObject(AMLObject::Type::PROCESSOR_DEFINITION)
                , AMLScope(std::move(name), this, nullptr)
                , m_procID(procID)
                , m_pblkAddress(pblkAddress)
                , m_pblkLength(pblkLength) {
        }

        [[nodiscard]] inline constexpr std::uint8_t
        procID() const {
            return m_procID;
        }

        [[nodiscard]] inline constexpr std::uint32_t
        pblkAddress() const {
            return m_pblkAddress;
        }

        [[nodiscard]] inline constexpr std::uint8_t
        pblkLength() const {
            return m_pblkLength;
        }

    private:
        std::uint8_t m_procID;
        std::uint32_t m_pblkAddress;
        std::uint8_t m_pblkLength;
    };

    using CreateByteField = CreateIntegerField<AMLObject::Type::CREATE_BYTE_FIELD>;
    using CreateWordField = CreateIntegerField<AMLObject::Type::CREATE_WORD_FIELD>;
    using CreateDWordField = CreateIntegerField<AMLObject::Type::CREATE_DWORD_FIELD>;
    using CreateQWordField = CreateIntegerField<AMLObject::Type::CREATE_QWORD_FIELD>;

} // namespace kernel::acpi::aml
