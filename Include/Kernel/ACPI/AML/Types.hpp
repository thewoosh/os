#pragma once

#include <cstdint>

namespace kernel::acpi::aml {

    //
    // Forwards
    //
    struct BufferData;
    enum class DataPrefix : std::uint8_t;
    struct Expression;
    enum class ExtendedOpCode : std::uint8_t;
    struct FieldFlags;
    enum class OpCode : std::uint8_t;
    enum class RegionSpace : std::uint8_t;
    struct Statement;

    //
    // Package Length Encoding
    //
    // PkgLength can be up to 28 bits.
    // https://uefi.org/specs/ACPI/6.4/20_AML_Specification/AML_Specification.html#package-length-encoding
    //
    using PkgLength = std::uint32_t;
    inline constexpr PkgLength k_invalid_package_length = 0xFFFFFFFF;


    // ArgObj := Arg0Op | Arg1Op | Arg2Op | Arg3Op | Arg4Op | Arg5Op | Arg6Op
    // https://uefi.org/specs/ACPI/6.4/20_AML_Specification/AML_Specification.html#argobj
    enum class ArgObjType
            : std::uint8_t {
        Arg0Op, Arg1Op, Arg2Op, Arg3Op, Arg4Op, Arg5Op, Arg6Op
    };

    // LocalObj := Local0Op | Local1Op | Local2Op | Local3Op | Local4Op | Local5Op | Local6Op | Local7Op
    // https://uefi.org/specs/ACPI/6.4/20_AML_Specification/AML_Specification.html#localobj
    enum class LocalObjType
            : std::uint8_t {
        Local0Op, Local1Op, Local2Op, Local3Op, Local4Op, Local5Op, Local6Op, Local7Op
    };

    enum class ConstObj
            : std::uint8_t {
        ZeroOp, OneOp, OnesOp
    };

} // namespace kernel::acpi::aml
