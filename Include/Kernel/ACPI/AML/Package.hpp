#pragma once

#include <vector>

#include "AMLObject.hpp"

namespace kernel::acpi::aml {

    struct Package
            : public AMLObject {
        [[nodiscard]] inline explicit
        Package(std::vector<std::unique_ptr<AMLObject>> &&elements)
                : AMLObject(AMLObject::Type::PACKAGE)
                , m_elements(std::move(elements)) {
        }

        Package(Package &&) = default;
        Package(const Package &) = delete;

        [[nodiscard]] inline std::vector<std::unique_ptr<AMLObject>> &
        elements() {
            return m_elements;
        }

        [[nodiscard]] inline const std::vector<std::unique_ptr<AMLObject>> &
        elements() const {
            return m_elements;
        }

    private:
        std::vector<std::unique_ptr<AMLObject>> m_elements;
    };

} // namespace kernel::acpi::aml
