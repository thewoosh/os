#pragma once

#include <memory>
#include <string_view>
#include <type_traits>
#include <vector>

#include <cstdint>

#include "Kernel/Print.hpp"

#include "Types.hpp"

namespace kernel::acpi::aml {

    struct AMLScope;

    struct AMLObject {
        enum class Type
                : std::uint8_t {
#define ACPI_AML_ITERATE_AML_OBJECT_TYPES(REGISTER_TYPE) \
            REGISTER_TYPE(ARGUMENT_OBJECT_REFERENCE) \
            REGISTER_TYPE(BUFFER) \
            REGISTER_TYPE(CONST_NUMBER) \
            REGISTER_TYPE(CONST_ONE) \
            REGISTER_TYPE(CONST_ONES) \
            REGISTER_TYPE(CONST_ZERO) \
            REGISTER_TYPE(CREATE_BYTE_FIELD) \
            REGISTER_TYPE(CREATE_DWORD_FIELD) \
            REGISTER_TYPE(CREATE_QWORD_FIELD) \
            REGISTER_TYPE(CREATE_WORD_FIELD) \
            REGISTER_TYPE(DATA_REF_OBJECT) \
            REGISTER_TYPE(DEBUG_OBJECT_REFERENCE) \
            REGISTER_TYPE(DEVICE) \
            REGISTER_TYPE(EXPRESSION) \
            REGISTER_TYPE(FIELD) \
            REGISTER_TYPE(FIELD_ELEMENT_ACCESS) \
            REGISTER_TYPE(FIELD_ELEMENT_CONNECT_WITH_BUFFER) \
            REGISTER_TYPE(FIELD_ELEMENT_CONNECT_WITH_STRING) \
            REGISTER_TYPE(FIELD_ELEMENT_NAMED) \
            REGISTER_TYPE(FIELD_ELEMENT_RESERVED) \
            REGISTER_TYPE(INVALID) \
            REGISTER_TYPE(LOCAL_OBJECT_REFERENCE) \
            REGISTER_TYPE(METHOD) \
            REGISTER_TYPE(MUTEX_DEFINITION) \
            REGISTER_TYPE(NAME_DEFINITION) \
            REGISTER_TYPE(NAME_STRING) \
            REGISTER_TYPE(NULL_NAME) \
            REGISTER_TYPE(OPERATION_REGION) \
            REGISTER_TYPE(PACKAGE) \
            REGISTER_TYPE(PROCESSOR_DEFINITION) \
            REGISTER_TYPE(ROOT_OBJECT) \
            REGISTER_TYPE(SCOPE) \
            REGISTER_TYPE(STATEMENT) \
            REGISTER_TYPE(STRING) \

#define REGISTER_TYPE(type) type,
            ACPI_AML_ITERATE_AML_OBJECT_TYPES(REGISTER_TYPE)
#undef REGISTER_TYPE
        };

        [[nodiscard]] inline constexpr explicit
        AMLObject(Type type)
                : m_type(type) {
        }

        inline virtual constexpr
        ~AMLObject() = default;

        [[gnu::const]] [[nodiscard]] inline virtual const AMLScope *
        asScope() const {
            return nullptr;
        }

        [[nodiscard]] inline constexpr Type
        objectType() const {
            return m_type;
        }

        [[nodiscard]] inline static constexpr std::unique_ptr<AMLObject>
        createInvalidUniquePtr() {
            return std::make_unique<AMLObject>(AMLObject::Type::INVALID);
        }

    private:
        Type m_type;
    };

    struct AMLScope {
        [[nodiscard]] inline constexpr
        AMLScope(std::string &&name, AMLObject *asObject, AMLScope *parent)
                : m_name(std::move(name))
                , m_parent(parent)
                , m_asObject(asObject) {
        }

        [[nodiscard]] inline const std::string &
        name() const {
            return m_name;
        }

        inline void
        overrideName(std::string &&name) {
#ifdef AMLOBJECT_SCOPE_DEBUG
            klog<LogLevel::DEBUG>("AMLScope") << "We're overriding our current name \""
                    << std::string_view{m_name.c_str(), m_name.length()} << " with \""
                    << std::string_view{name.c_str(), name.length()} << "\"";
#endif
            m_name = std::move(name);
        }

        [[nodiscard]] inline constexpr AMLObject *
        asObject() const {
            return m_asObject;
        }

        [[nodiscard]] inline constexpr AMLScope *
        parent() const {
            return m_parent;
        }

        inline constexpr void
        setParent(AMLScope *parent) {
            m_parent = parent;
        }

        [[nodiscard]] inline constexpr std::vector<std::unique_ptr<AMLObject>> &
        children() {
            return m_children;
        }

        [[nodiscard]] inline constexpr const std::vector<std::unique_ptr<AMLObject>> &
        children() const {
            return m_children;
        }

    private:
        std::string m_name;
        AMLScope *m_parent;
        AMLObject *m_asObject;
        std::vector<std::unique_ptr<AMLObject>> m_children{};
    };

    struct RootObject final
            : public AMLObject, AMLScope {
        [[nodiscard]] inline
        RootObject()
                : AMLObject(AMLObject::Type::ROOT_OBJECT)
                , AMLScope(std::string("INTERNAL_ROOT_OBJECT"), this, nullptr) {
        }

        [[nodiscard]] inline const AMLScope *
        asScope() const override final {
            return this;
        }
    };

    struct Device final
            : public AMLObject, AMLScope {
        [[nodiscard]] inline constexpr explicit
        Device(std::string &&name)
                : AMLObject(AMLObject::Type::DEVICE)
                , AMLScope(std::move(name), this, nullptr) {
        }

        [[nodiscard]] inline const AMLScope *
        asScope() const override final {
            return this;
        }
    };

    // NOTE: This object only gets created by the interpreter! This is therefore
    //       a runtime-only type!
    struct DataRefObject final
            : public AMLObject {
        // TODO somehow subscribe to the object's destructor
        //      => shared_ptr sounds ok, but then every object needs to have
        //         the overhead of that type.
        //      => special flag indicating the storage type (temporary, permanent)
        // The problem is that the reference m_object can become invalid.

        [[nodiscard]] inline constexpr explicit
        DataRefObject(std::unique_ptr<AMLObject> *objectReference)
                : AMLObject(AMLObject::Type::DATA_REF_OBJECT)
                , m_objectReference(objectReference) {
        }

        [[nodiscard]] inline constexpr std::unique_ptr<AMLObject> *
        objectReference() const {
            return m_objectReference;
        }

    private:
        std::unique_ptr<AMLObject> *m_objectReference;
    };

    struct Scope final
            : public AMLObject, AMLScope {
        [[nodiscard]] inline constexpr explicit
        Scope(std::string &&name)
                : AMLObject(AMLObject::Type::SCOPE)
                , AMLScope(std::move(name), this, nullptr) {
        }

        [[nodiscard]] inline const AMLScope *
        asScope() const override final {
            return this;
        }
    };

    [[nodiscard]] inline constexpr std::string_view
    toString(AMLObject::Type type) {
        switch (type) {
#define REGISTER_TYPE(type) \
            case AMLObject::Type::type: \
                return #type;

            ACPI_AML_ITERATE_AML_OBJECT_TYPES(REGISTER_TYPE)
#undef REGISTER_TYPE
        }

        ASSERT_NOT_REACHED();
        return "<invalid_type>";
    }

    template <typename ValueType, AMLObject::Type AMLObjectType, bool AllowCopyConstructor = true>
    struct AMLObjectWithSingleValue final
            : public AMLObject {

        [[nodiscard]] inline constexpr explicit
        AMLObjectWithSingleValue(const ValueType &value)
        requires(AllowCopyConstructor)
                : AMLObject(AMLObjectType)
                , m_value(value) {
        }

        [[nodiscard]] inline constexpr explicit
        AMLObjectWithSingleValue(ValueType &&value)
                : AMLObject(AMLObjectType)
                , m_value(std::forward<ValueType>(value)) {
        }

        [[nodiscard]] inline constexpr const ValueType &
        value() const {
            return m_value;
        }

    private:
        ValueType m_value;
    };

    struct MutexDefinition final
            : public AMLObject {

        struct [[gnu::packed]] SyncFlags {
            unsigned int argumentCount : 3;
            bool serializeFlag : 1;
            std::uint8_t syncLevel : 4;
        };

        static_assert(sizeof(SyncFlags) == 1);

        [[nodiscard]] inline constexpr explicit
        MutexDefinition(std::string &&name, SyncFlags syncFlags)
                : AMLObject(AMLObject::Type::MUTEX_DEFINITION)
                , m_name(std::move(name))
                , m_syncFlags(syncFlags) {
        }

        [[nodiscard]] inline constexpr const std::string &
        name() const {
            return m_name;
        }

        [[nodiscard]] inline constexpr SyncFlags
        syncFlags() const {
            return m_syncFlags;
        }

    private:
        std::string m_name;
        SyncFlags m_syncFlags;
    };

    struct NameDefinition final
            : public AMLObject {
        [[nodiscard]] inline constexpr explicit
        NameDefinition(std::string &&name, std::unique_ptr<AMLObject> &&object)
                : AMLObject(AMLObject::Type::NAME_DEFINITION)
                , m_name(std::move(name))
                , m_object(std::move(object)) {
        }

        [[nodiscard]] inline constexpr const std::string &
        name() const {
            return m_name;
        }

        [[nodiscard]] inline constexpr const AMLObject &
        value() const {
            return *m_object;
        }

    private:
        std::string m_name;
        std::unique_ptr<AMLObject> m_object;
    };

    struct OperationRegion final
            : public AMLObject {
        [[nodiscard]] inline constexpr
        OperationRegion(std::string &&name, RegionSpace regionSpace, std::uint64_t regionOffset,
                        std::uint64_t regionLength)
                : AMLObject(AMLObject::Type::OPERATION_REGION)
                , m_name(std::move(name))
                , m_regionSpace(regionSpace)
                , m_regionOffset(regionOffset)
                , m_regionLength(regionLength) {
        }

        [[nodiscard]] inline constexpr const std::string &
        name() const {
            return m_name;
        }

        [[nodiscard]] inline constexpr RegionSpace
        regionSpace() const {
            return m_regionSpace;
        }

        [[nodiscard]] inline constexpr std::uint64_t
        regionOffset() const {
            return m_regionOffset;
        }

        [[nodiscard]] inline constexpr std::uint64_t
        regionLength() const {
            return m_regionLength;
        }

    private:
        std::string m_name;
        RegionSpace m_regionSpace;
        std::uint64_t m_regionOffset;
        std::uint64_t m_regionLength;
    };

    using ArgumentObjectReference = AMLObjectWithSingleValue<std::uint8_t, AMLObject::Type::ARGUMENT_OBJECT_REFERENCE>;
    using ConstNumber = AMLObjectWithSingleValue<std::uint64_t, AMLObject::Type::CONST_NUMBER>;
    using LocalObjectReference = AMLObjectWithSingleValue<std::uint8_t, AMLObject::Type::LOCAL_OBJECT_REFERENCE>;
    using NameString = AMLObjectWithSingleValue<std::string, AMLObject::Type::NAME_STRING, false>;
    using StringObject = AMLObjectWithSingleValue<std::string, AMLObject::Type::STRING, false>;

} // namespace kernel::acpi::aml
