#pragma once

#include <cstdint>
#include <cstdlib>

#include <vector>

#include "AMLObject.hpp"
#include "BufferData.hpp"

namespace kernel::acpi::aml {

    struct Expression
            : public AMLObject {
        enum class Type {
#define ACPI_AML_ITERATE_EXPRESSION_TYPES(REGISER_TYPE) \
            REGISER_TYPE(ACQUIRE) \
            REGISER_TYPE(ADD) \
            REGISER_TYPE(AND) \
            REGISER_TYPE(BUFFER) \
            REGISER_TYPE(DEREFERENCE) \
            REGISER_TYPE(EQUAL_TO) \
            REGISER_TYPE(GREATER_THAN) \
            REGISER_TYPE(GREATER_THAN_OR_EQUAL_TO) \
            REGISER_TYPE(INCREMENT) \
            REGISER_TYPE(INDEX) \
            REGISER_TYPE(LESS_THAN) \
            REGISER_TYPE(LESS_THAN_OR_EQUAL_TO) \
            REGISER_TYPE(LOGICAL_AND) \
            REGISER_TYPE(LOGICAL_OR) \
            REGISER_TYPE(METHOD_INVOCATION) \
            REGISER_TYPE(NOT) \
            REGISER_TYPE(NOTIFY) \
            REGISER_TYPE(NOT_EQUAL_TO) \
            REGISER_TYPE(OR) \
            REGISER_TYPE(RELEASE) \
            REGISER_TYPE(SHIFT_LEFT) \
            REGISER_TYPE(SHIFT_RIGHT) \
            REGISER_TYPE(SIZEOF) \
            REGISER_TYPE(STORE) \
            REGISER_TYPE(SUBTRACT) \
            REGISER_TYPE(TO_BUFFER) \
            REGISER_TYPE(TO_HEX_STRING) \

#define REGISTER_TYPE(type) type,
            ACPI_AML_ITERATE_EXPRESSION_TYPES(REGISTER_TYPE)
#undef REGISTER_TYPE
        };

        [[nodiscard]] inline constexpr explicit
        Expression(Type type)
                : AMLObject(AMLObject::Type::EXPRESSION)
                , m_type(type) {
        }

        [[nodiscard]] inline constexpr Type
        expressionType() const {
            return m_type;
        }

    private:
        Type m_type;
    };

    [[nodiscard]] inline constexpr std::string_view
    toString(Expression::Type type) {
        switch (type) {
#define REGISTER_TYPE(type) case Expression::Type::type: return #type;
            ACPI_AML_ITERATE_EXPRESSION_TYPES(REGISTER_TYPE)
#undef REGISTER_TYPE
        }

        return "<invalid_type>";
    }

    struct AcquireExpression
            : public Expression {
        [[nodiscard]] inline
        AcquireExpression(std::unique_ptr<AMLObject> &&mutexObject, std::uint16_t timeout)
                : Expression(Expression::Type::ACQUIRE)
                , m_mutexObject(std::move(mutexObject))
                , m_timeout(timeout) {
        }

        [[nodiscard]] inline constexpr const AMLObject *
        mutexObject() const {
            return m_mutexObject.get();
        }

        [[nodiscard]] inline constexpr std::uint16_t
        timeout() const {
            return m_timeout;
        }

    private:
        std::unique_ptr<AMLObject> m_mutexObject;
        std::uint16_t m_timeout;
    };

    template<Expression::Type ExpressionType>
    struct BiExpression
            : public Expression {
        [[nodiscard]] inline
        BiExpression(std::unique_ptr<AMLObject> &&left, std::unique_ptr<AMLObject> &&right)
                : Expression(ExpressionType)
                , m_left(std::move(left))
                , m_right(std::move(right)) {
        }

        [[nodiscard]] inline const AMLObject &
        left() const {
            return *m_left;
        }

        [[nodiscard]] inline const AMLObject &
        right() const {
            return *m_right;
        }

    private:
        std::unique_ptr<AMLObject> m_left;
        std::unique_ptr<AMLObject> m_right;
    };

    struct BufferExpression
            : public Expression {
        [[nodiscard]] inline explicit
        BufferExpression(std::unique_ptr<BufferData> &&bufferData)
                : Expression(Expression::Type::BUFFER)
                , m_bufferData(std::move(bufferData)) {
        }

        [[nodiscard]] inline const BufferData &
        bufferData() const {
            return *m_bufferData;
        }

    private:
        std::unique_ptr<BufferData> m_bufferData;
    };

    struct DereferenceExpression final
            : public Expression {
        [[nodiscard]] inline explicit
        DereferenceExpression(std::unique_ptr<AMLObject> &&argument)
                : Expression(Expression::Type::DEREFERENCE)
                , m_argument(std::move(argument)) {
        }

        [[nodiscard]] inline const AMLObject &
        argument() const {
            return *m_argument;
        }

    private:
        std::unique_ptr<AMLObject> m_argument;
    };

    struct IndexExpression final
            : public Expression {
        [[nodiscard]] inline
        IndexExpression(std::unique_ptr<AMLObject> &&buffPkgStrObj, std::unique_ptr<AMLObject> &&indexValue,
                        std::unique_ptr<AMLObject> &&target)
                : Expression(Expression::Type::INDEX)
                , m_buffPkgStrObj(std::move(buffPkgStrObj))
                , m_indexValue(std::move(indexValue))
                , m_target(std::move(target)) {
        }

        [[nodiscard]] inline const AMLObject &
        buffPkgStrObj() const {
            return *m_buffPkgStrObj;
        }

        [[nodiscard]] inline const AMLObject &
        indexValue() const {
            return *m_indexValue;
        }

        [[nodiscard]] inline const AMLObject &
        target() const {
            return *m_target;
        }

    private:
        std::unique_ptr<AMLObject> m_buffPkgStrObj;
        std::unique_ptr<AMLObject>m_indexValue;
        std::unique_ptr<AMLObject> m_target;
    };

    template<Expression::Type ExpressionType>
    struct LeftOperandRightOperandTargetExpression final
            : public Expression {
        [[nodiscard]] inline
        LeftOperandRightOperandTargetExpression(std::unique_ptr<AMLObject> &&left, std::unique_ptr<AMLObject> &&right,
                        std::unique_ptr<AMLObject> &&target)
                : Expression(ExpressionType)
                , m_left(std::move(left))
                , m_right(std::move(right))
                , m_target(std::move(target)) {
        }

        [[nodiscard]] inline const AMLObject &
        left() const {
            return *m_left;
        }

        [[nodiscard]] inline const AMLObject &
        right() const {
            return *m_right;
        }

        [[nodiscard]] inline const AMLObject &
        target() const {
            return *m_target;
        }

    private:
        std::unique_ptr<AMLObject> m_left;
        std::unique_ptr<AMLObject> m_right;
        std::unique_ptr<AMLObject> m_target;
    };

    template<Expression::Type ExpressionType>
    struct OperandAndTargetExpression final
            : public Expression {
        [[nodiscard]] inline constexpr
        OperandAndTargetExpression(std::unique_ptr<AMLObject> &&operand, std::unique_ptr<AMLObject> &&target)
                : Expression(ExpressionType)
                , m_operand(std::move(operand))
                , m_target(std::move(target)) {
        }

        [[nodiscard]] inline const AMLObject &
        operand() const {
            return *m_operand;
        }

        [[nodiscard]] inline const AMLObject &
        target() const {
            return *m_target;
        }

    private:
        std::unique_ptr<AMLObject> m_operand;
        std::unique_ptr<AMLObject> m_target;
    };

    struct MethodInvocationExpression final
            : public Expression {
        using ArgumentsType = std::vector<std::unique_ptr<AMLObject>>;

        [[nodiscard]] inline explicit
        MethodInvocationExpression(std::string &&name, ArgumentsType &&arguments)
                : Expression(Expression::Type::METHOD_INVOCATION)
                , m_name(std::move(name))
                , m_arguments(std::move(arguments)) {
        }

        [[nodiscard]] inline const ArgumentsType &
        arguments() const {
            return m_arguments;
        }

        [[nodiscard]] inline const std::string &
        name() const {
            return m_name;
        }

    private:
        std::string m_name;
        ArgumentsType m_arguments;
    };

    template<Expression::Type ExpressionType>
    struct ShiftExpression final
            : public Expression {
        [[nodiscard]] inline
        ShiftExpression(std::unique_ptr<AMLObject> &&operand, std::unique_ptr<AMLObject> &&shiftCount,
                        std::unique_ptr<AMLObject> &&target)
                : Expression(ExpressionType)
                , m_operand(std::move(operand))
                , m_shiftCount(std::move(shiftCount))
                , m_target(std::move(target)) {
        }

        [[nodiscard]] inline const AMLObject &
        operand() const {
            return *m_operand;
        }

        [[nodiscard]] inline const AMLObject &
        shiftCount() const {
            return *m_shiftCount;
        }

        [[nodiscard]] inline const AMLObject &
        target() const {
            return *m_target;
        }

    private:
        std::unique_ptr<AMLObject> m_operand;
        std::unique_ptr<AMLObject> m_shiftCount;
        std::unique_ptr<AMLObject> m_target;
    };

    template <Expression::Type ExpressionType>
    struct SuperNameAndTermArgExpression final
            : public Expression {
        [[nodiscard]] inline
        SuperNameAndTermArgExpression(std::unique_ptr<AMLObject> &&termArg, std::unique_ptr<AMLObject> &&superName)
                : Expression(ExpressionType)
                , m_termArg(std::move(termArg))
                , m_superName(std::move(superName)) {
        }

        [[nodiscard]] inline const AMLObject &
        termArg() const {
            return *m_termArg;
        }

        [[nodiscard]] inline const AMLObject &
        superName() const {
            return *m_superName;
        }

    private:
        std::unique_ptr<AMLObject> m_termArg;
        std::unique_ptr<AMLObject> m_superName;
    };

    template<Expression::Type ExpressionType>
    struct SuperNameExpression final
            : public Expression {
        [[nodiscard]] inline explicit
        SuperNameExpression(std::unique_ptr<AMLObject> &&superName)
                : Expression(ExpressionType)
                , m_superName(std::move(superName)) {
        }

        [[nodiscard]] inline const AMLObject &
        superName() const {
            return *m_superName;
        }

    private:
        std::unique_ptr<AMLObject> m_superName;
    };

    template<Expression::Type ExpressionType>
    struct UniExpression
            : public Expression {
        [[nodiscard]] inline explicit
        UniExpression(std::unique_ptr<AMLObject> &&operand)
                : Expression(ExpressionType)
                , m_operand(std::move(operand)) {
        }

        [[nodiscard]] inline const AMLObject &
        operand() const {
            return *m_operand;
        }

    private:
        std::unique_ptr<AMLObject> m_operand;
    };

    using EqualsExpression = BiExpression<Expression::Type::EQUAL_TO>;
    using GreaterThanExpression = BiExpression<Expression::Type::GREATER_THAN>;
    using GreaterEqualExpression = BiExpression<Expression::Type::GREATER_THAN_OR_EQUAL_TO>;
    using LessThanExpression = BiExpression<Expression::Type::LESS_THAN>;
    using LessEqualExpression = BiExpression<Expression::Type::LESS_THAN_OR_EQUAL_TO>;
    using LogicalAndExpression = BiExpression<Expression::Type::LOGICAL_AND>;
    using LogicalOrExpression = BiExpression<Expression::Type::LOGICAL_OR>;
    using NotEqualsExpression = BiExpression<Expression::Type::NOT_EQUAL_TO>;

    using NotUniExpression = UniExpression<Expression::Type::NOT>;

    using AddExpression = LeftOperandRightOperandTargetExpression<Expression::Type::ADD>;
    using AndExpression = LeftOperandRightOperandTargetExpression<Expression::Type::AND>;
    using SubtractExpression = LeftOperandRightOperandTargetExpression<Expression::Type::SUBTRACT>;
    using OrExpression = LeftOperandRightOperandTargetExpression<Expression::Type::OR>;

    using ReleaseExpression = SuperNameExpression<Expression::Type::RELEASE>;
    using SizeofExpression = SuperNameExpression<Expression::Type::SIZEOF>;
    using IncrementExpression = SuperNameExpression<Expression::Type::INCREMENT>;

    using NotifyExpression = SuperNameAndTermArgExpression<Expression::Type::NOTIFY>;
    using StoreExpression = SuperNameAndTermArgExpression<Expression::Type::STORE>;

    using ToBufferExpression = OperandAndTargetExpression<Expression::Type::TO_BUFFER>;
    using ToHexStringExpression = OperandAndTargetExpression<Expression::Type::TO_HEX_STRING>;

} // namespace kernel::acpi::aml
