#pragma once

#include <string>

#include <cstdint>

#include "BufferData.hpp"
#include "Types.hpp"

namespace kernel::acpi::aml {

    enum class AccessAttrib
            : std::uint8_t {
        AttribQuick = 0x02,
        AttribSendReceive = 0x04,
        AttribByte = 0x06,
        AttribWord = 0x08,
        AttribBlock = 0x0A,
        AttribProcessCall = 0x0C,
        AttribBlockProcessCall = 0x0d,
    };

    enum class AccessType
            : std::uint8_t {
        AnyAcc = 0,
        ByteAcc = 1,
        WordAcc = 2,
        DWordAcc = 3,
        QWordAcc = 4,
        BufferAcc = 5,
        Reserved = 6,
        // 7-15 Reserved
    };

    // https://uefi.org/specs/ACPI/6.4/20_AML_Specification/AML_Specification.html#fieldflags
    struct [[gnu::packed]] FieldFlags {
        enum class UpdateRule
                : std::uint8_t {
            Preserve = 0,
            WriteAsOnes = 1,
            WriteAsZeros = 2,
        };

        [[nodiscard]] inline constexpr AccessType
        access() const {
            return m_access_type;
        }

        [[nodiscard]] inline constexpr bool
        locked() const {
            return m_locked;
        }

        [[nodiscard]] inline constexpr UpdateRule
        update_rule() const {
            return m_update_rule;
        }

    private:
        AccessType m_access_type : 4;
        bool m_locked : 1;
        UpdateRule m_update_rule : 2;
        [[maybe_unused]] bool m_reserved : 1;
    };

    static_assert(sizeof(FieldFlags) == 1);

    struct [[gnu::packed]] StandaloneAccessType {
        // The spec is so vague here, I have no idea what this is
        enum class NormalAccessAttributes
                : std::uint8_t {
            AttribBytes = 1,
            AttribRawBytes = 2,
            AttribRawProcessBytes = 3,
        };

        [[nodiscard]] inline constexpr AccessType
        access() const {
            return m_access_type;
        }

        [[nodiscard]] inline constexpr NormalAccessAttributes
        normal_access_attributes() const {
            return m_normal_access_attributes;
        }

    private:
        AccessType m_access_type : 4;
        [[maybe_unused]] bool m_reserved_0 : 1;
        [[maybe_unused]] bool m_reserved_1 : 1;
        NormalAccessAttributes m_normal_access_attributes : 2;
    };

    static_assert(sizeof(StandaloneAccessType) == 1);

    struct Field final
            : public AMLObject, public AMLScope {
        [[nodiscard]] inline constexpr
        Field(std::string &&name, FieldFlags flags)
                : AMLObject(AMLObject::Type::FIELD)
                , AMLScope(std::move(name), this, nullptr)
                , m_flags(flags) {
        }

        [[nodiscard]] inline constexpr FieldFlags
        flags() const {
            return m_flags;
        }

        [[nodiscard]] inline const AMLScope *
        asScope() const final {
            return this;
        }

    private:
        FieldFlags m_flags;
    };

    struct AccessFieldElement final
            : public AMLObject {
        [[nodiscard]] inline constexpr explicit
        AccessFieldElement(StandaloneAccessType accessType, AccessAttrib accessAttrib)
                : AMLObject(AMLObject::Type::FIELD_ELEMENT_ACCESS)
                , m_accessType(accessType)
                , m_accessAttrib(accessAttrib) {
        }

        [[nodiscard]] inline constexpr AccessAttrib
        accessAttrib() const {
            return m_accessAttrib;
        }

        [[nodiscard]] inline constexpr StandaloneAccessType
        accessType() const {
            return m_accessType;
        }

    private:
        StandaloneAccessType m_accessType;
        AccessAttrib m_accessAttrib;
    };

    struct ConnectFieldElementWithBuffer
            : public AMLObject {
        [[nodiscard]] inline explicit
        ConnectFieldElementWithBuffer(std::unique_ptr<BufferData> &&buffer)
                : AMLObject(AMLObject::Type::FIELD_ELEMENT_CONNECT_WITH_BUFFER)
                , m_buffer(std::move(buffer)) {
        }

        [[nodiscard]] inline constexpr const BufferData *
        buffer() const {
            return m_buffer.get();
        }

    private:
        std::unique_ptr<BufferData> m_buffer;
    };

    struct ConnectFieldElementWithString
            : public AMLObject {
        [[nodiscard]] inline explicit
        ConnectFieldElementWithString(std::string &&name)
                : AMLObject(AMLObject::Type::FIELD_ELEMENT_CONNECT_WITH_STRING)
                , m_name(std::move(name)) {
        }

        [[nodiscard]] inline constexpr const std::string &
        name() const {
            return m_name;
        }

    private:
        std::string m_name;
    };

    struct NamedFieldElement
            : public AMLObject {
        [[nodiscard]] inline
        NamedFieldElement(std::string &&name, PkgLength packageLength)
                : AMLObject(AMLObject::Type::FIELD_ELEMENT_NAMED)
                , m_name(std::move(name))
                , m_packageLength(packageLength) {
        }

        [[nodiscard]] inline constexpr const std::string &
        name() const {
            return m_name;
        }

        [[nodiscard]] inline constexpr PkgLength
        packageLength() const {
            return m_packageLength;
        }

    private:
        std::string m_name;
        PkgLength m_packageLength;
    };

    struct ReservedFieldElement final
            : public AMLObject {
        [[nodiscard]] inline constexpr explicit
        ReservedFieldElement(PkgLength packageLength)
                : AMLObject(AMLObject::Type::FIELD_ELEMENT_RESERVED)
                , m_packageLength(packageLength) {
        }

        [[nodiscard]] inline constexpr PkgLength
        packageLength() const {
            return m_packageLength;
        }

    private:
        PkgLength m_packageLength;
    };

} // namespace kernel::acpi::aml