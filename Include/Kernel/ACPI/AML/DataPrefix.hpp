#pragma once

#include <cstdint>

namespace kernel::acpi::aml {

    enum class DataPrefix
            : std::uint8_t {
        BytePrefix = 0x0A,
        WordPrefix = 0x0B,
        DWordPrefix = 0x0C,
        QWordPrefix = 0x0E,
    };

} // namespace kernel::acpi::aml