#pragma once

#include <cstdint>

#include "GenericAddressStructure.hpp"
#include "PreferredPowerManagementProfile.hpp"
#include "SDTHeader.hpp"

namespace kernel::acpi {

    //
    // Differentiated System Description Table (DSDT)
    //
    // https://uefi.org/specs/ACPI/6.4/05_ACPI_Software_Programming_Model/ACPI_Software_Programming_Model.html#differentiated-system-description-table-dsdt
    //
    struct [[gnu::packed]] DSDT {
        SDTHeader header;

        [[nodiscard]] const void *
        definition_block() const {
            return reinterpret_cast<const std::uint8_t *>(this) + sizeof(header);
        }
    };

} // namespace kernel::acpi