#pragma once

#include <cstdint>

#include "BootArchitectureFlags.hpp"
#include "GenericAddressStructure.hpp"
#include "PreferredPowerManagementProfile.hpp"
#include "SDTHeader.hpp"

namespace kernel::acpi {

    //
    // Fixed ACPI Description Table
    //
    struct [[gnu::packed]] FADT {
        SDTHeader header;

        struct Base {
            std::uint32_t firmware_control;
            std::uint32_t dsdt;

            // field used in ACPI 1.0; no longer in use, for compatibility only
            std::uint8_t reserved;

            PreferredPowerManagementProfile preferred_power_management_profile;
            std::uint16_t sci_interrupt;
            std::uint32_t smi_command_port;
            std::uint8_t acpi_enable;
            std::uint8_t acpi_disable;
            std::uint8_t s4bios_req;
            std::uint8_t pstate_control;
            std::uint32_t pm1a_event_block;
            std::uint32_t pm1b_event_block;
            std::uint32_t pm1a_control_block;
            std::uint32_t pm1b_control_block;
            std::uint32_t pm2_control_block;
            std::uint32_t pm_timer_block;
            std::uint32_t gpe0_block;
            std::uint32_t gpe1_block;
            std::uint8_t pm1_event_length;
            std::uint8_t pm1_control_length;
            std::uint8_t pm2_control_length;
            std::uint8_t pm_timer_length;
            std::uint8_t gpe0_length;
            std::uint8_t gpe1_length;
            std::uint8_t gpe1_base;
            std::uint8_t cstate_control;
            std::uint16_t worst_c2_latency;
            std::uint16_t worst_c3_latency;
            std::uint16_t flush_size;
            std::uint16_t flush_stride;
            std::uint8_t duty_offset;
            std::uint8_t duty_width;
            std::uint8_t day_alarm;
            std::uint8_t month_alarm;
            std::uint8_t century;

            // reserved in ACPI 1.0; used since ACPI 2.0+
            BootArchitectureFlags bootArchitectureFlags;

            std::uint8_t reserved2;
            std::uint32_t flags;

            GenericAddressStructure reset_reg;

            std::uint8_t reset_value;
            std::uint8_t reserved3[3];

            // 64bit pointers - Available on ACPI 2.0+
            std::uint64_t x_firmware_control;
            std::uint64_t x_dsdt;

            GenericAddressStructure x_pm1a_event_block;
            GenericAddressStructure x_pm1b_event_block;
            GenericAddressStructure x_pm1a_control_block;
            GenericAddressStructure x_pm1b_control_block;
            GenericAddressStructure x_pm2_control_block;
            GenericAddressStructure x_pm_timer_block;
            GenericAddressStructure x_gpe0_block;
            GenericAddressStructure x_gpe1_block;
        };

        [[nodiscard]] inline const Base &
        base() const {
            return m_base;
        }

        [[nodiscard]] inline const DSDT *
        dsdt() const {
            // Following the spec, but somehow this works on neither VBox
            // nor QEMU.
#if 0
            if (header.revision >= 2 && m_base.x_dsdt != 0)
                return reinterpret_cast<const DSDT *>(m_base.x_dsdt);
#endif
            return reinterpret_cast<const DSDT *>(m_base.dsdt);
        }

    private:
        Base m_base;
    };

} // namespace kernel::acpi