#pragma once

#include <cstdint>

#ifdef ACPI_ALLOW_DUMP
#include "Kernel/Print.hpp"
#endif

namespace kernel::acpi {

    struct [[gnu::packed]] SDTHeader {
        char signature[4];
        std::uint32_t length;
        std::uint8_t revision;
        std::uint8_t checksum;
        char oem_id[6];
        char oem_table_id[8];
        std::uint32_t oem_revision;
        std::uint32_t creator_id;
        std::uint32_t creator_revision;

        enum class ValidationFailure {
            NONE,
            SIGNATURE_IS_NULL,
        };

#ifdef ACPI_ALLOW_DUMP
        inline void
        dump() const {
            klog<LogLevel::DEBUG>("ACPI-SDTHeader") << "Dumping Header @ " << std::uint64_t(this);
            klog<LogLevel::DEBUG>("ACPI-SDTHeader") << "Signature: " << std::string_view{signature, 4};
            klog<LogLevel::DEBUG>("ACPI-SDTHeader") << "Length: " << length;
            klog<LogLevel::DEBUG>("ACPI-SDTHeader") << "Revision: " << revision;
            klog<LogLevel::DEBUG>("ACPI-SDTHeader") << "Checksum: " << checksum;
            klog<LogLevel::DEBUG>("ACPI-SDTHeader") << "OEMID: " << std::string_view{oem_id, 6};
            klog<LogLevel::DEBUG>("ACPI-SDTHeader") << "OEM Table ID: " << std::string_view{oem_table_id, 8};
            klog<LogLevel::DEBUG>("ACPI-SDTHeader") << "OEM Revision: " << oem_revision;
            klog<LogLevel::DEBUG>("ACPI-SDTHeader") << "Creator ID: " << creator_id;
            klog<LogLevel::DEBUG>("ACPI-SDTHeader") << "Creator Revision: " << creator_revision;
        }
#endif // ACPI_ALLOW_DUMP

        [[nodiscard]] inline std::string_view
        validate() const {
            if (signature[0] == 0)
                return "SIGNATURE_IS_NULL";

            if (length == 0)
                return "LENGTH_IS_ZERO";

            if (oem_id[0] == 0)
                return "OEM_ID_IS_NULL";

            std::uint8_t sum{0};
            const auto *ptr = reinterpret_cast<const std::uint8_t *>(this);
            for (std::size_t i = 0; i < length; ++i) {
                sum += ptr[i];
            }

            if (sum != 0)
                return "CHECKSUM_NOT_ZERO";

            return {};
        }
    };

} // namespace kernel::acpi
