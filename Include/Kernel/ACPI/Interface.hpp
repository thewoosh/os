#pragma once

#include <string_view>

#include "FADT.hpp"
#include "RSDT.hpp"
#include "XSDT.hpp"

#include "Kernel/Print.hpp"

namespace kernel::acpi {

    class Interface {
    public:
        enum class Type {
            RSDT,
            XSDT,
        };

    private:
        [[nodiscard]] inline static std::size_t
        calculate_sdt_count(Type type, const void *pointer) {
            if (!pointer)
                return 0;

            const auto *sdt = reinterpret_cast<const SDTHeader *>(pointer);

            switch (type) {
                case Type::RSDT:
                    return (sdt->length - sizeof(*sdt)) / 4;
                case Type::XSDT:
                    return (sdt->length - sizeof(*sdt)) / 8;
            }

            return 0;
        }

    public:
        [[nodiscard]] inline constexpr
        Interface(Type type, const void *main_sdt_pointer)
                : m_type(type)
                , m_main_sdt_pointer(main_sdt_pointer)
                , m_sdt_count(calculate_sdt_count(type, main_sdt_pointer)) {
        }

        template<typename Callback>
        inline void
        for_each_entry(Callback callback) const {
            for (std::size_t index = 0; index < m_sdt_count; ++index)
                callback(*table_by_index(index));
        }

        [[nodiscard]] inline const SDTHeader *
        find(std::string_view name) const {
#ifdef ACPI_INTERFACE_DEBUG
            klog<LogLevel::DEBUG>("ACPI_INTERFACE_FIND") << "Entry with name=\"" << name << "\" m_sdt_count=" << m_sdt_count;
#endif
            if (name.size() != 4) {
#ifdef ACPI_INTERFACE_DEBUG
                klog<LogLevel::DEBUG>("ACPI_INTERFACE_FIND") << "Skipping because name length != 4, data=\"" << name << "\"";
#endif
                return nullptr;
            }

            for (std::size_t index = 0; index < m_sdt_count; ++index) {
                const auto &sdt = *table_by_index(index);
#ifdef ACPI_INTERFACE_DEBUG
                klog<LogLevel::DEBUG>("ACPI_INTERFACE_FIND") << "Compare \"" << std::string_view(sdt.signature, 4) << "\" and \"" << name << "\" ptr_sig=" << reinterpret_cast<std::uint64_t>(&sdt.signature) << " sdt_header_size=" << sdt.length;
#endif
                if (std::string_view(sdt.signature, 4) == name)
                    return &sdt;
            }

            return nullptr;
        }

        [[nodiscard]] inline bool
        is_valid() const {
            if (!m_main_sdt_pointer || m_sdt_count == 0)
                return false;

            switch (m_type) {
                case Type::RSDT:
                    return rsdt()->sub_sdts && std::string_view(rsdt()->header.signature, 4) == "RSDT";
                case Type::XSDT:
                    return xsdt()->sub_sdts && std::string_view(xsdt()->header.signature, 4) == "XSDT";
            }

            return false;
        }

        [[nodiscard]] inline constexpr const RSDT *
        rsdt() const {
            if (m_type == Type::RSDT)
                return reinterpret_cast<const RSDT *>(m_main_sdt_pointer);
            return nullptr;
        }

        [[nodiscard]] inline constexpr const XSDT *
        xsdt() const {
            if (m_type == Type::XSDT)
                return reinterpret_cast<const XSDT *>(m_main_sdt_pointer);
            return nullptr;
        }

        [[nodiscard]] inline const SDTHeader *
        table_by_index(std::size_t index) const {
            if (index >= m_sdt_count)
                return nullptr;
            switch (m_type) {
                case Type::RSDT:
                    return reinterpret_cast<const kernel::acpi::SDTHeader *>((&rsdt()->sub_sdts)[index]);
                case Type::XSDT:
                    return reinterpret_cast<const kernel::acpi::SDTHeader *const *>(&xsdt()->sub_sdts)[index];
            }

            return nullptr;
        }

        [[nodiscard]] inline constexpr Type
        type() const {
            return m_type;
        }

    private:
        Type m_type;

        // The RSDT "Root System Description Table" contains the pointers to
        // other SDTs.
        // This pointer can also contain the XSDT "eXtended System Descriptor
        // Table", which is the 64-bit/newer variant of the RSDT.
        const void *m_main_sdt_pointer;

        std::size_t m_sdt_count;
    };

} // namespace kernel::acpi