#pragma once

#include <cstdint>

namespace kernel::acpi {

    //
    // Access Size type of the Generic Address Structure (GAS)
    //
    // ACPI 6.3
    //   Table 5-25
    //   Page 115
    //
    enum class AccessSizeType : std::uint8_t {
        UNDEFINED = 0,
        BYTE = 1,
        WORD = 2,
        DWORD = 3,
        QWORD = 4,
    };

} // namespace kernel::acpi
