#pragma once

#include <memory>
#include <string>
#include <utility>

#include <cstdint>

#include <Core/RegionPointer.hpp>
#include <Core/StackVector.hpp>
#include <Core/View.hpp>

// TODO: forward these types (and template<typename T> class std::unique_ptr<T>; )
#include "AML/AMLObject.hpp"
#include "AML/Field.hpp"
#include "AML/Types.hpp"

namespace kernel::acpi::aml {

    inline constexpr std::size_t kMaxRecursionDepth = 20;

    struct Method;
    struct NamedObject;
    struct Package;

    struct Parser {
        enum class Status {
#define ACPI_AML_PARSER_ITERATE_STATUSES(REGISTER_STATUS) \
            REGISTER_STATUS(OK) \
            \
            REGISTER_STATUS(TEMP_ERROR) \
            \
            REGISTER_STATUS(BLOCK_HEADER_TABLE_SIGNATURE_FIELD_MISSING) \
            REGISTER_STATUS(BLOCK_HEADER_TABLE_LENGTH_FIELD_MISSING) \
            REGISTER_STATUS(BLOCK_HEADER_TABLE_SPEC_COMPLIANCE_FIELD_MISSING) \
            REGISTER_STATUS(BLOCK_HEADER_TABLE_CHECKSUM_FIELD_MISSING) \
            REGISTER_STATUS(BLOCK_HEADER_TABLE_OEMID_MISSING) \
            REGISTER_STATUS(BLOCK_HEADER_TABLE_OEM_TABLE_ID_MISSING) \
            REGISTER_STATUS(BLOCK_HEADER_TABLE_OEM_REVISION_FIELD_MISSING) \
            REGISTER_STATUS(BLOCK_HEADER_TABLE_CREATOR_ID_FIELD_MISSING) \
            REGISTER_STATUS(BLOCK_HEADER_TABLE_CREATOR_REVISION_FIELD_MISSING) \
            \
            REGISTER_STATUS(NOT_A_CONST_NUMBER) \
            REGISTER_STATUS(EOS_WHILE_READING_CONST_NUMBER) \
            \
            /* DataObject */\
            REGISTER_STATUS(DATA_OBJECT_OP_CODE_END_OF_STREAM) \
            REGISTER_STATUS(DATA_OBJECT_OP_CODE_UNKNOWN) \
            \
            /* DefDevice */\
            REGISTER_STATUS(DEF_DEVICE_ERROR_WITH_PACKAGE_LENGTH) \
            \
            /* DefField */\
            REGISTER_STATUS(DEF_FIELD_ERROR_WITH_PACKAGE_LENGTH) \
            REGISTER_STATUS(DEF_FIELD_EOS_WHEN_READING_FIELD_FLAGS) \
            \
            /* DefMethod */\
            REGISTER_STATUS(DEF_METHOD_ERROR_WITH_PACKAGE_LENGTH) \
            REGISTER_STATUS(DEF_METHOD_EOS_READING_METHOD_FLAGS) \
            \
            /* DefMutex */\
            REGISTER_STATUS(DEF_MUTEX_FAILED_TO_READ_SYNC_FLAGS) \
            \
            /* DefOpRegion */\
            REGISTER_STATUS(DEF_OP_REGION_FAILED_TO_READ_REGION_SPACE) \
            \
            /*DefPackage */ \
            REGISTER_STATUS(DEF_PACKAGE_FAILED_TO_READ_NUM_ELEMENTS) \
            REGISTER_STATUS(DEF_PACKAGE_INVALID_PACKAGE_LENGTH) \
            REGISTER_STATUS(DEF_PACKAGE_FAILED_TO_READ_ELEMENT) \
            \
            /* DefProcessor */ \
            REGISTER_STATUS(DEF_PROCESSOR_INVALID_PACKAGE_LENGTH) \
            REGISTER_STATUS(DEF_PROCESSOR_FAILED_TO_READ_PROC_ID) \
            REGISTER_STATUS(DEF_PROCESSOR_FAILED_TO_READ_PBLKADDR) \
            REGISTER_STATUS(DEF_PROCESSOR_FAILED_TO_READ_PBLKLEN) \
            \
            /* DefScope */\
            REGISTER_STATUS(DEF_SCOPE_FAILED_TO_READ_PKG_LENGTH) \
            \
            /* DefToHexString */\
            REGISTER_STATUS(DEF_TO_HEX_STRING_EOS_FIRST_NAME_BYTE) \
            REGISTER_STATUS(DEF_TO_HEX_STRING_EOS_EXT_OP_CODE) \
            REGISTER_STATUS(DEF_TO_HEX_STRING_UNKNOWN_EXT_OP_CODE) \
            REGISTER_STATUS(DEF_TO_HEX_STRING_UNKNOWN_TARGET) \
            \
            /* Expression */\
            REGISTER_STATUS(EXPRESSION_EXTENDED_OP_CODE_END_OF_STREAM) \
            REGISTER_STATUS(EXPRESSION_OP_CODE_END_OF_STREAM) \
            REGISTER_STATUS(EXPRESSION_OP_CODE_UNKNOWN) \
            REGISTER_STATUS(DEF_ACQUIRE_FAILED_TO_READ_TIMEOUT) \
            REGISTER_STATUS(DEF_BUFFER_INVALID_PACKAGE_LENGTH) \
            REGISTER_STATUS(DEF_BUFFER_BUFFER_SIZE_GREATER_THAN_PACKAGE_LENGTH) \
            REGISTER_STATUS(DEF_BUFFER_BUFFER_SIZE_DOES_NOT_ADD_UP) \
            REGISTER_STATUS(DEF_BUFFER_FAILED_TO_READ_DATA) \
            \
            /* FieldElement */\
            REGISTER_STATUS(FIELD_ELEMENT_END_OF_STREAM) \
            REGISTER_STATUS(FIELD_ELEMENT_NAMED_FIELD_PKG_LENGTH_ERROR) \
            REGISTER_STATUS(FIELD_ELEMENT_RESERVED_FIELD_PKG_LENGTH_ERROR) \
            REGISTER_STATUS(FIELD_ELEMENT_ACCESS_FIELD_ACCESS_TYPE_READ_ERROR) \
            REGISTER_STATUS(FIELD_ELEMENT_ACCESS_FIELD_ACCESS_ATTRIB_READ_ERROR) \
            \
            /* FieldList */\
            REGISTER_STATUS(FIELD_LIST_PKG_LENGTH_TOO_LARGE) \
            REGISTER_STATUS(FIELD_LIST_LOOP_DETECTED) \
            \
            /* NameSeg */\
            REGISTER_STATUS(NAME_SEG_END_OF_STREAM) \
            REGISTER_STATUS(NAME_SEG_FIRST_CHAR_NOT_A_LEAD_NAME_CHAR)   \
            REGISTER_STATUS(NAME_SEG_SECOND_CHAR_NOT_A_NAME_CHAR) \
            REGISTER_STATUS(NAME_SEG_THIRD_CHAR_NOT_A_NAME_CHAR) \
            REGISTER_STATUS(NAME_SEG_FOURTH_CHAR_NOT_A_NAME_CHAR) \
            \
            /* NamedObj */\
            REGISTER_STATUS(NAMED_OBJECT_FAILED_TO_READ_OP_CODE) \
            REGISTER_STATUS(NAMED_OBJECT_FAILED_TO_READ_EXTENDED_OP_CODE) \
            REGISTER_STATUS(NAMED_OBJECT_UNRECOGNIZED_OP_CODE) \
            \
            /* NameSpaceModifierObj */\
            REGISTER_STATUS(NAME_SPACE_MODIFIER_OBJ_INVALID_OP_CODE) \
            \
            /* NameString */\
            REGISTER_STATUS(NAME_STRING_NO_FIRST_BYTE) \
            REGISTER_STATUS(NAME_STRING_INVALID_SEG_COUNT_IN_MULTI_NAME_PATH) \
            REGISTER_STATUS(NAME_STRING_SEG_COUNT_IS_ZERO) \
            \
            /* ReferenceTypeOpCode */\
            REGISTER_STATUS(REFERENCE_FAILED_TO_READ_OP_CODE) \
            REGISTER_STATUS(REFERENCE_INVALID_OP_CODE) \
            \
            /* Statement */\
            REGISTER_STATUS(STATEMENT_NO_FIRST_BYTE) \
            REGISTER_STATUS(STATEMENT_NOT_ON_STREAM) \
            REGISTER_STATUS(WHILE_STATEMENT_WITH_INVALID_PACKAGE_LENGTH) \
            REGISTER_STATUS(IF_ELSE_STATEMENT_WITH_INVALID_PACKAGE_LENGTH) \
            REGISTER_STATUS(ELSE_STATEMENT_WITH_INVALID_PACKAGE_LENGTH) \
            \
            /* Statement */\
            REGISTER_STATUS(STRING_FAILED_TO_READ) \
            REGISTER_STATUS(STRING_NOT_ASCII_CHAR_OR_NULL_TERMINATOR) \
            \
            /* Term Arg */\
            REGISTER_STATUS(TERM_ARG_FIRST_BYTE_EOS) \
            \
            /* Term Object */\
            REGISTER_STATUS(NO_TERM_OBJECT_FIRST_BYTE) \
            REGISTER_STATUS(TERM_OBJECT_FIRST_BYTE_UNEXPECTED) \
            REGISTER_STATUS(TERM_OBJECT_EXT_OP_PREFIX_END_OF_STREAM) \
            REGISTER_STATUS(TERM_OBJECT_EXT_OP_UNKOWN) \


#define REGISTER_STATUS(status) status,
            ACPI_AML_PARSER_ITERATE_STATUSES(REGISTER_STATUS)
#undef REGISTER_STATUS
        };

        template <typename Contained>
        struct StatusAnd {
            [[nodiscard]] inline constexpr
            StatusAnd(Status status, Contained &&contained)
                    : m_status(status)
                    , m_contained(std::move(contained)) {
            }

            [[nodiscard]] inline constexpr Status
            status() const {
                return m_status;
            }

            [[nodiscard]] inline constexpr Contained &
            contained() {
                return m_contained;
            }

            [[nodiscard]] inline constexpr const Contained &
            contained() const {
                return m_contained;
            }

        private:
            Status m_status;
            Contained m_contained;
        };

        [[nodiscard]] inline
        Parser(const void *data, std::size_t size)
                : m_data(static_cast<const std::uint8_t *>(data), size)
                , m_iter(m_data.begin()) {
            // TODO should we generate these objects ahead of time?
            //      and if so, append all objects.
            // The weird thing is, is that _SB_ is defined at the spec as predefined,
            // whilst with current functionings (2022-03-26) not predefining it works fine.
            //
            // 5.3.1. Predefined Root Namespaces
            // https://uefi.org/specs/ACPI/6.4/05_ACPI_Software_Programming_Model/ACPI_Software_Programming_Model.html#predefined-root-namespaces
            m_rootObject.children().push_back(std::make_unique<Scope>(std::string("_GPE")));
        }

        [[nodiscard]] const AMLObject *
        findObject(std::string_view name) const;

        [[nodiscard]] Status
        run();

        [[nodiscard]] inline constexpr RootObject &
        rootObject() {
            return m_rootObject;
        }

    private:
        enum class OperandOperandOperationType {
            LOGICAL_AND,
            LOGICAL_EQUAL,
            LOGICAL_GREATER,
            LOGICAL_GREATER_EQUAL,
            LOGICAL_LESS,
            LOGICAL_LESS_EQUAL,
            LOGICAL_NOT,
            LOGICAL_NOT_EQUAL,
            LOGICAL_OR,
        };

        struct InvocationTarget {
            const AMLObject *object;
            std::size_t argumentCount;
        };

        inline void
        assignObjectToParentScope(std::unique_ptr<AMLObject> &&object, AMLScope &parent);

        // Debug method for when the normal resolution doesn't work, and we try
        // to find where the object could be.
        template<typename CallbackType>
        static void
        findAllObjectsWithName(std::string_view, const AMLScope &parent, CallbackType callback);

        [[nodiscard]] InvocationTarget
        findInvocationTarget(std::string_view name) const;

        [[nodiscard]] const AMLObject *
        findObjectAbsolute(std::string_view name, const AMLScope *scope) const;

        [[nodiscard]] const AMLObject *
        findObjectRelative(std::string_view name, const AMLScope *scope) const;

        [[nodiscard]] const AMLObject *
        findObjectRelativeDownwards(std::string_view name, const AMLScope *scope) const;

        struct ParentInformation {
            const AMLScope *parent{nullptr};
            std::string_view parentPrefix{};
            std::string_view name{};
        };

        [[nodiscard]] ParentInformation
        findParent(const AMLObject &) const;

        template <typename T>
        [[nodiscard]] bool
        peek(T &data) const;

        template <typename T>
        [[nodiscard]] bool
        read(T &);

        // Read a ByteConst/WordConst/DWordConst/QWordConst
        [[nodiscard]] Status
        read_const_number(std::uint64_t &out);

        [[nodiscard]] StatusAnd<std::unique_ptr<AMLObject>>
        readDataObject(bool silentOnFailure);

        [[nodiscard]] Status
        read_def_block_header();

        [[nodiscard]] StatusAnd<std::unique_ptr<BufferData>>
        readDefBuffer();

        [[nodiscard]] StatusAnd<std::unique_ptr<AMLObject>>
        readDefDevice();

        [[nodiscard]] StatusAnd<std::unique_ptr<AMLObject>>
        read_def_field();

        [[nodiscard]] StatusAnd<std::unique_ptr<Method>>
        read_def_method();

        [[nodiscard]] StatusAnd<std::unique_ptr<MutexDefinition>>
        readDefMutex();

        [[nodiscard]] StatusAnd<std::unique_ptr<AMLObject>>
        read_def_op_region();

        [[nodiscard]] StatusAnd<std::unique_ptr<Package>>
        readDefPackage();

        [[nodiscard]] StatusAnd<std::unique_ptr<AMLObject>>
        readDefProcessor();

        [[nodiscard]] StatusAnd<std::unique_ptr<AMLObject>>
        read_def_scope();

//        [[nodiscard]] StatusAnd<
//        read_def_store();

        [[nodiscard]] StatusAnd<std::unique_ptr<Expression>>
        readExpression(bool silentOnFailure);

        [[nodiscard]] StatusAnd<std::unique_ptr<Expression>>
        readExpressionDerefOf();

        [[nodiscard]] StatusAnd<std::unique_ptr<Expression>>
        readExpressionIndex();

        [[nodiscard]] StatusAnd<std::unique_ptr<Expression>>
        readExpressionWithDefOpPrefix();

        [[nodiscard]] StatusAnd<std::unique_ptr<AMLObject>>
        read_field_element();

        [[nodiscard]] Status
        read_field_list(PkgLength, Field &parent);

        [[nodiscard]] StatusAnd<std::unique_ptr<Statement>>
        readIfElseStatement();

        [[nodiscard]] StatusAnd<std::unique_ptr<Expression>>
        readMethodInvocationExpression();

        [[nodiscard]] Status
        read_name_seg(core::View<char, 4>);

        [[nodiscard]] StatusAnd<std::unique_ptr<AMLObject>>
        readNamedObject();

        template<typename T>
        [[nodiscard]] StatusAnd<std::unique_ptr<AMLObject>>
        readNamedObjectCreateIntegerField();

        [[nodiscard]] StatusAnd<std::unique_ptr<AMLObject>>
        readNameSpaceModifierObj(OpCode);

        [[nodiscard]] StatusAnd<std::string>
        read_name_string();

        template <OperandOperandOperationType type>
        [[nodiscard]] StatusAnd<std::pair<std::unique_ptr<AMLObject>, std::unique_ptr<AMLObject>>>
        read_operand_operand_operation();

        template <typename ExpressionType>
        [[nodiscard]] StatusAnd<std::unique_ptr<Expression>>
        read_operand_operand_target_operation();

        template <typename ExpressionType>
        [[nodiscard]] StatusAnd<std::unique_ptr<Expression>>
        read_operand_target_operation();

        // A return value of `k_invalid_package_length` means error.
        [[nodiscard]] PkgLength
        read_package_length();

        // ReferenceTypeOpCode
        [[nodiscard]] StatusAnd<std::unique_ptr<AMLObject>>
        readReference();

        [[nodiscard]] StatusAnd<std::unique_ptr<Statement>>
        readStatement();

        // Not to be confused with the more common string: NameString
        [[nodiscard]] StatusAnd<std::unique_ptr<AMLObject>>
        readString();

        [[nodiscard]] StatusAnd<std::unique_ptr<AMLObject>>
        read_super_name();

        [[nodiscard]] StatusAnd<std::unique_ptr<AMLObject>>
        read_target();

        [[nodiscard]] StatusAnd<std::unique_ptr<AMLObject>>
        read_term_arg();

        [[nodiscard]] Status
        read_term_list(PkgLength, AMLScope &scope);

        template <typename ReturnObjectType = AMLObject, typename T>
        [[nodiscard]] StatusAnd<std::unique_ptr<ReturnObjectType>>
        readTermListAndReturn(std::unique_ptr<T> &&object, PkgLength);

        [[nodiscard]] StatusAnd<std::unique_ptr<AMLObject>>
        read_term_object();

        [[nodiscard]] StatusAnd<std::unique_ptr<Statement>>
        readWhileStatement();

        const core::RegionPointer<const std::uint8_t> m_data;
        decltype(m_data)::ConstIterator m_iter;

        struct PkgLengthEntry {
            std::size_t begin{0x6969696969696969};
            PkgLength size{0x42042042};
            std::string_view origin;
        };

        friend struct ScopedScope;
        friend struct ScopedPkgLength;

        core::StackVector<PkgLengthEntry, kMaxRecursionDepth> m_pkgLengths{};

        RootObject m_rootObject{};
        AMLScope *m_currentScope{&m_rootObject};
    };

    [[nodiscard]] inline constexpr std::string_view
    to_string(Parser::Status status) {
        switch (status) {
#define REGISTER_STATUS(status) case Parser::Status::status: return #status;
            ACPI_AML_PARSER_ITERATE_STATUSES(REGISTER_STATUS)
#undef REGISTER_STATUS
        }

        return "(invalid)";
    }

} // namespace kernel::acpi::aml