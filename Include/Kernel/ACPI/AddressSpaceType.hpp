#pragma once

#include <cstdint>

namespace kernel::acpi {

    //
    // Address Space ID type of the Generic Address Structure (GAS)
    //
    // ACPI 6.3
    //   Table 5-25
    //   Page 115
    //
    enum class AddressSpaceType : std::uint8_t {
        SYSTEM_MEMORY_SPACE = 0x00,
        SYSTEM_IO_SPACE = 0x01,
        PCI_CONFIGURATION_SPACE = 0x02,
        EMBEDDED_CONTROLLER = 0x03,
        SMBUS = 0x04,
        SYSTEM_CMOS = 0x05,
        PCI_BAR_TARGET = 0x06,
        IPMI = 0x07,
        GENERAL_PURPOSE_IO = 0x08,
        GENERIC_SERIAL_BUS = 0x09,
        PLATFORM_COMMUNICATIONS_CHANNEL = 0x0A,

        RESERVED_0B [[clang::unavailable("APCI Reserved")]] = 0x0B,
        // ...
        RESERVED_7E [[clang::unavailable("APCI Reserved")]] = 0x7E,

        FUNCTIONAL_FIXED_HARDWARE = 0x7F,

        RESERVED_80 [[clang::unavailable("APCI Reserved")]] = 0x80,
        // ...
        RESERVED_BF [[clang::unavailable("APCI Reserved")]] = 0xBF,

        OEM_DEFINED_C0 = 0xC0,
        // ...
        OEM_DEFINED_FF = 0xFF,
    };

} // namespace kernel::acpi
