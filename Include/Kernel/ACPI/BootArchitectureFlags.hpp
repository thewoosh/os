#pragma once

namespace kernel::acpi {

    //
    // IA-PC Boot Architecture Flags
    // ACPI 6.4 Section 5.2.9.3.
    // https://uefi.org/specs/ACPI/6.4/05_ACPI_Software_Programming_Model/ACPI_Software_Programming_Model.html#ia-pc-boot-architecture-flags
    //
    struct [[gnu::packed]] IAPCBootArchitectureFlags {
        bool legacyDevices : 1;
        bool has8042PS2Controller : 1;
        bool vgaNotPresent : 1;
        bool msiNotSupported : 1;
        bool pcieASPMControlers : 1;
        bool cmosRTCNotPresent : 1;
        [[clang::unavailable("Reserved")]] unsigned int reserved : 10;
    };
    static_assert(sizeof(IAPCBootArchitectureFlags) == 2);

    struct [[gnu::packed]] ARMArchitectureBootFlags {
        bool psciCompliant : 1;
        bool psciUseVHC : 1;
        [[clang::unavailable("Reserved")]] unsigned int reserved : 14;
    };
    static_assert(sizeof(ARMArchitectureBootFlags) == 2);

    // TODO: Change when targeting ARM
    using BootArchitectureFlags = IAPCBootArchitectureFlags;
    static_assert(sizeof(BootArchitectureFlags) == 2);

} // namespace kernel::acpi
