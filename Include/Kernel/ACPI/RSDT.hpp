#pragma once

#include "SDTHeader.hpp"

#include <cstdint>

namespace kernel::acpi {

    struct [[gnu::packed]] RSDT {
        SDTHeader header;
        std::uint32_t sub_sdts;
    };

} // namespace kernel::acpi