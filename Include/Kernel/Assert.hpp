/*
 * Assert: a check only in debug builds, expression NOT evaluated in release context
 * Verify: a check only in debug builds, expression evaluated in release context
 * Check: a check in every type of build
 */

#pragma once

#include <string_view>

namespace kernel::assert {

    void
    do_assert(std::string_view type_of_check, std::string_view file, std::size_t line, std::string_view function_name,
              std::string_view expression);

} // namespace kernel::assert

#define __KERNEL__ASSERT_PARAMETERS __FILE__, __LINE__, __PRETTY_FUNCTION__
#define __KERNEL__ASSERT_PARAMETERS_EXPR(expr) __KERNEL__ASSERT_PARAMETERS, #expr

#define CHECK(expr) \
        { \
            [[unlikely]] if (!(expr)) { \
                ::kernel::assert::do_assert("CHECK", __KERNEL__ASSERT_PARAMETERS_EXPR(expr)); \
            } \
        } \
        static_cast<void>(0)

#define CHECK_EQ(left, right) \
        { \
            [[unlikely]] if ((left) != (right)) { \
                klog<LogLevel::ERROR>("CHECK_EQ") << (left) << " != " << (right); \
                ::kernel::assert::do_assert("CHECK_EQ", __KERNEL__ASSERT_PARAMETERS_EXPR((left) != (right))); \
            } \
        } \
        static_cast<void>(0)

#define CHECK_NOT_REACHED() ::kernel::assert::do_assert("CHECK_NOT_REACHED", __KERNEL__ASSERT_PARAMETERS, "(none)")

#ifdef NDEBUG

#define ASSERT(expr) static_cast<void>(0)
#define VERIFY(expr) static_cast<void>(expr)
#define ASSERT_NOT_REACHED() static_cast<void>(0)

#else

#define ASSERT(expr) \
        { \
            [[unlikely]] if (!(expr)) { \
                ::kernel::assert::do_assert("ASSERT", __KERNEL__ASSERT_PARAMETERS_EXPR(expr)); \
            } \
        } \
        static_cast<void>(0)

#define VERIFY(expr) \
        { \
            [[unlikely]] if (!(expr)) { \
                ::kernel::assert::do_assert("VERIFY", __KERNEL__ASSERT_PARAMETERS_EXPR(expr)); \
            } \
        } \
        static_cast<void>(0)

#define ASSERT_NOT_REACHED() ::kernel::assert::do_assert("assert not reached", __KERNEL__ASSERT_PARAMETERS, "(none)")

#endif