/**
 * Copyright (C) 2021 Tristan Gerritsen <tristan-legal@outlook.com>
 * All Rights Reserved
 *
 * SPDX-License-Identifier: BSD-3-Clause
 */

#pragma once

#include <cstdint>

namespace kernel::io_ports {

    inline constexpr const std::uint16_t kIOWait = 0x80;

} // namespace kernel::io_ports

[[nodiscard]] inline std::uint8_t
inb(std::uint16_t portid) {
    uint8_t ret;
    asm volatile("inb %%dx, %%al":"=a"(ret):"d"(portid));
    return ret;
}

[[nodiscard]] inline std::uint16_t
inw(std::uint16_t portid) {
    uint16_t ret;
    asm volatile("inw %%dx, %%ax":"=a"(ret):"d"(portid));
    return ret;
}

[[nodiscard]] inline std::uint32_t
inl(std::uint16_t portid) {
    std::uint32_t ret;
    asm volatile("inl %%dx, %%eax":"=a"(ret):"d"(portid));
    return ret;
}

inline void
outb(std::uint16_t portid, std::uint8_t value) {
    asm volatile("outb %%al, %%dx": :"d" (portid), "a" (value));
}

inline void
outw(std::uint16_t portid, std::uint16_t value) noexcept {
    asm volatile("outw %%ax, %%dx": :"d" (portid), "a" (value));
}

inline void
outl(std::uint16_t portid, std::uint32_t value) {
    asm volatile("outl %%eax, %%dx": :"d" (portid), "a" (value));
}

inline void
ioWait() {
    outb(kernel::io_ports::kIOWait, 0x00);
}
