#pragma once

#include <Core/Pointer.hpp>

#include <cstdint>

#include "Kernel/Assert.hpp"

namespace kernel {

    // One IOAPIC can only 'access' 16 CPUs.
    class IOAPIC {
    public:
        struct [[gnu::packed]] Version {
            std::uint8_t version;
            std::uint8_t reserved0;
            std::uint8_t maxRedirectionEntry;
            std::uint8_t reserved1;
        };
        static_assert(sizeof(Version) == 4);

        struct [[gnu::packed]] RedirectionEntry {
            enum class DeliveryMode {
                FIXED, LOWEST_PRIORITY, SMI, NMI, INIT, EXTINT
            };

            enum class DestinationMode {
                PHYSICAL, LOGICAL
            };

            enum class DeliveryStatus {
                WAITING, SENT
            };

            enum class PinPolarity {
                ACTIVE_HIGH, ACTIVE_LOW
            };

            enum class TriggerMode {
                EDGE, LEVEL
            };

            std::uint8_t vector;
            DeliveryMode deliveryMode : 3;
            DestinationMode destinationMode : 1;
            DeliveryStatus deliveryStatus : 1;
            PinPolarity pinPolarity : 1;
            bool remoteIRR : 1;
            TriggerMode triggerMode : 1;
            bool mask : 1;

            std::uint64_t reserved : 39;

            // On which APIC ID of the CPU the interrupt should be received.
            std::uint8_t destination;
        };

        static_assert(sizeof(RedirectionEntry) == 8);

        [[nodiscard]] inline
        IOAPIC(volatile std::uint32_t *pointer, std::uint8_t id, std::uint32_t globalSystemInterruptBase)
                : m_selectorRegister(pointer)
                , m_dataRegister(core::advance_pointer_with_byte_offset(pointer, 0x10))
                , m_id(id)
                , m_globalSystemInterruptBase(globalSystemInterruptBase)
                , m_supportedInterruptCount(version().maxRedirectionEntry + 1) {
        }

        [[nodiscard]] inline std::uint32_t
        id() const {
            return *atOffset(0x00);
        }

        [[nodiscard]] inline Version
        version() const {
            union {
                std::uint32_t raw;
                Version result;
            } data{.raw = *atOffset(0x01)};
            return data.result;
        }

        [[nodiscard]] inline RedirectionEntry
        redirectionEntry(std::uint8_t index) {
            ASSERT(index >= m_globalSystemInterruptBase);
            ASSERT(index < m_supportedInterruptCount);
            union {
                struct [[gnu::packed]] {
                    std::uint32_t a;
                    std::uint32_t b;
                } raw;
                RedirectionEntry result;
            } data{
                .raw = {
                    .a = *atOffset(0x10 + index * 2),
                    .b = *atOffset(0x10 + index * 2 + 1)
                }
            };
            return data.result;
        }

        inline void
        setRedirectionEntry(std::uint8_t index, RedirectionEntry redirectionEntry) {
            ASSERT(index >= m_globalSystemInterruptBase);
            ASSERT(index < m_supportedInterruptCount);
            union {
                struct [[gnu::packed]] {
                    std::uint32_t a;
                    std::uint32_t b;
                } raw;
                RedirectionEntry result;
            } data{.result = redirectionEntry};
            *atOffset(0x10 + index * 2) = data.raw.a;
            *atOffset(0x10 + index * 2 + 1) = data.raw.b;
        }

    private:
        [[nodiscard]] inline volatile std::uint32_t *
        atOffset(std::uint32_t offset) const {
            *m_selectorRegister = offset;
            return m_dataRegister;
        }

        volatile std::uint32_t *m_selectorRegister;
        volatile std::uint32_t *m_dataRegister;
        [[maybe_unused]] std::uint8_t m_id;
        [[maybe_unused]] std::uint32_t m_globalSystemInterruptBase;
        [[maybe_unused]] std::uint8_t m_supportedInterruptCount{};
    };

} // namespace kernel
