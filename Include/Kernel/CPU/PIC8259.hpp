/*
 * Intel 8259 PIC Controller.
 *
 * https://wiki.osdev.org/8259_PIC
 */

#pragma once

#include <cstdint>

namespace kernel {

    enum class PIC8259Command : std::uint8_t;

    class PIC8259 {
        enum class Controller {
            PIC1,
            PIC2,
        };

    public:
        void
        disable();

        void
        initialize(std::uint8_t offset1, std::uint8_t offset2);

        void
        maskClear(std::uint8_t irqLine);

        void
        maskSet(std::uint8_t irqLine);

        // Let the PIC know that the interrupt was handled.
        void
        sendEndOfInterrupt(std::uint8_t irq);

    private:
        void
        sendCommand(Controller, PIC8259Command);

        bool m_enabled{false};
    };

} // namespace kernel
