#pragma once

#include <cstdint>

namespace kernel {

    struct [[gnu::packed]] LocalVectorTableRegister {
        std::uint8_t vector;
        std::uint8_t special : 3;
        [[maybe_unused]] bool reserved0 : 1;
        bool interruptPending : 1;
        bool polarityLowTriggered : 1;
        bool remoteIRR : 1;
        bool isTriggered : 1;
        bool mask : 1;
        [[maybe_unused]] std::uint16_t reserved1 : 15;
    };
    static_assert(sizeof(LocalVectorTableRegister) == 4);

    class LocalAPIC {
    public:
        [[nodiscard]] inline constexpr explicit
        LocalAPIC(volatile std::uint32_t *pointer)
                : m_pointer(pointer) {
        }

        [[nodiscard]] inline std::uint32_t
        id() const {
            return *atOffset(0x20);
        }

        [[nodiscard]] inline std::uint32_t
        version() const {
            return *atOffset(0x30);
        }

        [[nodiscard]] inline std::uint32_t
        spuriousInterruptVector() const {
            return *atOffset(0xF0);
        }

        inline void
        setSpuriousInterruptVector(std::uint32_t value) const {
            *atOffset(0xF0) = value;
        }

        [[nodiscard]] inline LocalVectorTableRegister
        timer() const {
            return getLVTRegisterAtOffset(0x320);
        }

        inline void
        setTimer(LocalVectorTableRegister reg) {
            setLVTRegisterAtOffset(0x320, reg);
        }

        [[nodiscard]] inline LocalVectorTableRegister
        lint0() const {
            return getLVTRegisterAtOffset(0x350);
        }

        inline void
        setLINT0(LocalVectorTableRegister reg) {
            setLVTRegisterAtOffset(0x350, reg);
        }

        [[nodiscard]] inline LocalVectorTableRegister
        lint1() const {
            return getLVTRegisterAtOffset(0x360);
        }

        inline void
        setLINT1(LocalVectorTableRegister reg) {
            setLVTRegisterAtOffset(0x360, reg);
        }

    private:
        [[nodiscard]] inline LocalVectorTableRegister
        getLVTRegisterAtOffset(std::size_t offset) const {
            union {
                std::uint32_t raw;
                LocalVectorTableRegister result;
            } data{.raw = *atOffset(offset)};
            return data.result;
        }

        inline void
        setLVTRegisterAtOffset(std::size_t offset, LocalVectorTableRegister reg) {
            union {
                std::uint32_t raw;
                LocalVectorTableRegister input;
            } data{.input = reg};
            *atOffset(offset) = data.raw;
        }

        [[nodiscard]] inline volatile std::uint32_t *
        atOffset(std::size_t offset) const {
            union {
                volatile std::uint32_t *pointer;
                std::size_t raw;
            } data{.pointer = m_pointer};
            data.raw += offset;
            return data.pointer;
        }

        volatile std::uint32_t *m_pointer;
    };

} // namespace kernel
