#pragma once

#include <cstdint>

namespace Kernel {

    void LoadInterruptDescriptorTable() noexcept;

} // namespace Kernel

namespace kernel::cpu {

    inline void
    native_flush_tlb_single(unsigned long addr) {
        asm volatile("invlpg (%0)" ::"r" (addr) : "memory");
    }

    [[nodiscard]] [[gnu::always_inline]] inline unsigned int
    read_cr3() {
        unsigned int res;
        asm("mov %%cr3, %%rax" : "=a" (res) :);
        return res;
    }

    template<typename ResultType = std::uint64_t>
    [[nodiscard]] [[gnu::always_inline]] inline ResultType
    read_rbp() {
        if constexpr (sizeof(ResultType) == 8) {
            ResultType res;
            asm("mov %%rbp, %%rax" : "=a" (res) :);
            return res;
        }

        std::uint64_t res;
        asm("mov %%rbp, %%rax" : "=a" (res) :);
        return reinterpret_cast<ResultType>(res);
    }

    [[gnu::always_inline]] inline void
    write_cr3(unsigned int value) {
        asm("mov %%rax, %%cr3\n\t" : : "A" (value));
    }

    enum class MSR
            : std::uint32_t {
        APIC_BASE = 0x0000001B,
    };

    struct MSRData {
        std::uint64_t rax;
        std::uint64_t rdx;
    };

    [[nodiscard]] [[gnu::always_inline]] inline MSRData
    readMSR(MSR msr) {
        MSRData data;
        asm volatile("rdmsr" : "=a"(data.rax), "=d"(data.rdx) : "c"(static_cast<std::uint32_t>(msr)));
        return data;
    }

    [[gnu::always_inline]] inline void
    writeMSR(MSR msr, MSRData data) {
        asm volatile("wrmsr" : : "a"(data.rax), "d"(data.rdx), "c"(static_cast<std::uint32_t>(msr)));
    }

} // namespace kernel::cpu
