#pragma once

#include <Core/StackVector.hpp>

#include <cstddef>
#include <cstdint>

namespace kernel {

    inline constexpr const std::size_t kMaxCores = 64;

    namespace acpi {
        struct MADT;
        struct IOAPICStructure;
        struct ProcessorLocalAPICStructure;
        struct LocalAPICAddressOverride;
    } // namespace acpi

    class APIC {
        struct ProcessorCore {
            const acpi::ProcessorLocalAPICStructure *localAPICStructure;
        };

    public:
        [[nodiscard]] bool
        initialize(const acpi::MADT *);

    private:
        [[nodiscard]] bool
        parseMADT(const acpi::MADT *);

        [[nodiscard]] bool
        parseMADTIOAPIC(const acpi::IOAPICStructure *);

        [[nodiscard]] bool
        parseMADTProcessorLocalAPIC(const acpi::ProcessorLocalAPICStructure *);

        [[nodiscard]] std::uint32_t
        ioAPICRead(std::uint8_t reg);

        void
        ioAPICWrite(std::uint8_t reg, std::uint32_t value);

        void
        setupIOAPIC();

        core::StackVector<ProcessorCore, kMaxCores> m_coreAPICData{};
        const acpi::IOAPICStructure *m_ioAPICStructure{nullptr};
//        const acpi::LocalAPICAddressOverride *m_localAPICAddressOverride{nullptr};
    };

} // namespace kernel
