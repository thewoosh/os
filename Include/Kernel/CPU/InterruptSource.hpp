#pragma once

namespace kernel {

    enum class InterruptSource {
        PIC, APIC, IO_APIC,
    };

} // namespace kernel
