/*
 * Address Resolution Protocol (ARP)
 *
 * RFC 5494 "IANA Allocation Guidelines for the Address Resolution Protocol (ARP)"
 *      https://datatracker.ietf.org/doc/html/rfc5494
 *
 * IANA ARP Parameters
 *      https://www.iana.org/assignments/arp-parameters/arp-parameters.xhtml
 */

#pragma once

#include <cstdint>

namespace kernel::arp {

#define ARP_ITERATE_HARDWARE_TYPES_LOW(REGISTER_TYPE) \
        REGISTER_TYPE(RESERVED, 0) \
        REGISTER_TYPE(ETHERNET, 1) \
        REGISTER_TYPE(EXPERIMENTAL_ETHERNET, 2) \
        REGISTER_TYPE(AMATEUR_RADIO_AX25, 3) \
        REGISTER_TYPE(PROTEON_PRONET_TOKENRING, 4) \
        REGISTER_TYPE(CHAOS, 5) \
        REGISTER_TYPE(IEEE802_NETWORKS, 6) \
        REGISTER_TYPE(ARCNET, 7) \
        REGISTER_TYPE(HYPERCHANNEL, 8) \
        REGISTER_TYPE(LANSTAR, 9) \
        REGISTER_TYPE(AUTONET_SHORT_ADDRESS, 10) \
        REGISTER_TYPE(LOCAL_TALK, 11) \
        REGISTER_TYPE(LOCAL_NET, 12) \
        REGISTER_TYPE(ULTRA_LINK, 13) \
        REGISTER_TYPE(SMDS, 14) \
        REGISTER_TYPE(FRAME_RELAY, 15) \
        REGISTER_TYPE(ASYNCHRONOUS_TRANSMISSION_MODE_1, 16) \
        REGISTER_TYPE(HDLC, 17) \
        REGISTER_TYPE(FIBRE_CHANNEL, 18) \
        REGISTER_TYPE(ASYNCHRONOUS_TRANSMISSION_MODE_2, 19) \
        REGISTER_TYPE(SERIAL_LINE, 20) \
        REGISTER_TYPE(ASYNCHRONOUS_TRANSMISSION_MODE_3, 21) \
        REGISTER_TYPE(MIL_STD_188_220, 22) \
        REGISTER_TYPE(METRICOM, 23) \
        REGISTER_TYPE(IEEE_1394_1995, 24) \
        REGISTER_TYPE(MAPOS, 25) \
        REGISTER_TYPE(TWINAXIAL, 26) \
        REGISTER_TYPE(EUI_64, 27) \
        REGISTER_TYPE(HIPARP, 28) \
        REGISTER_TYPE(IP_AND_ARP_OVER_7816_3, 29) \
        REGISTER_TYPE(ARPSEC, 30) \
        REGISTER_TYPE(IPSEC_TUNNEL, 31) \
        REGISTER_TYPE(INFINIBAND, 32) \
        REGISTER_TYPE(TIA_102_PROJECT25_COMMON_AIR_INTERFACE, 33) \
        REGISTER_TYPE(WIEGAND_INTERFACE, 34) \
        REGISTER_TYPE(PURE_IP, 35) \
        REGISTER_TYPE(HW_EXP1, 36) \
        REGISTER_TYPE(HFI, 37) \

#define ARP_ITERATE_HARDWARE_TYPES_HIGH(REGISTER_TYPE) \
        REGISTER_TYPE(AETHERNET, 257) \

    enum class HardwareTypeShort
            : std::uint8_t {
#define REGISTER_TYPE(enumeration, id) enumeration = id,
        ARP_ITERATE_HARDWARE_TYPES_LOW(REGISTER_TYPE)
#undef REGISTER_TYPE
    };

    enum class HardwareTypeLong
            : std::uint16_t {
#define REGISTER_TYPE(enumeration, id) enumeration = id,
        ARP_ITERATE_HARDWARE_TYPES_LOW(REGISTER_TYPE)
        ARP_ITERATE_HARDWARE_TYPES_HIGH(REGISTER_TYPE)
#undef REGISTER_TYPE
    };

} // namespace kernel::arp
