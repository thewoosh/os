/*
 * Dynamic Host Configuration Protocol
 *
 * RFC 2131 "Dynamic Host Configuration Protocol"
 *      https://datatracker.ietf.org/doc/html/rfc2131
 *
 * RFC 2132 ""
 *      https://www.rfc-editor.org/rfc/rfc2132.html
 *
 * IANA DHCP Parameters
 *      https://www.iana.org/assignments/bootp-dhcp-parameters/bootp-dhcp-parameters.xhtml
 */

#pragma once

#include "Kernel/Network/DHCP/MessageType.hpp"

namespace kernel::dhcp {

    enum class OptionType {
#define DHCP_ITERATE_OPTION_TYPES(REGISTER_TYPE)\
        REGISTER_TYPE(PAD, 0) \
        REGISTER_TYPE(END, 255) \

#define REGISTER_TYPE(enumeration, id) enumeration = id,
        DHCP_ITERATE_OPTION_TYPES(REGISTER_TYPE)
#undef REGISTER_TYPE
    };

    struct Option {
        [[nodiscard]] inline constexpr explicit
        Option(OptionType type)
                : m_type(type) {
        }

        [[nodiscard]] inline constexpr OptionType
        type() const {
            return m_type;
        }

    private:
        OptionType m_type;
    };

    struct PadOption : public Option {
        [[nodiscard]] inline constexpr
        PadOption() : Option(OptionType::PAD) {}
    };

    struct MessageTypeOption : public Option {
        [[nodiscard]] inline constexpr explicit
        MessageTypeOption(MessageType value)
                : Option(OptionType::PAD)
                , m_value(value) {
        }

        [[nodiscard]] inline constexpr MessageType
        value() const {
            return m_value;
        }

    private:
        MessageType m_value;
    };

    struct EndOption : public Option {
        [[nodiscard]] inline constexpr
        EndOption() : Option(OptionType::END) {}
    };

} // namespace kernel::dhcp
