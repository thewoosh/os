/*
 * Dynamic Host Configuration Protocol
 *
 * RFC 2131 "BOOTSTRAP PROTOCOL (BOOTP)"
 *      https://datatracker.ietf.org/doc/html/rfc951
 */

#pragma once

#include <cstdint>

namespace kernel::dhcp {

    enum class OpCode : std::uint8_t {
        REQUEST = 1,
        REPLY = 2,
    };

} // namespace kernel::dhcp
