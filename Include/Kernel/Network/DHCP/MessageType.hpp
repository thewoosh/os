/*
 * Dynamic Host Configuration Protocol
 *
 * RFC 2131 "Dynamic Host Configuration Protocol"
 *      https://datatracker.ietf.org/doc/html/rfc2131
 *
 * IANA DHCP Message Type
 *      https://www.iana.org/assignments/bootp-dhcp-parameters/bootp-dhcp-parameters.xhtml#message-type-53
 */

#pragma once

#include <cstdint>

namespace kernel::dhcp {

    enum class MessageType
            : std::uint8_t {
        DISCOVER = 1,
        OFFER = 2,
        REQUEST = 3,
        DECLINE = 4,
        ACK = 5,
        NAK = 6,
        RELEASE = 7,
        INFORM = 8,
        FORCERENEW = 9,
        LEASEQUERY = 10,
        LEASEUNASSIGNED = 11,
        LEASEUNKNOWN = 12,
        LEASEACTIVE = 13,
        BULKLEASEQUERY = 14,
        LEASEQUERYDONE = 15,
        ACTIVELEASEQUERY = 16,
        DHCPLEASEQUERYSTATUS = 17,
        TLS = 18,
    };

} // namespace kernel::dhcp
