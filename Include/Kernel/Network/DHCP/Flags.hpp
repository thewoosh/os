#pragma once

#include <cstdint>

namespace kernel::dhcp {

    struct [[gnu::packed]] Flags {
        bool broadcast : 1;
        std::uint16_t mustBeZero : 15;
    };

} // namespace kernel::dhcp
