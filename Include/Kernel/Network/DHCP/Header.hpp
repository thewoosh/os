/*
 * Dynamic Host Configuration Protocol
 *
 * RFC 2131 "Dynamic Host Configuration Protocol"
 *      https://datatracker.ietf.org/doc/html/rfc2131
 *
 * IANA DHCP Parameters
 *      https://www.iana.org/assignments/bootp-dhcp-parameters/bootp-dhcp-parameters.xhtml
 */

#pragma once

#include <array>

#include <cstdint>

#include "Kernel/Network/DHCP/Flags.hpp"
#include "Kernel/Network/DHCP/OpCode.hpp"
#include "Kernel/Network/IP/v4/Address.hpp"

namespace kernel::arp { enum class HardwareTypeShort : std::uint8_t; }

namespace kernel::dhcp {

    struct [[gnu::packed]] Header {
        OpCode opCode;
        arp::HardwareTypeShort hardwareAddressType;

        // 6 for ethernet (MAC)
        std::uint8_t hardwareAddressLength;

        // zero for clients
        std::uint8_t hops;

        // randomly chosen by client
        std::uint32_t transactionID;

        // The seconds elapsed since client began address acquisition or renewal
        // process.
        std::uint16_t secondsSinceProcessBegin;

        Flags flags;

        // Only filled in BOUND, RENEW or REBINDING state
        ip::v4::IPAddress clientIPAddress;

        // ???
        ip::v4::IPAddress yourIPAddress;

        // Returned in OFFER and ACK messages
        ip::v4::IPAddress serverIPAddress;

        ip::v4::IPAddress relayAgentIPAddress;

        std::array<std::uint8_t, 16> clientHardwareAddress;

        std::array<char, 64> serverHostName;
        std::array<char, 128> bootFileName;
    };

    static_assert(sizeof(Header) == 236);

} // namespace kernel::dhcp
