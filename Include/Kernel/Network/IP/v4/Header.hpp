#pragma once

#include <cstdint>

#include <Core/StackVector.hpp>

#include "Kernel/Network/IP/DifferentialServices.hpp"
#include "Kernel/Network/IP/IPProtocol.hpp"
#include "Kernel/Network/IP/v4/Address.hpp"

namespace kernel::ip::v4 {

    struct [[gnu::packed]] Header {
        std::uint8_t version : 4;
        std::uint8_t internetHeaderLength : 4;
        DifferentialServices differentialServices;
        std::uint16_t totalLength;
        std::uint16_t identification;
        std::uint8_t flags : 3;
        unsigned int fragmentOffset : 13;
        std::uint8_t timeToLive;
        IPProtocol protocol;
        std::uint16_t headerChecksum;
        IPAddress sourceAddress;
        IPAddress destinationAddress;

        inline void
        calculateChecksum([[maybe_unused]]

        const std::uint8_t *data, [[maybe_unused]] std::size_t length) {
//            std::size_t sum{};
//
//            for (std::size_t i = 0; i < sizeof(Header); ++i) {
//                memcpy
//            }
//
//            while (count > 1)  {
//                /*  This is the inner loop */
//                sum += * (unsigned short) addr++;
//                count -= 2;
//            }
//
//            /*  Add left-over byte, if any */
//            if( count > 0 )
//                sum += * (unsigned char *) addr;
//
//            /*  Fold 32-bit sum to 16 bits */
//            while (sum>>16)
//                sum = (sum & 0xffff) + (sum >> 16);
//
//            checksum = ~sum;
        }
    };

    static_assert(sizeof(Header) == 20);

} // namespace kernel::ip::v4
