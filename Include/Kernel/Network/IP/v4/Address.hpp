#pragma once

#include <array>

#include <cstdint>

#include "Include/Core/Endianness.hpp"

class TextModeBufferViewWriter;

namespace kernel::ip::v4 {

    struct IPAddress {
        using DataType = std::array<std::uint8_t, 4>;

        [[nodiscard]] inline constexpr
        IPAddress(std::uint8_t a, std::uint8_t b, std::uint8_t c, std::uint8_t d)
                : m_data{.array = {c, d, a, b}} {
        }

        [[nodiscard]] inline static constexpr IPAddress
        fromNetwork(std::uint32_t value) {
            return IPAddress{core::endian_swap_network_and_host(value)};
        }

        [[nodiscard]] inline static constexpr IPAddress
        fromSystem(std::uint32_t value) {
            return IPAddress{core::endian_swap_network_and_host(value)};
        }

        [[nodiscard]] inline constexpr DataType
        data() const {
            auto data = m_data;
            data.raw = core::endian_swap_network_and_host(data.raw);
            return m_data.array;
        }

        [[nodiscard]] inline constexpr std::uint32_t
        raw() const {
            return m_data.raw;
        }

        template<std::uint8_t Index>
        [[nodiscard]] inline constexpr std::uint8_t
        get() const {
            static_assert(Index < 4);
            return data()[Index];
        }

    private:
        [[nodiscard]] constexpr explicit
        IPAddress(std::uint32_t value)
                : m_data{.raw = value} {
        }

        // This data is stored network-order (big endian)
        union {
            DataType array;
            std::uint32_t raw;
        } m_data;
    };

    TextModeBufferViewWriter &
    operator<<(TextModeBufferViewWriter &stream, const IPAddress &);

} // namespace kernel::ip::v4
