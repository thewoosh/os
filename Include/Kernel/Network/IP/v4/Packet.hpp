#pragma once

#include "Kernel/Network/IP/v4/Header.hpp"

namespace kernel::ip::v4 {

    template <typename SegmentType>
    struct Packet {
        Header header;
        SegmentType segment;
    };

} // namespace kernel::ip::v4
