#pragma once

namespace kernel::ip {

    enum class IPProtocol
            : unsigned char {
        HOPOPT          = 0x00,
        ICMP            = 0x01,
        IGMP            = 0x02,
        GGP             = 0x03,
        IP_IN_IP        = 0x04,
        ST              = 0x05,
        TCP             = 0x06,
        CBT             = 0x07,
        EGP             = 0x08,
        IGP             = 0x09,
        BBN_RCC_MON     = 0x0A,
        NVP_II          = 0x0B,
        PUP             = 0x0C,
        ARGUS           = 0x0D,
        EMCON           = 0x0E,
        XNET            = 0x0F,
        CHAOS           = 0x10,
        UDP             = 0x11,
        MUX             = 0x12,
        DCN_MEAS        = 0x13,
        HMP             = 0x14,
        PRM             = 0x15,
        // ...
        // https://en.wikipedia.org/wiki/List_of_IP_protocol_numbers
    };

    static_assert(sizeof(IPProtocol) == 1);

} // namespace kernel::ip
