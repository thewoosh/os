#pragma once

#include <cstdint>

#include "Kernel/Network/IP/v4/Address.hpp"

namespace kernel::ip::v4 {

    // https://datatracker.ietf.org/doc/html/rfc2474
    // https://datatracker.ietf.org/doc/html/rfc4594
    enum class DifferentialServices : unsigned char {
        NETWORK_CONTROL             = 0b00110000,
        TELEPHONY                   = 0b00101110,
        SIGNALING                   = 0b00101000,
        MULTIMEDIA_CONFERENCING1    = 0b00100010,
        MULTIMEDIA_CONFERENCING2    = 0b00100100,
        MULTIMEDIA_CONFERENCING3    = 0b00100110,
        REAL_TIME_INTERACTIVE       = 0b00100000,
        MULTIMEDIA_STREAMING1       = 0b00011010,
        MULTIMEDIA_STREAMING2       = 0b00011100,
        MULTIMEDIA_STREAMING3       = 0b00011110,
        BROADCAST_VIDEO             = 0b00011000,
        LOW_LATENCY_DATA1           = 0b00010010,
        LOW_LATENCY_DATA2           = 0b00010100,
        LOW_LATENCY_DATA3           = 0b00010110,
        OAM                         = 0b01000000,
        HIGH_THROUGHPUT_DATA1       = 0b00001010,
        HIGH_THROUGHPUT_DATA2       = 0b00001100,
        HIGH_THROUGHPUT_DATA3       = 0b00001110,
        NORMAL = 0b00000000,
        LOW_PRIORITY = 0b00001000,
    };

    static_assert(sizeof(DifferentialServices) == 1);

} // namespace kernel::ip::v4
