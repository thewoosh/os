#pragma once

#include <cstdint>

namespace kernel::udp {

    struct [[gnu::packed]] Header {
        std::uint16_t sourcePort;
        std::uint16_t destinationPort;

        // Length of the data plus the length of this header
        std::uint16_t length;

        std::uint16_t checksum;
    };

    static_assert(sizeof(Header) == 8);

} // namespace kernel::udp
