#pragma once

#include <cstdint>

namespace kernel::udp::ports {

    inline constexpr const std::uint16_t DHCPClient = 68;
    inline constexpr const std::uint16_t DHCPServer = 67;

} // namespace kernel::udp::ports
