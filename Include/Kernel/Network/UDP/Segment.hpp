#pragma once

#include "Kernel/Network/UDP/Header.hpp"

namespace kernel::udp {

    template <typename DataType>
    struct Segment {
        Header header;
        DataType *data;
    };

} // namespace kernel::udp
