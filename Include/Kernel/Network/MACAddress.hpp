#pragma once

#include <array>

#include <cstdint>

class TextModeBufferViewWriter;

namespace kernel {

    struct MACAddress {
        using DataType = std::array<std::uint8_t, 6>;

        [[nodiscard]] inline constexpr
        MACAddress() = default;

        [[nodiscard]] inline constexpr DataType &
        data() {
            return m_data;
        }

        [[nodiscard]] inline constexpr const DataType &
        data() const {
            return m_data;
        }

        [[nodiscard]] inline constexpr std::uint8_t &
        operator[](std::size_t index) {
            return m_data[index];
        }

        [[nodiscard]] inline constexpr const std::uint8_t &
        operator[](std::size_t index) const {
            return m_data[index];
        }

    private:
        DataType m_data;
    };

    TextModeBufferViewWriter &
    operator<<(TextModeBufferViewWriter &stream, const MACAddress &);

} // namespace kernel
