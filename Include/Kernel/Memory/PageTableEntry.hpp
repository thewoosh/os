// TODO license
#pragma once

#include <cstdint>

#ifdef SHOW_PRINT_OVERLOADS
#   include "../Print.hpp"
#endif

namespace kernel {

    template<typename Underlying>
    struct [[gnu::packed]] SomePageTableEntry {
        bool present : 1;
        bool read_write : 1;
        bool user_supervisor : 1;
        bool write_through : 1;
        bool cache_disabled : 1;
        bool accessed : 1;
        bool dirty : 1;
        bool larger_pages : 1;
        bool ignore1 : 1;
        std::uint8_t available : 3;
        std::uint64_t address : 52;

        inline void
        setAddress(std::uint64_t inAddress) {
            address = inAddress;
        }

        [[nodiscard]] Underlying *
        get() const {
            return reinterpret_cast<Underlying *>(address << 12);
        }
    };

    using PageTableEntry = SomePageTableEntry<void>;

    static_assert(sizeof(PageTableEntry) == 8);

#ifdef SHOW_PRINT_OVERLOADS
    inline TextModeBufferViewWriter &
    operator<<(TextModeBufferViewWriter &stream, const PageTableEntry &page_table_entry) {
#define BOOLIFY(thing) ((page_table_entry.thing) ? "true" : "false")
#define BITIFY(thing) ((thing) ? "1" : "0")
        stream << "PageTableEntry{present=" << BOOLIFY(present)
               << ", read_write=" << BOOLIFY(read_write)
               << ", user_super=" << BOOLIFY(user_supervisor)
               << ", write_through=" << BOOLIFY(write_through)
               << ", cache_disabled=" << BOOLIFY(cache_disabled)
               << ", accessed=" << BOOLIFY(accessed)
               << ", larger_pages=" << BOOLIFY(larger_pages)
               << ", available=" << BITIFY(page_table_entry.available & 0x1) << BITIFY(page_table_entry.available & 0x2) << BITIFY(page_table_entry.available & 0x4)
               << ", address=" << (static_cast<std::uint64_t>(page_table_entry.address) << 12)
               << "}";
        return stream;
    }
#endif

} // namespace kernel
