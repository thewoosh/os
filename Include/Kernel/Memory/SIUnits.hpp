#pragma once

#include <cstdint>

inline constexpr std::uint16_t KiB = 1024;
inline constexpr std::uint32_t MiB = 1024 * KiB;
inline constexpr std::uint32_t GiB = 1024 * MiB;
