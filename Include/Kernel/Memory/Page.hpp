#pragma once

#include <Core/BitSetView.hpp>

namespace kernel {

    inline constexpr const std::uint16_t kPageSize = 4096;

    template <typename T>
    [[nodiscard]] [[gnu::always_inline]] inline constexpr T
    page_floor(T value) {
        const auto val = reinterpret_cast<std::uint64_t>(value);
        return reinterpret_cast<T>(val - (val % kPageSize));
    }

    template <typename T>
    [[nodiscard]] [[gnu::always_inline]] inline constexpr T
    page_ceil(T value) {
        const auto val = reinterpret_cast<std::uint64_t>(value);
        const auto offset = val % kPageSize;
        if (offset == 0)
            return value;
        return reinterpret_cast<T>(val + kPageSize - offset);
    }

    template <typename T>
    [[nodiscard]] [[gnu::always_inline]] inline constexpr bool
    isPageAligned(T value) {
        return std::size_t(value) % kPageSize == 0;
    }

} // namespace kernel
