#pragma once

#include <Core/BitSetView.hpp>
#include <array>

#include "Kernel/Memory/Page.hpp"

namespace kernel {

    class PageFrameAllocator {
        enum class LockResult {
            OK,
            ALREADY_LOCKED,
            SOME_IN_RANGE_ALREADY_LOCKED,

            INPUT_ADDRESS_TOO_LOW,
            INPUT_ADDRESS_TOO_HIGH,
        };

        friend class MemoryManager;

    public:
        [[nodiscard]] explicit
        PageFrameAllocator(void *memory_begin, std::size_t size);

        static void
        earlyIdentityMapPage(const void *);

        [[nodiscard]] void *
        allocate_or_crash();

        [[nodiscard]] inline void *
        allocate_or_null() {
            return allocate();
        }

        void
        free_page(void *address);

        inline void
        free_pages_count(void *address, std::size_t count) {
            for (std::size_t i = 0; i != count; ++i)
                free_page(static_cast<std::uint8_t *>(address) + i * kPageSize);
        }

        inline void
        free_pages_region(void *begin, void *end) {
            for (void *ptr = begin; ptr < end; *reinterpret_cast<std::uint8_t **>(&ptr) += kPageSize)
                free_page(ptr);
        }

        inline void
        free_pages_size(void *address, std::size_t size) {
            for (std::size_t i = 0; i != size; i += kPageSize)
                free_page(static_cast<std::uint8_t *>(address) + i);
        }

        [[nodiscard]] LockResult
        lock_page(void *address);

        [[nodiscard]] inline LockResult
        lock_pages_count(void *address, std::size_t count) {
            for (std::size_t i = 0; i != count; ++i)
                if (lock_page(static_cast<std::uint8_t *>(address) + i * kPageSize) == LockResult::ALREADY_LOCKED)
                    return LockResult::SOME_IN_RANGE_ALREADY_LOCKED;
            return LockResult::OK;
        }

        [[nodiscard]] inline LockResult
        lock_pages_region(void *begin, void *end) {
            for (void *ptr = begin; ptr < end; *reinterpret_cast<std::uint8_t **>(&ptr) += kPageSize)
                if (lock_page(ptr) == LockResult::ALREADY_LOCKED)
                    return LockResult::SOME_IN_RANGE_ALREADY_LOCKED;
            return LockResult::OK;
        }

        [[nodiscard]] inline LockResult
        lock_pages_size(void *address, std::size_t size) {
            for (std::size_t i = 0; i <= size; i += kPageSize)
                if (lock_page(static_cast<std::uint8_t *>(address) + i) == LockResult::ALREADY_LOCKED)
                    return LockResult::SOME_IN_RANGE_ALREADY_LOCKED;
            return LockResult::OK;
        }

    protected:
        [[nodiscard]] inline constexpr std::uint8_t *
        chunk_begin() const {
            return m_chunk_begin;
        }

        [[nodiscard]] inline constexpr std::uint8_t *
        chunk_end() const {
            return m_chunk_end;
        }

    private:
        [[nodiscard]] void *
        allocate();

        void
        set_bitmap(std::size_t index, bool value);

        [[nodiscard]] bool
        get_bitmap(std::size_t index) const;

        [[nodiscard]] std::size_t
        bitmap_count() const;

        template <typename T>
        [[nodiscard]] [[gnu::always_inline]] inline constexpr std::size_t
        calculate_index(T *address) {
            return static_cast<std::uint64_t>(static_cast<decltype(m_chunk_begin)>(address) - m_chunk_begin) / kPageSize;
        }

#ifdef PAGE_FRAME_ALLOCATOR_HAS_BEGIN_BITSET
        std::array<std::uint8_t, 8> m_begin_bitset_buffer{};
        core::BitSetView m_begin_bitset{m_begin_bitset_buffer.data(), m_begin_bitset_buffer.size() * 8};
#endif

        core::BitSetView m_extended_bitset{nullptr, 0};
        std::size_t m_free_memory{};
        std::size_t m_used_memory{};
        std::uint8_t *m_chunk_begin{nullptr};
        std::uint8_t *m_chunk_end{nullptr};
    };

} // namespace kernel
