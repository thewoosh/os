#pragma once

#include <cstddef>

namespace kernel {

    void
    set_allocatable_memory(void *begin, std::size_t size);

} // namespace kernel

extern "C" void
calloc(std::size_t);

extern "C" void
free(void *);

[[nodiscard]] extern "C" void *
malloc(std::size_t);

extern "C" void
realloc(void *, std::size_t);
