#pragma once

#include <array>

#include "PageTableEntry.hpp"

namespace kernel {

    struct PageDirectoryTable;
    struct VirtualAddress;

    struct PageDirectoryPointerTable {
        using Type = SomePageTableEntry<PageDirectoryTable>;

        [[nodiscard]] static PageDirectoryPointerTable &
        the();

        [[nodiscard]] PageTableEntry *
        find(VirtualAddress) const;

        [[nodiscard]] inline constexpr Type &
        operator[](std::uint16_t index) {
            return m_data[index];
        }

        [[nodiscard]] inline constexpr const Type &
        operator[](std::uint16_t index) const {
            return m_data[index];
        }

    private:
        std::array<Type, 512> m_data;
    };

} // namespace kernel
