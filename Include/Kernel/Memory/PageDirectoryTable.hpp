#pragma once

#include <array>

#include "PageTableEntry.hpp"

namespace kernel {

    struct PageTable;

    struct [[gnu::packed]] PageTable {
        using Type = PageTableEntry;

        [[nodiscard]] inline constexpr Type &
        operator[](std::uint16_t index) {
            return m_data[index];
        }

        [[nodiscard]] inline constexpr const Type &
        operator[](std::uint16_t index) const {
            return m_data[index];
        }

    private:
        std::array<Type, 512> m_data;
    };

    struct [[gnu::packed]] PageDirectoryTable {
        using Type = SomePageTableEntry<PageTable>;

        [[nodiscard]] inline constexpr Type &
        operator[](std::uint16_t index) {
            return m_data[index];
        }

        [[nodiscard]] inline constexpr const Type &
        operator[](std::uint16_t index) const {
            return m_data[index];
        }

    private:
        std::array<Type, 512> m_data;
    };

} // namespace kernel
