#pragma once

#include <string_view>

#include <cstddef>

#include <Core/Pointer.hpp>

#include "Kernel/Specifications/UEFI/MemoryMap.hpp"
#include "Kernel/Specifications/Multiboot/Tags.hpp"

namespace kernel {

    struct MemoryMapRegion {
        enum class Type {
#define ITERATE_MEMORY_MAP_REGIONS(REGISTER_REGION) \
            REGISTER_REGION(UNKNOWN) /* Unknown */\
            REGISTER_REGION(RESERVED) /* Reserved */\
            \
            REGISTER_REGION(AVAILABLE) /* Free/writable memory (can be used) */\
            REGISTER_REGION(UNUSABLE ) /* Bad memory */\
            REGISTER_REGION(KERNEL_CODE) /* The region where the kernel code resides (can be used) */\
            \
            REGISTER_REGION(ACPI_NVS) /* ACPI-reserved */\
            REGISTER_REGION(ACPI_RECLAIMABLE) /* Holds ACPI-tables (can be used) */\
            \
            REGISTER_REGION(UEFI_CODE_BOOT_SERVICES) /* The code where the BootService functions reside (can be used) */\
            REGISTER_REGION(UEFI_CODE_RUNTIME_SERVICES) /* The code where the RuntimeServices reside (depends) */\
            REGISTER_REGION(UEFI_POOL_MEMORY_KERNEL) /* ? (can be used)  */\
            REGISTER_REGION(UEFI_POOL_MEMORY_BOOT_SERVICES) /* ?  (can be used) */\
            REGISTER_REGION(UEFI_POOL_MEMORY_RUNTIME_SERVICES) /* ?  (depends) */\
            \
            REGISTER_REGION(FIRMWARE) /* Other firmware code and/or data */\
            \
            REGISTER_REGION(MEMORY_MAPPED_IO) /*  */\
            REGISTER_REGION(MEMORY_MAPPED_IO_PORT_SPACE) /*  */\

#define REGISTER_REGION(enumeration) enumeration,
            ITERATE_MEMORY_MAP_REGIONS(REGISTER_REGION)
#undef REGISTER_REGION

            FIRST_MAP_MAGIC = 0x648CAFE
        };

#ifdef MEMORY_MAP_IMPLEMENTATION
        [[nodiscard]] inline constexpr
        MemoryMapRegion() = default;

        [[nodiscard]] inline constexpr
        MemoryMapRegion(Type type, void *begin, std::size_t size)
                : m_type(type), m_begin(begin), m_size(size) {
        }

#else
        MemoryMapRegion() = delete;
#endif // defined(MEMORY_MAP_IMPLEMENTATION)MEMORY_MAP_IMPLEMENTATION

        [[nodiscard]] inline constexpr Type
        type() const {
            return m_type;
        }

        [[nodiscard]] inline constexpr void *
        begin() const {
            return m_begin;
        }

        [[nodiscard]] inline constexpr void *
        end() const {
            return core::advance_pointer_with_byte_offset(m_begin, m_size);
        }

        [[nodiscard]] inline constexpr std::size_t
        size() const {
            return m_size;
        }

        [[nodiscard]] inline constexpr bool
        contains(const void *physicalAddress) const {
            return core::does_pointer_lay_within(physicalAddress, m_begin, m_size);
        }

    private:
        Type m_type;
        void *m_begin;
        std::size_t m_size;
    };

    class MemoryMapRegionManager {
    public:
        MemoryMapRegionManager() = delete;

        static void
        forEachRegion(void (const MemoryMapRegion &));

        [[nodiscard]] static MemoryMapRegion
        getRegion(const void *physicalAddress);

        static void
        initialize(const uefi::MemoryMap &);

        static void
        initialize(const multiboot2::MemoryMapTag &);
    };

    [[nodiscard]] inline constexpr std::string_view
    toString(MemoryMapRegion::Type type) {
        switch (type) {
#define REGISTER_REGION(enumeration) case MemoryMapRegion::Type::enumeration: return #enumeration;
            ITERATE_MEMORY_MAP_REGIONS(REGISTER_REGION)
#undef REGISTER_REGION
            case MemoryMapRegion::Type::FIRST_MAP_MAGIC: return "FIRST_MAP_MAGIC";
        }

        return "(invalid)";
    }

} // namespace kernel
