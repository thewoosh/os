// TODO license

#pragma once

#include <cstddef>

#include "Kernel/Memory/VirtualAddress.hpp"
#include "Kernel/Specifications/Multiboot/Forward.hpp"

namespace kernel {

    class PageFrameAllocator;
    struct VirtualAddress;
    struct PageDirectoryTable;
    struct MemoryMapRegion;

    template <typename T>
    struct SomePageTableEntry;

    class MemoryManager {
    public:
        [[nodiscard]] explicit
        MemoryManager(PageFrameAllocator *);

        inline
        ~MemoryManager() {
            s_instance = nullptr;
        }

        [[nodiscard]] inline static MemoryManager &
        the() {
            return *s_instance;
        }

        [[nodiscard]] inline static constexpr void *
        translateVirtualToPhysicalAddress(VirtualAddress virtualAddress) {
            // Everything is Identity Mapped, for now!
            return virtualAddress.address();
        }

        [[nodiscard]] static SomePageTableEntry<void> *
        find_table(VirtualAddress);

        [[nodiscard]] void *
        allocate_or_crash(std::size_t number_of_pages);

        void
        free_pages(void *begin, std::size_t number_of_consecutive_pages);

        [[nodiscard]] PageDirectoryTable *
        get_pdp(VirtualAddress);

        [[nodiscard]] PageDirectoryTable *
        get_pd(PageDirectoryTable *pdp, VirtualAddress);

        [[nodiscard]] PageDirectoryTable *
        get_pt(PageDirectoryTable *pd, VirtualAddress);

        enum MapFlags : std::size_t {
            MAP_FLAG_DEBUG = 1 << 0,
            MAP_FLAG_NON_WRITABLE = 1 << 1,
            MAP_FLAG_CLEAR_CACHE = 1 << 2,
            MAP_FLAG_CACHE_DISABLED = 1 << 3,
        };

        void
        map(VirtualAddress, void *physical_address, std::size_t flags={});

    private:
        void
        preallocate_page_tables();

        void
        prepareMap(VirtualAddress);

        PageFrameAllocator *m_pageFrameAllocator;

        static MemoryManager *s_instance;
    };

} // namespace kernel
