#pragma once

#include <cstdint>
#include <cstring>

#include <type_traits>

#include "Kernel/CPU/CPU.hpp"
#ifdef PAGE_HIERARCHY_TRAVERSER_WITH_DEBUGGING
#include "Kernel/Print.hpp"
#endif
#include "Kernel/Runtime/Crash.hpp"

#include "Page.hpp"
#include "PageTableEntry.hpp"
#include "PageDirectoryTable.hpp"
#include "PageDirectoryPointerTable.hpp"
#include "VirtualAddress.hpp"

namespace kernel::page_hierarchy_traverser {

    struct PageManagerIndexType {
        [[nodiscard]] inline constexpr explicit
        PageManagerIndexType(std::uint16_t value)
                : m_value(value) {
        }

        [[nodiscard]] inline constexpr std::uint16_t
        value() const {
            return m_value;
        }

    private:
        std::uint16_t m_value;
    };

    [[nodiscard]] inline const SomePageTableEntry<PageDirectoryTable> &
    get_pdp_by_index(std::uint16_t index) {
        return PageDirectoryPointerTable::the()[index];
    }

    [[nodiscard]] inline const SomePageTableEntry<PageDirectoryTable> &
    get_pd_by_index(std::uint16_t pdp_index, std::uint16_t index) {
        return reinterpret_cast<SomePageTableEntry<PageDirectoryTable> &>(
                get_pdp_by_index(pdp_index).get()->operator[](index));
    }

    [[nodiscard]] inline const SomePageTableEntry<PageTable> &
    get_pt_by_index(std::uint16_t pdp_index, std::uint16_t pd_index, std::uint16_t index) {
        return get_pd_by_index(pdp_index, pd_index).get()->operator[](index);
    }

    [[nodiscard]] inline const PageTableEntry &
    get_page_by_index(std::uint16_t pdp_index, std::uint16_t pd_index, std::uint16_t pt_index, std::uint16_t index) {
        return get_pt_by_index(pdp_index, pd_index, pt_index).get()->operator[](index);
    }

#if 0
    [[nodiscard]] inline PageTableEntry *
    getPageTableEntry(VirtualAddress virtualAddress) {
        klog<LogLevel::DEBUG>("GetPageTableEntry") << "Address: " << std::size_t(virtualAddress.address())
                << " PML4E=" << virtualAddress.pml4e() << " PDPE=" << virtualAddress.pdpe()
                << " PDE=" << virtualAddress.pde() << " PTE=" << virtualAddress.pte();

        auto &pdpde = PageDirectoryPointerTable::the()[virtualAddress.pml4e()];
        if (!pdpde.present || !pdpde.address) {
            klog<LogLevel::DEBUG>("GetPageTableEntry") << "PDPDE missing";
            return nullptr;
        }

        klog<LogLevel::DEBUG>("GetPageTableEntry") << "  PDPDE @ " << std::size_t(&pdpde);

        auto &pdpe = pdpde.get()->operator[](virtualAddress.pdpe());
        if (!pdpe.present || !pdpe.address) {
            klog<LogLevel::DEBUG>("GetPageTableEntry") << "PDPE missing";
            return nullptr;
        }

        klog<LogLevel::DEBUG>("GetPageTableEntry") << "  PDPE @ " << std::size_t(&pdpe);

        auto &pde = pdpe.get()->operator[](virtualAddress.pde());
        if (!pde.present || !pde.address) {
            klog<LogLevel::DEBUG>("GetPageTableEntry") << "PDE missing";
            return nullptr;
        }

        klog<LogLevel::DEBUG>("GetPageTableEntry") << "  PDE @ " << std::size_t(&pde);

        auto &pte = reinterpret_cast<decltype(pdpe)>(pde).get()->operator[](virtualAddress.pde());
        return &pte;
//        if (!pte.present) {
//            klog<LogLevel::DEBUG>("GetPageTableEntry") << "PTE missing";
//            return;
//        }
//
//        klog<LogLevel::DEBUG>("GetPageTableEntry") << "  PTE @ " << std::size_t(&pte);
//
//        klog<LogLevel::DEBUG>("GetPageTableEntry") << "Address OK!";
    }
#endif // 0

    template <typename PageAllocator>
    [[nodiscard]] inline PageDirectoryTable *
    get_pdp(VirtualAddress virtual_address, PageAllocator page_allocator) {
        auto &pde = PageDirectoryPointerTable::the()[static_cast<std::uint16_t>(virtual_address.pml4e())];
        if (!pde.address || !pde.present) {
#ifdef PAGE_HIERARCHY_TRAVERSER_WITH_DEBUGGING
            klog<LogLevel::DEBUG>("PHT") << "Requesting a page for PDP, PML4E=" << virtual_address.pml4e();
#endif // PAGE_HIERARCHY_TRAVERSER_WITH_DEBUGGING
            auto *pdp = static_cast<PageDirectoryTable *>(page_allocator());
            if (!pdp)
                return nullptr;

            pde.setAddress(reinterpret_cast<std::uint64_t>(pdp) >> 12);
            pde.present = true;
            pde.read_write = true;

            std::memset(pdp, 0, kPageSize);
            return pdp;
        }
//
//        static std::size_t count = 0;
//        if (std::size_t(virtual_address.address()) == 0xA0D000) {
//            if (++count == 12)
//                CRASH("OK!");
//        }

#ifdef PAGE_HIERARCHY_TRAVERSER_WITH_DEBUGGING
        klog<LogLevel::DEBUG>(__PRETTY_FUNCTION__) << "PML4E " << virtual_address.pml4e()
                << " PDP already found @ " << reinterpret_cast<std::uint64_t>(pde.get())
                << " for address " << std::size_t(virtual_address.address());
#endif // PAGE_HIERARCHY_TRAVERSER_WITH_DEBUGGING
        return pde.get();
    }

    template <typename PageAllocator>
    [[nodiscard]] inline PageDirectoryTable *
    get_pd(PageDirectoryTable *pdp, VirtualAddress virtual_address, PageAllocator page_allocator) {
        auto &pde = (*pdp)[static_cast<std::uint16_t>(virtual_address.pdpe())];
        if (!pde.address || !pde.present) {
#ifdef PAGE_HIERARCHY_TRAVERSER_WITH_DEBUGGING
            klog<LogLevel::DEBUG>("PHT") << "Requesting a page for PD, PDPE=" << virtual_address.pdpe() << " PML4E=" << virtual_address.pml4e();
#endif // PAGE_HIERARCHY_TRAVERSER_WITH_DEBUGGING
            auto *pd = static_cast<PageDirectoryTable *>(page_allocator());
            if (!pd)
                return nullptr;

            pde.setAddress(reinterpret_cast<std::uint64_t>(pd) >> 12);
            pde.present = true;
            pde.read_write = true;

            std::memset(pd, 0, kPageSize);
            return pd;
        }
#ifdef PAGE_HIERARCHY_TRAVERSER_WITH_DEBUGGING
        klog<LogLevel::DEBUG>(__PRETTY_FUNCTION__) << "PDPE " << virtual_address.pdpe()
                << " PD already found @ " << reinterpret_cast<std::uint64_t>(pde.get());
#endif // PAGE_HIERARCHY_TRAVERSER_WITH_DEBUGGING
        return reinterpret_cast<PageDirectoryTable *>(pde.get());
    }

    template <typename PageAllocator>
    [[nodiscard]] inline PageDirectoryTable *
    get_pt(PageDirectoryTable *pd, VirtualAddress virtual_address, PageAllocator page_allocator) {
        auto &pde = (*pd)[static_cast<std::uint16_t>(virtual_address.pde())];
        if (!pde.address || !pde.present) {
#ifdef PAGE_HIERARCHY_TRAVERSER_WITH_DEBUGGING
            klog<LogLevel::DEBUG>("PHT") << "Requesting a page for PT, PDE=" << virtual_address.pde() << " PDPE=" << virtual_address.pdpe() << " PML4E=" << virtual_address.pml4e();
#endif // PAGE_HIERARCHY_TRAVERSER_WITH_DEBUGGING

            auto *pt = static_cast<PageDirectoryTable *>(page_allocator());
            if (!pt)
                return nullptr;

#ifdef PAGE_HIERARCHY_TRAVERSER_WITH_DEBUGGING
            klog<LogLevel::DEBUG>("2022-02-11T16T18") << "Memset-ing brand new pageTable @ " << std::size_t(pt)
                    << " in pd @ " << std::size_t(pd);
#endif
            pde = {
                    .present = true,
                    .read_write = true,
            };
            pde.setAddress(reinterpret_cast<std::uint64_t>(pt) >> 12);
            pde.present = true;
            pde.read_write = true;

            //
            // TODO: This was enabled for debugging purposes, there should be
            //       no reason to do this (all the time at least), right?
            //
//            cpu::native_flush_tlb_single(std::size_t(pd));
//            cpu::native_flush_tlb_single(std::size_t(pt));
//            cpu::native_flush_tlb_single(std::size_t(&pde));
//            cpu::write_cr3(cpu::read_cr3());

#ifdef PAGE_HIERARCHY_TRAVERSER_WITH_DEBUGGING
            klog<LogLevel::DEBUG>("PHT") << "OK! Now memsetting the just allocated page (as PT) empty";
#endif // PAGE_HIERARCHY_TRAVERSER_WITH_DEBUGGING
            std::memset(pt, 0, kPageSize);
        }

#ifdef PAGE_HIERARCHY_TRAVERSER_WITH_DEBUGGING
        klog<LogLevel::DEBUG>(__PRETTY_FUNCTION__) << "PDE " << virtual_address.pde()
                                                   << " PT already found @ " << reinterpret_cast<std::uint64_t>(pde.get());
#endif // PAGE_HIERARCHY_TRAVERSER_WITH_DEBUGGING
        return reinterpret_cast<PageDirectoryTable *>(pde.get());
    }

} // namespace kernel::page_hierarchy_traverser
