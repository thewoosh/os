#pragma once

#include <cstddef>
#include <cstdint>

#include "PageTableEntry.hpp"

namespace kernel {

    struct VirtualAddress {
        bool debug{false};

        [[nodiscard]] inline static constexpr VirtualAddress
        from_indices(std::uint16_t pml4_index, std::uint16_t pdp_index, std::uint16_t pd_index, std::uint16_t pt_index,
                     std::uint16_t physical_page_offset) {
            return VirtualAddress{reinterpret_cast<void *>(
                    (std::uint64_t(pml4_index & 0x1FF) << 39) +
                    (std::uint64_t(pdp_index & 0x1FF) << 30) +
                    (std::uint64_t(pd_index & 0x1FF) << 21) +
                    (std::uint64_t(pt_index & 0x1FF) << 12) +
                    (physical_page_offset & 0xFFF)
            )};
        }

        [[gnu::always_inline]] inline constexpr
        VirtualAddress([[gnu::nonnull()]] void *address)
                : m_address_as_pointer(address) {
        }

        [[nodiscard]] [[gnu::always_inline]] inline constexpr void *
        address() const {
            return m_address_as_pointer;
        }

        [[nodiscard]] [[gnu::always_inline]] inline std::uint16_t
        physical_page_offset() const {
            return m_address_as_integer & 0xFFF;
        }

        [[nodiscard]] [[gnu::always_inline]] inline std::uint16_t
        pte() const {
            return (m_address_as_integer >> 12) & 0x1FF;
        }

        [[nodiscard]] [[gnu::always_inline]] inline std::uint16_t
        pde() const {
            return (m_address_as_integer >> 21) & 0x1FF;
        }

        [[nodiscard]] [[gnu::always_inline]] inline std::uint16_t
        pdpe() const {
            return (m_address_as_integer >> 30) & 0x1FF;
        }

        [[nodiscard]] [[gnu::always_inline]] inline constexpr std::uint16_t
        pml4e() const {
            return (m_address_as_integer >> 39) & 0x1FF;
        }

        [[nodiscard]] const PageTableEntry *
        pdp_table() const;

        [[nodiscard]] const PageTableEntry *
        pd_table() const;

        [[nodiscard]] const PageTableEntry *
        pt_table() const;

        [[nodiscard]] inline static constexpr VirtualAddress
        null() {
            return {};
        }

    private:
        [[gnu::always_inline]] inline constexpr
        VirtualAddress()
                : m_address_as_pointer(nullptr) {
        }

        union {
            void *m_address_as_pointer{};
            std::uint64_t m_address_as_integer;
        };
    };

} // namespace kernel
