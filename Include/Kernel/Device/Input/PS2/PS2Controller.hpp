#pragma once

#include <cstddef>
#include <cstdint>

namespace kernel::device::input::ps2 {

    enum class PS2Command : std::uint8_t;

    class PS2Controller {
    public:
        struct [[gnu::packed]] StatusRegister {
            bool outputBufferAvailable : 1;
            bool inputBufferAvailable : 1;
            bool systemFlag : 1;
            bool isCommandNotData : 1;
            bool vendorSpecific0 : 1;
            bool vendorSpecific1 : 1;
            bool timeoutError : 1;
            bool parityError : 1;
        };

        static_assert(sizeof(StatusRegister) == 1);

        PS2Controller();

        [[nodiscard]] inline constexpr bool
        isDualChannel() const {
            return m_isDualChannel;
        }

        [[nodiscard]] static std::uint8_t
        readPortData();

        [[nodiscard]] static StatusRegister
        readPortStatus();

        static void
        writePortData(std::uint8_t);

        template <typename...Arg>
        inline static void
        writePortCommand(PS2Command command, Arg &&...args) {
            writePortCommand(static_cast<std::uint8_t>(command));
            (writePortCommandType(args), ...);
        }

    private:
        bool m_isDualChannel{false};

        template <typename T>
        inline static void
        writePortCommandType(T type) {
            union {
                T typed;
                std::uint8_t raw;
            } data {.typed = type};
            writePortCommand(data.raw);
        }

        static void
        writePortCommand(std::uint8_t);
    };

} // namespace kernel::device::input::ps2
