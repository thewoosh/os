#pragma once

#include <cstdint>

namespace kernel::device::display::vm_svga_ii {

    inline constexpr std::uint32_t kVersionMagic = 0x90000000;

    [[nodiscard]] inline constexpr std::uint32_t
    createVersion(std::uint32_t id) {
        return kVersionMagic | id;
    }

    enum class Version
            : std::uint32_t {
        VERSION_0 = createVersion(0),
        VERSION_1 = createVersion(1),
        VERSION_2 = createVersion(2),

        INVALID = 0xFFFFFFFF
    };

} // namespace kernel::device::display::vm_svga_ii
