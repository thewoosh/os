#pragma once

#include <cstdint>

namespace kernel::pci { class AbstractDevice; }

//
// VMware SVGA II Specification
// https://sourceforge.net/p/vmware-svga/git/ci/master/tree/doc/svga_interface.txt
//
namespace kernel::device::display::vm_svga_ii {

    enum class Register : std::uint32_t;
    enum class Version : std::uint32_t;

    class Manager {
    public:
        void
        launch(pci::AbstractDevice *device);

    private:
        [[nodiscard]] bool
        determineVersion();

        [[nodiscard]] std::uint32_t
        readRegister(Register);

        void
        writeRegister(Register, std::uint32_t);

        pci::AbstractDevice *m_pciDevice{nullptr};
        std::uint16_t m_bar0{0};

        Version m_version{0xFFFFFFFF};
    };

} // namespace kernel::device::display::vm_svga_ii
