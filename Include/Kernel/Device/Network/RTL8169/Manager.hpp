#pragma once

#include <cstdint>

#include <array>

namespace kernel::pci { class AbstractDevice; }

//
// Realtek RTL8169,
// https://wiki.osdev.org/RTL8169
// http://realtek.info/pdf/rtl8169s.pdf
//
namespace kernel::device::network::rtl8169 {

    class Manager {
    public:
        void
        launch(pci::AbstractDevice *device);

    private:
        void
        getMACAddress();

        pci::AbstractDevice *m_pciDevice{nullptr};
        std::array<std::uint8_t, 6> m_macAddress{};
    };

} // namespace kernel::device::network::rtl8169
