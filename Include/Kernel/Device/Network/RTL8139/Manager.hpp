#pragma once

#include <vector>

#include <Core/RegionPointer.hpp>

#include "Kernel/Device/Status.hpp"
#include "Kernel/Network/MACAddress.hpp"

namespace kernel::pci { class AbstractDevice; }

//
// Realtek RTL8169,
// https://wiki.osdev.org/RTL8169
// http://realtek.info/pdf/rtl8169s.pdf
//
namespace kernel::device::network::rtl8139 {

    class Manager {
    public:
        void
        launch(pci::AbstractDevice *device);

        [[nodiscard]] bool
        send(core::RegionPointer<const std::uint8_t>);

    private:
        void
        configurePCI();

        void
        getMACAddress();

        void
        initializeReceiveBuffer();

        void
        reset();

        DeviceStatus m_deviceStatus{};

        pci::AbstractDevice *m_pciDevice{nullptr};
        MACAddress m_macAddress{};
        std::uint16_t m_ioAddress{};

        std::vector<std::uint8_t> m_receiveBuffer;
    };

} // namespace kernel::device::network::rtl8139
