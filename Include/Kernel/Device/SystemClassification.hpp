#pragma once

namespace kernel {

    enum class VirtualizationSoftwareType {
        // Unknown if the system is running on a hypervisor or not.
        UNKNOWN = 0,

        // The system is known to be physical.
        NONE,

        // http://bochs.sourceforge.net/
        BOCHS,

        // https://www.qemu.org/
        QEMU,

        // https://www.virtualbox.org/
        VIRTUAL_BOX,

        // https://www.vmware.com/products/workstation-player/workstation-player-evaluation.html
        VMWARE,
    };

    struct SystemClassification {
        VirtualizationSoftwareType virtualization_software{};
    };

} // namespace kernel
