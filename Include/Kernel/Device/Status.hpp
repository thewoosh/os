/*
 * Generic device status field.
 */

#pragma once

namespace kernel {

    enum class DeviceStatus {
        UNINITIALIZED,

        INITIALIZED = 0x00010000,
        INITIALIZING = 0x00010001,
        INITIALIZATION_TIMEOUT = 0x00010002,

    };

} // namespace kernel
