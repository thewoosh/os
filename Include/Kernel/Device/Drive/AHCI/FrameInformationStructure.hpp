#pragma once

#include <cstdint>

namespace kernel::device::drive::ahci {

    enum class FISType
            : std::uint8_t {
        REGISTER_HOST_TO_DEVICE = 0x27,
        REGISTER_DEVICE_TO_HOST = 0x34,
        DMA_ACTIVATE = 0x39,
        DMA_SETUP = 0x41,
        DATA = 0x46,
        BIST_ACTIVATE = 0x58,
        PIO_SETUP = 0x5F,
        SET_DEVICE_BITS = 0xA1,

        // Unrecognized/vendor-specific/reserved types:
        //   Read Serial ATA Revision 3.5a chapter 9.7
    };

    struct [[gnu::packed]] FrameInformationStructure_DATA {
        FISType type;

        std::uint8_t portMultiplierPort : 4;
        std::uint8_t reserved0 : 4;

        std::uint8_t  reserved1[2];

        // DWORD 1 ~ N
        std::uint32_t payloadBegin;	// Payload
    };

} // namespace kernel::device::drive::ahci
