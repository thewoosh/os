#pragma once

#include <cstdint>

namespace kernel::device::drive::ahci {

    // 3.3.10 Offset 28h: PxSSTS – Port x Serial ATA Status (SCR0: SStatus)
    struct [[gnu::packed]] SATAStatus {
        enum class DeviceDetection {
            NO_DEVICE_DETECTED = 0x0,
            DEVICE_PRESENT_BUT_NO_PHY_COMMUNICATION_ESTABLISHED = 0x1,
            DEVICE_PRESENT_AND_PHY_COMMUNICATION_ESTABLISHED = 0x3,
            PHY_OFFLINE = 0x4,
        };

        enum class CurrentInterfaceSpeed {
            DEVICE_NOT_PRESENT_OR_NO_COMMUNICATION_ESTABLISHED = 0x0,
            GENERATION_1 = 0x1,
            GENERATION_2 = 0x2,
            GENERATION_3 = 0x3,
        };

        enum class InterfacePowerManagement {
            DEVICE_NOT_PRESENT_OR_NO_COMMUNICATION_ESTABLISHED = 0x0,
            ACTIVE = 0x1,
            PARTIAL_POWER = 0x2,
            SLUMBER_POWER = 0x6,
            DEVSLEEP_POWER = 0x8,
        };

        DeviceDetection deviceDetection : 4;
        CurrentInterfaceSpeed currentInterfaceSpeed : 4;
        InterfacePowerManagement interfacePowerManagement : 4;
        [[deprecated]] std::uint32_t reserved : 20;
    };

    static_assert(sizeof(SATAStatus) == 4);

} // namespace kernel::device::drive::ahci
