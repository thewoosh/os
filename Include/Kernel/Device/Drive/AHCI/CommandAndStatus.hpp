#pragma once

#include <cstdint>

namespace kernel::device::drive::ahci {

    // 3.3.7 Offset 18h: PxCMD – Port x Command and Status
    struct [[gnu::packed]] CommandAndStatus {
        //
        bool start : 1;
        bool spinUpDevice : 1;
        bool powerOnDevice : 1;
        bool commandListOverride : 1;
        bool fisReceiveEnabled : 1;
        [[deprecated]] std::uint8_t reserved : 3;
        std::uint8_t currentCommandSlot : 5;
        bool mechanicalPresenceSwitchState : 1;
        bool fisReceiveRunning : 1;
        bool commandListRunning : 1;
        bool coldPresenceState : 1;
        bool portMultiplierAttached : 1;
        bool hotPlugCapablePort : 1;
        bool mechanicalPresenceSwitchAttachedToPort : 1;
        bool coldPresenceDetection : 1;
        bool externalSATAPort : 1;
        bool fisBasedSwitchingCapablePort : 1;
        bool automaticPartialToSlumberTransitionsEnabled : 1;
        bool deviceIsATAPI : 1;
        bool driveLEDOnATAPIEnable : 1;
        bool aggressiveLinkPowerManagementEnable : 1;
        bool aggressiveSlumberPartial : 1;
    };

    static_assert(sizeof(CommandAndStatus) == 4);

} // namespace kernel::device::drive::ahci
