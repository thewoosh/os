#pragma once

#include <cstdint>

#include "Kernel/Device/Drive/AHCI/CommandAndStatus.hpp"
#include "Kernel/Device/Drive/AHCI/SATAStatus.hpp"

namespace kernel::device::drive::ahci {

    struct [[gnu::packed]] HostBusAdapterPort {
        enum class Signature : std::uint32_t {
#define AHCI_HOST_BUS_ADAPTER_PORT_ITERATE_SIGNATURES(REGISTER_SIGNATURE) \
            REGISTER_SIGNATURE(SATA_DRIVE, 0x00000101, "SATA Drive") \
            REGISTER_SIGNATURE(SATA_PI, 0xEB140101, "SATAPI Drive") \
            REGISTER_SIGNATURE(ENCLOSURE_MANAGEMENT_BRIDGE, 0xC33C0101, "Enclosure Management Bridge") \
            REGISTER_SIGNATURE(PORT_MULTIPLIER, 0x96690101, "Port Multiplier") \

#define REGISTER_SIGNATURE(enumeration, id, description) enumeration = id,
            AHCI_HOST_BUS_ADAPTER_PORT_ITERATE_SIGNATURES(REGISTER_SIGNATURE)
#undef REGISTER_SIGNATURE
        };

        std::uint32_t commandListBaseAddress;
        std::uint32_t commandListBaseAddressUpper;
        std::uint32_t fisBaseAddress;
        std::uint32_t fisBaseAddressUpper;
        std::uint32_t interruptStatus;
        std::uint32_t interruptEnable;
        CommandAndStatus commandAndStatus;
        [[deprecated]] std::uint32_t reserved;
        std::uint32_t taskFileData;
        Signature signature;
        SATAStatus sataStatus;
        std::uint32_t sataControl;
        std::uint32_t sataError;
        std::uint32_t sataActive;
        std::uint32_t commandIssue;
        std::uint32_t sataNotification;
        std::uint32_t fisBasedSwitchControl;
        [[deprecated]] std::uint32_t reserved2[11];
        std::uint32_t vendorSpecific[4];

        inline void
        stopCommandEngine() volatile {
            commandAndStatus.start = false;
            commandAndStatus.fisReceiveEnabled = false;

            while (commandAndStatus.fisReceiveRunning || commandAndStatus.commandListRunning) {
                // yield
            }
        }
    };

    [[nodiscard]] inline constexpr std::string_view
    toString(HostBusAdapterPort::Signature signature) {
        switch (signature) {
#define REGISTER_SIGNATURE(enumeration, id, description) case HostBusAdapterPort::Signature::enumeration: return description;
            AHCI_HOST_BUS_ADAPTER_PORT_ITERATE_SIGNATURES(REGISTER_SIGNATURE)
#undef REGISTER_SIGNATURE
        }

        return "(unknown)";
    }

} // namespace kernel::device::drive::ahci
