#pragma once

#include <cstddef>
#include <cstdint>

#include <string_view>

namespace kernel::device::drive::ahci {

    struct [[gnu::packed]] GenericHostControl {
        enum class Version : std::uint32_t {
#define AHCI_GENERIC_HOST_CONTROL_ITERATE_VERSIONS(REGISTER_VERSION) \
            REGISTER_VERSION(COMPLIANT_0_95, 0x00000905) \
            REGISTER_VERSION(COMPLIANT_1_0, 0x00010000) \
            REGISTER_VERSION(COMPLIANT_1_1, 0x00010100) \
            REGISTER_VERSION(COMPLIANT_1_2, 0x00010200) \
            REGISTER_VERSION(COMPLIANT_1_3, 0x00010300) \
            REGISTER_VERSION(COMPLIANT_1_3_1, 0x00010301) \

#define REGISTER_VERSION(enumeration, id) enumeration = id,
            AHCI_GENERIC_HOST_CONTROL_ITERATE_VERSIONS(REGISTER_VERSION)
#undef REGISTER_VERSION
        };

        std::uint32_t hostCapabilities;
        std::uint32_t globalHostControl;
        std::uint32_t interruptStatus;
        std::uint32_t portsImplemented;
        Version version;
        std::uint32_t commandCompleteCoalescingControl;
        std::uint32_t commandCompleteCoalescingPorts;
        std::uint32_t enclosureManagementLocation;
        std::uint32_t enclosureManagementControl;
        std::uint32_t hostCapabilitiesExtended;
        std::uint32_t biosOSHandoffControlAndStatus;
    };

    [[nodiscard]] inline constexpr std::string_view
    toString(GenericHostControl::Version version) {
        switch (version) {
#define REGISTER_VERSION(enumeration, id) case GenericHostControl::Version::enumeration: return #enumeration;
            AHCI_GENERIC_HOST_CONTROL_ITERATE_VERSIONS(REGISTER_VERSION)
#undef REGISTER_VERSION
        }

        return "(unknown)";
    }

} // namespace kernel::device::drive::ahci
