#pragma once

#include "Kernel/PCI/Device.hpp"

//
// SATA AHCI Specification, Revision 1.3.1
// https://www.intel.com/content/dam/www/public/us/en/documents/technical-specifications/serial-ata-ahci-spec-rev1-3-1.pdf
//
namespace kernel::device::drive::ahci {

    struct FrameInformationStructure;
    struct HostBusAdapterMemory;
    struct HostBusAdapterPort;

    class AHCIManager {
    public:
        void
        launch(pci::AbstractDevice *device);

    private:
        void
        checkPort(volatile HostBusAdapterPort &);

        void
        checkPorts();

        pci::AbstractDevice *m_pciDevice{nullptr};
        volatile HostBusAdapterMemory *m_hbaMemory{nullptr};
    };

} // namespace kernel::device::drive::ahci
