#pragma once

#include <array>

#include <cstdint>

#include "Kernel/Device/Drive/AHCI/GenericHostControl.hpp"
#include "Kernel/Device/Drive/AHCI/HostBusAdapterPort.hpp"

namespace kernel::device::drive::ahci {

    struct [[gnu::packed]] HostBusAdapterMemory {
        GenericHostControl hostControl;

        // HERE (0x28) could be BIOS/OS handoff control and status
        std::uint8_t reserved[0x60 - 0x2C];
        std::uint8_t reservedForNVMHCI[0xA0 - 0x60];

        std::uint8_t vendorSpecific[0x100 - 0xA0];

        HostBusAdapterPort ports[32];
    };

    static_assert(sizeof(HostBusAdapterMemory) == 0x1100);

} // namespace kernel::device::drive::ahci
