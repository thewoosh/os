#pragma once

#include "Kernel/PCI/Forward.hpp"

namespace kernel::device::drive {

    class IDEController {
    public:
        [[nodiscard]]
        IDEController(const pci::Entry &)

        void
        launch();

    private:
        pci::Entry m_pciDevice;
    };

} // namespace kernel::device::drive
