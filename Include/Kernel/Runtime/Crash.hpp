#pragma once

#include <string_view>

#define CRASH(message) ::kernel::crash(message, __FILE__, __LINE__, __PRETTY_FUNCTION__)
#define CRASH_FROM_ASSERT(message) ::kernel::crash(message, __FILE__, __LINE__, __PRETTY_FUNCTION__, \
        ::kernel::CrashSource::ASSERT)

namespace kernel {

    enum class CrashSource {
        OTHER,
        ASSERT,
    };

    [[noreturn]] void
    crash(std::string_view message,
          std::string_view file_name,
          std::size_t line_number,
          std::string_view function_name,
          CrashSource = CrashSource::OTHER);

} // namespace kernel
