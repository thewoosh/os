#pragma once

#include <string_view>

namespace kernel::multiboot2 {

    struct ELFSymbolsTag;

} // namespace kernel::multiboot2

namespace kernel::elf64 { struct SectionHeader; }

namespace kernel::symbolizer {

    [[nodiscard]] std::string_view
    demangle(std::string_view);

    void
    ensureELFHeaderIsMapped();

    [[nodiscard]] std::string_view
    get_name_of_function(void *address);

    void
    set_multiboot_tag(const multiboot2::ELFSymbolsTag *);

} // namespace kernel::symbolizer
