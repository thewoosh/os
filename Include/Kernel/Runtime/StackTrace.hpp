#pragma once

#include <cstddef>
#include <cstdint>

#include <string_view>

#include "Include/Kernel/CPU/CPU.hpp"
#include "Kernel/Runtime/Symbolizer.hpp"

namespace kernel {

    struct StackFrameInformation {
        std::size_t stack_number;
        std::uint64_t rip;
        std::string_view function_name;
    };

    template <typename Callback>
    inline void
    traverse_stack_trace(Callback callback, std::size_t max_frames = 100) {
        struct StackFrame {
            StackFrame *ebp;
            std::uint64_t rip;
        };

        auto *stk = cpu::read_rbp<StackFrame *>();

        for(std::size_t frame = 0; stk && frame < max_frames; ++frame) {
//            if (frame == 0)
//                stk->

            callback(StackFrameInformation{
                .stack_number = frame,
                .rip = stk->rip,
                .function_name = symbolizer::get_name_of_function(reinterpret_cast<void *>(stk->rip)),
            });

            stk = stk->ebp;
        }
    }

} // namespace kernel
