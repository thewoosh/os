#pragma once

#include <array>

#include "Source/Implementation/amd64/GDT.hpp"

namespace kernel {

    class TaskManager {
        inline static constexpr std::uint8_t k_ring_0_code_index = 1;
        inline static constexpr std::uint8_t k_ring_0_data_index = 2;
        inline static constexpr std::uint8_t k_ring_3_code_index = 3;
        inline static constexpr std::uint8_t k_ring_3_data_index = 4;

    public:
        void
        launch();

    private:
        void
        setup_gdt();

        void
        setup_tss();

        std::array<GDTEntry, 5> m_gdt{};
    };

} // namespace kernel
