/*
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 *
 * Copyright (C) 2021 Tristan Gerritsen
 * All Rights Reserved.
 */

#pragma once

#include <cstdint>

#include <algorithm>
#include <string_view>

#include "Kernel/Serial/Forward.hpp"

enum class PrintColor {
    BLACK,
    BLUE,
    GREEN,
    CYAN,
    RED,
    MAGENTA,
    BROWN,
    LIGHT_GRAY,
    DARK_GRAY,
    LIGHT_BLUE,
    LIGHT_GREEN,
    LIGHT_CYAN,
    LIGHT_RED,
    PINK,
    YELLOW,
    WHITE
};

#define HONOR_PRINT_TO_SCREEN 1

#if HONOR_PRINT_TO_SCREEN
#   define __SHOULD_PRINT_TO_SCREEN (m_printToScreenAvailable && m_print_to_screen)
#else
#   define __SHOULD_PRINT_TO_SCREEN true
#endif

template <std::size_t ColumnCount = 80, std::size_t RowCount = 25>
class TextModeBufferView {
    Serial::COMPort *m_hostVMPort{nullptr};

    struct [[gnu::packed]] Char {
        std::uint8_t m_character;
        std::uint8_t m_color;

        Char() noexcept = default;
        Char(Char &&) noexcept = default;
        Char(const Char &) noexcept = default;
        Char &operator=(Char &&) noexcept = default;
        Char &operator=(const Char &) noexcept = default;

        [[nodiscard]] inline constexpr
        Char(char character, std::uint8_t color)
                : m_character(static_cast<std::uint8_t>(character))
                , m_color(color) {
        }

        [[nodiscard]] inline constexpr std::uint8_t
        character() const {
            return m_character;
        }

        [[nodiscard]] inline constexpr std::uint8_t
        color() const {
            return m_color;
        }
    };

public:
    [[nodiscard]] inline constexpr std::size_t
    current_row() const {
        return m_currentRow;
    }

    [[nodiscard]] inline static constexpr std::uint8_t
    composeColor(PrintColor foreground, PrintColor background) {
        return static_cast<std::uint8_t>(
                static_cast<std::uint8_t>(foreground)
                | (static_cast<std::uint8_t>(background) << 4)
        );
    }

    inline void
    clearRow(std::size_t index) {
        const Char replacement{' ', m_currentColor};
        Char *const first = row(index);
        Char *const last = first + ColumnCount;

        std::fill(first, last, replacement);
    }

    inline constexpr void
    clear() {
        m_currentRow = 0;
        m_currentColumn = 0;
        m_currentColor = composeColor(PrintColor::WHITE, PrintColor::BLACK);
        std::fill(row(0), &row(RowCount - 1)[ColumnCount], Char{' ', composeColor(PrintColor::WHITE, PrintColor::BLACK)});
    }

    inline void
    new_line() {
        m_currentColumn = 0;
        if (m_currentRow + 1 >= RowCount) {
            m_currentRow = RowCount - 1;
            for (std::size_t i = 1; i <= RowCount; ++i) {
                std::memcpy(row(i - 1), row(i), ColumnCount * sizeof(Char) - 1);
            }
            clearRow(m_currentRow);
        } else {
            ++m_currentRow;
        }
//        clearRow(m_currentRow);
    }

    inline void
    printCharacter(char character) {
        if (!__SHOULD_PRINT_TO_SCREEN)
            return;

        const bool isNewline = character == '\n';
        if (m_currentRow >= RowCount)
            m_currentRow = RowCount - 1;
        if (m_currentColumn >= ColumnCount)
            m_currentColumn = ColumnCount - 1;

        if (!isNewline) {
            row(m_currentRow)[m_currentColumn] = {character, m_currentColor};
        }

        if (isNewline || ++m_currentColumn >= ColumnCount) {
            new_line();
        }
    }

    inline constexpr void
    setColors(PrintColor foreground, PrintColor background) noexcept {
        m_currentColor = composeColor(foreground, background);
    }

    inline constexpr void
    setHostVMCOMPort(Serial::COMPort *port) noexcept {
        m_hostVMPort = port;
    }

    [[nodiscard]] inline constexpr Serial::COMPort *
    hostVMCOMPort() noexcept {
        return m_hostVMPort;
    }

    inline constexpr void
    enable_print_to_screen() {
        m_print_to_screen = true;
    }

    inline constexpr void
    disable_print_to_screen() {
        m_print_to_screen = false;
    }

    inline constexpr void
    setPrintToScreenAvailable(bool status) {
        m_printToScreenAvailable = status;
    }

    [[nodiscard]] inline constexpr bool
    isPrintToScreenAvailable() const {
        return m_printToScreenAvailable;
    }

    void
    resetCursor() {
        m_currentRow = 0;
        m_currentColumn = 0;
        m_currentColor = composeColor(PrintColor::WHITE, PrintColor::BLACK);
    }

private:
    std::size_t m_currentColumn{0};
    std::size_t m_currentRow{0};
    std::uint8_t m_currentColor = composeColor(PrintColor::WHITE, PrintColor::BLACK);
    bool m_print_to_screen{true};
    bool m_printToScreenAvailable{false};

    [[nodiscard]] inline Char *
    row(std::size_t index) const noexcept {
        return reinterpret_cast<struct Char *>(0xb8000 + (index * ColumnCount * 2));
    }
};

enum class LogLevel {
    CRITICAL,
    DEBUG,
    ERROR,
    INFO,
    WARNING,
};

extern TextModeBufferView<80, 25> g_textModeBufferView;

[[nodiscard]] inline static constexpr std::string_view
translateLogLevelToString(LogLevel level) noexcept {
    switch (level) {
        case LogLevel::CRITICAL: return "CRITICAL";
        case LogLevel::DEBUG: return "DEBUG";
        case LogLevel::ERROR: return "ERROR";
        case LogLevel::INFO: return "INFO";
        case LogLevel::WARNING: return "WARNING";
        default: return "INVALID-LOGLEVEL";
    }
}

enum class HexnumberPrefix {
    NO,
    YES
};

void
kputchar(char);

void
kputdecnumber(std::uint64_t, std::size_t width = 0);

void
kputhexnumber(std::uint64_t, HexnumberPrefix = HexnumberPrefix::YES);

inline void
kputs(std::string_view string) noexcept {
    for (char character : string) {
        kputchar(character);
    }
}

class TextModeBufferViewWriter {
public:
    enum NumberMode {
        Hexadecimal,
        Decimal,
    };

    inline
    TextModeBufferViewWriter(LogLevel level, std::string_view module) noexcept {
        g_textModeBufferView.setColors(PrintColor::LIGHT_GRAY, PrintColor::BLACK);
        kputchar('[');
        g_textModeBufferView.setColors(PrintColor::LIGHT_BLUE, PrintColor::BLACK);
        kputs(module);
        g_textModeBufferView.setColors(PrintColor::LIGHT_GRAY, PrintColor::BLACK);
        kputs("] (");
        g_textModeBufferView.setColors(PrintColor::YELLOW, PrintColor::BLACK);
        kputs(translateLogLevelToString(level));
        g_textModeBufferView.setColors(PrintColor::LIGHT_GRAY, PrintColor::BLACK);
        kputs(") ");
        g_textModeBufferView.setColors(PrintColor::WHITE, PrintColor::BLACK);
    }

    inline
    ~TextModeBufferViewWriter() {
        finish();
    }

    inline void
    finish() {
        if (!m_finished) {
            m_finished = true;
            kputchar('\n');
        }
    }

    inline TextModeBufferViewWriter &
    operator<<(std::string_view string) {
        kputs(string);
        return *this;
    }

    TextModeBufferViewWriter &
    operator<<(std::uint8_t value) {
        return operator<<(static_cast<std::uint64_t>(value));
    }

    TextModeBufferViewWriter &
    operator<<(std::uint16_t value) {
        return operator<<(static_cast<std::uint64_t>(value));
    }

    TextModeBufferViewWriter &
    operator<<(std::uint32_t value) {
        return operator<<(static_cast<std::uint64_t>(value));
    }

    TextModeBufferViewWriter &
    operator<<(std::uint64_t);

    [[nodiscard]] inline constexpr TextModeBufferViewWriter &
    operator<<(NumberMode number_mode) {
        m_number_mode = number_mode;
        return *this;
    }

private:
    NumberMode m_number_mode{};
    bool m_finished{false};
};

template <LogLevel LogLevelValue>
[[nodiscard]] inline TextModeBufferViewWriter
klog(std::string_view module) noexcept {
    return {LogLevelValue, module};
}

struct BitPrinter {
    template <typename Type>
    explicit
    BitPrinter(Type value)
            : m_value(static_cast<std::uint64_t>(value)) {
    }

    inline friend TextModeBufferViewWriter &
    operator<<(TextModeBufferViewWriter &writer, const BitPrinter &value) {
        writer << "0b";
//        bool has_seen_one = false;

        for (std::size_t i = sizeof(std::uint64_t) * 8; i > 0; --i) {
            auto val = value.m_value >> (i - 1);
            if (val & 0x1) {
                writer << "1";
//                has_seen_one = true;
            } else // if (has_seen_one || i == 1)
                writer << "0";
        }

        return writer;
    }

private:
    std::uint64_t m_value;
};
