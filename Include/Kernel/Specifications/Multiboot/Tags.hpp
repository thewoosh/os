#pragma once
#define KERNEL_MULTIBOOT2_DEBUG
#include <Kernel/Print.hpp>
#include <Kernel/ACPI/RSDP.hpp>
#include <Kernel/Specifications/ELF/ELF.hpp>

namespace kernel::multiboot2 {

#define MULTIBOOT2_ITERATE_TAGS(REGISTER_TAG) \
        REGISTER_TAG(BootCommandLineTag) \
        REGISTER_TAG(BootLoaderNameTag) \
        REGISTER_TAG(ModulesTag) \
        REGISTER_TAG(BasicMemoryInformationTag) \
        REGISTER_TAG(BIOSBootDeviceTag) \
        REGISTER_TAG(MemoryMapTag) \
        REGISTER_TAG(VBEInfoTag) \
        REGISTER_TAG(FramebufferInfoTag) \
        REGISTER_TAG(ELFSymbolsTag) \
        REGISTER_TAG(APMTag) \
        REGISTER_TAG(EFI32BitSystemTablePointerTag) \
        REGISTER_TAG(EFI64BitSystemTablePointerTag) \
        REGISTER_TAG(SMBIOSTableTag) \
        REGISTER_TAG(ACPIOldRSDPTag) \
        REGISTER_TAG(ACPINewRSDPTag) \
        REGISTER_TAG(NetworkingInformationTag) \
        REGISTER_TAG(EFIMemoryMapTag) \
        REGISTER_TAG(EFIBootServicesNotTerminatedTag) \
        REGISTER_TAG(EFI32BitImageHandlePointerTag) \
        REGISTER_TAG(EFI64BitImageHandlePointerTag) \
        REGISTER_TAG(ImageLoadBasePhysicalAddressTag) \


#define MULTIBOOT2_TAG struct [[gnu::packed]]
#define MULTIBOOT2_STRING_MEMBER \
        std::uint8_t string{}; \
        [[nodiscard]] inline std::string_view \
        string_as_view() const { \
            return std::string_view(reinterpret_cast<const char *>(&string), tag.size - sizeof(tag)); \
        }

    MULTIBOOT2_TAG BasicTag {
        std::uint32_t type;
        std::uint32_t size;
    };

    MULTIBOOT2_TAG BootCommandLineTag {
        BasicTag tag{1, 0};
        MULTIBOOT2_STRING_MEMBER

#ifdef KERNEL_MULTIBOOT2_DEBUG
        inline void
        dump() const {
            klog<LogLevel::DEBUG>("MultibootDump") << "Tag \"BootCommandLineTag\"";
            klog<LogLevel::DEBUG>("MultibootDump") << "type = " << static_cast<std::uint64_t>(tag.type);
            klog<LogLevel::DEBUG>("MultibootDump") << "size = " << static_cast<std::uint64_t>(tag.size);
            klog<LogLevel::DEBUG>("MultibootDump") << "string[" << (tag.size - sizeof(tag)) << "] = " << string_as_view();
        }
#endif
    };

    MULTIBOOT2_TAG BootLoaderNameTag {
        BasicTag tag{2, 0};
        MULTIBOOT2_STRING_MEMBER

#ifdef KERNEL_MULTIBOOT2_DEBUG
        inline void
        dump() const {
            klog<LogLevel::DEBUG>("MultibootDump") << "Tag \"BootLoaderNameTag\"";
            klog<LogLevel::DEBUG>("MultibootDump") << "type = " << static_cast<std::uint64_t>(tag.type);
            klog<LogLevel::DEBUG>("MultibootDump") << "size = " << static_cast<std::uint64_t>(tag.size);
            klog<LogLevel::DEBUG>("MultibootDump") << "string[" << (tag.size - sizeof(tag)) << "] = " << string_as_view();
        }
#endif
    };

    MULTIBOOT2_TAG ModulesTag {
        BasicTag tag{3, 0};
        std::uint32_t mod_start{};
        std::uint32_t mod_end{};
        std::uint8_t string{};

#ifdef KERNEL_MULTIBOOT2_DEBUG
        inline void
        dump() const {
            klog<LogLevel::DEBUG>("MultibootDump") << "Tag \"ModulesTag\"";
            klog<LogLevel::DEBUG>("MultibootDump") << "type = " << static_cast<std::uint64_t>(tag.type);
            klog<LogLevel::DEBUG>("MultibootDump") << "size = " << static_cast<std::uint64_t>(tag.size);
            klog<LogLevel::DEBUG>("MultibootDump") << "mod_start = " << static_cast<std::uint64_t>(mod_start);
            klog<LogLevel::DEBUG>("MultibootDump") << "mod_end = " << static_cast<std::uint64_t>(mod_end);
            klog<LogLevel::DEBUG>("MultibootDump") << "string[" << (tag.size - sizeof(tag)) << "] = " << std::string_view(reinterpret_cast<const char *>(&string), tag.size - sizeof(tag));
        }
#endif
    };

    MULTIBOOT2_TAG BasicMemoryInformationTag {
        BasicTag tag{4, 0};
        std::uint32_t lower{};
        std::uint32_t upper{};

#ifdef KERNEL_MULTIBOOT2_DEBUG
        inline void
        dump() const {
            klog<LogLevel::DEBUG>("MultibootDump") << "Tag \"BasicMemoryInformationTag\"";
            klog<LogLevel::DEBUG>("MultibootDump") << "type = " << static_cast<std::uint64_t>(tag.type);
            klog<LogLevel::DEBUG>("MultibootDump") << "size = " << static_cast<std::uint64_t>(tag.size);
            klog<LogLevel::DEBUG>("MultibootDump") << "lower = " << static_cast<std::uint64_t>(lower);
            klog<LogLevel::DEBUG>("MultibootDump") << "upper = " << static_cast<std::uint64_t>(upper);
        }
#endif
    };

    MULTIBOOT2_TAG BIOSBootDeviceTag {
        BasicTag tag{5, 0};
        std::uint32_t biosdev{};
        std::uint32_t partition{};
        std::uint32_t sub_partition{};

#ifdef KERNEL_MULTIBOOT2_DEBUG
        inline void
        dump() const {
            klog<LogLevel::DEBUG>("MultibootDump") << "Tag \"BIOSBootDeviceTag\"";
            klog<LogLevel::DEBUG>("MultibootDump") << "type = " << static_cast<std::uint64_t>(tag.type);
            klog<LogLevel::DEBUG>("MultibootDump") << "size = " << static_cast<std::uint64_t>(tag.size);
            klog<LogLevel::DEBUG>("MultibootDump") << "biosdev = " << static_cast<std::uint64_t>(biosdev);
            klog<LogLevel::DEBUG>("MultibootDump") << "partition = " << static_cast<std::uint64_t>(partition);
            klog<LogLevel::DEBUG>("MultibootDump") << "sub_partition = " << static_cast<std::uint64_t>(sub_partition);
        }
#endif
    };

    MULTIBOOT2_TAG MemoryMapTag {
        struct [[gnu::packed]] Entry {
            enum class Type
                    : std::uint32_t{
                AVAILABLE = 1,
                RESERVED = 2,
                ACPI_RECLAIMABLE = 3,
                PRESERVE_HIBERNATION = 4,
                BADRAM = 5
            };

            std::uint64_t base_addr;
            std::uint64_t length;
            Type type;
            std::uint32_t reserved;
        };

        BasicTag tag{6, 0};
        std::uint32_t entry_size{};
        std::uint32_t entry_version{};
        Entry entries{};

        template<typename Callback>
        void
        for_each_entry(Callback callback) const {
            for (auto *ptr = reinterpret_cast<const std::uint8_t *>(&entries);
                 ptr < (reinterpret_cast<const std::uint8_t *>(&tag) + tag.size);
                 ptr += entry_size) {
                callback(*reinterpret_cast<const Entry *>(ptr));
            }
        }

        inline void
        dump() const {
#ifdef KERNEL_MULTIBOOT2_DEBUG
            klog<LogLevel::DEBUG>("MultibootDump") << "Tag \"MemoryMapTag\"";
            klog<LogLevel::DEBUG>("MultibootDump") << "  type = " << static_cast<std::uint64_t>(tag.type);
            klog<LogLevel::DEBUG>("MultibootDump") << "  size = " << static_cast<std::uint64_t>(tag.size);
            klog<LogLevel::DEBUG>("MultibootDump") << "  entry_size = " << static_cast<std::uint64_t>(entry_size);
            klog<LogLevel::DEBUG>("MultibootDump") << "  entry_version = " << static_cast<std::uint64_t>(entry_version);
            std::size_t entry_index = 0;
            for (auto *ptr = reinterpret_cast<const std::uint8_t *>(&entries);
                    ptr < (reinterpret_cast<const std::uint8_t *>(&tag) + tag.size);
                    ptr += entry_size) {
                const auto &entry = *reinterpret_cast<const Entry *>(ptr);
                auto stream = klog<LogLevel::DEBUG>("MultibootDump");
                stream << "    entries[" << entry_index << "] base_addr=" << entry.base_addr
                       << " length=" << entry.length;
                switch (entry.type) {
                    case Entry::Type::AVAILABLE: stream << " type=available(0x1)"; break;
                    case Entry::Type::RESERVED: stream << " type=reserved(0x2)"; break;
                    case Entry::Type::ACPI_RECLAIMABLE: stream << " type=acpi_reclaimable(0x3)"; break;
                    case Entry::Type::PRESERVE_HIBERNATION: stream << " type=nvs(0x4)"; break;
                    case Entry::Type::BADRAM: stream << " type=badram(0x5)"; break;
                    default: stream << " type=u_reserved(" << std::uint32_t(entry.type) << ")"; break;
                }

                ++entry_index;
            }
#endif
        }
    };

    MULTIBOOT2_TAG VBEInfoTag {
        BasicTag tag{7, 0};
        std::uint16_t vbe_mode{};
        std::uint16_t vbe_interface_seg{};
        std::uint16_t vbe_interface_off{};
        std::uint16_t vbe_interface_len{};
        std::uint8_t vbe_control_info[512]{};
        std::uint8_t vbe_mode_info[256]{};

        inline void
        dump() const {
#ifdef KERNEL_MULTIBOOT2_DEBUG
            klog<LogLevel::DEBUG>("MultibootDump") << "Tag \"VBEInfoTag\"";
            klog<LogLevel::DEBUG>("MultibootDump") << "type = " << static_cast<std::uint64_t>(tag.type);
            klog<LogLevel::DEBUG>("MultibootDump") << "size = " << static_cast<std::uint64_t>(tag.size);
            klog<LogLevel::DEBUG>("MultibootDump") << "vbe_mode = " << static_cast<std::uint64_t>(vbe_mode);
            klog<LogLevel::DEBUG>("MultibootDump") << "vbe_interface_seg = " << static_cast<std::uint64_t>(vbe_interface_seg);
            klog<LogLevel::DEBUG>("MultibootDump") << "vbe_interface_off = " << static_cast<std::uint64_t>(vbe_interface_off);
            klog<LogLevel::DEBUG>("MultibootDump") << "vbe_interface_len = " << static_cast<std::uint64_t>(vbe_interface_len);
            klog<LogLevel::DEBUG>("MultibootDump") << "vbe_control_info = ...";
            klog<LogLevel::DEBUG>("MultibootDump") << "vbe_mode_info = ...";
#endif
        }
    };

    MULTIBOOT2_TAG FramebufferInfoTag {
        BasicTag tag{8, 0};
        std::uint64_t framebuffer_addr{};
        std::uint32_t framebuffer_pitch{};
        std::uint32_t framebuffer_width{};
        std::uint32_t framebuffer_height{};
        std::uint8_t framebuffer_bpp{};
        std::uint8_t framebuffer_type{};
        std::uint8_t reserved{};

        inline void
        dump() const {
#ifdef KERNEL_MULTIBOOT2_DEBUG
            klog<LogLevel::DEBUG>("MultibootDump") << "Tag \"FramebufferInfoTag\"";
            klog<LogLevel::DEBUG>("MultibootDump") << "type = " << static_cast<std::uint64_t>(tag.type);
            klog<LogLevel::DEBUG>("MultibootDump") << "size = " << static_cast<std::uint64_t>(tag.size);
            klog<LogLevel::DEBUG>("MultibootDump") << "framebuffer_addr = " << framebuffer_addr;
            klog<LogLevel::DEBUG>("MultibootDump") << "framebuffer_pitch = " << static_cast<std::uint64_t>(framebuffer_pitch);
            klog<LogLevel::DEBUG>("MultibootDump") << "framebuffer_width = " << static_cast<std::uint64_t>(framebuffer_width);
            klog<LogLevel::DEBUG>("MultibootDump") << "framebuffer_height = " << static_cast<std::uint64_t>(framebuffer_height);
            klog<LogLevel::DEBUG>("MultibootDump") << "framebuffer_bpp = " << static_cast<std::uint64_t>(framebuffer_bpp);
            klog<LogLevel::DEBUG>("MultibootDump") << "framebuffer_type = " << static_cast<std::uint64_t>(framebuffer_type);
            klog<LogLevel::DEBUG>("MultibootDump") << "reserved = " << static_cast<std::uint64_t>(reserved);
#endif
        }
    };

    MULTIBOOT2_TAG ELFSymbolsTag {
        BasicTag tag{9, 0};
        std::uint32_t num{};
        std::uint32_t entsize{};
        std::uint32_t shndx{};

        std::uint8_t sections_begin{};

        inline static void
        dump_section_header(const elf64::SectionHeader &section_header) {
            klog<LogLevel::DEBUG>("ELFDump(amd64)") << "this = " << reinterpret_cast<std::uint64_t>(&section_header);
            klog<LogLevel::DEBUG>("ELFDump(amd64)") << "sh_name = " << section_header.sh_name;
            klog<LogLevel::DEBUG>("ELFDump(amd64)") << "sh_type = " << static_cast<std::uint64_t>(section_header.sh_type);
            klog<LogLevel::DEBUG>("ELFDump(amd64)") << "sh_flags = " << section_header.sh_flags;
            klog<LogLevel::DEBUG>("ELFDump(amd64)") << "sh_addr = " << section_header.sh_addr;
            klog<LogLevel::DEBUG>("ELFDump(amd64)") << "sh_offset = " << section_header.sh_offset;
            klog<LogLevel::DEBUG>("ELFDump(amd64)") << "sh_size = " << section_header.sh_size;
            klog<LogLevel::DEBUG>("ELFDump(amd64)") << "sh_link = " << section_header.sh_link;
            klog<LogLevel::DEBUG>("ELFDump(amd64)") << "sh_info = " << section_header.sh_info;
            klog<LogLevel::DEBUG>("ELFDump(amd64)") << "sh_addralign = " << section_header.sh_addralign;
            klog<LogLevel::DEBUG>("ELFDump(amd64)") << "sh_entsize = " << section_header.sh_entsize;
        }

        inline void
        dump() const {
#ifdef KERNEL_MULTIBOOT2_DEBUG
#ifdef KERNEL_MULTIBOOT2_DEBUG_ELF_SYMBOLS_TAG
            klog<LogLevel::DEBUG>("MultibootDump") << "Tag \"ELFSymbolsTag\"";
            klog<LogLevel::DEBUG>("MultibootDump") << "  type = " << tag.type;
            klog<LogLevel::DEBUG>("MultibootDump") << "  size = " << tag.size;
            klog<LogLevel::DEBUG>("MultibootDump") << "  num = " << num;
            klog<LogLevel::DEBUG>("MultibootDump") << "  entsize = " << entsize;
            klog<LogLevel::DEBUG>("MultibootDump") << "  shndx = " << shndx;

            const auto sections_offset = static_cast<size_t>(&sections_begin - reinterpret_cast<const std::uint8_t *>(this));
            const auto sections_size = tag.size - sections_offset;
            klog<LogLevel::DEBUG>("MultibootDump") << "sections_offset=" << sections_offset << " sections_size=" << sections_size
                    << " individual_section_size64=" << sizeof(elf64::SectionHeader);

            const auto *elf_sections = reinterpret_cast<const elf64::SectionHeader *>(&sections_begin);
            const auto &name_table = elf_sections[shndx];
            klog<LogLevel::DEBUG>("MultibootDump") << "Name Table: ";
            dump_section_header(name_table);

            const auto *name_offset = reinterpret_cast<const char **>(name_table.sh_offset);

            for (std::size_t section_id = 0; section_id < num; ++section_id) {
                const auto &section = *elf_sections;
                klog<LogLevel::DEBUG>("MultibootDump") << "Section #" << section_id << " name_ptr=" << reinterpret_cast<std::uint64_t>(name_offset + section.sh_name);

                dump_section_header(section);

                elf_sections = reinterpret_cast<const elf64::SectionHeader *>(
                        reinterpret_cast<const std::uint8_t *>(elf_sections) + entsize
                );
            }

//            const auto entry_count = sections_size / sizeof(elf::Elf64_Shdr);
//            klog<LogLevel::DEBUG>("MultibootDump") << "sections_offset=" << sections_offset << " sections_size=" << sections_size
//                    << " entry_count=" << entry_count << " entry_size=" << sizeof(elf::Elf64_Shdr) << " leftover=" << (sections_size - (entry_count * sizeof(elf::Elf64_Shdr)));
#endif // KERNEL_MULTIBOOT2_DEBUG_ELF_SYMBOLS_TAG
#endif // KERNEL_MULTIBOOT2_DEBUG
        }
    };

    MULTIBOOT2_TAG APMTag {
        BasicTag tag{10, 0};
        std::uint16_t version{};
        std::uint16_t cseg{};
        std::uint32_t offset{};
        std::uint16_t cseg_16{};
        std::uint16_t dseg{};
        std::uint16_t flags{};
        std::uint16_t cseg_len{};
        std::uint16_t cseg_16_len{};
        std::uint16_t dseg_len{};

        inline void
        dump() const {
#ifdef KERNEL_MULTIBOOT2_DEBUG
            klog<LogLevel::DEBUG>("MultibootDump") << "Tag \"APMTag\"";
            klog<LogLevel::DEBUG>("MultibootDump") << "type = " << static_cast<std::uint64_t>(tag.type);
            klog<LogLevel::DEBUG>("MultibootDump") << "size = " << static_cast<std::uint64_t>(tag.size);
            klog<LogLevel::DEBUG>("MultibootDump") << "version = " << static_cast<std::uint64_t>(version);
            klog<LogLevel::DEBUG>("MultibootDump") << "cseg = " << static_cast<std::uint64_t>(cseg);
            klog<LogLevel::DEBUG>("MultibootDump") << "offset = " << static_cast<std::uint64_t>(offset);
            klog<LogLevel::DEBUG>("MultibootDump") << "cseg_16 = " << static_cast<std::uint64_t>(cseg_16);
            klog<LogLevel::DEBUG>("MultibootDump") << "dseg = " << static_cast<std::uint64_t>(dseg);
            klog<LogLevel::DEBUG>("MultibootDump") << "flags = " << static_cast<std::uint64_t>(flags);
            klog<LogLevel::DEBUG>("MultibootDump") << "cseg_len = " << static_cast<std::uint64_t>(cseg_len);
            klog<LogLevel::DEBUG>("MultibootDump") << "cseg_16_len = " << static_cast<std::uint64_t>(cseg_16_len);
            klog<LogLevel::DEBUG>("MultibootDump") << "dseg_len = " << static_cast<std::uint64_t>(dseg_len);
#endif
        }
    };

    MULTIBOOT2_TAG EFI32BitSystemTablePointerTag {
        BasicTag tag{11, 0};
        std::uint32_t pointer{};

        inline void
        dump() const {
#ifdef KERNEL_MULTIBOOT2_DEBUG
            klog<LogLevel::DEBUG>("MultibootDump") << "Tag \"EFI32BitSystemTablePointerTag\"";
            klog<LogLevel::DEBUG>("MultibootDump") << "type = " << static_cast<std::uint64_t>(tag.type);
            klog<LogLevel::DEBUG>("MultibootDump") << "size = " << static_cast<std::uint64_t>(tag.size);
            klog<LogLevel::DEBUG>("MultibootDump") << "pointer = " << static_cast<std::uint64_t>(pointer);
#endif
        }
    };

    MULTIBOOT2_TAG EFI64BitSystemTablePointerTag {
        BasicTag tag{12, 0};
        std::uint64_t pointer{};

        inline void
        dump() const {
#ifdef KERNEL_MULTIBOOT2_DEBUG
            klog<LogLevel::DEBUG>("MultibootDump") << "Tag \"EFI64BitSystemTablePointerTag\"";
            klog<LogLevel::DEBUG>("MultibootDump") << "type = " << static_cast<std::uint64_t>(tag.type);
            klog<LogLevel::DEBUG>("MultibootDump") << "size = " << static_cast<std::uint64_t>(tag.size);
            klog<LogLevel::DEBUG>("MultibootDump") << "pointer = " << pointer;
#endif
        }
    };

    MULTIBOOT2_TAG SMBIOSTableTag {
        BasicTag tag{13, 0};
        std::uint8_t major{};
        std::uint8_t minor{};
        std::uint8_t reserved[6]{};

        inline void
        dump() const {
#ifdef KERNEL_MULTIBOOT2_DEBUG
            klog<LogLevel::DEBUG>("MultibootDump") << "Tag \"SMBIOSTableTag\"";
            klog<LogLevel::DEBUG>("MultibootDump") << "type = " << static_cast<std::uint64_t>(tag.type);
            klog<LogLevel::DEBUG>("MultibootDump") << "size = " << static_cast<std::uint64_t>(tag.size);
            klog<LogLevel::DEBUG>("MultibootDump") << "major = " << static_cast<std::uint64_t>(major);
            klog<LogLevel::DEBUG>("MultibootDump") << "minor = " << static_cast<std::uint64_t>(minor);
#endif
        }
    };

    MULTIBOOT2_TAG ACPIOldRSDPTag {
        BasicTag tag{14, 0};
        acpi::RSDPDescriptor10 descriptor{};

        inline void
        dump() const {
#ifdef KERNEL_MULTIBOOT2_DEBUG
            klog<LogLevel::DEBUG>("MultibootDump") << "Tag \"ACPIOldRSDPTag\"";
            klog<LogLevel::DEBUG>("MultibootDump") << "type = " << static_cast<std::uint64_t>(tag.type);
            klog<LogLevel::DEBUG>("MultibootDump") << "size = " << static_cast<std::uint64_t>(tag.size);
            klog<LogLevel::DEBUG>("MultibootDump") << "descriptor.oem_id = " << std::string_view(descriptor.oem_id, 6);
            klog<LogLevel::DEBUG>("MultibootDump") << "descriptor.signature = " << std::string_view(descriptor.signature, 8);
#endif
        }
    };

    MULTIBOOT2_TAG ACPINewRSDPTag {
        BasicTag tag{15, 0};
        acpi::RSDPDescriptor20 descriptor{};

        inline void
        dump() const {
#ifdef KERNEL_MULTIBOOT2_DEBUG
            klog<LogLevel::DEBUG>("MultibootDump") << "Tag \"ACPINewRSDPTag\"";
            klog<LogLevel::DEBUG>("MultibootDump") << "type = " << static_cast<std::uint64_t>(tag.type);
            klog<LogLevel::DEBUG>("MultibootDump") << "size = " << static_cast<std::uint64_t>(tag.size);
            klog<LogLevel::DEBUG>("MultibootDump") << "descriptor.first_gen.oem_id = " << std::string_view(descriptor.first_gen.oem_id, 6);
            klog<LogLevel::DEBUG>("MultibootDump") << "descriptor.first_gen.signature = " << std::string_view(descriptor.first_gen.signature, 8);
#endif
        }
    };

    MULTIBOOT2_TAG NetworkingInformationTag {
        BasicTag tag{16, 0};

        inline void
        dump() const {
#ifdef KERNEL_MULTIBOOT2_DEBUG
            klog<LogLevel::DEBUG>("MultibootDump") << "Tag \"NetworkingInformationTag\"";
            klog<LogLevel::DEBUG>("MultibootDump") << "type = " << static_cast<std::uint64_t>(tag.type);
            klog<LogLevel::DEBUG>("MultibootDump") << "size = " << static_cast<std::uint64_t>(tag.size);
#endif
        }
    };

    MULTIBOOT2_TAG EFIMemoryMapTag {
        BasicTag tag{17, 0};
        std::uint32_t descriptor_size{};
        std::uint32_t descriptor_version{};
        // TODO EFI memory map
        char efiMemoryMap{};

        inline void
        dump() const {
#ifdef KERNEL_MULTIBOOT2_DEBUG
            klog<LogLevel::DEBUG>("MultibootDump") << "Tag \"EFIMemoryMapTag\"";
            klog<LogLevel::DEBUG>("MultibootDump") << "type = " << static_cast<std::uint64_t>(tag.type);
            klog<LogLevel::DEBUG>("MultibootDump") << "size = " << static_cast<std::uint64_t>(tag.size);
            klog<LogLevel::DEBUG>("MultibootDump") << "descriptor_size = " << static_cast<std::uint64_t>(descriptor_size);
            klog<LogLevel::DEBUG>("MultibootDump") << "descriptor_version = " << static_cast<std::uint64_t>(descriptor_version);
#endif
        }
    };

    MULTIBOOT2_TAG EFIBootServicesNotTerminatedTag {
        BasicTag tag{18, 0};

        inline void
        dump() const {
#ifdef KERNEL_MULTIBOOT2_DEBUG
            klog<LogLevel::DEBUG>("MultibootDump") << "Tag \"EFIBootServicesNotTerminatedTag\"";
            klog<LogLevel::DEBUG>("MultibootDump") << "type = " << static_cast<std::uint64_t>(tag.type);
            klog<LogLevel::DEBUG>("MultibootDump") << "size = " << static_cast<std::uint64_t>(tag.size);
#endif
        }
    };

    MULTIBOOT2_TAG EFI32BitImageHandlePointerTag {
        BasicTag tag{19, 0};
        std::uint32_t pointer{};

        inline void
        dump() const {
#ifdef KERNEL_MULTIBOOT2_DEBUG
            klog<LogLevel::DEBUG>("MultibootDump") << "Tag \"EFI32BitImageHandlePointerTag\"";
            klog<LogLevel::DEBUG>("MultibootDump") << "type = " << static_cast<std::uint64_t>(tag.type);
            klog<LogLevel::DEBUG>("MultibootDump") << "size = " << static_cast<std::uint64_t>(tag.size);
            klog<LogLevel::DEBUG>("MultibootDump") << "pointer = " << static_cast<std::uint64_t>(pointer);
#endif
        }
    };

    MULTIBOOT2_TAG EFI64BitImageHandlePointerTag {
        BasicTag tag{20, 0};
        std::uint64_t pointer{};

        inline void
        dump() const {
#ifdef KERNEL_MULTIBOOT2_DEBUG
            klog<LogLevel::DEBUG>("MultibootDump") << "Tag \"EFI64BitImageHandlePointerTag\"";
            klog<LogLevel::DEBUG>("MultibootDump") << "type = " << static_cast<std::uint64_t>(tag.type);
            klog<LogLevel::DEBUG>("MultibootDump") << "size = " << static_cast<std::uint64_t>(tag.size);
            klog<LogLevel::DEBUG>("MultibootDump") << "pointer = " << pointer;
#endif
        }
    };

    MULTIBOOT2_TAG ImageLoadBasePhysicalAddressTag {
        BasicTag tag{21, 0};
        std::uint32_t load_base_addr{};

        inline void
        dump() const {
#ifdef KERNEL_MULTIBOOT2_DEBUG
            klog<LogLevel::DEBUG>("MultibootDump") << "Tag \"ImageLoadBasePhysicalAddressTag\"";
            klog<LogLevel::DEBUG>("MultibootDump") << "type = " << static_cast<std::uint64_t>(tag.type);
            klog<LogLevel::DEBUG>("MultibootDump") << "size = " << static_cast<std::uint64_t>(tag.size);
            klog<LogLevel::DEBUG>("MultibootDump") << "load_base_addr = " << static_cast<std::uint64_t>(load_base_addr);
#endif
        }
    };

} // namespace kernel::multiboot2