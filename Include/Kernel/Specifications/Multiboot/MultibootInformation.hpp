#pragma once

#define KERNEL_MULTIBOOT2_DEBUG
#include "Tags.hpp"

#include <Kernel/Print.hpp>
#include <Kernel/ACPI/RSDP.hpp>
#include <Kernel/Specifications/ELF/ELF.hpp>

namespace kernel::multiboot2 {

    class MultibootInformation {
    public:
        [[nodiscard]] inline explicit
        MultibootInformation(const void *pointer)
                : m_begin(static_cast<const std::uint8_t *>(pointer) + 8)
                , m_size(*static_cast<const std::uint32_t *>(pointer) - 8) {
#ifdef KERNEL_MULTIBOOT2_DEBUG
            klog<LogLevel::DEBUG>("MultibootInformation") << "Size=" << static_cast<std::uint64_t>(m_size)
                            << " Begin=" << reinterpret_cast<std::uint64_t>(m_begin);
#endif
        }

        inline void
        dump_all() const {
#ifdef KERNEL_MULTIBOOT2_DEBUG
            klog<LogLevel::DEBUG>("MultibootDump") << "Main";
            klog<LogLevel::DEBUG>("MultibootDump") << "  First Tag = " << reinterpret_cast<std::uint64_t>(m_begin);
            klog<LogLevel::DEBUG>("MultibootDump") << "  Size = " << static_cast<std::uint64_t>(m_size);

#define CHECK_TAG(tag_name) \
                constexpr const auto _internal_##tag_name##_length = tag_name{}.tag.type;
            MULTIBOOT2_ITERATE_TAGS(CHECK_TAG)
#undef CHECK_TAG

            for (auto *pointer = m_begin; pointer < m_begin + m_size; ) {
                const auto *tag = reinterpret_cast<const BasicTag *>(pointer);
                klog<LogLevel::DEBUG>("MultibootDump/NewTag") << "Pointer = " << reinterpret_cast<std::uint64_t>(tag)
                        << " Type=" << tag->type << " Size=" << tag->size;
                if (tag->type == 0 && tag->size == 8) {
                    klog<LogLevel::DEBUG>("MultibootDump") << "NULL End Tag";
                    break;
                }

#define CHECK_TAG(tag_name) \
                else if (_internal_##tag_name##_length == tag->type) \
                    reinterpret_cast<const tag_name *>(tag)->dump();
                MULTIBOOT2_ITERATE_TAGS(CHECK_TAG)
#undef CHECK_TAG
                else
                    klog<LogLevel::DEBUG>("MultibootDump") << "unknown tag at " << reinterpret_cast<std::uint64_t>(tag) << ", type=" << static_cast<std::uint64_t>(tag->type)
                            << ", size=" << static_cast<std::uint64_t>(tag->size);

                pointer += tag->size;
                if (tag->size % 8 != 0)
                    pointer += 8 - (tag->size % 8);
            }
#endif
        }

        [[nodiscard]] inline const BasicTag *
        find_tag(std::uint32_t type) const {
            for (auto *pointer = m_begin; pointer < m_begin + m_size; ) {
                const auto *tag = reinterpret_cast<const BasicTag *>(pointer);
#ifdef KERNEL_MULTIBOOT2_DEBUG
                klog<LogLevel::DEBUG>("MultibootInformation") << "Found tag @ " << std::size_t(tag)
                                << " of type=" << static_cast<std::uint64_t>(tag->type)
                                << " size=" << static_cast<std::uint64_t>(tag->size);
#endif
                if (tag->type == 0 && tag->size == 8)
                    break;
                if (tag->type == type)
                    return tag;

                pointer += tag->size;
                if (tag->size % 8 != 0)
                    pointer += 8 - (tag->size % 8);
            }
            return nullptr;
        }

        template <typename TagType>
        [[nodiscard]] inline const TagType *
        find_tag() const {
            static constexpr const auto type = TagType{}.tag.type;
            return reinterpret_cast<const TagType *>(find_tag(type));
        }

        [[nodiscard]] inline constexpr const std::uint8_t *
        ptr() const {
            return m_begin;
        }

        [[nodiscard]] inline constexpr std::size_t
        size() const {
            return m_size;
        }

    private:
        const std::uint8_t *m_begin;
        std::size_t m_size;
    };

} // kernel::multiboot2
