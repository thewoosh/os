#pragma once

namespace kernel::multiboot2 {

    struct BasicTag;
    struct BootCommandLineTag;
    struct BootLoaderNameTag;
    struct ModulesTag;
    struct BasicMemoryInformationTag;
    struct BIOSBootDeviceTag;
    struct MemoryMapTag;
    struct VBEInfoTag;
    struct FramebufferInfoTag;
    struct ELFSymbolsTag;
    struct APMTag;
    struct EFI32BitSystemTablePointerTag;
    struct EFI64BitSystemTablePointerTag;
    struct SMBIOSTableTag;
    struct ACPIOldRSDPTag;
    struct ACPINewRSDPTag;
    struct NetworkingInformationTag;
    struct EFIMemoryMapTag;
    struct EFIBootServicesNotTerminatedTag;
    struct EFI32BitImageHandlePointerTag;
    struct EFI64BitImageHandlePointerTag;
    struct ImageLoadBasePhysicalAddressTag;

} // namespace kernel::multiboot2
