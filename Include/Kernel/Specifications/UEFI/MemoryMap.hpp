#pragma once

#include <cstddef>
#include <cstdint>

namespace kernel::uefi {

    struct EFIMemoryDescriptor;

    struct MemoryMap {
        std::size_t size;
        const EFIMemoryDescriptor *map;
        std::size_t mapKey;
        std::size_t descriptorSize;
        std::uint32_t descriptorVersion;
    };

} // namespace kernel::uefi
