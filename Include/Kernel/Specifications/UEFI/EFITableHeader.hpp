#pragma once

#include <cstdint>

namespace kernel::uefi {

    enum class Signature : std::uint64_t;

    struct EFITableHeader {
        Signature signature;
        std::uint32_t revision;
        std::uint32_t headerSize;
        std::uint32_t crc32;
        std::uint32_t reserved;
    };

} // namespace kernel::uefi
