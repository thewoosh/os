#pragma once

#include <cstdint>

namespace kernel::uefi {

    enum class EFIMemoryType : std::uint32_t;

    struct EFIMemoryDescriptor {
        EFIMemoryType memoryType;
        [[deprecated("Explicit EFI misalignment fix")]] std::uint32_t padding;
        void *physicalStart;
        void *virtualStart;
        std::uint64_t numberOfPages;
        std::uint64_t attribute;
    };

} // namespace kernel::uefi
