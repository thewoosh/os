#pragma once

namespace kernel::uefi {

    struct EFISimpleTextInputProtocol;

} // namespace kernel::uefi
