#pragma once

#include "Kernel/Assert.hpp"
#include "Kernel/Specifications/UEFI/EFIStatus.hpp"

#define EFIAPI __attribute__((ms_abi))

#define EFI_ASSERT(expr) ASSERT((expr) == kernel::uefi::EFIStatus::SUCCESS)
