#pragma once

#include <cstdint>

namespace kernel::uefi {

    struct SimpleTextOutputMode {
        std::uint32_t maxMode;
        std::uint32_t mode;
        std::uint32_t attribute;
        std::uint32_t cursorColumn;
        std::uint32_t cursorRow;
        bool cursorVisible;
    };

} // namespace kernel::uefi
