#pragma once

#include <cstddef>
#include <cstdint>

#include "Kernel/Specifications/UEFI/EFIAPI.hpp"

namespace kernel::uefi {

    struct EFISimpleTextOutputProtocol;
    enum class EFIStatus : std::size_t;
    struct SimpleTextOutputMode;

    enum class CursorVisible {
        NO,
        YES
    };

    using EFIResetCallback = EFIStatus (EFIAPI *) (EFISimpleTextOutputProtocol *, bool extendedVerification);
    using EFITextStringCallback = EFIStatus (EFIAPI *) (EFISimpleTextOutputProtocol *, const char16_t *);
    using EFITextTestStringCallback = EFIStatus (EFIAPI *) (EFISimpleTextOutputProtocol *, const char16_t *);
    using EFITextQueryMode = EFIStatus(EFIAPI *) (EFISimpleTextOutputProtocol *, std::size_t textMode,
            std::size_t *columns, std::size_t *rows);
    using EFITextSetMode = EFIStatus (EFIAPI *) (EFISimpleTextOutputProtocol *, std::size_t textMode);
    using EFITextSetAttribute = EFIStatus (EFIAPI *) (EFISimpleTextOutputProtocol *, std::size_t attribute);
    using EFITextClearToScreen = EFIStatus (EFIAPI *) (EFISimpleTextOutputProtocol *);
    using EFITextSetCursorPosition = EFIStatus (EFIAPI *) (EFISimpleTextOutputProtocol *, std::size_t column,
            std::size_t row);
    using EFITextEnableCursor = EFIStatus (EFIAPI *) (EFISimpleTextOutputProtocol *, CursorVisible visible);

    enum class EFITextForegroundColor : std::uint8_t {
        BLACK = 0x00,
        BLUE = 0x01,
        GREEN = 0x02,
        CYAN = 0x03,
        RED = 0x04,
        MAGENTA = 0x05,
        BROWN = 0x06,
        LIGHT_GRAY = 0x07,
        BRIGHT = 0x08,
        DARK_GRAY = 0x08,
        LIGHT_BLUE = 0x09,
        LIGHT_GREEN = 0x0A,
        LIGHT_CYAN = 0x0B,
        LIGHT_RED = 0x0C,
        LIGHT_MAGENTA = 0x0D,
        YELLOW = 0x0E,
        WHITE = 0x0F,
    };

    enum class EFITextBackgroundColor : std::uint8_t {
        BLACK = 0x00,
        BLUE = 0x10,
        GREEN = 0x20,
        CYAN = 0x30,
        RED = 0x40,
        MAGENTA = 0x50,
        BROWN = 0x60,
        LIGHT_GRAY = 0x70,
    };

    [[nodiscard]] inline constexpr std::uint8_t
    createTextColor(EFITextForegroundColor foregroundColor, EFITextBackgroundColor backgroundColor) {
        return std::uint8_t(foregroundColor) | std::uint8_t(backgroundColor);
    }

    struct EFISimpleTextOutputProtocol {
        [[nodiscard]] inline EFIStatus
        reset(bool extendedVerification) {
            return m_reset(this, extendedVerification);
        }

        [[nodiscard]] inline EFIStatus
        outputString(const char16_t *string) {
            return m_outputString(this, string);
        }

        [[nodiscard]] inline EFIStatus
        testString(const char16_t *string) {
            return m_testString(this, string);
        }

        [[nodiscard]] inline EFIStatus
        queryMode(std::size_t textMode, std::size_t *columns, std::size_t *rows) {
            return m_queryMode(this, textMode, columns, rows);
        }

        [[nodiscard]] inline EFIStatus
        setMode(std::size_t textMode) {
            return m_setMode(this, textMode);
        }

        [[nodiscard]] inline EFIStatus
        setAttribute(std::uint32_t attribute) {
            return m_setAttribute(this, attribute);
        }

        [[nodiscard]] inline EFIStatus
        clearScreen() {
            return m_clearScreen(this);
        }

        [[nodiscard]] inline EFIStatus
        setCursorPosition(std::uint32_t column, std::uint32_t row) {
            return m_setCursorPosition(this, column, row);
        }

        [[nodiscard]] inline EFIStatus
        enableCursor(CursorVisible visible) {
            return m_enableCursor(this, visible);
        }

        [[nodiscard]] inline SimpleTextOutputMode *
        mode() {
            return m_mode;
        }

    private:
        EFIResetCallback m_reset;
        EFITextStringCallback m_outputString;
        EFITextTestStringCallback m_testString;
        EFITextQueryMode m_queryMode;
        EFITextSetMode m_setMode;
        EFITextSetAttribute m_setAttribute;
        EFITextClearToScreen m_clearScreen;
        EFITextSetCursorPosition m_setCursorPosition;
        EFITextEnableCursor m_enableCursor;
        SimpleTextOutputMode *m_mode;
    };

} // namespace kernel::uefi
