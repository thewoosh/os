#pragma once

#include <cstddef>

namespace kernel::uefi {

    enum class EFIStatus : std::size_t {
        SUCCESS = 0,

        LOAD_ERROR = 1,
        INVALID_PARAMETER = 2,
        UNSUPPORTED = 3,
        BAD_BUFFER_SIZE = 4,

        EFI_BUFFER_TOO_SMALL = 5,
        EFI_NOT_READY = 6,
        EFI_DEVICE_ERROR = 7,
        EFI_WRITE_PROTECTED = 8,
        EFI_OUT_OF_RESOURCES = 9,
        EFI_VOLUME_CORRUPTED = 10,
        EFI_VOLUME_FULL = 11,
        EFI_NO_MEDIA = 12,
        EFI_MEDIA_CHANGED = 13,
        EFI_NOT_FOUND = 14,
        EFI_ACCESS_DENIED = 15,
        EFI_NO_RESPONSE = 16,
        EFI_NO_MAPPING = 17,
        EFI_TIMEOUT = 18,
        EFI_NOT_STARTED = 19,
        EFI_ALREADY_STARTED = 20,
        EFI_ABORTED = 21,
        EFI_ICMP_ERROR = 22,
        EFI_TFTP_ERROR = 23,
        EFI_PROTOCOL_ERROR = 24,
        EFI_INCOMPATIBLE_VERSION = 25,
        EFI_SECURITY_VIOLATION = 26,
        EFI_CRC_ERROR = 27,
        EFI_END_OF_MEDIA = 28,
        EFI_END_OF_FILE = 31,
        EFI_INVALID_LANGUAGE = 32,
        EFI_COMPROMISED_DATA = 33,
        EFI_IP_ADDRESS_CONFLICT = 34,
        EFI_HTTP_ERROR = 35,
    };

} // namespace kernel::uefi
