#pragma once

#include "Kernel/Specifications/UEFI/MemoryMap.hpp"

namespace kernel::uefi {

    struct EFISystemTable;

    class UEFIManager {
    public:
        [[nodiscard]] inline constexpr explicit
        UEFIManager(const EFISystemTable *efiSystemTable)
                : m_efiSystemTable(efiSystemTable) {
        }

        [[nodiscard]] bool
        launch();

    private:
        [[nodiscard]] bool
        mapSystemTableMemory();

        const EFISystemTable *m_efiSystemTable;
    };

} // namespace kernel::uefi
