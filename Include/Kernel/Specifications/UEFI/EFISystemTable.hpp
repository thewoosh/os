#pragma once

#include <cstddef>

#include "Kernel/Specifications/UEFI/EFITableHeader.hpp"

namespace kernel::uefi {

#define EFI_SYSTEM_TABLE_SIGNATURE 0x5453595320494249
#define EFI_2_90_SYSTEM_TABLE_REVISION ((2<<16) | (90))
#define EFI_2_80_SYSTEM_TABLE_REVISION ((2<<16) | (80))
#define EFI_2_70_SYSTEM_TABLE_REVISION ((2<<16) | (70))
#define EFI_2_60_SYSTEM_TABLE_REVISION ((2<<16) | (60))
#define EFI_2_50_SYSTEM_TABLE_REVISION ((2<<16) | (50))
#define EFI_2_40_SYSTEM_TABLE_REVISION ((2<<16) | (40))
#define EFI_2_31_SYSTEM_TABLE_REVISION ((2<<16) | (31))
#define EFI_2_30_SYSTEM_TABLE_REVISION ((2<<16) | (30))
#define EFI_2_20_SYSTEM_TABLE_REVISION ((2<<16) | (20))
#define EFI_2_10_SYSTEM_TABLE_REVISION ((2<<16) | (10))
#define EFI_2_00_SYSTEM_TABLE_REVISION ((2<<16) | (00))
#define EFI_1_10_SYSTEM_TABLE_REVISION ((1<<16) | (10))
#define EFI_1_02_SYSTEM_TABLE_REVISION ((1<<16) | (02))

    struct EFIBootServices;
    struct EFIConfigurationTable;
    struct EFIRuntimeServices;
    struct EFISimpleTextInputProtocol;
    struct EFISimpleTextOutputProtocol;

    struct EFISystemTable {
        EFITableHeader header;
        char16_t *firmwareVendor;
        std::uint32_t firmwareRevision;

        void *consoleInHandle;
        EFISimpleTextInputProtocol *conIn;

        void *consoleOutHandle;
        EFISimpleTextOutputProtocol *conOut;

        void *standardErrorHandle;
        EFISimpleTextOutputProtocol *stdErr;

        EFIRuntimeServices *runtimeServices;
        EFIBootServices *bootServices;
        std::size_t numberOfTableEntries;
        EFIConfigurationTable *configurationTable;
    };

} // namespace kernel::uefi
