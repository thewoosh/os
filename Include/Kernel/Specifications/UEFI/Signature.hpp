#pragma once

#include <cstdint>

namespace kernel::uefi {

    enum class Signature : std::uint64_t{
        SYSTEM_TABLE = 0x5453595320494249,
    };

} // namespace kernel::uefi
