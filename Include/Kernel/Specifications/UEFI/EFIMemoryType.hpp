#pragma once

#include <string_view>

#include <cstdint>

namespace kernel::uefi {

    enum class EFIMemoryType : std::uint32_t {
#define UEFI_ITERATE_MEMORY_TYPES(REGISTER_MEMORY_TYPE) \
        REGISTER_MEMORY_TYPE(RESERVED_MEMORY_TYPE) \
        REGISTER_MEMORY_TYPE(LOADER_CODE) \
        REGISTER_MEMORY_TYPE(LOADER_DATA) \
        REGISTER_MEMORY_TYPE(BOOT_SERVICES_CODE) \
        REGISTER_MEMORY_TYPE(BOOT_SERVICES_DATA) \
        REGISTER_MEMORY_TYPE(RUNTIME_SERVICES_CODE) \
        REGISTER_MEMORY_TYPE(RUNTIME_SERVICES_DATA) \
        REGISTER_MEMORY_TYPE(CONVENTIONAL_MEMORY) \
        REGISTER_MEMORY_TYPE(UNUSABLE_MEMORY) \
        REGISTER_MEMORY_TYPE(ACPI_RECLAIMABLE_MEMORY) \
        REGISTER_MEMORY_TYPE(ACPI_MEMORY_NVS) \
        REGISTER_MEMORY_TYPE(MEMORY_MAPPED_IO) \
        REGISTER_MEMORY_TYPE(MEMORY_MAPPED_IO_PORT_SPACE) \
        REGISTER_MEMORY_TYPE(PAL_CODE) \
        REGISTER_MEMORY_TYPE(PERSISTENT_MEMORY) \
        REGISTER_MEMORY_TYPE(UNACCEPTED_MEMORY_TYPE)

#define REGISTER_MEMORY_TYPE(enumeration) enumeration,
        UEFI_ITERATE_MEMORY_TYPES(REGISTER_MEMORY_TYPE)
#undef REGISTER_MEMORY_TYPE
    };

    [[nodiscard]] inline constexpr std::string_view
    toString(EFIMemoryType efiMemoryType) {
        switch (efiMemoryType) {
#define REGISTER_MEMORY_TYPE(enumeration) case EFIMemoryType::enumeration: return #enumeration;
            UEFI_ITERATE_MEMORY_TYPES(REGISTER_MEMORY_TYPE)
#undef REGISTER_MEMORY_TYPE
        }

        return {};
    }

} // namespace kernel::uefi
