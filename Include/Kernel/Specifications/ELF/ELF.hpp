// TODO license

#pragma once

#include <cstdint>

namespace kernel::elf64 {

    using Elf64_Addr = std::uint64_t;
    using Elf64_Off = std::uint64_t;
    using Elf64_Word = std::uint32_t;
    using Elf64_Xword = std::uint64_t;
    using Elf64_Half = std::uint16_t;

    enum class SectionType : Elf64_Word {
        T_NULL = 0,
        PROGBITS =1,
        SYMTAB = 2,
        STRTAB = 3,
        RELA = 4,
        HASH = 5,
        DYNAMIC = 6,
        NOTE = 7,
        NOBITS = 8,
        REL = 9,
        SHLIB =	10,
        DYNSYM = 11,
        NUM = 12,
        LOPROC = 0x70000000,
        HIPROC = 0x7fffffff,
        LOUSER = 0x80000000,
        HIUSER = 0xffffffff,
    };

    struct SectionHeader {
        Elf64_Word sh_name;		/* Section name, index in string tbl */
        SectionType sh_type;		/* Type of section */
        Elf64_Xword sh_flags;		/* Miscellaneous section attributes */
        Elf64_Addr sh_addr;		/* Section virtual addr at execution */
        Elf64_Off sh_offset;		/* Section file offset */
        Elf64_Xword sh_size;		/* Size of section in bytes */
        Elf64_Word sh_link;		/* Index of another section */
        Elf64_Word sh_info;		/* Additional section information */
        Elf64_Xword sh_addralign;	/* Section alignment */
        Elf64_Xword sh_entsize;	/* Entry size if section holds table */
    };

    struct Symbol {
        Elf64_Word st_name;		    /* Symbol name, index in string tbl */
        unsigned char	st_info;	/* Type and binding attributes */
        unsigned char	st_other;	/* No defined meaning, 0 */
        Elf64_Half st_shndx;		/* Associated section index */
        Elf64_Addr st_value;		/* Value of the symbol */
        Elf64_Xword st_size;		/* Associated symbol size */
    };

} // namespace kernel::elf64
