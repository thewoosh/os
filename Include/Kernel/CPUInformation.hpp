/*
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 *
 * Copyright (C) 2021 Tristan Gerritsen
 * All Rights Reserved.
 */

#pragma once

#include <cstdint>

#include <array>

namespace Kernel {

    enum class CPUFeature {

        // ecx
        SSE3,
        PCLMUL,
        DTES64,
        MONITOR,
        DS_CPL,
        VMX,
        SMX,
        EST,
        TM2,
        SSSE3,
        CID,
        FMA,
        CX16,
        ETPRD,
        PDCM,
        PCIDE,
        DCA,
        SSE4_1,
        SSE4_2,
        X2APIC,
        MOVBE,
        POPCNT,
        AES,
        XSAVE,
        OSXSAVE,
        AVX,

        // edx
        FPU,
        VME,
        DE,
        PSE,
        TSC,
        MSR,
        PAE,
        MCE,
        CX8,
        APIC,
        SEP,
        MTRR,
        PGE,
        MCA,
        CMOV,
        PAT,
        PSE36,
        PSN,
        CLF,
        DTES,
        ACPI,
        MMX,
        FXSR,
        SSE,
        SSE2,
        SS,
        HTT,
        TM1,
        IA64,
        PBE,

    };

    struct CPUInformation {
        std::array<char, 12> vendor{};
        std::array<char, 48> processorName{};

        std::uint32_t featureECX;
        std::uint32_t featureEDX;

        [[nodiscard]] inline constexpr bool
        SupportsFeature(CPUFeature feature) const noexcept {
            const auto id = static_cast<std::size_t>(feature);
            constexpr const auto edxStart = static_cast<std::size_t>(CPUFeature::FPU);

            if (id < edxStart) {
                return featureECX & (1 << id);
            }

            return featureEDX & (1 << (id - edxStart));
        }
    };

    [[nodiscard]] CPUInformation
    ProduceCPUInformation() noexcept;

} // namespace Kernel
