#!/bin/bash

# Created by RehabMan 2015-11-21

if [[ "$1" == "" ]]; then
    echo "usage: eisaid.sh 0x[eisa-number], or eisaid.sh [eisaid-string]"
    exit 0
fi

if [[ "$1" == 0x* ]]; then
    let vendor="$1 & 0xFFFF"
    let device="$1 >> 16"
    let device_1="$device & 0xFF"
    let device_2="$device >> 8"
    let vendor_rev="(($vendor & 0xFF) << 8) | $vendor >> 8"
    let vendor_1="(($vendor_rev >> 10)&0x1f)+64"
    let vendor_2="(($vendor_rev >> 5)&0x1f)+64"
    let vendor_3="(($vendor_rev >> 0)&0x1f)+64"
    vendor_1=`printf "%x" $vendor_1|xxd -r -p`
    vendor_2=`printf "%x" $vendor_2|xxd -r -p`
    vendor_3=`printf "%x" $vendor_3|xxd -r -p`
    printf "EisaId string for 0x%08x: %s%s%s%02X%02X\n" "$1" $vendor_1 $vendor_2 $vendor_3 $device_1 $device_2
else
    if [[ "$1" =~ [A-Z]{3}[A-Fa-f0-9]{4} ]]; then
        vendor_1=${1:0:1}
        vendor_2=${1:1:1}
        vendor_3=${1:2:1}
        vendor_1=`printf "%d" \'$vendor_1`
        vendor_2=`printf "%d" \'$vendor_2`
        vendor_3=`printf "%d" \'$vendor_3`
        let vendor_rev="($vendor_1-64 << 10) | ($vendor_2-64 << 5) | $vendor_3-64"
        let vendor="(($vendor_rev & 0xFF) << 8) | $vendor_rev >> 8"
        printf "EisaId(\"%s\") value: 0x%02x%02x%04x\n" "$1" 0x${1:5:2} 0x${1:3:2} $vendor
    else
        echo \""$1"\" is not a valid EisaId!
    fi
fi
#EOF
