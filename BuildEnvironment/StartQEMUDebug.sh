#!/bin/bash

cd /home/tager/Code/Public/OperatingSystem/cmake-build-debug-clang || exit
ninja || exit
qemu-system-x86_64 Source/kernel.iso -d guest_errors,cpu_reset,int -no-reboot -no-shutdown -serial mon:stdio -s -S
