# Paging
This document describes the current paging design.

## 1. Before Long Mode
To enter long mode, paging must be enabled. Since the OS is 64-bit and C++ code only gets entered in long mode, we have to do this in assembly  

The current design is somewhat impractical. In the future I might prefer to have some bootstrapping code in C++ land.

Anyhow, the PML4 is defined in assembly as `paging_pml4`. The subroutine `setup_page_tables` adds an entry to this table, 
the `paging_pdp_first`. That table gets the first entry `paging_pd_first` and that entry gets `paging_pt_first`.

`paging_pdp_first` is filled from 0 to 512 with identity pages. This way, the first 2 kiB gets mapped and the kernel can jump to 64 bit.

## 2. In Long Mode
### 2.1. Page Frame Allocation
The PFA uses the biggest chunk of available memory (according to the MemoryMapTag passed by the 
Multiboot2 bootloader). At the end of that chunk of memory the bitmap is placed. This bitmap is used as a way to mark pages
in this chunk as either available or locked.
