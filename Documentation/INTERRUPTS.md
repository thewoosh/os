# Interrupts
This document describes the current interrupt design.

IRQ vector: 0x50

### LAPIC
| Type     | INT#  |
|----------|-------|
| INT0     | 0x61  |
| INT1     | 0x62  |
| TIMER    | 0x63  |
| Spurious | 0x1FF |

### I/O APIC
0xC8 + GlobalSystemInterruptOffset \
to \
0xC8 + GlobalSystemInterruptOffset + MaxRedirectionEntry


